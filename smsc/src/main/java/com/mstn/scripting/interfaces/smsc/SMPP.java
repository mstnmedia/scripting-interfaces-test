package com.mstn.scripting.interfaces.smsc;

import com.mstn.scripting.core.models.InterfaceConfigs;
import org.smpp.Data;
import org.smpp.Session;
import org.smpp.TCPIPConnection;
import org.smpp.pdu.Address;
import org.smpp.pdu.BindRequest;
import org.smpp.pdu.BindResponse;
import org.smpp.pdu.BindTransmitter;
import org.smpp.pdu.EnquireLink;
import org.smpp.pdu.EnquireLinkResp;
import org.smpp.pdu.SubmitSM;
import org.smpp.pdu.SubmitSMResp;
import org.smpp.pdu.WrongLengthOfStringException;

/**
 * Clase que posee los métodos necesario para enviar un mensaje SMS a un número
 * telefónico: inicia una sesión con el servidor, prepara el mensaje y lo envía.
 *
 * @author MSTN Media
 */
public class SMPP {

	static Session getSession() throws Exception {
		String smscHost = InterfaceConfigs.get("smscHost");
		int smscPort = Integer.parseInt(InterfaceConfigs.get("smscPort"));
		String smscUsername = InterfaceConfigs.get("smscUsername");
		String smscPassword = InterfaceConfigs.get("smscPassword");

		BindRequest request = new BindTransmitter();
		request.setSystemId(smscUsername);
		request.setPassword(smscPassword);
		// request.setSystemType(systemType);
		// request.setAddressRange(addressRange);
		request.setInterfaceVersion((byte) 0x34); // SMPP protocol version

		TCPIPConnection connection = new TCPIPConnection(smscHost, smscPort);
		// connection.setReceiveTimeout(BIND_TIMEOUT);
		Session session = new Session(connection);

		BindResponse response = session.bind(request);
		return session;
	}

	static Address createSourceAddress() throws WrongLengthOfStringException {
		String address = InterfaceConfigs.get("smscSourceAddress");
		Address addressInst = new Address();
		addressInst.setTon((byte) 5); // national ton
		addressInst.setNpi((byte) 0); // numeric plan indicator
		addressInst.setAddress(address, Data.SM_ADDR_LEN);
		return addressInst;
	}

	static Address createDestAddress(String address) throws WrongLengthOfStringException {
		Address addressInst = new Address();
		addressInst.setTon((byte) 0); // national ton
		addressInst.setNpi((byte) 0); // numeric plan indicator
		addressInst.setAddress(address, Data.SM_ADDR_LEN);
		return addressInst;
	}

	static Object checkStatus(Session session) throws Exception {
		final EnquireLink request = new EnquireLink();
		final EnquireLinkResp response = session.enquireLink(request);
		return response;
	}

	/**
	 *
	 * @param recipientPhoneNumber
	 * @param messageText
	 * @return
	 * @throws Exception
	 */
	static public SubmitSMResp send(String recipientPhoneNumber, String messageText) throws Exception {
		SubmitSM request = new SubmitSM();
		request.setSourceAddr(createSourceAddress()); // you can skip this
		request.setDestAddr(createDestAddress(recipientPhoneNumber));
		request.setShortMessage(messageText);
		// request.setScheduleDeliveryTime(deliveryTime); // you can skip this
		request.setReplaceIfPresentFlag((byte) 0);
		request.setEsmClass((byte) 0);
		request.setProtocolId((byte) 0);
		request.setPriorityFlag((byte) 0);
//		request.setRegisteredDelivery((byte) 1); // we want delivery reports
		request.setDataCoding((byte) 0);
		request.setSmDefaultMsgId((byte) 0);

		Session session = getSession();
		final SubmitSMResp response = session.submit(request);
		session.unbind();
		session.close();
		return response;
	}

}
