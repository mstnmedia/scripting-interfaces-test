/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.smsc;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.Velocity;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Log;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.smpp.pdu.PDU;
import org.smpp.pdu.SubmitSMResp;

/**
 * Interfaz que agrega a Scripting la funcionalidad de enviar mensajes SMS desde
 * una transacción en curso.
 *
 * @author amatos
 */
public class SMSC extends InterfaceBase {

	static Getter SOAP = new SMSC.Getter();

	/**
	 *
	 */
	public SMSC() {
		super(0, "smsc", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("smscHost") + ":" + InterfaceConfigs.get("smscPort"));
		return inter;
	}

	@Override
	protected List<Field> getClassFields(Class _class) {
		if (SubmitSMResp.class == _class) {
			return Utils.getFieldsUpTo(_class, PDU.class);
		} else {
			return super.getClassFields(_class);
		}
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("subscriberNo")) {
			field.setLabel("Para");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_PHONE);
		} else if (fieldName.endsWith("messageText")) {
			field.setLabel("Mensaje");
			field.setId_type(InterfaceFieldTypes.TEXTAREA);
		}
		return field;
	}

	@Override
	protected Object getParamValueFromForm(Class paramType, String paramName) throws Exception {
		Object value = super.getParamValueFromForm(paramType, paramName);
		if (paramName.endsWith("messageText")) {
			String localValue = Utils.coalesce((String) value, "");
			try {
				Map<String, Object> values = mapResults(mapppedResults, true);
				localValue = Velocity.interpolate(localValue, values);
			} catch (Exception ex) {
				Utils.logException(this.getClass(), "Error interpolando " + paramName, ex);
			}
			return localValue;
		}
		return value;
	}

	@Override
	protected Object sendParamsToMethod(Interface inter, Method method, List<Object> params) throws Exception {
		SubmitSMResp responseObj = (SubmitSMResp) super.sendParamsToMethod(inter, method, params);
		response.getLogs().add(new Log(
				0, user.getId(), "SMS", transaction.getId(),
				"Enviar SMS", new Date(),
				JSON.toString(params), JSON.toString(responseObj), user.getIp()));
		return responseObj;
	}

//	@Override
//	protected List<Transaction_Result> getTransactionResultsFromClass(Method method, Interface inter, String paramName, List<Class> inspectedClases) {
//		List<Transaction_Result> transactionResults = new ArrayList();
//		Class _class = inspectedClases.get(inspectedClases.size() - 1);
//		if (SubmitSMResp.class==_class) {
//			return Arrays.asList(
//					new Transaction_Result()
//			);
//		} 
//		return super.getTransactionResultsFromClass(method, inter, paramName, inspectedClases);
//	}
	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("smscHost").toString();
		Integer.parseInt(InterfaceConfigs.get("smscPort").toString());
		InterfaceConfigs.get("smscUsername").toString();
		InterfaceConfigs.get("smscPassword").toString();
		InterfaceConfigs.get("smscSourceAddress").toString();
		super.test();

	}

	/**
	 *
	 */
	static public class Getter {

		/**
		 *
		 */
		static public class Params {

			/**
			 *
			 */
			public String subscriberNo;

			/**
			 *
			 */
			public String messageText;
		}

		/**
		 *
		 * @param params
		 * @return
		 * @throws Exception
		 */
		public SubmitSMResp sender(Params params) throws Exception {
			SubmitSMResp response = SMPP.send(params.subscriberNo, params.messageText);
			return response;
		}
	}
}
