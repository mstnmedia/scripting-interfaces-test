/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.smsc.SMPP;
import com.mstn.scripting.interfaces.smsc.SMSC;

/**
 * Clase ejecutable con métodos de pruebas para probar las diferentes
 * funcionalidades en este proyecto.
 *
 * @author amatos
 */
public class Main {

	/**
	 * @param args the command line arguments
	 * @throws java.lang.Exception
	 */
	static TransactionInterfacesPayload payload = new TransactionInterfacesPayload(
			new V_Transaction(), new Workflow_Step(), "{}"
	);

	static Object testSMSC() throws Exception {
		SMSC service = new SMSC();
		String to = "8294961819";
		String message = "Development message from Scripting\n"
				+ "ID Transaction: $transaction.id \n"
				+ "ID Transaction: $transaction.getId()\n "
				+ "ID Transaction: $transaction_id_transaction\n ";
		String form = JSON.newObjectNode()
				.put("smsc_sender_arg0_subscriberNo", to)
				.put("smsc_sender_param_subscriberNo", to)
				.put("smsc_sender_arg0_messageText", message)
				.put("smsc_sender_param_messageText", message)
				.toString();
		payload.setForm(form);

		Interface inter = service.getInterfaces(true).get(0);
		return service.callMethod(inter, payload, User.SYSTEM);
	}

	static void staticCalls() throws Exception {
		Object result = null;
		result = SMPP.send("8294961819", "Abel1");
		System.out.println("Abel1\n" + JSON.toString(result));
		result = SMPP.send("8299868827", "Fermín");
		System.out.println("Fermín\n" + JSON.toString(result));
		result = SMPP.send("8094775888", "Arsenys");
		System.out.println("Arsenys \n" + JSON.toString(result));
		result = SMPP.send("8294961819", "Abel2");
		System.out.println("Abel2\n" + JSON.toString(result));
	}

	/**
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		InterfaceConfigs.showLogs = true;
		InterfaceConfigs.setConfig();
		Object result = null;
		result = testSMSC();
//		staticCalls();

		System.out.println(JSON.toString(result));
	}
}
