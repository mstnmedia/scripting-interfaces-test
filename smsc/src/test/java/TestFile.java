
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Before;
import org.junit.Test;
import org.reflections.Reflections;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author amatos
 */
public class TestFile {

	static final List<InterfaceBase> BASES = new ArrayList();

//	@Before

	/**
	 *
	 * @throws Exception
	 */
	public void setup() throws Exception {
		InterfaceConfigs.setConfig();

		Reflections reflections = new Reflections("com.mstn.scripting");
		List<Class<? extends InterfaceBase>> baseClasses = reflections.getSubTypesOf(InterfaceBase.class)
				.stream()
				.sorted((a, b) -> {
					return a.getName().compareTo(b.getName());
				})
				.collect(Collectors.toList());
		for (int i = 0; i < baseClasses.size(); i++) {
			Class<? extends InterfaceBase> baseClass = baseClasses.get(i);
			System.out.println("Instancing " + baseClass.getName() + "...");
			InterfaceBase base = baseClass.newInstance();
			base.setShowBaseLogs(true);
			BASES.add(base);
		}
	}

//	@Test

	/**
	 *
	 * @throws Exception
	 */
	public void testFieldsAndResults() throws Exception {

		List<Interface> fails = new ArrayList();
		for (int i = 0; i < BASES.size(); i++) {
			InterfaceBase base = BASES.get(i);
			String baseName = base.getNAME();
			System.out.println("BASE: " + baseName);
			if (ignoreBase(baseName)) {
				continue;
			}
			List<Interface> interfaces = base.getInterfaces(true);
			for (int j = 0; j < interfaces.size(); j++) {
				Interface inter = interfaces.get(j);
				ObjectNode form = JSON.newObjectNode();
				for (int k = 0; k < inter.getInterface_field().size(); k++) {
					Interface_Field field = inter.getInterface_field().get(k);
					form.putPOJO(field.getName(), "\"" + String.valueOf(getFormValue(field)) + "\"");
				}
				try {
					base.callMethod(
							inter,
							new TransactionInterfacesPayload(
									new V_Transaction(),
									new Workflow_Step(),
									form.toString()
							),
							null
					);
				} catch (Exception ex) {
					Utils.logException(this.getClass(), inter.getName(), ex);
					fails.add(inter);
				}
			}
		}
		System.out.println("Interfaces: ");
		System.out.println(JSON.toString(fails.stream().map(i -> i.getName()).collect(Collectors.toList())));
	}

	Object getFormValue(Interface_Field field) {
		switch (field.getId_type()) {
			case InterfaceFieldTypes.BOOLEAN:
			case InterfaceFieldTypes.CHECKBOX:
				return true;
			case InterfaceFieldTypes.EMAIL:
				return "amatos@mstn.com";
			case InterfaceFieldTypes.NUMBER:
				return 45;
			case InterfaceFieldTypes.TAGS:
				return "hello,bye";
			case InterfaceFieldTypes.DATE:
				return "2018-05-31";
			case InterfaceFieldTypes.DATETIME:
				return "2018-05-31T12:15:30";
			case InterfaceFieldTypes.TIME:
				return "12:15:30";
			case InterfaceFieldTypes.AUTOCOMPLETE:
			case InterfaceFieldTypes.HIDDEN:
			case InterfaceFieldTypes.LINK:
			case InterfaceFieldTypes.OPTIONS:
			case InterfaceFieldTypes.RADIO:
			case InterfaceFieldTypes.TEXT:
			case InterfaceFieldTypes.TEXTAREA:
			default:
				return "Test";
		}
	}

	boolean ignoreBase(String name) {
		final List<String> IGNORED_BASES = Arrays.asList(
				"clarovideo", "crmesb",
				"diagnosticoidw", "dth", "ensambleejb",
				"iptv",
				"miclaroagp",
				""
		);

		if (IGNORED_BASES.contains(name)) {
			return true;
		}
		if (name.startsWith("crm")) {
			return true;
		}
		if (name.startsWith("ensamble")) {
			return true;
		}
		if (name.startsWith("fixed")) {
			return true;
		}
		if (name.startsWith("oms")) {
			return true;
		}
		if (name.startsWith("ppg")) {
			return true;
		}
		if (name.startsWith("complex")) {
			return true;
		}
		return false;
	}

}
