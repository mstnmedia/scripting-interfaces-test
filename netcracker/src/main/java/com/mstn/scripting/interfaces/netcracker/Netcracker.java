package com.mstn.scripting.interfaces.netcracker;

import _do.com.claro._2016.som.service.customerorder.v1.CustomerOrder;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;

/**
 * Interfaz que agrega a Scripting la conexión al web service para
 * administración de casos de CRM desde una transacción en curso.
 *
 * @author amatos
 */
public class Netcracker extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = Netcracker.class;
		try {
			if ("true".equals(InterfaceConfigs.get("ignore_netcracker"))) {
				Utils.logWarn(_class, "Ignorando interfaces en clase " + _class.getCanonicalName());
				SOAP = new Object();
			} else {
				SOAP = CustomerOrder.getInstance();
			}
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint Netcracker", ex);
			if ("true".equals(InterfaceConfigs.get("ignoreExceptions_netcracker"))) {
				Utils.logWarn(_class, "Error ignorado por configuración. Continuará el proceso.");
				SOAP = new Object();
			} else {
				throw ex;
			}
		}
	}

	public Netcracker() {
		super(0, "netcracker", SOAP);
		IGNORED_METHODS.add("cancelCustomerOrderByOrderId");
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("netcracker"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("_orderId")) {
			field.setLabel("Número de orden");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		}
		return field;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("netcracker").toString();
		super.test();
	}
}
