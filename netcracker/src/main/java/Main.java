/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.netcracker.Netcracker;
import java.util.List;

/**
 * Clase ejecutable con métodos de pruebas para probar las diferentes
 * funcionalidades en este proyecto.
 *
 * @author amatos
 */
public class Main {

	static TransactionInterfacesPayload payload = new TransactionInterfacesPayload(
			new V_Transaction(), new Workflow_Step(), "{}"
	);
//	static String caseId = "12281843";
//	static String caseId = "34881581";
//	static String caseId = "34881581";
//	static String caseId = "10002942";
	static String caseId = "10002942";


	static Object testNetcracker() throws Exception {
		Netcracker service = new Netcracker();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces
				.stream()
				.filter(item -> item.getName().endsWith("getCustomerOrderByOrderId"))
				.findFirst().get();
		ObjectNode form = JSON.newObjectNode();
		String[] value = new String[]{caseId, "6789", "SCRIPTING"};
		for (int i = 0; i < inter.getInterface_field().size(); i++) {
			Interface_Field field = inter.getInterface_field().get(i);
			form.put(field.getName(), value[i]);
		}
		payload.setForm(form.toString());
		TransactionInterfacesResponse response = service.callMethod(inter, payload, User.SYSTEM);
		return response;
	}

	public static void main(String[] args) throws Exception {
		InterfaceConfigs.setConfig();
		Object result = null;
//		result = getCaseDetailsById();
		result = testNetcracker();
//		result = closeCase();

		System.out.println(JSON.toString(result));
	}

}
