package _do.com.claro._2016.som.service.customerorder.v1;

import com.mstn.scripting.core.models.InterfaceConfigs;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.1.3-b02- Generated
 * source version: 2.1
 *
 */
@WebServiceClient(name = "CustomerOrder", targetNamespace = "http://www.claro.com.do/2016/SOM/Service/CustomerOrder/v1", wsdlLocation = "http://soaqaenv1:6010/services/integration/customerordermanagement/customerorder?wsdl")
public class CustomerOrder extends Service {

	private final static URL CUSTOMERORDER_WSDL_LOCATION;
	private final static Logger LOGGER = Logger.getLogger(_do.com.claro._2016.som.service.customerorder.v1.CustomerOrder.class.getName());

	static public final String CONFIG_KEY = "netcracker";

	static {
		URL url = null;
		String configURL = InterfaceConfigs.get(CONFIG_KEY);
		try {
			URL baseUrl;
			baseUrl = _do.com.claro._2016.som.service.customerorder.v1.CustomerOrder.class.getResource(".");
			url = new URL(baseUrl, configURL);
		} catch (MalformedURLException e) {
			LOGGER.log(Level.WARNING, "Failed to create URL for the wsdl Location: ''{0}'', retrying as a local file", configURL);
			LOGGER.warning(e.getMessage());
		}
		CUSTOMERORDER_WSDL_LOCATION = url;
	}

	static public CustomerOrderService getInstance() {
		if (!InterfaceConfigs.wsClients.containsKey(CONFIG_KEY)) {
			CustomerOrderService port = new CustomerOrder().getCustomerOrderSOAP();
			InterfaceConfigs.wsClients.put(CONFIG_KEY, port);
		}
		return (CustomerOrderService) InterfaceConfigs.wsClients.get(CONFIG_KEY);
	}

	public CustomerOrder(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public CustomerOrder() {
		super(CUSTOMERORDER_WSDL_LOCATION, new QName("http://www.claro.com.do/2016/SOM/Service/CustomerOrder/v1", "CustomerOrder"));
	}

	/**
	 *
	 * @return returns CustomerOrderService
	 */
	@WebEndpoint(name = "CustomerOrderSOAP")
	public CustomerOrderService getCustomerOrderSOAP() {
		return super.getPort(new QName("http://www.claro.com.do/2016/SOM/Service/CustomerOrder/v1", "CustomerOrderSOAP"), CustomerOrderService.class);
	}

	/**
	 *
	 * @param features A list of {@link javax.xml.ws.WebServiceFeature} to
	 * configure on the proxy. Supported features not in the
	 * <code>features</code> parameter will have their default values.
	 * @return returns CustomerOrderService
	 */
	@WebEndpoint(name = "CustomerOrderSOAP")
	public CustomerOrderService getCustomerOrderSOAP(WebServiceFeature... features) {
		return super.getPort(new QName("http://www.claro.com.do/2016/SOM/Service/CustomerOrder/v1", "CustomerOrderSOAP"), CustomerOrderService.class, features);
	}

}
