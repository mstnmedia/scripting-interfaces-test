
package _do.com.claro._2016.som.schema.customerorder.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceOrder">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceSpecification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceInstanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="action" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accessId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subscriptionId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="cpeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpeSubtype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="creationDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastUpdateDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="assignedTelephoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pendingManualTasks" type="{http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1}PendingManualTasks" minOccurs="0"/>
 *         &lt;element name="channelPackages" type="{http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1}ChannelPackages" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceOrder", propOrder = {
    "id",
    "serviceSpecification",
    "serviceInstanceId",
    "action",
    "accessId",
    "subscriptionId",
    "cpeType",
    "cpeSubtype",
    "status",
    "creationDate",
    "lastUpdateDate",
    "assignedTelephoneNumber",
    "pendingManualTasks",
    "channelPackages"
})
public class ServiceOrder {

    protected String id;
    protected String serviceSpecification;
    protected String serviceInstanceId;
    protected String action;
    protected String accessId;
    protected Long subscriptionId;
    protected String cpeType;
    protected String cpeSubtype;
    protected String status;
    protected String creationDate;
    protected String lastUpdateDate;
    protected String assignedTelephoneNumber;
    protected PendingManualTasks pendingManualTasks;
    protected ChannelPackages channelPackages;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the serviceSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceSpecification() {
        return serviceSpecification;
    }

    /**
     * Sets the value of the serviceSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceSpecification(String value) {
        this.serviceSpecification = value;
    }

    /**
     * Gets the value of the serviceInstanceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceInstanceId() {
        return serviceInstanceId;
    }

    /**
     * Sets the value of the serviceInstanceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceInstanceId(String value) {
        this.serviceInstanceId = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the accessId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessId() {
        return accessId;
    }

    /**
     * Sets the value of the accessId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessId(String value) {
        this.accessId = value;
    }

    /**
     * Gets the value of the subscriptionId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSubscriptionId() {
        return subscriptionId;
    }

    /**
     * Sets the value of the subscriptionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSubscriptionId(Long value) {
        this.subscriptionId = value;
    }

    /**
     * Gets the value of the cpeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpeType() {
        return cpeType;
    }

    /**
     * Sets the value of the cpeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpeType(String value) {
        this.cpeType = value;
    }

    /**
     * Gets the value of the cpeSubtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpeSubtype() {
        return cpeSubtype;
    }

    /**
     * Sets the value of the cpeSubtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpeSubtype(String value) {
        this.cpeSubtype = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationDate(String value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the lastUpdateDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    /**
     * Sets the value of the lastUpdateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastUpdateDate(String value) {
        this.lastUpdateDate = value;
    }

    /**
     * Gets the value of the assignedTelephoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedTelephoneNumber() {
        return assignedTelephoneNumber;
    }

    /**
     * Sets the value of the assignedTelephoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedTelephoneNumber(String value) {
        this.assignedTelephoneNumber = value;
    }

    /**
     * Gets the value of the pendingManualTasks property.
     * 
     * @return
     *     possible object is
     *     {@link PendingManualTasks }
     *     
     */
    public PendingManualTasks getPendingManualTasks() {
        return pendingManualTasks;
    }

    /**
     * Sets the value of the pendingManualTasks property.
     * 
     * @param value
     *     allowed object is
     *     {@link PendingManualTasks }
     *     
     */
    public void setPendingManualTasks(PendingManualTasks value) {
        this.pendingManualTasks = value;
    }

    /**
     * Gets the value of the channelPackages property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelPackages }
     *     
     */
    public ChannelPackages getChannelPackages() {
        return channelPackages;
    }

    /**
     * Sets the value of the channelPackages property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelPackages }
     *     
     */
    public void setChannelPackages(ChannelPackages value) {
        this.channelPackages = value;
    }

}
