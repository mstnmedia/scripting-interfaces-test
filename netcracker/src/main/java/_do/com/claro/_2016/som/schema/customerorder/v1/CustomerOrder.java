
package _do.com.claro._2016.som.schema.customerorder.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerOrder">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="customerAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="salesChannel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="creationDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="customerRequestValidation" type="{http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1}CustomerRequestValidation" minOccurs="0"/>
 *         &lt;element name="productOrders" type="{http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1}ProductOrders" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="workOrders" type="{http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1}WorkOrders" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerOrder", propOrder = {
    "id",
    "customerAccountNumber",
    "salesChannel",
    "creationDate",
    "customerRequestValidation",
    "productOrders",
    "workOrders"
})
public class CustomerOrder {

    protected long id;
    @XmlElement(required = true)
    protected String customerAccountNumber;
    @XmlElement(required = true)
    protected String salesChannel;
    @XmlElement(required = true)
    protected String creationDate;
    protected CustomerRequestValidation customerRequestValidation;
    protected List<ProductOrders> productOrders;
    protected WorkOrders workOrders;

    /**
     * Gets the value of the id property.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Gets the value of the customerAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerAccountNumber() {
        return customerAccountNumber;
    }

    /**
     * Sets the value of the customerAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerAccountNumber(String value) {
        this.customerAccountNumber = value;
    }

    /**
     * Gets the value of the salesChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesChannel() {
        return salesChannel;
    }

    /**
     * Sets the value of the salesChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesChannel(String value) {
        this.salesChannel = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationDate(String value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the customerRequestValidation property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerRequestValidation }
     *     
     */
    public CustomerRequestValidation getCustomerRequestValidation() {
        return customerRequestValidation;
    }

    /**
     * Sets the value of the customerRequestValidation property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerRequestValidation }
     *     
     */
    public void setCustomerRequestValidation(CustomerRequestValidation value) {
        this.customerRequestValidation = value;
    }

    /**
     * Gets the value of the productOrders property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productOrders property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductOrders().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductOrders }
     * 
     * 
     */
    public List<ProductOrders> getProductOrders() {
        if (productOrders == null) {
            productOrders = new ArrayList<ProductOrders>();
        }
        return this.productOrders;
    }

    /**
     * Gets the value of the workOrders property.
     * 
     * @return
     *     possible object is
     *     {@link WorkOrders }
     *     
     */
    public WorkOrders getWorkOrders() {
        return workOrders;
    }

    /**
     * Sets the value of the workOrders property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkOrders }
     *     
     */
    public void setWorkOrders(WorkOrders value) {
        this.workOrders = value;
    }

}
