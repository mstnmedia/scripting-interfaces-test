
package _do.com.claro._2016.som.schema.customerorder.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cancellationReasonTypeList.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="cancellationReasonTypeList">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Fase de Negociacion Expirada"/>
 *     &lt;enumeration value="A requerimiento del tecnico"/>
 *     &lt;enumeration value="Cliente no está Listo"/>
 *     &lt;enumeration value="Tubería Obstruida"/>
 *     &lt;enumeration value="Casa en Construcción"/>
 *     &lt;enumeration value="Tiene servicio con Otra Orden"/>
 *     &lt;enumeration value="A Solicitud del Cliente"/>
 *     &lt;enumeration value="Cliente debe poner Poste"/>
 *     &lt;enumeration value="Cliente no pudo ser Contactado"/>
 *     &lt;enumeration value="Central no lista"/>
 *     &lt;enumeration value="Cliente se mudo"/>
 *     &lt;enumeration value="Codetel no tiene equipos Disponibles."/>
 *     &lt;enumeration value="Condiciones no Proceden"/>
 *     &lt;enumeration value="Excede Distancia de Datos"/>
 *     &lt;enumeration value="Excede Distancia de Voz"/>
 *     &lt;enumeration value="No me Interesa el Servicio"/>
 *     &lt;enumeration value="No Disponibilidad de Puertos."/>
 *     &lt;enumeration value="Viola Propiedad Privada"/>
 *     &lt;enumeration value="Flujo de la Orden Detenido"/>
 *     &lt;enumeration value="A Requerimiento de Codetel"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "cancellationReasonTypeList")
@XmlEnum
public enum CancellationReasonTypeList {

    @XmlEnumValue("Fase de Negociacion Expirada")
    FASE_DE_NEGOCIACION_EXPIRADA("Fase de Negociacion Expirada"),
    @XmlEnumValue("A requerimiento del tecnico")
    A_REQUERIMIENTO_DEL_TECNICO("A requerimiento del tecnico"),
    @XmlEnumValue("Cliente no est\u00e1 Listo")
    CLIENTE_NO_ESTÁ_LISTO("Cliente no est\u00e1 Listo"),
    @XmlEnumValue("Tuber\u00eda Obstruida")
    TUBERÍA_OBSTRUIDA("Tuber\u00eda Obstruida"),
    @XmlEnumValue("Casa en Construcci\u00f3n")
    CASA_EN_CONSTRUCCIÓN("Casa en Construcci\u00f3n"),
    @XmlEnumValue("Tiene servicio con Otra Orden")
    TIENE_SERVICIO_CON_OTRA_ORDEN("Tiene servicio con Otra Orden"),
    @XmlEnumValue("A Solicitud del Cliente")
    A_SOLICITUD_DEL_CLIENTE("A Solicitud del Cliente"),
    @XmlEnumValue("Cliente debe poner Poste")
    CLIENTE_DEBE_PONER_POSTE("Cliente debe poner Poste"),
    @XmlEnumValue("Cliente no pudo ser Contactado")
    CLIENTE_NO_PUDO_SER_CONTACTADO("Cliente no pudo ser Contactado"),
    @XmlEnumValue("Central no lista")
    CENTRAL_NO_LISTA("Central no lista"),
    @XmlEnumValue("Cliente se mudo")
    CLIENTE_SE_MUDO("Cliente se mudo"),
    @XmlEnumValue("Codetel no tiene equipos Disponibles.")
    CODETEL_NO_TIENE_EQUIPOS_DISPONIBLES("Codetel no tiene equipos Disponibles."),
    @XmlEnumValue("Condiciones no Proceden")
    CONDICIONES_NO_PROCEDEN("Condiciones no Proceden"),
    @XmlEnumValue("Excede Distancia de Datos")
    EXCEDE_DISTANCIA_DE_DATOS("Excede Distancia de Datos"),
    @XmlEnumValue("Excede Distancia de Voz")
    EXCEDE_DISTANCIA_DE_VOZ("Excede Distancia de Voz"),
    @XmlEnumValue("No me Interesa el Servicio")
    NO_ME_INTERESA_EL_SERVICIO("No me Interesa el Servicio"),
    @XmlEnumValue("No Disponibilidad de Puertos.")
    NO_DISPONIBILIDAD_DE_PUERTOS("No Disponibilidad de Puertos."),
    @XmlEnumValue("Viola Propiedad Privada")
    VIOLA_PROPIEDAD_PRIVADA("Viola Propiedad Privada"),
    @XmlEnumValue("Flujo de la Orden Detenido")
    FLUJO_DE_LA_ORDEN_DETENIDO("Flujo de la Orden Detenido"),
    @XmlEnumValue("A Requerimiento de Codetel")
    A_REQUERIMIENTO_DE_CODETEL("A Requerimiento de Codetel");
    private final String value;

    CancellationReasonTypeList(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CancellationReasonTypeList fromValue(String v) {
        for (CancellationReasonTypeList c: CancellationReasonTypeList.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
