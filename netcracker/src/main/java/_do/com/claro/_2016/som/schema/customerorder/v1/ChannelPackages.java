
package _do.com.claro._2016.som.schema.customerorder.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChannelPackages complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChannelPackages">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="channelPackage" type="{http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1}ChannelPackage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChannelPackages", propOrder = {
    "channelPackage"
})
public class ChannelPackages {

    protected List<ChannelPackage> channelPackage;

    /**
     * Gets the value of the channelPackage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the channelPackage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChannelPackage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChannelPackage }
     * 
     * 
     */
    public List<ChannelPackage> getChannelPackage() {
        if (channelPackage == null) {
            channelPackage = new ArrayList<ChannelPackage>();
        }
        return this.channelPackage;
    }

}
