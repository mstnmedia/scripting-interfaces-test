
package _do.com.claro._2016.som.schema.customerorder.v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Work complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Work">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="lastUpdateDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="lastUpdateUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="organization" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="technicianId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="technicianName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="technicianPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="technicianSupervisor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="company" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="lineTestSuccess" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="responses" type="{http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1}WorkResponses" minOccurs="0"/>
 *         &lt;element name="workItems" type="{http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1}WorkItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Work", propOrder = {
    "id",
    "name",
    "status",
    "lastUpdateDate",
    "lastUpdateUser",
    "organization",
    "technicianId",
    "technicianName",
    "technicianPhone",
    "technicianSupervisor",
    "company",
    "lineTestSuccess",
    "responses",
    "workItems"
})
public class Work {

    @XmlElement(required = true)
    protected BigInteger id;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected String status;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lastUpdateDate;
    @XmlElement(required = true)
    protected String lastUpdateUser;
    @XmlElement(required = true)
    protected BigInteger organization;
    @XmlElement(required = true)
    protected String technicianId;
    @XmlElement(required = true)
    protected String technicianName;
    @XmlElement(required = true)
    protected String technicianPhone;
    @XmlElement(required = true)
    protected String technicianSupervisor;
    @XmlElement(required = true)
    protected String company;
    protected boolean lineTestSuccess;
    protected WorkResponses responses;
    protected WorkItems workItems;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setId(BigInteger value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the lastUpdateDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastUpdateDate() {
        return lastUpdateDate;
    }

    /**
     * Sets the value of the lastUpdateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastUpdateDate(XMLGregorianCalendar value) {
        this.lastUpdateDate = value;
    }

    /**
     * Gets the value of the lastUpdateUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    /**
     * Sets the value of the lastUpdateUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastUpdateUser(String value) {
        this.lastUpdateUser = value;
    }

    /**
     * Gets the value of the organization property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOrganization() {
        return organization;
    }

    /**
     * Sets the value of the organization property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOrganization(BigInteger value) {
        this.organization = value;
    }

    /**
     * Gets the value of the technicianId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnicianId() {
        return technicianId;
    }

    /**
     * Sets the value of the technicianId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnicianId(String value) {
        this.technicianId = value;
    }

    /**
     * Gets the value of the technicianName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnicianName() {
        return technicianName;
    }

    /**
     * Sets the value of the technicianName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnicianName(String value) {
        this.technicianName = value;
    }

    /**
     * Gets the value of the technicianPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnicianPhone() {
        return technicianPhone;
    }

    /**
     * Sets the value of the technicianPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnicianPhone(String value) {
        this.technicianPhone = value;
    }

    /**
     * Gets the value of the technicianSupervisor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnicianSupervisor() {
        return technicianSupervisor;
    }

    /**
     * Sets the value of the technicianSupervisor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnicianSupervisor(String value) {
        this.technicianSupervisor = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompany(String value) {
        this.company = value;
    }

    /**
     * Gets the value of the lineTestSuccess property.
     * 
     */
    public boolean isLineTestSuccess() {
        return lineTestSuccess;
    }

    /**
     * Sets the value of the lineTestSuccess property.
     * 
     */
    public void setLineTestSuccess(boolean value) {
        this.lineTestSuccess = value;
    }

    /**
     * Gets the value of the responses property.
     * 
     * @return
     *     possible object is
     *     {@link WorkResponses }
     *     
     */
    public WorkResponses getResponses() {
        return responses;
    }

    /**
     * Sets the value of the responses property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkResponses }
     *     
     */
    public void setResponses(WorkResponses value) {
        this.responses = value;
    }

    /**
     * Gets the value of the workItems property.
     * 
     * @return
     *     possible object is
     *     {@link WorkItems }
     *     
     */
    public WorkItems getWorkItems() {
        return workItems;
    }

    /**
     * Sets the value of the workItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkItems }
     *     
     */
    public void setWorkItems(WorkItems value) {
        this.workItems = value;
    }

}
