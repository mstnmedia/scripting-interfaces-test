
package _do.com.claro._2016.som.schema.customerorder.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro._2016.som.schema.customerorder.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetCustomerOrderByOrderIdResponse_QNAME = new QName("http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1", "getCustomerOrderByOrderIdResponse");
    private final static QName _CancelCustomerOrderByOrderIdRequest_QNAME = new QName("http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1", "cancelCustomerOrderByOrderIdRequest");
    private final static QName _CancelCustomerOrderByOrderIdResponse_QNAME = new QName("http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1", "cancelCustomerOrderByOrderIdResponse");
    private final static QName _FaultMessage_QNAME = new QName("http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1", "faultMessage");
    private final static QName _PendingManualTasks_QNAME = new QName("http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1", "pendingManualTasks");
    private final static QName _GetCustomerOrderByOrderIdRequest_QNAME = new QName("http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1", "getCustomerOrderByOrderIdRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro._2016.som.schema.customerorder.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProductOrderRemark }
     * 
     */
    public ProductOrderRemark createProductOrderRemark() {
        return new ProductOrderRemark();
    }

    /**
     * Create an instance of {@link CustomerRequestValidation }
     * 
     */
    public CustomerRequestValidation createCustomerRequestValidation() {
        return new CustomerRequestValidation();
    }

    /**
     * Create an instance of {@link CancelCustomerOrderByOrderIdResponse }
     * 
     */
    public CancelCustomerOrderByOrderIdResponse createCancelCustomerOrderByOrderIdResponse() {
        return new CancelCustomerOrderByOrderIdResponse();
    }

    /**
     * Create an instance of {@link GetCustomerOrderByIdRequest }
     * 
     */
    public GetCustomerOrderByIdRequest createGetCustomerOrderByIdRequest() {
        return new GetCustomerOrderByIdRequest();
    }

    /**
     * Create an instance of {@link PendingManualTask }
     * 
     */
    public PendingManualTask createPendingManualTask() {
        return new PendingManualTask();
    }

    /**
     * Create an instance of {@link ProductOrders }
     * 
     */
    public ProductOrders createProductOrders() {
        return new ProductOrders();
    }

    /**
     * Create an instance of {@link PendingManualTasks }
     * 
     */
    public PendingManualTasks createPendingManualTasks() {
        return new PendingManualTasks();
    }

    /**
     * Create an instance of {@link Work }
     * 
     */
    public Work createWork() {
        return new Work();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link ServiceOrder }
     * 
     */
    public ServiceOrder createServiceOrder() {
        return new ServiceOrder();
    }

    /**
     * Create an instance of {@link FaultMessage }
     * 
     */
    public FaultMessage createFaultMessage() {
        return new FaultMessage();
    }

    /**
     * Create an instance of {@link ProductOrderRemarks }
     * 
     */
    public ProductOrderRemarks createProductOrderRemarks() {
        return new ProductOrderRemarks();
    }

    /**
     * Create an instance of {@link WorkItems }
     * 
     */
    public WorkItems createWorkItems() {
        return new WorkItems();
    }

    /**
     * Create an instance of {@link ChannelPackages }
     * 
     */
    public ChannelPackages createChannelPackages() {
        return new ChannelPackages();
    }

    /**
     * Create an instance of {@link WorkResponse }
     * 
     */
    public WorkResponse createWorkResponse() {
        return new WorkResponse();
    }

    /**
     * Create an instance of {@link WorkList }
     * 
     */
    public WorkList createWorkList() {
        return new WorkList();
    }

    /**
     * Create an instance of {@link WorkOrders }
     * 
     */
    public WorkOrders createWorkOrders() {
        return new WorkOrders();
    }

    /**
     * Create an instance of {@link GetCustomerOrderByOrderIdResponse }
     * 
     */
    public GetCustomerOrderByOrderIdResponse createGetCustomerOrderByOrderIdResponse() {
        return new GetCustomerOrderByOrderIdResponse();
    }

    /**
     * Create an instance of {@link ProductOrder }
     * 
     */
    public ProductOrder createProductOrder() {
        return new ProductOrder();
    }

    /**
     * Create an instance of {@link ServiceOrders }
     * 
     */
    public ServiceOrders createServiceOrders() {
        return new ServiceOrders();
    }

    /**
     * Create an instance of {@link CustomerOrder }
     * 
     */
    public CustomerOrder createCustomerOrder() {
        return new CustomerOrder();
    }

    /**
     * Create an instance of {@link ChannelPackage }
     * 
     */
    public ChannelPackage createChannelPackage() {
        return new ChannelPackage();
    }

    /**
     * Create an instance of {@link WorkItem }
     * 
     */
    public WorkItem createWorkItem() {
        return new WorkItem();
    }

    /**
     * Create an instance of {@link CancelCustomerOrderByOrderIdRequest }
     * 
     */
    public CancelCustomerOrderByOrderIdRequest createCancelCustomerOrderByOrderIdRequest() {
        return new CancelCustomerOrderByOrderIdRequest();
    }

    /**
     * Create an instance of {@link WorkResponses }
     * 
     */
    public WorkResponses createWorkResponses() {
        return new WorkResponses();
    }

    /**
     * Create an instance of {@link WorkOrder }
     * 
     */
    public WorkOrder createWorkOrder() {
        return new WorkOrder();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerOrderByOrderIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1", name = "getCustomerOrderByOrderIdResponse")
    public JAXBElement<GetCustomerOrderByOrderIdResponse> createGetCustomerOrderByOrderIdResponse(GetCustomerOrderByOrderIdResponse value) {
        return new JAXBElement<GetCustomerOrderByOrderIdResponse>(_GetCustomerOrderByOrderIdResponse_QNAME, GetCustomerOrderByOrderIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelCustomerOrderByOrderIdRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1", name = "cancelCustomerOrderByOrderIdRequest")
    public JAXBElement<CancelCustomerOrderByOrderIdRequest> createCancelCustomerOrderByOrderIdRequest(CancelCustomerOrderByOrderIdRequest value) {
        return new JAXBElement<CancelCustomerOrderByOrderIdRequest>(_CancelCustomerOrderByOrderIdRequest_QNAME, CancelCustomerOrderByOrderIdRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelCustomerOrderByOrderIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1", name = "cancelCustomerOrderByOrderIdResponse")
    public JAXBElement<CancelCustomerOrderByOrderIdResponse> createCancelCustomerOrderByOrderIdResponse(CancelCustomerOrderByOrderIdResponse value) {
        return new JAXBElement<CancelCustomerOrderByOrderIdResponse>(_CancelCustomerOrderByOrderIdResponse_QNAME, CancelCustomerOrderByOrderIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1", name = "faultMessage")
    public JAXBElement<FaultMessage> createFaultMessage(FaultMessage value) {
        return new JAXBElement<FaultMessage>(_FaultMessage_QNAME, FaultMessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PendingManualTasks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1", name = "pendingManualTasks")
    public JAXBElement<PendingManualTasks> createPendingManualTasks(PendingManualTasks value) {
        return new JAXBElement<PendingManualTasks>(_PendingManualTasks_QNAME, PendingManualTasks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerOrderByIdRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/2016/SOM/Schema/CustomerOrder/v1", name = "getCustomerOrderByOrderIdRequest")
    public JAXBElement<GetCustomerOrderByIdRequest> createGetCustomerOrderByOrderIdRequest(GetCustomerOrderByIdRequest value) {
        return new JAXBElement<GetCustomerOrderByIdRequest>(_GetCustomerOrderByOrderIdRequest_QNAME, GetCustomerOrderByIdRequest.class, null, value);
    }

}
