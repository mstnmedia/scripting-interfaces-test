
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetOrdenEstatusM6Result" type="{http://tempuri.org/}ArrayOfOrdenEstatusM6" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getOrdenEstatusM6Result"
})
@XmlRootElement(name = "GetOrdenEstatusM6Response")
public class GetOrdenEstatusM6Response {

	/**
	 *
	 */
	@XmlElement(name = "GetOrdenEstatusM6Result")
    protected ArrayOfOrdenEstatusM6 getOrdenEstatusM6Result;

    /**
     * Gets the value of the getOrdenEstatusM6Result property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOrdenEstatusM6 }
     *     
     */
    public ArrayOfOrdenEstatusM6 getGetOrdenEstatusM6Result() {
        return getOrdenEstatusM6Result;
    }

    /**
     * Sets the value of the getOrdenEstatusM6Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOrdenEstatusM6 }
     *     
     */
    public void setGetOrdenEstatusM6Result(ArrayOfOrdenEstatusM6 value) {
        this.getOrdenEstatusM6Result = value;
    }

}
