
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetDistritoOrdenCerradaResult" type="{http://tempuri.org/}OrdenDistritoInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDistritoOrdenCerradaResult"
})
@XmlRootElement(name = "GetDistritoOrdenCerradaResponse")
public class GetDistritoOrdenCerradaResponse {

	/**
	 *
	 */
	@XmlElement(name = "GetDistritoOrdenCerradaResult")
    protected OrdenDistritoInfo getDistritoOrdenCerradaResult;

    /**
     * Gets the value of the getDistritoOrdenCerradaResult property.
     * 
     * @return
     *     possible object is
     *     {@link OrdenDistritoInfo }
     *     
     */
    public OrdenDistritoInfo getGetDistritoOrdenCerradaResult() {
        return getDistritoOrdenCerradaResult;
    }

    /**
     * Sets the value of the getDistritoOrdenCerradaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrdenDistritoInfo }
     *     
     */
    public void setGetDistritoOrdenCerradaResult(OrdenDistritoInfo value) {
        this.getDistritoOrdenCerradaResult = value;
    }

}
