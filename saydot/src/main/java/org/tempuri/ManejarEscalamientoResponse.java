
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ManejarEscalamientoResult" type="{http://tempuri.org/}ManejarEscalamientoResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "manejarEscalamientoResult"
})
@XmlRootElement(name = "ManejarEscalamientoResponse")
public class ManejarEscalamientoResponse {

	/**
	 *
	 */
	@XmlElement(name = "ManejarEscalamientoResult")
    protected ManejarEscalamientoResult manejarEscalamientoResult;

    /**
     * Gets the value of the manejarEscalamientoResult property.
     * 
     * @return
     *     possible object is
     *     {@link ManejarEscalamientoResult }
     *     
     */
    public ManejarEscalamientoResult getManejarEscalamientoResult() {
        return manejarEscalamientoResult;
    }

    /**
     * Sets the value of the manejarEscalamientoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ManejarEscalamientoResult }
     *     
     */
    public void setManejarEscalamientoResult(ManejarEscalamientoResult value) {
        this.manejarEscalamientoResult = value;
    }

}
