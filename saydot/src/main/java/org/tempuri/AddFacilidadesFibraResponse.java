
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddFacilidadesFibraResult" type="{http://tempuri.org/}StatusCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "addFacilidadesFibraResult"
})
@XmlRootElement(name = "AddFacilidadesFibraResponse")
public class AddFacilidadesFibraResponse {

	/**
	 *
	 */
	@XmlElement(name = "AddFacilidadesFibraResult")
    protected StatusCode addFacilidadesFibraResult;

    /**
     * Gets the value of the addFacilidadesFibraResult property.
     * 
     * @return
     *     possible object is
     *     {@link StatusCode }
     *     
     */
    public StatusCode getAddFacilidadesFibraResult() {
        return addFacilidadesFibraResult;
    }

    /**
     * Sets the value of the addFacilidadesFibraResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusCode }
     *     
     */
    public void setAddFacilidadesFibraResult(StatusCode value) {
        this.addFacilidadesFibraResult = value;
    }

}
