
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetOrdenEstatusDomainResult" type="{http://tempuri.org/}ArrayOfOrdenEstatusDomain" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getOrdenEstatusDomainResult"
})
@XmlRootElement(name = "GetOrdenEstatusDomainResponse")
public class GetOrdenEstatusDomainResponse {

	/**
	 *
	 */
	@XmlElement(name = "GetOrdenEstatusDomainResult")
    protected ArrayOfOrdenEstatusDomain getOrdenEstatusDomainResult;

    /**
     * Gets the value of the getOrdenEstatusDomainResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOrdenEstatusDomain }
     *     
     */
    public ArrayOfOrdenEstatusDomain getGetOrdenEstatusDomainResult() {
        return getOrdenEstatusDomainResult;
    }

    /**
     * Sets the value of the getOrdenEstatusDomainResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOrdenEstatusDomain }
     *     
     */
    public void setGetOrdenEstatusDomainResult(ArrayOfOrdenEstatusDomain value) {
        this.getOrdenEstatusDomainResult = value;
    }

}
