
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrdenEstatusOMSViewModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrdenEstatusOMSViewModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EstatusActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaDeCreacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaUltimaActualizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Task" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaskStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrdenEstatusOMSViewModel", propOrder = {
    "estatusActual",
    "fechaDeCreacion",
    "fechaUltimaActualizacion",
    "task",
    "taskStatus"
})
public class OrdenEstatusOMSViewModel {

	/**
	 *
	 */
	@XmlElement(name = "EstatusActual")
    protected String estatusActual;

	/**
	 *
	 */
	@XmlElement(name = "FechaDeCreacion")
    protected String fechaDeCreacion;

	/**
	 *
	 */
	@XmlElement(name = "FechaUltimaActualizacion")
    protected String fechaUltimaActualizacion;

	/**
	 *
	 */
	@XmlElement(name = "Task")
    protected String task;

	/**
	 *
	 */
	@XmlElement(name = "TaskStatus")
    protected String taskStatus;

    /**
     * Gets the value of the estatusActual property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstatusActual() {
        return estatusActual;
    }

    /**
     * Sets the value of the estatusActual property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstatusActual(String value) {
        this.estatusActual = value;
    }

    /**
     * Gets the value of the fechaDeCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaDeCreacion() {
        return fechaDeCreacion;
    }

    /**
     * Sets the value of the fechaDeCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaDeCreacion(String value) {
        this.fechaDeCreacion = value;
    }

    /**
     * Gets the value of the fechaUltimaActualizacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaUltimaActualizacion() {
        return fechaUltimaActualizacion;
    }

    /**
     * Sets the value of the fechaUltimaActualizacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaUltimaActualizacion(String value) {
        this.fechaUltimaActualizacion = value;
    }

    /**
     * Gets the value of the task property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTask() {
        return task;
    }

    /**
     * Sets the value of the task property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTask(String value) {
        this.task = value;
    }

    /**
     * Gets the value of the taskStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskStatus() {
        return taskStatus;
    }

    /**
     * Sets the value of the taskStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskStatus(String value) {
        this.taskStatus = value;
    }

}
