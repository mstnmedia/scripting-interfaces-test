
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfOrdenEstatusDomainTarea complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfOrdenEstatusDomainTarea">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrdenEstatusDomainTarea" type="{http://tempuri.org/}OrdenEstatusDomainTarea" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfOrdenEstatusDomainTarea", propOrder = {
    "ordenEstatusDomainTarea"
})
public class ArrayOfOrdenEstatusDomainTarea {

	/**
	 *
	 */
	@XmlElement(name = "OrdenEstatusDomainTarea", nillable = true)
    protected List<OrdenEstatusDomainTarea> ordenEstatusDomainTarea;

    /**
     * Gets the value of the ordenEstatusDomainTarea property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordenEstatusDomainTarea property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdenEstatusDomainTarea().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrdenEstatusDomainTarea }
     * 
     * 
	 * @return 
     */
    public List<OrdenEstatusDomainTarea> getOrdenEstatusDomainTarea() {
        if (ordenEstatusDomainTarea == null) {
            ordenEstatusDomainTarea = new ArrayList<OrdenEstatusDomainTarea>();
        }
        return this.ordenEstatusDomainTarea;
    }

}
