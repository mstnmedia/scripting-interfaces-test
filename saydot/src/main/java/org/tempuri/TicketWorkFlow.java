
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TicketWorkFlow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TicketWorkFlow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TicketID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartFlowDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="EndFlowDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Days" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TicketWorkFlow", propOrder = {
    "ticketID",
    "startFlowDate",
    "endFlowDate",
    "days",
    "description"
})
public class TicketWorkFlow {

	/**
	 *
	 */
	@XmlElement(name = "TicketID")
    protected String ticketID;

	/**
	 *
	 */
	@XmlElement(name = "StartFlowDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startFlowDate;

	/**
	 *
	 */
	@XmlElement(name = "EndFlowDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endFlowDate;

	/**
	 *
	 */
	@XmlElement(name = "Days")
    protected int days;

	/**
	 *
	 */
	@XmlElement(name = "Description")
    protected String description;

    /**
     * Gets the value of the ticketID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketID() {
        return ticketID;
    }

    /**
     * Sets the value of the ticketID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketID(String value) {
        this.ticketID = value;
    }

    /**
     * Gets the value of the startFlowDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartFlowDate() {
        return startFlowDate;
    }

    /**
     * Sets the value of the startFlowDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartFlowDate(XMLGregorianCalendar value) {
        this.startFlowDate = value;
    }

    /**
     * Gets the value of the endFlowDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndFlowDate() {
        return endFlowDate;
    }

    /**
     * Sets the value of the endFlowDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndFlowDate(XMLGregorianCalendar value) {
        this.endFlowDate = value;
    }

    /**
     * Gets the value of the days property.
     * 
	 * @return 
     */
    public int getDays() {
        return days;
    }

    /**
     * Sets the value of the days property.
     * 
	 * @param value
     */
    public void setDays(int value) {
        this.days = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

}
