
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfTicketWorkFlow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfTicketWorkFlow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TicketWorkFlow" type="{http://tempuri.org/}TicketWorkFlow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfTicketWorkFlow", propOrder = {
    "ticketWorkFlow"
})
public class ArrayOfTicketWorkFlow {

	/**
	 *
	 */
	@XmlElement(name = "TicketWorkFlow", nillable = true)
    protected List<TicketWorkFlow> ticketWorkFlow;

    /**
     * Gets the value of the ticketWorkFlow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ticketWorkFlow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTicketWorkFlow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TicketWorkFlow }
     * 
     * 
	 * @return 
     */
    public List<TicketWorkFlow> getTicketWorkFlow() {
        if (ticketWorkFlow == null) {
            ticketWorkFlow = new ArrayList<TicketWorkFlow>();
        }
        return this.ticketWorkFlow;
    }

}
