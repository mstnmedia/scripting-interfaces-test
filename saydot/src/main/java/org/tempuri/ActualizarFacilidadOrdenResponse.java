
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActualizarFacilidadOrdenResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "actualizarFacilidadOrdenResult"
})
@XmlRootElement(name = "ActualizarFacilidadOrdenResponse")
public class ActualizarFacilidadOrdenResponse {

	/**
	 *
	 */
	@XmlElement(name = "ActualizarFacilidadOrdenResult")
    protected String actualizarFacilidadOrdenResult;

    /**
     * Gets the value of the actualizarFacilidadOrdenResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActualizarFacilidadOrdenResult() {
        return actualizarFacilidadOrdenResult;
    }

    /**
     * Sets the value of the actualizarFacilidadOrdenResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActualizarFacilidadOrdenResult(String value) {
        this.actualizarFacilidadOrdenResult = value;
    }

}
