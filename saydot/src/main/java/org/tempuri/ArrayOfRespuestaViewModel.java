
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRespuestaViewModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRespuestaViewModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RespuestaViewModel" type="{http://tempuri.org/}RespuestaViewModel" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRespuestaViewModel", propOrder = {
    "respuestaViewModel"
})
public class ArrayOfRespuestaViewModel {

	/**
	 *
	 */
	@XmlElement(name = "RespuestaViewModel", nillable = true)
    protected List<RespuestaViewModel> respuestaViewModel;

    /**
     * Gets the value of the respuestaViewModel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the respuestaViewModel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRespuestaViewModel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaViewModel }
     * 
     * 
	 * @return 
     */
    public List<RespuestaViewModel> getRespuestaViewModel() {
        if (respuestaViewModel == null) {
            respuestaViewModel = new ArrayList<RespuestaViewModel>();
        }
        return this.respuestaViewModel;
    }

}
