
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrdenInfoGeneralViewModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrdenInfoGeneralViewModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NumeroOrden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NombreCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaCompromiso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DireccionCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoModem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CantidadExtensiones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CantidadDias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TecnicoDespacho" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SupervisorDespacho" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RequiereVisita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoRecibido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CitaRecibida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrdenesHermanas" type="{http://tempuri.org/}ArrayOfOrdenHermanaViewModel" minOccurs="0"/>
 *         &lt;element name="TrackingRespuestas" type="{http://tempuri.org/}ArrayOfRespuestaViewModel" minOccurs="0"/>
 *         &lt;element name="IntentoContactos" type="{http://tempuri.org/}ArrayOfIntentoContactoViewModel" minOccurs="0"/>
 *         &lt;element name="InfoCompleta" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="TipoDeOrdenSaydot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoDeOrdenOMS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Producto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flujo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaRecibida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaDeCarga" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Distrito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Oferta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Canal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Distribuidor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoDeOrden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaCodigoRecibido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClaseDeServicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Segmento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CajasIPTV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaquetesCable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Remarks" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="EquiposTelefonicos" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="EquiposFinanciamiento" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="TipoDeOrdenSaydotDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoDeOrdenOMSDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Despachada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactoAdicional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactoAlterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrdenInfoGeneralViewModel", propOrder = {
    "numeroOrden",
    "nombreCliente",
    "fechaSolicitud",
    "fechaCompromiso",
    "direccionCliente",
    "addressReference",
    "tipoModem",
    "cantidadExtensiones",
    "cantidadDias",
    "tecnicoDespacho",
    "supervisorDespacho",
    "requiereVisita",
    "codigoRecibido",
    "citaRecibida",
    "ordenesHermanas",
    "trackingRespuestas",
    "intentoContactos",
    "infoCompleta",
    "tipoDeOrdenSaydot",
    "tipoDeOrdenOMS",
    "producto",
    "flujo",
    "fechaRecibida",
    "fechaDeCarga",
    "distrito",
    "reason",
    "oferta",
    "canal",
    "distribuidor",
    "tipoDeOrden",
    "fechaCodigoRecibido",
    "claseDeServicio",
    "segmento",
    "cajasIPTV",
    "paquetesCable",
    "remarks",
    "equiposTelefonicos",
    "equiposFinanciamiento",
    "tipoDeOrdenSaydotDescripcion",
    "tipoDeOrdenOMSDescripcion",
    "despachada",
    "contactoAdicional",
    "contactoAlterno"
})
public class OrdenInfoGeneralViewModel {

	/**
	 *
	 */
	@XmlElement(name = "NumeroOrden")
    protected String numeroOrden;

	/**
	 *
	 */
	@XmlElement(name = "NombreCliente")
    protected String nombreCliente;

	/**
	 *
	 */
	@XmlElement(name = "FechaSolicitud")
    protected String fechaSolicitud;

	/**
	 *
	 */
	@XmlElement(name = "FechaCompromiso")
    protected String fechaCompromiso;

	/**
	 *
	 */
	@XmlElement(name = "DireccionCliente")
    protected String direccionCliente;

	/**
	 *
	 */
	@XmlElement(name = "AddressReference")
    protected String addressReference;

	/**
	 *
	 */
	@XmlElement(name = "TipoModem")
    protected String tipoModem;

	/**
	 *
	 */
	@XmlElement(name = "CantidadExtensiones")
    protected String cantidadExtensiones;

	/**
	 *
	 */
	@XmlElement(name = "CantidadDias")
    protected String cantidadDias;

	/**
	 *
	 */
	@XmlElement(name = "TecnicoDespacho")
    protected String tecnicoDespacho;

	/**
	 *
	 */
	@XmlElement(name = "SupervisorDespacho")
    protected String supervisorDespacho;

	/**
	 *
	 */
	@XmlElement(name = "RequiereVisita")
    protected String requiereVisita;

	/**
	 *
	 */
	@XmlElement(name = "CodigoRecibido")
    protected String codigoRecibido;

	/**
	 *
	 */
	@XmlElement(name = "CitaRecibida")
    protected String citaRecibida;

	/**
	 *
	 */
	@XmlElement(name = "OrdenesHermanas")
    protected ArrayOfOrdenHermanaViewModel ordenesHermanas;

	/**
	 *
	 */
	@XmlElement(name = "TrackingRespuestas")
    protected ArrayOfRespuestaViewModel trackingRespuestas;

	/**
	 *
	 */
	@XmlElement(name = "IntentoContactos")
    protected ArrayOfIntentoContactoViewModel intentoContactos;

	/**
	 *
	 */
	@XmlElement(name = "InfoCompleta")
    protected boolean infoCompleta;

	/**
	 *
	 */
	@XmlElement(name = "TipoDeOrdenSaydot")
    protected String tipoDeOrdenSaydot;

	/**
	 *
	 */
	@XmlElement(name = "TipoDeOrdenOMS")
    protected String tipoDeOrdenOMS;

	/**
	 *
	 */
	@XmlElement(name = "Producto")
    protected String producto;

	/**
	 *
	 */
	@XmlElement(name = "Flujo")
    protected String flujo;

	/**
	 *
	 */
	@XmlElement(name = "FechaRecibida")
    protected String fechaRecibida;

	/**
	 *
	 */
	@XmlElement(name = "FechaDeCarga")
    protected String fechaDeCarga;

	/**
	 *
	 */
	@XmlElement(name = "Distrito")
    protected String distrito;

	/**
	 *
	 */
	@XmlElement(name = "Reason")
    protected String reason;

	/**
	 *
	 */
	@XmlElement(name = "Oferta")
    protected String oferta;

	/**
	 *
	 */
	@XmlElement(name = "Canal")
    protected String canal;

	/**
	 *
	 */
	@XmlElement(name = "Distribuidor")
    protected String distribuidor;

	/**
	 *
	 */
	@XmlElement(name = "TipoDeOrden")
    protected String tipoDeOrden;

	/**
	 *
	 */
	@XmlElement(name = "FechaCodigoRecibido")
    protected String fechaCodigoRecibido;

	/**
	 *
	 */
	@XmlElement(name = "ClaseDeServicio")
    protected String claseDeServicio;

	/**
	 *
	 */
	@XmlElement(name = "Segmento")
    protected String segmento;

	/**
	 *
	 */
	@XmlElement(name = "CajasIPTV")
    protected String cajasIPTV;

	/**
	 *
	 */
	@XmlElement(name = "PaquetesCable")
    protected String paquetesCable;

	/**
	 *
	 */
	@XmlElement(name = "Remarks")
    protected ArrayOfString remarks;

	/**
	 *
	 */
	@XmlElement(name = "EquiposTelefonicos")
    protected ArrayOfString equiposTelefonicos;

	/**
	 *
	 */
	@XmlElement(name = "EquiposFinanciamiento")
    protected ArrayOfString equiposFinanciamiento;

	/**
	 *
	 */
	@XmlElement(name = "TipoDeOrdenSaydotDescripcion")
    protected String tipoDeOrdenSaydotDescripcion;

	/**
	 *
	 */
	@XmlElement(name = "TipoDeOrdenOMSDescripcion")
    protected String tipoDeOrdenOMSDescripcion;

	/**
	 *
	 */
	@XmlElement(name = "Despachada")
    protected String despachada;

	/**
	 *
	 */
	@XmlElement(name = "ContactoAdicional")
    protected String contactoAdicional;

	/**
	 *
	 */
	@XmlElement(name = "ContactoAlterno")
    protected String contactoAlterno;

    /**
     * Gets the value of the numeroOrden property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroOrden() {
        return numeroOrden;
    }

    /**
     * Sets the value of the numeroOrden property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroOrden(String value) {
        this.numeroOrden = value;
    }

    /**
     * Gets the value of the nombreCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * Sets the value of the nombreCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCliente(String value) {
        this.nombreCliente = value;
    }

    /**
     * Gets the value of the fechaSolicitud property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    /**
     * Sets the value of the fechaSolicitud property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaSolicitud(String value) {
        this.fechaSolicitud = value;
    }

    /**
     * Gets the value of the fechaCompromiso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaCompromiso() {
        return fechaCompromiso;
    }

    /**
     * Sets the value of the fechaCompromiso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaCompromiso(String value) {
        this.fechaCompromiso = value;
    }

    /**
     * Gets the value of the direccionCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccionCliente() {
        return direccionCliente;
    }

    /**
     * Sets the value of the direccionCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccionCliente(String value) {
        this.direccionCliente = value;
    }

    /**
     * Gets the value of the addressReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressReference() {
        return addressReference;
    }

    /**
     * Sets the value of the addressReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressReference(String value) {
        this.addressReference = value;
    }

    /**
     * Gets the value of the tipoModem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoModem() {
        return tipoModem;
    }

    /**
     * Sets the value of the tipoModem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoModem(String value) {
        this.tipoModem = value;
    }

    /**
     * Gets the value of the cantidadExtensiones property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantidadExtensiones() {
        return cantidadExtensiones;
    }

    /**
     * Sets the value of the cantidadExtensiones property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantidadExtensiones(String value) {
        this.cantidadExtensiones = value;
    }

    /**
     * Gets the value of the cantidadDias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantidadDias() {
        return cantidadDias;
    }

    /**
     * Sets the value of the cantidadDias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantidadDias(String value) {
        this.cantidadDias = value;
    }

    /**
     * Gets the value of the tecnicoDespacho property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTecnicoDespacho() {
        return tecnicoDespacho;
    }

    /**
     * Sets the value of the tecnicoDespacho property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTecnicoDespacho(String value) {
        this.tecnicoDespacho = value;
    }

    /**
     * Gets the value of the supervisorDespacho property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupervisorDespacho() {
        return supervisorDespacho;
    }

    /**
     * Sets the value of the supervisorDespacho property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupervisorDespacho(String value) {
        this.supervisorDespacho = value;
    }

    /**
     * Gets the value of the requiereVisita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequiereVisita() {
        return requiereVisita;
    }

    /**
     * Sets the value of the requiereVisita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequiereVisita(String value) {
        this.requiereVisita = value;
    }

    /**
     * Gets the value of the codigoRecibido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoRecibido() {
        return codigoRecibido;
    }

    /**
     * Sets the value of the codigoRecibido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoRecibido(String value) {
        this.codigoRecibido = value;
    }

    /**
     * Gets the value of the citaRecibida property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCitaRecibida() {
        return citaRecibida;
    }

    /**
     * Sets the value of the citaRecibida property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCitaRecibida(String value) {
        this.citaRecibida = value;
    }

    /**
     * Gets the value of the ordenesHermanas property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOrdenHermanaViewModel }
     *     
     */
    public ArrayOfOrdenHermanaViewModel getOrdenesHermanas() {
        return ordenesHermanas;
    }

    /**
     * Sets the value of the ordenesHermanas property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOrdenHermanaViewModel }
     *     
     */
    public void setOrdenesHermanas(ArrayOfOrdenHermanaViewModel value) {
        this.ordenesHermanas = value;
    }

    /**
     * Gets the value of the trackingRespuestas property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRespuestaViewModel }
     *     
     */
    public ArrayOfRespuestaViewModel getTrackingRespuestas() {
        return trackingRespuestas;
    }

    /**
     * Sets the value of the trackingRespuestas property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRespuestaViewModel }
     *     
     */
    public void setTrackingRespuestas(ArrayOfRespuestaViewModel value) {
        this.trackingRespuestas = value;
    }

    /**
     * Gets the value of the intentoContactos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfIntentoContactoViewModel }
     *     
     */
    public ArrayOfIntentoContactoViewModel getIntentoContactos() {
        return intentoContactos;
    }

    /**
     * Sets the value of the intentoContactos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfIntentoContactoViewModel }
     *     
     */
    public void setIntentoContactos(ArrayOfIntentoContactoViewModel value) {
        this.intentoContactos = value;
    }

    /**
     * Gets the value of the infoCompleta property.
     * 
	 * @return 
     */
    public boolean isInfoCompleta() {
        return infoCompleta;
    }

    /**
     * Sets the value of the infoCompleta property.
     * 
	 * @param value
     */
    public void setInfoCompleta(boolean value) {
        this.infoCompleta = value;
    }

    /**
     * Gets the value of the tipoDeOrdenSaydot property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDeOrdenSaydot() {
        return tipoDeOrdenSaydot;
    }

    /**
     * Sets the value of the tipoDeOrdenSaydot property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDeOrdenSaydot(String value) {
        this.tipoDeOrdenSaydot = value;
    }

    /**
     * Gets the value of the tipoDeOrdenOMS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDeOrdenOMS() {
        return tipoDeOrdenOMS;
    }

    /**
     * Sets the value of the tipoDeOrdenOMS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDeOrdenOMS(String value) {
        this.tipoDeOrdenOMS = value;
    }

    /**
     * Gets the value of the producto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducto() {
        return producto;
    }

    /**
     * Sets the value of the producto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducto(String value) {
        this.producto = value;
    }

    /**
     * Gets the value of the flujo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlujo() {
        return flujo;
    }

    /**
     * Sets the value of the flujo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlujo(String value) {
        this.flujo = value;
    }

    /**
     * Gets the value of the fechaRecibida property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaRecibida() {
        return fechaRecibida;
    }

    /**
     * Sets the value of the fechaRecibida property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaRecibida(String value) {
        this.fechaRecibida = value;
    }

    /**
     * Gets the value of the fechaDeCarga property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaDeCarga() {
        return fechaDeCarga;
    }

    /**
     * Sets the value of the fechaDeCarga property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaDeCarga(String value) {
        this.fechaDeCarga = value;
    }

    /**
     * Gets the value of the distrito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistrito() {
        return distrito;
    }

    /**
     * Sets the value of the distrito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistrito(String value) {
        this.distrito = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

    /**
     * Gets the value of the oferta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOferta() {
        return oferta;
    }

    /**
     * Sets the value of the oferta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOferta(String value) {
        this.oferta = value;
    }

    /**
     * Gets the value of the canal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanal() {
        return canal;
    }

    /**
     * Sets the value of the canal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanal(String value) {
        this.canal = value;
    }

    /**
     * Gets the value of the distribuidor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistribuidor() {
        return distribuidor;
    }

    /**
     * Sets the value of the distribuidor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistribuidor(String value) {
        this.distribuidor = value;
    }

    /**
     * Gets the value of the tipoDeOrden property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDeOrden() {
        return tipoDeOrden;
    }

    /**
     * Sets the value of the tipoDeOrden property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDeOrden(String value) {
        this.tipoDeOrden = value;
    }

    /**
     * Gets the value of the fechaCodigoRecibido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaCodigoRecibido() {
        return fechaCodigoRecibido;
    }

    /**
     * Sets the value of the fechaCodigoRecibido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaCodigoRecibido(String value) {
        this.fechaCodigoRecibido = value;
    }

    /**
     * Gets the value of the claseDeServicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaseDeServicio() {
        return claseDeServicio;
    }

    /**
     * Sets the value of the claseDeServicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaseDeServicio(String value) {
        this.claseDeServicio = value;
    }

    /**
     * Gets the value of the segmento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmento() {
        return segmento;
    }

    /**
     * Sets the value of the segmento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmento(String value) {
        this.segmento = value;
    }

    /**
     * Gets the value of the cajasIPTV property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCajasIPTV() {
        return cajasIPTV;
    }

    /**
     * Sets the value of the cajasIPTV property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCajasIPTV(String value) {
        this.cajasIPTV = value;
    }

    /**
     * Gets the value of the paquetesCable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaquetesCable() {
        return paquetesCable;
    }

    /**
     * Sets the value of the paquetesCable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaquetesCable(String value) {
        this.paquetesCable = value;
    }

    /**
     * Gets the value of the remarks property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getRemarks() {
        return remarks;
    }

    /**
     * Sets the value of the remarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setRemarks(ArrayOfString value) {
        this.remarks = value;
    }

    /**
     * Gets the value of the equiposTelefonicos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getEquiposTelefonicos() {
        return equiposTelefonicos;
    }

    /**
     * Sets the value of the equiposTelefonicos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setEquiposTelefonicos(ArrayOfString value) {
        this.equiposTelefonicos = value;
    }

    /**
     * Gets the value of the equiposFinanciamiento property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getEquiposFinanciamiento() {
        return equiposFinanciamiento;
    }

    /**
     * Sets the value of the equiposFinanciamiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setEquiposFinanciamiento(ArrayOfString value) {
        this.equiposFinanciamiento = value;
    }

    /**
     * Gets the value of the tipoDeOrdenSaydotDescripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDeOrdenSaydotDescripcion() {
        return tipoDeOrdenSaydotDescripcion;
    }

    /**
     * Sets the value of the tipoDeOrdenSaydotDescripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDeOrdenSaydotDescripcion(String value) {
        this.tipoDeOrdenSaydotDescripcion = value;
    }

    /**
     * Gets the value of the tipoDeOrdenOMSDescripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDeOrdenOMSDescripcion() {
        return tipoDeOrdenOMSDescripcion;
    }

    /**
     * Sets the value of the tipoDeOrdenOMSDescripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDeOrdenOMSDescripcion(String value) {
        this.tipoDeOrdenOMSDescripcion = value;
    }

    /**
     * Gets the value of the despachada property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDespachada() {
        return despachada;
    }

    /**
     * Sets the value of the despachada property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDespachada(String value) {
        this.despachada = value;
    }

    /**
     * Gets the value of the contactoAdicional property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactoAdicional() {
        return contactoAdicional;
    }

    /**
     * Sets the value of the contactoAdicional property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactoAdicional(String value) {
        this.contactoAdicional = value;
    }

    /**
     * Gets the value of the contactoAlterno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactoAlterno() {
        return contactoAlterno;
    }

    /**
     * Sets the value of the contactoAlterno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactoAlterno(String value) {
        this.contactoAlterno = value;
    }

}
