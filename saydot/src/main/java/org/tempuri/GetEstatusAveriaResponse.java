
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetEstatusAveriaResult" type="{http://tempuri.org/}EstatusAveria" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getEstatusAveriaResult"
})
@XmlRootElement(name = "GetEstatusAveriaResponse")
public class GetEstatusAveriaResponse {

	/**
	 *
	 */
	@XmlElement(name = "GetEstatusAveriaResult")
    protected EstatusAveria getEstatusAveriaResult;

    /**
     * Gets the value of the getEstatusAveriaResult property.
     * 
     * @return
     *     possible object is
     *     {@link EstatusAveria }
     *     
     */
    public EstatusAveria getGetEstatusAveriaResult() {
        return getEstatusAveriaResult;
    }

    /**
     * Sets the value of the getEstatusAveriaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link EstatusAveria }
     *     
     */
    public void setGetEstatusAveriaResult(EstatusAveria value) {
        this.getEstatusAveriaResult = value;
    }

}
