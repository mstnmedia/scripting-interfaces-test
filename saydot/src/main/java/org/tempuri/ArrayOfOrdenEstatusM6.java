
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfOrdenEstatusM6 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfOrdenEstatusM6">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrdenEstatusM6" type="{http://tempuri.org/}OrdenEstatusM6" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfOrdenEstatusM6", propOrder = {
    "ordenEstatusM6"
})
public class ArrayOfOrdenEstatusM6 {

	/**
	 *
	 */
	@XmlElement(name = "OrdenEstatusM6", nillable = true)
    protected List<OrdenEstatusM6> ordenEstatusM6;

    /**
     * Gets the value of the ordenEstatusM6 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordenEstatusM6 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdenEstatusM6().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrdenEstatusM6 }
     * 
     * 
	 * @return 
     */
    public List<OrdenEstatusM6> getOrdenEstatusM6() {
        if (ordenEstatusM6 == null) {
            ordenEstatusM6 = new ArrayList<OrdenEstatusM6>();
        }
        return this.ordenEstatusM6;
    }

}
