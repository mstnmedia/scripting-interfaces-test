
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetOrdenInfoGeneralResult" type="{http://tempuri.org/}OrdenInfoGeneralViewModel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getOrdenInfoGeneralResult"
})
@XmlRootElement(name = "GetOrdenInfoGeneralResponse")
public class GetOrdenInfoGeneralResponse {

	/**
	 *
	 */
	@XmlElement(name = "GetOrdenInfoGeneralResult")
    protected OrdenInfoGeneralViewModel getOrdenInfoGeneralResult;

    /**
     * Gets the value of the getOrdenInfoGeneralResult property.
     * 
     * @return
     *     possible object is
     *     {@link OrdenInfoGeneralViewModel }
     *     
     */
    public OrdenInfoGeneralViewModel getGetOrdenInfoGeneralResult() {
        return getOrdenInfoGeneralResult;
    }

    /**
     * Sets the value of the getOrdenInfoGeneralResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrdenInfoGeneralViewModel }
     *     
     */
    public void setGetOrdenInfoGeneralResult(OrdenInfoGeneralViewModel value) {
        this.getOrdenInfoGeneralResult = value;
    }

}
