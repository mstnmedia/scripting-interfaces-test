
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrdenEstatusM6Track complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrdenEstatusM6Track">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TASK_STATUS_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TASK_STATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TASK_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LAST_MODIFIED_USERID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LAST_MODIFIED_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WORK_QUEUE_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SCHEDULED_COMPLETION_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ESTIMATED_COMPLETION_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrdenEstatusM6Track", propOrder = {
    "taskstatusdate",
    "taskstatus",
    "tasktype",
    "lastmodifieduserid",
    "lastmodifieddate",
    "workqueueid",
    "sequence",
    "scheduledcompletiondate",
    "estimatedcompletiondate"
})
public class OrdenEstatusM6Track {

	/**
	 *
	 */
	@XmlElement(name = "TASK_STATUS_DATE")
    protected String taskstatusdate;

	/**
	 *
	 */
	@XmlElement(name = "TASK_STATUS")
    protected String taskstatus;

	/**
	 *
	 */
	@XmlElement(name = "TASK_TYPE")
    protected String tasktype;

	/**
	 *
	 */
	@XmlElement(name = "LAST_MODIFIED_USERID")
    protected String lastmodifieduserid;

	/**
	 *
	 */
	@XmlElement(name = "LAST_MODIFIED_DATE")
    protected String lastmodifieddate;

	/**
	 *
	 */
	@XmlElement(name = "WORK_QUEUE_ID")
    protected String workqueueid;

	/**
	 *
	 */
	@XmlElement(name = "SEQUENCE")
    protected String sequence;

	/**
	 *
	 */
	@XmlElement(name = "SCHEDULED_COMPLETION_DATE")
    protected String scheduledcompletiondate;

	/**
	 *
	 */
	@XmlElement(name = "ESTIMATED_COMPLETION_DATE")
    protected String estimatedcompletiondate;

    /**
     * Gets the value of the taskstatusdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTASKSTATUSDATE() {
        return taskstatusdate;
    }

    /**
     * Sets the value of the taskstatusdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTASKSTATUSDATE(String value) {
        this.taskstatusdate = value;
    }

    /**
     * Gets the value of the taskstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTASKSTATUS() {
        return taskstatus;
    }

    /**
     * Sets the value of the taskstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTASKSTATUS(String value) {
        this.taskstatus = value;
    }

    /**
     * Gets the value of the tasktype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTASKTYPE() {
        return tasktype;
    }

    /**
     * Sets the value of the tasktype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTASKTYPE(String value) {
        this.tasktype = value;
    }

    /**
     * Gets the value of the lastmodifieduserid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLASTMODIFIEDUSERID() {
        return lastmodifieduserid;
    }

    /**
     * Sets the value of the lastmodifieduserid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLASTMODIFIEDUSERID(String value) {
        this.lastmodifieduserid = value;
    }

    /**
     * Gets the value of the lastmodifieddate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLASTMODIFIEDDATE() {
        return lastmodifieddate;
    }

    /**
     * Sets the value of the lastmodifieddate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLASTMODIFIEDDATE(String value) {
        this.lastmodifieddate = value;
    }

    /**
     * Gets the value of the workqueueid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWORKQUEUEID() {
        return workqueueid;
    }

    /**
     * Sets the value of the workqueueid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWORKQUEUEID(String value) {
        this.workqueueid = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEQUENCE() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEQUENCE(String value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the scheduledcompletiondate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSCHEDULEDCOMPLETIONDATE() {
        return scheduledcompletiondate;
    }

    /**
     * Sets the value of the scheduledcompletiondate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSCHEDULEDCOMPLETIONDATE(String value) {
        this.scheduledcompletiondate = value;
    }

    /**
     * Gets the value of the estimatedcompletiondate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getESTIMATEDCOMPLETIONDATE() {
        return estimatedcompletiondate;
    }

    /**
     * Sets the value of the estimatedcompletiondate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setESTIMATEDCOMPLETIONDATE(String value) {
        this.estimatedcompletiondate = value;
    }

}
