
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegistrarFacilidadOrdenResult" type="{http://tempuri.org/}StatusCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "registrarFacilidadOrdenResult"
})
@XmlRootElement(name = "RegistrarFacilidadOrdenResponse")
public class RegistrarFacilidadOrdenResponse {

	/**
	 *
	 */
	@XmlElement(name = "RegistrarFacilidadOrdenResult")
    protected StatusCode registrarFacilidadOrdenResult;

    /**
     * Gets the value of the registrarFacilidadOrdenResult property.
     * 
     * @return
     *     possible object is
     *     {@link StatusCode }
     *     
     */
    public StatusCode getRegistrarFacilidadOrdenResult() {
        return registrarFacilidadOrdenResult;
    }

    /**
     * Sets the value of the registrarFacilidadOrdenResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusCode }
     *     
     */
    public void setRegistrarFacilidadOrdenResult(StatusCode value) {
        this.registrarFacilidadOrdenResult = value;
    }

}
