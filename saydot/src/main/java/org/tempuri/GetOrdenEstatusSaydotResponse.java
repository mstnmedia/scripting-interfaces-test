
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetOrdenEstatusSaydotResult" type="{http://tempuri.org/}OrdenEstatusSaydotViewModel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getOrdenEstatusSaydotResult"
})
@XmlRootElement(name = "GetOrdenEstatusSaydotResponse")
public class GetOrdenEstatusSaydotResponse {

	/**
	 *
	 */
	@XmlElement(name = "GetOrdenEstatusSaydotResult")
    protected OrdenEstatusSaydotViewModel getOrdenEstatusSaydotResult;

    /**
     * Gets the value of the getOrdenEstatusSaydotResult property.
     * 
     * @return
     *     possible object is
     *     {@link OrdenEstatusSaydotViewModel }
     *     
     */
    public OrdenEstatusSaydotViewModel getGetOrdenEstatusSaydotResult() {
        return getOrdenEstatusSaydotResult;
    }

    /**
     * Sets the value of the getOrdenEstatusSaydotResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrdenEstatusSaydotViewModel }
     *     
     */
    public void setGetOrdenEstatusSaydotResult(OrdenEstatusSaydotViewModel value) {
        this.getOrdenEstatusSaydotResult = value;
    }

}
