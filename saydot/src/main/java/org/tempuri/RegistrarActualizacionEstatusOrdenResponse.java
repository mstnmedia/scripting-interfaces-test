
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegistrarActualizacionEstatusOrdenResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "registrarActualizacionEstatusOrdenResult"
})
@XmlRootElement(name = "RegistrarActualizacionEstatusOrdenResponse")
public class RegistrarActualizacionEstatusOrdenResponse {

	/**
	 *
	 */
	@XmlElement(name = "RegistrarActualizacionEstatusOrdenResult")
    protected String registrarActualizacionEstatusOrdenResult;

    /**
     * Gets the value of the registrarActualizacionEstatusOrdenResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrarActualizacionEstatusOrdenResult() {
        return registrarActualizacionEstatusOrdenResult;
    }

    /**
     * Sets the value of the registrarActualizacionEstatusOrdenResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrarActualizacionEstatusOrdenResult(String value) {
        this.registrarActualizacionEstatusOrdenResult = value;
    }

}
