
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrdenEstatusDomain complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrdenEstatusDomain">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoOrden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EstatusActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaInicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoProceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Socc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tareas" type="{http://tempuri.org/}ArrayOfOrdenEstatusDomainTarea" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrdenEstatusDomain", propOrder = {
    "tipoOrden",
    "estatusActual",
    "fechaInicio",
    "tipoProceso",
    "socc",
    "tareas"
})
public class OrdenEstatusDomain {

	/**
	 *
	 */
	@XmlElement(name = "TipoOrden")
    protected String tipoOrden;

	/**
	 *
	 */
	@XmlElement(name = "EstatusActual")
    protected String estatusActual;

	/**
	 *
	 */
	@XmlElement(name = "FechaInicio")
    protected String fechaInicio;

	/**
	 *
	 */
	@XmlElement(name = "TipoProceso")
    protected String tipoProceso;

	/**
	 *
	 */
	@XmlElement(name = "Socc")
    protected String socc;

	/**
	 *
	 */
	@XmlElement(name = "Tareas")
    protected ArrayOfOrdenEstatusDomainTarea tareas;

    /**
     * Gets the value of the tipoOrden property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOrden() {
        return tipoOrden;
    }

    /**
     * Sets the value of the tipoOrden property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOrden(String value) {
        this.tipoOrden = value;
    }

    /**
     * Gets the value of the estatusActual property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstatusActual() {
        return estatusActual;
    }

    /**
     * Sets the value of the estatusActual property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstatusActual(String value) {
        this.estatusActual = value;
    }

    /**
     * Gets the value of the fechaInicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Sets the value of the fechaInicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaInicio(String value) {
        this.fechaInicio = value;
    }

    /**
     * Gets the value of the tipoProceso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoProceso() {
        return tipoProceso;
    }

    /**
     * Sets the value of the tipoProceso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoProceso(String value) {
        this.tipoProceso = value;
    }

    /**
     * Gets the value of the socc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSocc() {
        return socc;
    }

    /**
     * Sets the value of the socc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSocc(String value) {
        this.socc = value;
    }

    /**
     * Gets the value of the tareas property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOrdenEstatusDomainTarea }
     *     
     */
    public ArrayOfOrdenEstatusDomainTarea getTareas() {
        return tareas;
    }

    /**
     * Sets the value of the tareas property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOrdenEstatusDomainTarea }
     *     
     */
    public void setTareas(ArrayOfOrdenEstatusDomainTarea value) {
        this.tareas = value;
    }

}
