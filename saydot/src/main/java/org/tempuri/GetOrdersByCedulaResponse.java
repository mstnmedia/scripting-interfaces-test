
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetOrdersByCedulaResult" type="{http://tempuri.org/}ArrayOfOrdenConsultaByCedula" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getOrdersByCedulaResult"
})
@XmlRootElement(name = "GetOrdersByCedulaResponse")
public class GetOrdersByCedulaResponse {

	/**
	 *
	 */
	@XmlElement(name = "GetOrdersByCedulaResult")
    protected ArrayOfOrdenConsultaByCedula getOrdersByCedulaResult;

    /**
     * Gets the value of the getOrdersByCedulaResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOrdenConsultaByCedula }
     *     
     */
    public ArrayOfOrdenConsultaByCedula getGetOrdersByCedulaResult() {
        return getOrdersByCedulaResult;
    }

    /**
     * Sets the value of the getOrdersByCedulaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOrdenConsultaByCedula }
     *     
     */
    public void setGetOrdersByCedulaResult(ArrayOfOrdenConsultaByCedula value) {
        this.getOrdersByCedulaResult = value;
    }

}
