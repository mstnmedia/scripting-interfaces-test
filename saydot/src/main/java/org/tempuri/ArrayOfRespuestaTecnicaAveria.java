
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRespuestaTecnicaAveria complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRespuestaTecnicaAveria">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RespuestaTecnicaAveria" type="{http://tempuri.org/}RespuestaTecnicaAveria" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRespuestaTecnicaAveria", propOrder = {
    "respuestaTecnicaAveria"
})
public class ArrayOfRespuestaTecnicaAveria {

	/**
	 *
	 */
	@XmlElement(name = "RespuestaTecnicaAveria", nillable = true)
    protected List<RespuestaTecnicaAveria> respuestaTecnicaAveria;

    /**
     * Gets the value of the respuestaTecnicaAveria property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the respuestaTecnicaAveria property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRespuestaTecnicaAveria().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaTecnicaAveria }
     * 
     * 
	 * @return 
     */
    public List<RespuestaTecnicaAveria> getRespuestaTecnicaAveria() {
        if (respuestaTecnicaAveria == null) {
            respuestaTecnicaAveria = new ArrayList<RespuestaTecnicaAveria>();
        }
        return this.respuestaTecnicaAveria;
    }

}
