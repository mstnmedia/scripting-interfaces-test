
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrdenEstatusM6 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrdenEstatusM6">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DOCNUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RELATED_PON" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PON" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tracking" type="{http://tempuri.org/}ArrayOfOrdenEstatusM6Track" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrdenEstatusM6", propOrder = {
    "docnum",
    "relatedpon",
    "pon",
    "tracking"
})
public class OrdenEstatusM6 {

	/**
	 *
	 */
	@XmlElement(name = "DOCNUM")
    protected String docnum;

	/**
	 *
	 */
	@XmlElement(name = "RELATED_PON")
    protected String relatedpon;

	/**
	 *
	 */
	@XmlElement(name = "PON")
    protected String pon;

	/**
	 *
	 */
	@XmlElement(name = "Tracking")
    protected ArrayOfOrdenEstatusM6Track tracking;

    /**
     * Gets the value of the docnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOCNUM() {
        return docnum;
    }

    /**
     * Sets the value of the docnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOCNUM(String value) {
        this.docnum = value;
    }

    /**
     * Gets the value of the relatedpon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRELATEDPON() {
        return relatedpon;
    }

    /**
     * Sets the value of the relatedpon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRELATEDPON(String value) {
        this.relatedpon = value;
    }

    /**
     * Gets the value of the pon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPON() {
        return pon;
    }

    /**
     * Sets the value of the pon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPON(String value) {
        this.pon = value;
    }

    /**
     * Gets the value of the tracking property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOrdenEstatusM6Track }
     *     
     */
    public ArrayOfOrdenEstatusM6Track getTracking() {
        return tracking;
    }

    /**
     * Sets the value of the tracking property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOrdenEstatusM6Track }
     *     
     */
    public void setTracking(ArrayOfOrdenEstatusM6Track value) {
        this.tracking = value;
    }

}
