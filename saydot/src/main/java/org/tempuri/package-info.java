/**
 * Este paquete contiene las clases que fueron autogeneradas por ws-import para
 * implementar la conexión de Scripting con el sistema SaydotQ.
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://tempuri.org/", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package org.tempuri;
