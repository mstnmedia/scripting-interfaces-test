
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FacilidadFibraInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FacilidadFibraInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Localidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PuertoCard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Card" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Olt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PuertoTerminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SplitterTerminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SplitterConector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SplitterportConector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SplitterTerminalTronco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LlevaOnt" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="UsuarioOnt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PasswordOnt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Puerto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Equipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FacilidadFibraInfo", propOrder = {
    "localidad",
    "puertoCard",
    "card",
    "olt",
    "puertoTerminal",
    "splitterTerminal",
    "splitterConector",
    "splitterportConector",
    "splitterTerminalTronco",
    "llevaOnt",
    "usuarioOnt",
    "passwordOnt",
    "puerto",
    "equipo"
})
public class FacilidadFibraInfo {

	/**
	 *
	 */
	@XmlElement(name = "Localidad")
    protected String localidad;

	/**
	 *
	 */
	@XmlElement(name = "PuertoCard")
    protected String puertoCard;

	/**
	 *
	 */
	@XmlElement(name = "Card")
    protected String card;

	/**
	 *
	 */
	@XmlElement(name = "Olt")
    protected String olt;

	/**
	 *
	 */
	@XmlElement(name = "PuertoTerminal")
    protected String puertoTerminal;

	/**
	 *
	 */
	@XmlElement(name = "SplitterTerminal")
    protected String splitterTerminal;

	/**
	 *
	 */
	@XmlElement(name = "SplitterConector")
    protected String splitterConector;

	/**
	 *
	 */
	@XmlElement(name = "SplitterportConector")
    protected String splitterportConector;

	/**
	 *
	 */
	@XmlElement(name = "SplitterTerminalTronco")
    protected String splitterTerminalTronco;

	/**
	 *
	 */
	@XmlElement(name = "LlevaOnt")
    protected boolean llevaOnt;

	/**
	 *
	 */
	@XmlElement(name = "UsuarioOnt")
    protected String usuarioOnt;

	/**
	 *
	 */
	@XmlElement(name = "PasswordOnt")
    protected String passwordOnt;

	/**
	 *
	 */
	@XmlElement(name = "Puerto")
    protected String puerto;

	/**
	 *
	 */
	@XmlElement(name = "Equipo")
    protected String equipo;

    /**
     * Gets the value of the localidad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalidad() {
        return localidad;
    }

    /**
     * Sets the value of the localidad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalidad(String value) {
        this.localidad = value;
    }

    /**
     * Gets the value of the puertoCard property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPuertoCard() {
        return puertoCard;
    }

    /**
     * Sets the value of the puertoCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPuertoCard(String value) {
        this.puertoCard = value;
    }

    /**
     * Gets the value of the card property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCard() {
        return card;
    }

    /**
     * Sets the value of the card property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCard(String value) {
        this.card = value;
    }

    /**
     * Gets the value of the olt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOlt() {
        return olt;
    }

    /**
     * Sets the value of the olt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOlt(String value) {
        this.olt = value;
    }

    /**
     * Gets the value of the puertoTerminal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPuertoTerminal() {
        return puertoTerminal;
    }

    /**
     * Sets the value of the puertoTerminal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPuertoTerminal(String value) {
        this.puertoTerminal = value;
    }

    /**
     * Gets the value of the splitterTerminal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSplitterTerminal() {
        return splitterTerminal;
    }

    /**
     * Sets the value of the splitterTerminal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSplitterTerminal(String value) {
        this.splitterTerminal = value;
    }

    /**
     * Gets the value of the splitterConector property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSplitterConector() {
        return splitterConector;
    }

    /**
     * Sets the value of the splitterConector property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSplitterConector(String value) {
        this.splitterConector = value;
    }

    /**
     * Gets the value of the splitterportConector property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSplitterportConector() {
        return splitterportConector;
    }

    /**
     * Sets the value of the splitterportConector property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSplitterportConector(String value) {
        this.splitterportConector = value;
    }

    /**
     * Gets the value of the splitterTerminalTronco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSplitterTerminalTronco() {
        return splitterTerminalTronco;
    }

    /**
     * Sets the value of the splitterTerminalTronco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSplitterTerminalTronco(String value) {
        this.splitterTerminalTronco = value;
    }

    /**
     * Gets the value of the llevaOnt property.
     * 
	 * @return 
     */
    public boolean isLlevaOnt() {
        return llevaOnt;
    }

    /**
     * Sets the value of the llevaOnt property.
     * 
	 * @param value
     */
    public void setLlevaOnt(boolean value) {
        this.llevaOnt = value;
    }

    /**
     * Gets the value of the usuarioOnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioOnt() {
        return usuarioOnt;
    }

    /**
     * Sets the value of the usuarioOnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioOnt(String value) {
        this.usuarioOnt = value;
    }

    /**
     * Gets the value of the passwordOnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasswordOnt() {
        return passwordOnt;
    }

    /**
     * Sets the value of the passwordOnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasswordOnt(String value) {
        this.passwordOnt = value;
    }

    /**
     * Gets the value of the puerto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPuerto() {
        return puerto;
    }

    /**
     * Sets the value of the puerto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPuerto(String value) {
        this.puerto = value;
    }

    /**
     * Gets the value of the equipo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipo() {
        return equipo;
    }

    /**
     * Sets the value of the equipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipo(String value) {
        this.equipo = value;
    }

}
