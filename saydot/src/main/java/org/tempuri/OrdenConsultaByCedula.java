
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OrdenConsultaByCedula complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrdenConsultaByCedula">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NoOrden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SOCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodRebote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaCompromiso" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Cedula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoEstatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaRecibida" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrdenConsultaByCedula", propOrder = {
    "noOrden",
    "socc",
    "codRebote",
    "fechaCompromiso",
    "cedula",
    "codigoEstatus",
    "fechaRecibida"
})
public class OrdenConsultaByCedula {

	/**
	 *
	 */
	@XmlElement(name = "NoOrden")
    protected String noOrden;

	/**
	 *
	 */
	@XmlElement(name = "SOCC")
    protected String socc;

	/**
	 *
	 */
	@XmlElement(name = "CodRebote")
    protected String codRebote;

	/**
	 *
	 */
	@XmlElement(name = "FechaCompromiso", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaCompromiso;

	/**
	 *
	 */
	@XmlElement(name = "Cedula")
    protected String cedula;

	/**
	 *
	 */
	@XmlElement(name = "CodigoEstatus")
    protected String codigoEstatus;

	/**
	 *
	 */
	@XmlElement(name = "FechaRecibida", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaRecibida;

    /**
     * Gets the value of the noOrden property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoOrden() {
        return noOrden;
    }

    /**
     * Sets the value of the noOrden property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoOrden(String value) {
        this.noOrden = value;
    }

    /**
     * Gets the value of the socc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSOCC() {
        return socc;
    }

    /**
     * Sets the value of the socc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSOCC(String value) {
        this.socc = value;
    }

    /**
     * Gets the value of the codRebote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRebote() {
        return codRebote;
    }

    /**
     * Sets the value of the codRebote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRebote(String value) {
        this.codRebote = value;
    }

    /**
     * Gets the value of the fechaCompromiso property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCompromiso() {
        return fechaCompromiso;
    }

    /**
     * Sets the value of the fechaCompromiso property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCompromiso(XMLGregorianCalendar value) {
        this.fechaCompromiso = value;
    }

    /**
     * Gets the value of the cedula property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * Sets the value of the cedula property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCedula(String value) {
        this.cedula = value;
    }

    /**
     * Gets the value of the codigoEstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoEstatus() {
        return codigoEstatus;
    }

    /**
     * Sets the value of the codigoEstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoEstatus(String value) {
        this.codigoEstatus = value;
    }

    /**
     * Gets the value of the fechaRecibida property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaRecibida() {
        return fechaRecibida;
    }

    /**
     * Sets the value of the fechaRecibida property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaRecibida(XMLGregorianCalendar value) {
        this.fechaRecibida = value;
    }

}
