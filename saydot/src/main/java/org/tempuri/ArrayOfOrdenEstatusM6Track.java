
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfOrdenEstatusM6Track complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfOrdenEstatusM6Track">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrdenEstatusM6Track" type="{http://tempuri.org/}OrdenEstatusM6Track" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfOrdenEstatusM6Track", propOrder = {
    "ordenEstatusM6Track"
})
public class ArrayOfOrdenEstatusM6Track {

	/**
	 *
	 */
	@XmlElement(name = "OrdenEstatusM6Track", nillable = true)
    protected List<OrdenEstatusM6Track> ordenEstatusM6Track;

    /**
     * Gets the value of the ordenEstatusM6Track property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordenEstatusM6Track property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdenEstatusM6Track().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrdenEstatusM6Track }
     * 
     * 
	 * @return 
     */
    public List<OrdenEstatusM6Track> getOrdenEstatusM6Track() {
        if (ordenEstatusM6Track == null) {
            ordenEstatusM6Track = new ArrayList<OrdenEstatusM6Track>();
        }
        return this.ordenEstatusM6Track;
    }

}
