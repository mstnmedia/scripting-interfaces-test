
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for EstatusAveria complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EstatusAveria">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CaseID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Estatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Direccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ciudad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Distrito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Creado" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Proceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FAC_CABLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Poligono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cabina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Puerto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Sintoma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Prueba4Tel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Terminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DiasCalendario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodDistrito" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ProcesoNombreCorto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Segmento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SplitterPort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Caso_Escalamiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RespuestaTecnicaAverias" type="{http://tempuri.org/}ArrayOfRespuestaTecnicaAveria" minOccurs="0"/>
 *         &lt;element name="ReclamacionAverias" type="{http://tempuri.org/}ArrayOfReclamacionAveria" minOccurs="0"/>
 *         &lt;element name="HistoricoAverias" type="{http://tempuri.org/}ArrayOfHistoricoAveria" minOccurs="0"/>
 *         &lt;element name="AveriaTicketRefefidos" type="{http://tempuri.org/}ArrayOfAveriaTicketRefefido" minOccurs="0"/>
 *         &lt;element name="GroupFlows" type="{http://tempuri.org/}ArrayOfGroupFlow" minOccurs="0"/>
 *         &lt;element name="AsignacionAverias" type="{http://tempuri.org/}ArrayOfAsignacionAveria" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EstatusAveria", propOrder = {
    "caseID",
    "telefono",
    "estatus",
    "cliente",
    "direccion",
    "ciudad",
    "distrito",
    "creado",
    "proceso",
    "faccable",
    "poligono",
    "cabina",
    "puerto",
    "sintoma",
    "prueba4Tel",
    "terminal",
    "diasCalendario",
    "tipoCliente",
    "codDistrito",
    "procesoNombreCorto",
    "segmento",
    "splitterPort",
    "casoEscalamiento",
    "respuestaTecnicaAverias",
    "reclamacionAverias",
    "historicoAverias",
    "averiaTicketRefefidos",
    "groupFlows",
    "asignacionAverias"
})
public class EstatusAveria {

	/**
	 *
	 */
	@XmlElement(name = "CaseID")
    protected String caseID;

	/**
	 *
	 */
	@XmlElement(name = "Telefono")
    protected String telefono;

	/**
	 *
	 */
	@XmlElement(name = "Estatus")
    protected String estatus;

	/**
	 *
	 */
	@XmlElement(name = "Cliente")
    protected String cliente;

	/**
	 *
	 */
	@XmlElement(name = "Direccion")
    protected String direccion;

	/**
	 *
	 */
	@XmlElement(name = "Ciudad")
    protected String ciudad;

	/**
	 *
	 */
	@XmlElement(name = "Distrito")
    protected String distrito;

	/**
	 *
	 */
	@XmlElement(name = "Creado", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creado;

	/**
	 *
	 */
	@XmlElement(name = "Proceso")
    protected String proceso;

	/**
	 *
	 */
	@XmlElement(name = "FAC_CABLE")
    protected String faccable;

	/**
	 *
	 */
	@XmlElement(name = "Poligono")
    protected String poligono;

	/**
	 *
	 */
	@XmlElement(name = "Cabina")
    protected String cabina;

	/**
	 *
	 */
	@XmlElement(name = "Puerto")
    protected String puerto;

	/**
	 *
	 */
	@XmlElement(name = "Sintoma")
    protected String sintoma;

	/**
	 *
	 */
	@XmlElement(name = "Prueba4Tel")
    protected String prueba4Tel;

	/**
	 *
	 */
	@XmlElement(name = "Terminal")
    protected String terminal;

	/**
	 *
	 */
	@XmlElement(name = "DiasCalendario")
    protected String diasCalendario;

	/**
	 *
	 */
	@XmlElement(name = "TipoCliente")
    protected String tipoCliente;

	/**
	 *
	 */
	@XmlElement(name = "CodDistrito")
    protected int codDistrito;

	/**
	 *
	 */
	@XmlElement(name = "ProcesoNombreCorto")
    protected String procesoNombreCorto;

	/**
	 *
	 */
	@XmlElement(name = "Segmento")
    protected String segmento;

	/**
	 *
	 */
	@XmlElement(name = "SplitterPort")
    protected String splitterPort;

	/**
	 *
	 */
	@XmlElement(name = "Caso_Escalamiento")
    protected String casoEscalamiento;

	/**
	 *
	 */
	@XmlElement(name = "RespuestaTecnicaAverias")
    protected ArrayOfRespuestaTecnicaAveria respuestaTecnicaAverias;

	/**
	 *
	 */
	@XmlElement(name = "ReclamacionAverias")
    protected ArrayOfReclamacionAveria reclamacionAverias;

	/**
	 *
	 */
	@XmlElement(name = "HistoricoAverias")
    protected ArrayOfHistoricoAveria historicoAverias;

	/**
	 *
	 */
	@XmlElement(name = "AveriaTicketRefefidos")
    protected ArrayOfAveriaTicketRefefido averiaTicketRefefidos;

	/**
	 *
	 */
	@XmlElement(name = "GroupFlows")
    protected ArrayOfGroupFlow groupFlows;

	/**
	 *
	 */
	@XmlElement(name = "AsignacionAverias")
    protected ArrayOfAsignacionAveria asignacionAverias;

    /**
     * Gets the value of the caseID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseID() {
        return caseID;
    }

    /**
     * Sets the value of the caseID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseID(String value) {
        this.caseID = value;
    }

    /**
     * Gets the value of the telefono property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Sets the value of the telefono property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono(String value) {
        this.telefono = value;
    }

    /**
     * Gets the value of the estatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstatus() {
        return estatus;
    }

    /**
     * Sets the value of the estatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstatus(String value) {
        this.estatus = value;
    }

    /**
     * Gets the value of the cliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * Sets the value of the cliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCliente(String value) {
        this.cliente = value;
    }

    /**
     * Gets the value of the direccion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Sets the value of the direccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion(String value) {
        this.direccion = value;
    }

    /**
     * Gets the value of the ciudad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * Sets the value of the ciudad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudad(String value) {
        this.ciudad = value;
    }

    /**
     * Gets the value of the distrito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistrito() {
        return distrito;
    }

    /**
     * Sets the value of the distrito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistrito(String value) {
        this.distrito = value;
    }

    /**
     * Gets the value of the creado property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreado() {
        return creado;
    }

    /**
     * Sets the value of the creado property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreado(XMLGregorianCalendar value) {
        this.creado = value;
    }

    /**
     * Gets the value of the proceso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProceso() {
        return proceso;
    }

    /**
     * Sets the value of the proceso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProceso(String value) {
        this.proceso = value;
    }

    /**
     * Gets the value of the faccable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFACCABLE() {
        return faccable;
    }

    /**
     * Sets the value of the faccable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFACCABLE(String value) {
        this.faccable = value;
    }

    /**
     * Gets the value of the poligono property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoligono() {
        return poligono;
    }

    /**
     * Sets the value of the poligono property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoligono(String value) {
        this.poligono = value;
    }

    /**
     * Gets the value of the cabina property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabina() {
        return cabina;
    }

    /**
     * Sets the value of the cabina property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabina(String value) {
        this.cabina = value;
    }

    /**
     * Gets the value of the puerto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPuerto() {
        return puerto;
    }

    /**
     * Sets the value of the puerto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPuerto(String value) {
        this.puerto = value;
    }

    /**
     * Gets the value of the sintoma property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSintoma() {
        return sintoma;
    }

    /**
     * Sets the value of the sintoma property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSintoma(String value) {
        this.sintoma = value;
    }

    /**
     * Gets the value of the prueba4Tel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrueba4Tel() {
        return prueba4Tel;
    }

    /**
     * Sets the value of the prueba4Tel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrueba4Tel(String value) {
        this.prueba4Tel = value;
    }

    /**
     * Gets the value of the terminal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminal() {
        return terminal;
    }

    /**
     * Sets the value of the terminal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminal(String value) {
        this.terminal = value;
    }

    /**
     * Gets the value of the diasCalendario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiasCalendario() {
        return diasCalendario;
    }

    /**
     * Sets the value of the diasCalendario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCalendario(String value) {
        this.diasCalendario = value;
    }

    /**
     * Gets the value of the tipoCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCliente() {
        return tipoCliente;
    }

    /**
     * Sets the value of the tipoCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCliente(String value) {
        this.tipoCliente = value;
    }

    /**
     * Gets the value of the codDistrito property.
     * 
	 * @return 
     */
    public int getCodDistrito() {
        return codDistrito;
    }

    /**
     * Sets the value of the codDistrito property.
     * 
	 * @param value
     */
    public void setCodDistrito(int value) {
        this.codDistrito = value;
    }

    /**
     * Gets the value of the procesoNombreCorto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcesoNombreCorto() {
        return procesoNombreCorto;
    }

    /**
     * Sets the value of the procesoNombreCorto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcesoNombreCorto(String value) {
        this.procesoNombreCorto = value;
    }

    /**
     * Gets the value of the segmento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmento() {
        return segmento;
    }

    /**
     * Sets the value of the segmento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmento(String value) {
        this.segmento = value;
    }

    /**
     * Gets the value of the splitterPort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSplitterPort() {
        return splitterPort;
    }

    /**
     * Sets the value of the splitterPort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSplitterPort(String value) {
        this.splitterPort = value;
    }

    /**
     * Gets the value of the casoEscalamiento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCasoEscalamiento() {
        return casoEscalamiento;
    }

    /**
     * Sets the value of the casoEscalamiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCasoEscalamiento(String value) {
        this.casoEscalamiento = value;
    }

    /**
     * Gets the value of the respuestaTecnicaAverias property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRespuestaTecnicaAveria }
     *     
     */
    public ArrayOfRespuestaTecnicaAveria getRespuestaTecnicaAverias() {
        return respuestaTecnicaAverias;
    }

    /**
     * Sets the value of the respuestaTecnicaAverias property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRespuestaTecnicaAveria }
     *     
     */
    public void setRespuestaTecnicaAverias(ArrayOfRespuestaTecnicaAveria value) {
        this.respuestaTecnicaAverias = value;
    }

    /**
     * Gets the value of the reclamacionAverias property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfReclamacionAveria }
     *     
     */
    public ArrayOfReclamacionAveria getReclamacionAverias() {
        return reclamacionAverias;
    }

    /**
     * Sets the value of the reclamacionAverias property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfReclamacionAveria }
     *     
     */
    public void setReclamacionAverias(ArrayOfReclamacionAveria value) {
        this.reclamacionAverias = value;
    }

    /**
     * Gets the value of the historicoAverias property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfHistoricoAveria }
     *     
     */
    public ArrayOfHistoricoAveria getHistoricoAverias() {
        return historicoAverias;
    }

    /**
     * Sets the value of the historicoAverias property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfHistoricoAveria }
     *     
     */
    public void setHistoricoAverias(ArrayOfHistoricoAveria value) {
        this.historicoAverias = value;
    }

    /**
     * Gets the value of the averiaTicketRefefidos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAveriaTicketRefefido }
     *     
     */
    public ArrayOfAveriaTicketRefefido getAveriaTicketRefefidos() {
        return averiaTicketRefefidos;
    }

    /**
     * Sets the value of the averiaTicketRefefidos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAveriaTicketRefefido }
     *     
     */
    public void setAveriaTicketRefefidos(ArrayOfAveriaTicketRefefido value) {
        this.averiaTicketRefefidos = value;
    }

    /**
     * Gets the value of the groupFlows property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGroupFlow }
     *     
     */
    public ArrayOfGroupFlow getGroupFlows() {
        return groupFlows;
    }

    /**
     * Sets the value of the groupFlows property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGroupFlow }
     *     
     */
    public void setGroupFlows(ArrayOfGroupFlow value) {
        this.groupFlows = value;
    }

    /**
     * Gets the value of the asignacionAverias property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAsignacionAveria }
     *     
     */
    public ArrayOfAsignacionAveria getAsignacionAverias() {
        return asignacionAverias;
    }

    /**
     * Sets the value of the asignacionAverias property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAsignacionAveria }
     *     
     */
    public void setAsignacionAverias(ArrayOfAsignacionAveria value) {
        this.asignacionAverias = value;
    }

}
