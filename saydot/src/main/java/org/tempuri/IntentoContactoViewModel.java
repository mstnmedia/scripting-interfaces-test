
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntentoContactoViewModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntentoContactoViewModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Comentario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Intento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaInsercion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NombreContacto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TelefonoContacto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntentoContactoViewModel", propOrder = {
    "comentario",
    "intento",
    "fechaInsercion",
    "nombreContacto",
    "telefonoContacto",
    "usuario"
})
public class IntentoContactoViewModel {

	/**
	 *
	 */
	@XmlElement(name = "Comentario")
    protected String comentario;

	/**
	 *
	 */
	@XmlElement(name = "Intento")
    protected String intento;

	/**
	 *
	 */
	@XmlElement(name = "FechaInsercion")
    protected String fechaInsercion;

	/**
	 *
	 */
	@XmlElement(name = "NombreContacto")
    protected String nombreContacto;

	/**
	 *
	 */
	@XmlElement(name = "TelefonoContacto")
    protected String telefonoContacto;

	/**
	 *
	 */
	@XmlElement(name = "Usuario")
    protected String usuario;

    /**
     * Gets the value of the comentario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * Sets the value of the comentario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComentario(String value) {
        this.comentario = value;
    }

    /**
     * Gets the value of the intento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntento() {
        return intento;
    }

    /**
     * Sets the value of the intento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntento(String value) {
        this.intento = value;
    }

    /**
     * Gets the value of the fechaInsercion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaInsercion() {
        return fechaInsercion;
    }

    /**
     * Sets the value of the fechaInsercion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaInsercion(String value) {
        this.fechaInsercion = value;
    }

    /**
     * Gets the value of the nombreContacto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreContacto() {
        return nombreContacto;
    }

    /**
     * Sets the value of the nombreContacto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreContacto(String value) {
        this.nombreContacto = value;
    }

    /**
     * Gets the value of the telefonoContacto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    /**
     * Sets the value of the telefonoContacto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefonoContacto(String value) {
        this.telefonoContacto = value;
    }

    /**
     * Gets the value of the usuario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Sets the value of the usuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuario(String value) {
        this.usuario = value;
    }

}
