
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ManejarEscalamientoResult_QNAME = new QName("http://tempuri.org/", "ManejarEscalamientoResult");
    private final static QName _ArrayOfOrdenConsultaByCedula_QNAME = new QName("http://tempuri.org/", "ArrayOfOrdenConsultaByCedula");
    private final static QName _EstatusAveria_QNAME = new QName("http://tempuri.org/", "EstatusAveria");
    private final static QName _OrdenDistritoInfo_QNAME = new QName("http://tempuri.org/", "OrdenDistritoInfo");
    private final static QName _OrdenEstatusOMSViewModel_QNAME = new QName("http://tempuri.org/", "OrdenEstatusOMSViewModel");
    private final static QName _OrdenInfoGeneralViewModel_QNAME = new QName("http://tempuri.org/", "OrdenInfoGeneralViewModel");
    private final static QName _ArrayOfOrdenEstatusDomain_QNAME = new QName("http://tempuri.org/", "ArrayOfOrdenEstatusDomain");
    private final static QName _OrderDetailsIcarus_QNAME = new QName("http://tempuri.org/", "OrderDetailsIcarus");
    private final static QName _OrdenEstatusSaydotViewModel_QNAME = new QName("http://tempuri.org/", "OrdenEstatusSaydotViewModel");
    private final static QName _String_QNAME = new QName("http://tempuri.org/", "string");
    private final static QName _SWTrackingDetail_QNAME = new QName("http://tempuri.org/", "SW_TrackingDetail");
    private final static QName _OrdenSimpleViewModel_QNAME = new QName("http://tempuri.org/", "OrdenSimpleViewModel");
    private final static QName _StatusCode_QNAME = new QName("http://tempuri.org/", "StatusCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfReclamacionAveria }
     * 
	 * @return 
     */
    public ArrayOfReclamacionAveria createArrayOfReclamacionAveria() {
        return new ArrayOfReclamacionAveria();
    }

    /**
     * Create an instance of {@link RegistrarFacilidadOrden }
     * 
	 * @return 
     */
    public RegistrarFacilidadOrden createRegistrarFacilidadOrden() {
        return new RegistrarFacilidadOrden();
    }

    /**
     * Create an instance of {@link HistoricoAveria }
     * 
	 * @return 
     */
    public HistoricoAveria createHistoricoAveria() {
        return new HistoricoAveria();
    }

    /**
     * Create an instance of {@link ArrayOfGroupFlow }
     * 
	 * @return 
     */
    public ArrayOfGroupFlow createArrayOfGroupFlow() {
        return new ArrayOfGroupFlow();
    }

    /**
     * Create an instance of {@link GetEstatusAveria }
     * 
	 * @return 
     */
    public GetEstatusAveria createGetEstatusAveria() {
        return new GetEstatusAveria();
    }

    /**
     * Create an instance of {@link StatusCode }
     * 
	 * @return 
     */
    public StatusCode createStatusCode() {
        return new StatusCode();
    }

    /**
     * Create an instance of {@link GetOrdenEstatusIcarusResponse }
     * 
	 * @return 
     */
    public GetOrdenEstatusIcarusResponse createGetOrdenEstatusIcarusResponse() {
        return new GetOrdenEstatusIcarusResponse();
    }

    /**
     * Create an instance of {@link OrdenDistritoInfo }
     * 
	 * @return 
     */
    public OrdenDistritoInfo createOrdenDistritoInfo() {
        return new OrdenDistritoInfo();
    }

    /**
     * Create an instance of {@link ArrayOfOrdenEstatusDomain }
     * 
	 * @return 
     */
    public ArrayOfOrdenEstatusDomain createArrayOfOrdenEstatusDomain() {
        return new ArrayOfOrdenEstatusDomain();
    }

    /**
     * Create an instance of {@link OrdenEstatusOMSViewModel }
     * 
	 * @return 
     */
    public OrdenEstatusOMSViewModel createOrdenEstatusOMSViewModel() {
        return new OrdenEstatusOMSViewModel();
    }

    /**
     * Create an instance of {@link ActualizarFacilidadOrden }
     * 
	 * @return 
     */
    public ActualizarFacilidadOrden createActualizarFacilidadOrden() {
        return new ActualizarFacilidadOrden();
    }

    /**
     * Create an instance of {@link ArrayOfRespuestaTecnicaAveria }
     * 
	 * @return 
     */
    public ArrayOfRespuestaTecnicaAveria createArrayOfRespuestaTecnicaAveria() {
        return new ArrayOfRespuestaTecnicaAveria();
    }

    /**
     * Create an instance of {@link GetOrdersByCedulaResponse }
     * 
	 * @return 
     */
    public GetOrdersByCedulaResponse createGetOrdersByCedulaResponse() {
        return new GetOrdersByCedulaResponse();
    }

    /**
     * Create an instance of {@link AsignacionAveria }
     * 
	 * @return 
     */
    public AsignacionAveria createAsignacionAveria() {
        return new AsignacionAveria();
    }

    /**
     * Create an instance of {@link OrdenConsultaByCedula }
     * 
	 * @return 
     */
    public OrdenConsultaByCedula createOrdenConsultaByCedula() {
        return new OrdenConsultaByCedula();
    }

    /**
     * Create an instance of {@link GetOrdenEstatusSaydot }
     * 
	 * @return 
     */
    public GetOrdenEstatusSaydot createGetOrdenEstatusSaydot() {
        return new GetOrdenEstatusSaydot();
    }

    /**
     * Create an instance of {@link GetOrdenEstatusDomainResponse }
     * 
	 * @return 
     */
    public GetOrdenEstatusDomainResponse createGetOrdenEstatusDomainResponse() {
        return new GetOrdenEstatusDomainResponse();
    }

    /**
     * Create an instance of {@link GetOrdenEstatusOMSResponse }
     * 
	 * @return 
     */
    public GetOrdenEstatusOMSResponse createGetOrdenEstatusOMSResponse() {
        return new GetOrdenEstatusOMSResponse();
    }

    /**
     * Create an instance of {@link ArrayOfOrdenEstatusDomainTarea }
     * 
	 * @return 
     */
    public ArrayOfOrdenEstatusDomainTarea createArrayOfOrdenEstatusDomainTarea() {
        return new ArrayOfOrdenEstatusDomainTarea();
    }

    /**
     * Create an instance of {@link GetDistritoOrdenCerradaResponse }
     * 
	 * @return 
     */
    public GetDistritoOrdenCerradaResponse createGetDistritoOrdenCerradaResponse() {
        return new GetDistritoOrdenCerradaResponse();
    }

    /**
     * Create an instance of {@link OrdenEstatusSaydotViewModel }
     * 
	 * @return 
     */
    public OrdenEstatusSaydotViewModel createOrdenEstatusSaydotViewModel() {
        return new OrdenEstatusSaydotViewModel();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
	 * @return 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link AddFacilidadesFibra }
     * 
	 * @return 
     */
    public AddFacilidadesFibra createAddFacilidadesFibra() {
        return new AddFacilidadesFibra();
    }

    /**
     * Create an instance of {@link ArrayOfTicketWorkFlow }
     * 
	 * @return 
     */
    public ArrayOfTicketWorkFlow createArrayOfTicketWorkFlow() {
        return new ArrayOfTicketWorkFlow();
    }

    /**
     * Create an instance of {@link GetTrackingDetailByTicketIDResponse }
     * 
	 * @return 
     */
    public GetTrackingDetailByTicketIDResponse createGetTrackingDetailByTicketIDResponse() {
        return new GetTrackingDetailByTicketIDResponse();
    }

    /**
     * Create an instance of {@link AveriaTicketRefefido }
     * 
	 * @return 
     */
    public AveriaTicketRefefido createAveriaTicketRefefido() {
        return new AveriaTicketRefefido();
    }

    /**
     * Create an instance of {@link OrdenSimpleViewModel }
     * 
	 * @return 
     */
    public OrdenSimpleViewModel createOrdenSimpleViewModel() {
        return new OrdenSimpleViewModel();
    }

    /**
     * Create an instance of {@link OrdenEstatusDomain }
     * 
	 * @return 
     */
    public OrdenEstatusDomain createOrdenEstatusDomain() {
        return new OrdenEstatusDomain();
    }

    /**
     * Create an instance of {@link ManejarEscalamientoResponse }
     * 
	 * @return 
     */
    public ManejarEscalamientoResponse createManejarEscalamientoResponse() {
        return new ManejarEscalamientoResponse();
    }

    /**
     * Create an instance of {@link SWTrackingDetail }
     * 
	 * @return 
     */
    public SWTrackingDetail createSWTrackingDetail() {
        return new SWTrackingDetail();
    }

    /**
     * Create an instance of {@link GetOrdenEstatusSaydotResponse }
     * 
	 * @return 
     */
    public GetOrdenEstatusSaydotResponse createGetOrdenEstatusSaydotResponse() {
        return new GetOrdenEstatusSaydotResponse();
    }

    /**
     * Create an instance of {@link GetOrdenInfoGeneralResponse }
     * 
	 * @return 
     */
    public GetOrdenInfoGeneralResponse createGetOrdenInfoGeneralResponse() {
        return new GetOrdenInfoGeneralResponse();
    }

    /**
     * Create an instance of {@link ReclamacionAveria }
     * 
	 * @return 
     */
    public ReclamacionAveria createReclamacionAveria() {
        return new ReclamacionAveria();
    }

    /**
     * Create an instance of {@link RegistrarActualizacionEstatusOrden }
     * 
	 * @return 
     */
    public RegistrarActualizacionEstatusOrden createRegistrarActualizacionEstatusOrden() {
        return new RegistrarActualizacionEstatusOrden();
    }

    /**
     * Create an instance of {@link RespuestaViewModel }
     * 
	 * @return 
     */
    public RespuestaViewModel createRespuestaViewModel() {
        return new RespuestaViewModel();
    }

    /**
     * Create an instance of {@link OrdenEstatusDomainTarea }
     * 
	 * @return 
     */
    public OrdenEstatusDomainTarea createOrdenEstatusDomainTarea() {
        return new OrdenEstatusDomainTarea();
    }

    /**
     * Create an instance of {@link GroupFlow }
     * 
	 * @return 
     */
    public GroupFlow createGroupFlow() {
        return new GroupFlow();
    }

    /**
     * Create an instance of {@link ArrayOfOrdenEstatusM6Track }
     * 
	 * @return 
     */
    public ArrayOfOrdenEstatusM6Track createArrayOfOrdenEstatusM6Track() {
        return new ArrayOfOrdenEstatusM6Track();
    }

    /**
     * Create an instance of {@link GetOrdenEstatusM6Response }
     * 
	 * @return 
     */
    public GetOrdenEstatusM6Response createGetOrdenEstatusM6Response() {
        return new GetOrdenEstatusM6Response();
    }

    /**
     * Create an instance of {@link GetEstatusOS }
     * 
	 * @return 
     */
    public GetEstatusOS createGetEstatusOS() {
        return new GetEstatusOS();
    }

    /**
     * Create an instance of {@link ArrayOfOrdenHermanaViewModel }
     * 
	 * @return 
     */
    public ArrayOfOrdenHermanaViewModel createArrayOfOrdenHermanaViewModel() {
        return new ArrayOfOrdenHermanaViewModel();
    }

    /**
     * Create an instance of {@link TicketWorkFlow }
     * 
	 * @return 
     */
    public TicketWorkFlow createTicketWorkFlow() {
        return new TicketWorkFlow();
    }

    /**
     * Create an instance of {@link GetEstatusAveriaResponse }
     * 
	 * @return 
     */
    public GetEstatusAveriaResponse createGetEstatusAveriaResponse() {
        return new GetEstatusAveriaResponse();
    }

    /**
     * Create an instance of {@link OrdenEstatusM6 }
     * 
	 * @return 
     */
    public OrdenEstatusM6 createOrdenEstatusM6() {
        return new OrdenEstatusM6();
    }

    /**
     * Create an instance of {@link GetDistritoOrdenCerrada }
     * 
	 * @return 
     */
    public GetDistritoOrdenCerrada createGetDistritoOrdenCerrada() {
        return new GetDistritoOrdenCerrada();
    }

    /**
     * Create an instance of {@link IntentoContactoViewModel }
     * 
	 * @return 
     */
    public IntentoContactoViewModel createIntentoContactoViewModel() {
        return new IntentoContactoViewModel();
    }

    /**
     * Create an instance of {@link RespuestaTecnicaAveria }
     * 
	 * @return 
     */
    public RespuestaTecnicaAveria createRespuestaTecnicaAveria() {
        return new RespuestaTecnicaAveria();
    }

    /**
     * Create an instance of {@link GetOrdenEstatusOMS }
     * 
	 * @return 
     */
    public GetOrdenEstatusOMS createGetOrdenEstatusOMS() {
        return new GetOrdenEstatusOMS();
    }

    /**
     * Create an instance of {@link ArrayOfAveriaTicketRefefido }
     * 
	 * @return 
     */
    public ArrayOfAveriaTicketRefefido createArrayOfAveriaTicketRefefido() {
        return new ArrayOfAveriaTicketRefefido();
    }

    /**
     * Create an instance of {@link OrderDetailsIcarus }
     * 
	 * @return 
     */
    public OrderDetailsIcarus createOrderDetailsIcarus() {
        return new OrderDetailsIcarus();
    }

    /**
     * Create an instance of {@link ArrayOfHistoricoAveria }
     * 
	 * @return 
     */
    public ArrayOfHistoricoAveria createArrayOfHistoricoAveria() {
        return new ArrayOfHistoricoAveria();
    }

    /**
     * Create an instance of {@link AddFacilidadesFibraResponse }
     * 
	 * @return 
     */
    public AddFacilidadesFibraResponse createAddFacilidadesFibraResponse() {
        return new AddFacilidadesFibraResponse();
    }

    /**
     * Create an instance of {@link ArrayOfOrdenEstatusM6 }
     * 
	 * @return 
     */
    public ArrayOfOrdenEstatusM6 createArrayOfOrdenEstatusM6() {
        return new ArrayOfOrdenEstatusM6();
    }

    /**
     * Create an instance of {@link RegistrarActualizacionEstatusOrdenResponse }
     * 
	 * @return 
     */
    public RegistrarActualizacionEstatusOrdenResponse createRegistrarActualizacionEstatusOrdenResponse() {
        return new RegistrarActualizacionEstatusOrdenResponse();
    }

    /**
     * Create an instance of {@link RegistrarFacilidadOrdenResponse }
     * 
	 * @return 
     */
    public RegistrarFacilidadOrdenResponse createRegistrarFacilidadOrdenResponse() {
        return new RegistrarFacilidadOrdenResponse();
    }

    /**
     * Create an instance of {@link GetOrdenEstatusIcarus }
     * 
	 * @return 
     */
    public GetOrdenEstatusIcarus createGetOrdenEstatusIcarus() {
        return new GetOrdenEstatusIcarus();
    }

    /**
     * Create an instance of {@link OrdenFacilidadFibraInfo }
     * 
	 * @return 
     */
    public OrdenFacilidadFibraInfo createOrdenFacilidadFibraInfo() {
        return new OrdenFacilidadFibraInfo();
    }

    /**
     * Create an instance of {@link FacilidadFibraInfo }
     * 
	 * @return 
     */
    public FacilidadFibraInfo createFacilidadFibraInfo() {
        return new FacilidadFibraInfo();
    }

    /**
     * Create an instance of {@link OrdenInfoGeneralViewModel }
     * 
	 * @return 
     */
    public OrdenInfoGeneralViewModel createOrdenInfoGeneralViewModel() {
        return new OrdenInfoGeneralViewModel();
    }

    /**
     * Create an instance of {@link ArrayOfIntentoContactoViewModel }
     * 
	 * @return 
     */
    public ArrayOfIntentoContactoViewModel createArrayOfIntentoContactoViewModel() {
        return new ArrayOfIntentoContactoViewModel();
    }

    /**
     * Create an instance of {@link GetTrackingDetailByTicketID }
     * 
	 * @return 
     */
    public GetTrackingDetailByTicketID createGetTrackingDetailByTicketID() {
        return new GetTrackingDetailByTicketID();
    }

    /**
     * Create an instance of {@link OrdenEstatusM6Track }
     * 
	 * @return 
     */
    public OrdenEstatusM6Track createOrdenEstatusM6Track() {
        return new OrdenEstatusM6Track();
    }

    /**
     * Create an instance of {@link ActualizarFacilidadOrdenResponse }
     * 
	 * @return 
     */
    public ActualizarFacilidadOrdenResponse createActualizarFacilidadOrdenResponse() {
        return new ActualizarFacilidadOrdenResponse();
    }

    /**
     * Create an instance of {@link GetOrdenEstatusDomain }
     * 
	 * @return 
     */
    public GetOrdenEstatusDomain createGetOrdenEstatusDomain() {
        return new GetOrdenEstatusDomain();
    }

    /**
     * Create an instance of {@link GetOrdenEstatusM6 }
     * 
	 * @return 
     */
    public GetOrdenEstatusM6 createGetOrdenEstatusM6() {
        return new GetOrdenEstatusM6();
    }

    /**
     * Create an instance of {@link GetOrdersByCedula }
     * 
	 * @return 
     */
    public GetOrdersByCedula createGetOrdersByCedula() {
        return new GetOrdersByCedula();
    }

    /**
     * Create an instance of {@link ManejarEscalamiento }
     * 
	 * @return 
     */
    public ManejarEscalamiento createManejarEscalamiento() {
        return new ManejarEscalamiento();
    }

    /**
     * Create an instance of {@link ArrayOfOrdenConsultaByCedula }
     * 
	 * @return 
     */
    public ArrayOfOrdenConsultaByCedula createArrayOfOrdenConsultaByCedula() {
        return new ArrayOfOrdenConsultaByCedula();
    }

    /**
     * Create an instance of {@link GetEstatusOSResponse }
     * 
	 * @return 
     */
    public GetEstatusOSResponse createGetEstatusOSResponse() {
        return new GetEstatusOSResponse();
    }

    /**
     * Create an instance of {@link TecnologiaInfo }
     * 
	 * @return 
     */
    public TecnologiaInfo createTecnologiaInfo() {
        return new TecnologiaInfo();
    }

    /**
     * Create an instance of {@link ArrayOfRespuestaViewModel }
     * 
	 * @return 
     */
    public ArrayOfRespuestaViewModel createArrayOfRespuestaViewModel() {
        return new ArrayOfRespuestaViewModel();
    }

    /**
     * Create an instance of {@link OrdenHermanaViewModel }
     * 
	 * @return 
     */
    public OrdenHermanaViewModel createOrdenHermanaViewModel() {
        return new OrdenHermanaViewModel();
    }

    /**
     * Create an instance of {@link GetOrdenInfoGeneral }
     * 
	 * @return 
     */
    public GetOrdenInfoGeneral createGetOrdenInfoGeneral() {
        return new GetOrdenInfoGeneral();
    }

    /**
     * Create an instance of {@link ArrayOfAsignacionAveria }
     * 
	 * @return 
     */
    public ArrayOfAsignacionAveria createArrayOfAsignacionAveria() {
        return new ArrayOfAsignacionAveria();
    }

    /**
     * Create an instance of {@link EstatusAveria }
     * 
	 * @return 
     */
    public EstatusAveria createEstatusAveria() {
        return new EstatusAveria();
    }

    /**
     * Create an instance of {@link ManejarEscalamientoResult }
     * 
	 * @return 
     */
    public ManejarEscalamientoResult createManejarEscalamientoResult() {
        return new ManejarEscalamientoResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ManejarEscalamientoResult }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ManejarEscalamientoResult")
    public JAXBElement<ManejarEscalamientoResult> createManejarEscalamientoResult(ManejarEscalamientoResult value) {
        return new JAXBElement<ManejarEscalamientoResult>(_ManejarEscalamientoResult_QNAME, ManejarEscalamientoResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfOrdenConsultaByCedula }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfOrdenConsultaByCedula")
    public JAXBElement<ArrayOfOrdenConsultaByCedula> createArrayOfOrdenConsultaByCedula(ArrayOfOrdenConsultaByCedula value) {
        return new JAXBElement<ArrayOfOrdenConsultaByCedula>(_ArrayOfOrdenConsultaByCedula_QNAME, ArrayOfOrdenConsultaByCedula.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EstatusAveria }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "EstatusAveria")
    public JAXBElement<EstatusAveria> createEstatusAveria(EstatusAveria value) {
        return new JAXBElement<EstatusAveria>(_EstatusAveria_QNAME, EstatusAveria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrdenDistritoInfo }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "OrdenDistritoInfo")
    public JAXBElement<OrdenDistritoInfo> createOrdenDistritoInfo(OrdenDistritoInfo value) {
        return new JAXBElement<OrdenDistritoInfo>(_OrdenDistritoInfo_QNAME, OrdenDistritoInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrdenEstatusOMSViewModel }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "OrdenEstatusOMSViewModel")
    public JAXBElement<OrdenEstatusOMSViewModel> createOrdenEstatusOMSViewModel(OrdenEstatusOMSViewModel value) {
        return new JAXBElement<OrdenEstatusOMSViewModel>(_OrdenEstatusOMSViewModel_QNAME, OrdenEstatusOMSViewModel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrdenInfoGeneralViewModel }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "OrdenInfoGeneralViewModel")
    public JAXBElement<OrdenInfoGeneralViewModel> createOrdenInfoGeneralViewModel(OrdenInfoGeneralViewModel value) {
        return new JAXBElement<OrdenInfoGeneralViewModel>(_OrdenInfoGeneralViewModel_QNAME, OrdenInfoGeneralViewModel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfOrdenEstatusDomain }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfOrdenEstatusDomain")
    public JAXBElement<ArrayOfOrdenEstatusDomain> createArrayOfOrdenEstatusDomain(ArrayOfOrdenEstatusDomain value) {
        return new JAXBElement<ArrayOfOrdenEstatusDomain>(_ArrayOfOrdenEstatusDomain_QNAME, ArrayOfOrdenEstatusDomain.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderDetailsIcarus }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "OrderDetailsIcarus")
    public JAXBElement<OrderDetailsIcarus> createOrderDetailsIcarus(OrderDetailsIcarus value) {
        return new JAXBElement<OrderDetailsIcarus>(_OrderDetailsIcarus_QNAME, OrderDetailsIcarus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrdenEstatusSaydotViewModel }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "OrdenEstatusSaydotViewModel")
    public JAXBElement<OrdenEstatusSaydotViewModel> createOrdenEstatusSaydotViewModel(OrdenEstatusSaydotViewModel value) {
        return new JAXBElement<OrdenEstatusSaydotViewModel>(_OrdenEstatusSaydotViewModel_QNAME, OrdenEstatusSaydotViewModel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SWTrackingDetail }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "SW_TrackingDetail")
    public JAXBElement<SWTrackingDetail> createSWTrackingDetail(SWTrackingDetail value) {
        return new JAXBElement<SWTrackingDetail>(_SWTrackingDetail_QNAME, SWTrackingDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrdenSimpleViewModel }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "OrdenSimpleViewModel")
    public JAXBElement<OrdenSimpleViewModel> createOrdenSimpleViewModel(OrdenSimpleViewModel value) {
        return new JAXBElement<OrdenSimpleViewModel>(_OrdenSimpleViewModel_QNAME, OrdenSimpleViewModel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusCode }{@code >}}
     * 
	 * @param value
	 * @return 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "StatusCode")
    public JAXBElement<StatusCode> createStatusCode(StatusCode value) {
        return new JAXBElement<StatusCode>(_StatusCode_QNAME, StatusCode.class, null, value);
    }

}
