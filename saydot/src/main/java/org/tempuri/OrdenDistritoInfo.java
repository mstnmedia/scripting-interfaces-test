
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrdenDistritoInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrdenDistritoInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NumeroOrden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoDistrito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Descripcion2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NombreTecnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TarjetaTecnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flota" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoRebote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoReboteDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaCompleta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RequiereVisita" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Estatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrdenDistritoInfo", propOrder = {
    "numeroOrden",
    "codigoDistrito",
    "descripcion",
    "descripcion2",
    "nombreTecnico",
    "tarjetaTecnico",
    "flota",
    "codigoRebote",
    "codigoReboteDescripcion",
    "fechaCompleta",
    "requiereVisita",
    "estatus"
})
public class OrdenDistritoInfo {

	/**
	 *
	 */
	@XmlElement(name = "NumeroOrden")
    protected String numeroOrden;

	/**
	 *
	 */
	@XmlElement(name = "CodigoDistrito")
    protected String codigoDistrito;

	/**
	 *
	 */
	@XmlElement(name = "Descripcion")
    protected String descripcion;

	/**
	 *
	 */
	@XmlElement(name = "Descripcion2")
    protected String descripcion2;

	/**
	 *
	 */
	@XmlElement(name = "NombreTecnico")
    protected String nombreTecnico;

	/**
	 *
	 */
	@XmlElement(name = "TarjetaTecnico")
    protected String tarjetaTecnico;

	/**
	 *
	 */
	@XmlElement(name = "Flota")
    protected String flota;

	/**
	 *
	 */
	@XmlElement(name = "CodigoRebote")
    protected String codigoRebote;

	/**
	 *
	 */
	@XmlElement(name = "CodigoReboteDescripcion")
    protected String codigoReboteDescripcion;

	/**
	 *
	 */
	@XmlElement(name = "FechaCompleta")
    protected String fechaCompleta;

	/**
	 *
	 */
	@XmlElement(name = "RequiereVisita")
    protected boolean requiereVisita;

	/**
	 *
	 */
	@XmlElement(name = "Estatus")
    protected String estatus;

    /**
     * Gets the value of the numeroOrden property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroOrden() {
        return numeroOrden;
    }

    /**
     * Sets the value of the numeroOrden property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroOrden(String value) {
        this.numeroOrden = value;
    }

    /**
     * Gets the value of the codigoDistrito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDistrito() {
        return codigoDistrito;
    }

    /**
     * Sets the value of the codigoDistrito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDistrito(String value) {
        this.codigoDistrito = value;
    }

    /**
     * Gets the value of the descripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Sets the value of the descripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Gets the value of the descripcion2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion2() {
        return descripcion2;
    }

    /**
     * Sets the value of the descripcion2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion2(String value) {
        this.descripcion2 = value;
    }

    /**
     * Gets the value of the nombreTecnico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreTecnico() {
        return nombreTecnico;
    }

    /**
     * Sets the value of the nombreTecnico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreTecnico(String value) {
        this.nombreTecnico = value;
    }

    /**
     * Gets the value of the tarjetaTecnico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarjetaTecnico() {
        return tarjetaTecnico;
    }

    /**
     * Sets the value of the tarjetaTecnico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarjetaTecnico(String value) {
        this.tarjetaTecnico = value;
    }

    /**
     * Gets the value of the flota property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlota() {
        return flota;
    }

    /**
     * Sets the value of the flota property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlota(String value) {
        this.flota = value;
    }

    /**
     * Gets the value of the codigoRebote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoRebote() {
        return codigoRebote;
    }

    /**
     * Sets the value of the codigoRebote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoRebote(String value) {
        this.codigoRebote = value;
    }

    /**
     * Gets the value of the codigoReboteDescripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoReboteDescripcion() {
        return codigoReboteDescripcion;
    }

    /**
     * Sets the value of the codigoReboteDescripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoReboteDescripcion(String value) {
        this.codigoReboteDescripcion = value;
    }

    /**
     * Gets the value of the fechaCompleta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaCompleta() {
        return fechaCompleta;
    }

    /**
     * Sets the value of the fechaCompleta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaCompleta(String value) {
        this.fechaCompleta = value;
    }

    /**
     * Gets the value of the requiereVisita property.
     * 
	 * @return 
     */
    public boolean isRequiereVisita() {
        return requiereVisita;
    }

    /**
     * Sets the value of the requiereVisita property.
     * 
	 * @param value
     */
    public void setRequiereVisita(boolean value) {
        this.requiereVisita = value;
    }

    /**
     * Gets the value of the estatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstatus() {
        return estatus;
    }

    /**
     * Sets the value of the estatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstatus(String value) {
        this.estatus = value;
    }

}
