
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetOrdenEstatusOMSResult" type="{http://tempuri.org/}OrdenEstatusOMSViewModel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getOrdenEstatusOMSResult"
})
@XmlRootElement(name = "GetOrdenEstatusOMSResponse")
public class GetOrdenEstatusOMSResponse {

	/**
	 *
	 */
	@XmlElement(name = "GetOrdenEstatusOMSResult")
    protected OrdenEstatusOMSViewModel getOrdenEstatusOMSResult;

    /**
     * Gets the value of the getOrdenEstatusOMSResult property.
     * 
     * @return
     *     possible object is
     *     {@link OrdenEstatusOMSViewModel }
     *     
     */
    public OrdenEstatusOMSViewModel getGetOrdenEstatusOMSResult() {
        return getOrdenEstatusOMSResult;
    }

    /**
     * Sets the value of the getOrdenEstatusOMSResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrdenEstatusOMSViewModel }
     *     
     */
    public void setGetOrdenEstatusOMSResult(OrdenEstatusOMSViewModel value) {
        this.getOrdenEstatusOMSResult = value;
    }

}
