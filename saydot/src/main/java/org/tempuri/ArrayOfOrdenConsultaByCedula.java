
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfOrdenConsultaByCedula complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfOrdenConsultaByCedula">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrdenConsultaByCedula" type="{http://tempuri.org/}OrdenConsultaByCedula" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfOrdenConsultaByCedula", propOrder = {
    "ordenConsultaByCedula"
})
public class ArrayOfOrdenConsultaByCedula {

	/**
	 *
	 */
	@XmlElement(name = "OrdenConsultaByCedula", nillable = true)
    protected List<OrdenConsultaByCedula> ordenConsultaByCedula;

    /**
     * Gets the value of the ordenConsultaByCedula property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordenConsultaByCedula property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdenConsultaByCedula().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrdenConsultaByCedula }
     * 
     * 
	 * @return 
     */
    public List<OrdenConsultaByCedula> getOrdenConsultaByCedula() {
        if (ordenConsultaByCedula == null) {
            ordenConsultaByCedula = new ArrayList<OrdenConsultaByCedula>();
        }
        return this.ordenConsultaByCedula;
    }

}
