
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetEstatusOSResult" type="{http://tempuri.org/}OrdenSimpleViewModel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getEstatusOSResult"
})
@XmlRootElement(name = "GetEstatusOSResponse")
public class GetEstatusOSResponse {

	/**
	 *
	 */
	@XmlElement(name = "GetEstatusOSResult")
    protected OrdenSimpleViewModel getEstatusOSResult;

    /**
     * Gets the value of the getEstatusOSResult property.
     * 
     * @return
     *     possible object is
     *     {@link OrdenSimpleViewModel }
     *     
     */
    public OrdenSimpleViewModel getGetEstatusOSResult() {
        return getEstatusOSResult;
    }

    /**
     * Sets the value of the getEstatusOSResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrdenSimpleViewModel }
     *     
     */
    public void setGetEstatusOSResult(OrdenSimpleViewModel value) {
        this.getEstatusOSResult = value;
    }

}
