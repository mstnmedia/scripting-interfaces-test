
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrdenFacilidadFibraInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrdenFacilidadFibraInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NoOrden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Facilidad" type="{http://tempuri.org/}FacilidadFibraInfo" minOccurs="0"/>
 *         &lt;element name="Tecnologia" type="{http://tempuri.org/}TecnologiaInfo" minOccurs="0"/>
 *         &lt;element name="ActivateVisita" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="TipoOrden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrdenFacilidadFibraInfo", propOrder = {
    "noOrden",
    "telefono",
    "facilidad",
    "tecnologia",
    "activateVisita",
    "tipoOrden"
})
public class OrdenFacilidadFibraInfo {

	/**
	 *
	 */
	@XmlElement(name = "NoOrden")
    protected String noOrden;

	/**
	 *
	 */
	@XmlElement(name = "Telefono")
    protected String telefono;

	/**
	 *
	 */
	@XmlElement(name = "Facilidad")
    protected FacilidadFibraInfo facilidad;

	/**
	 *
	 */
	@XmlElement(name = "Tecnologia")
    protected TecnologiaInfo tecnologia;

	/**
	 *
	 */
	@XmlElement(name = "ActivateVisita")
    protected boolean activateVisita;

	/**
	 *
	 */
	@XmlElement(name = "TipoOrden")
    protected String tipoOrden;

    /**
     * Gets the value of the noOrden property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoOrden() {
        return noOrden;
    }

    /**
     * Sets the value of the noOrden property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoOrden(String value) {
        this.noOrden = value;
    }

    /**
     * Gets the value of the telefono property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Sets the value of the telefono property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono(String value) {
        this.telefono = value;
    }

    /**
     * Gets the value of the facilidad property.
     * 
     * @return
     *     possible object is
     *     {@link FacilidadFibraInfo }
     *     
     */
    public FacilidadFibraInfo getFacilidad() {
        return facilidad;
    }

    /**
     * Sets the value of the facilidad property.
     * 
     * @param value
     *     allowed object is
     *     {@link FacilidadFibraInfo }
     *     
     */
    public void setFacilidad(FacilidadFibraInfo value) {
        this.facilidad = value;
    }

    /**
     * Gets the value of the tecnologia property.
     * 
     * @return
     *     possible object is
     *     {@link TecnologiaInfo }
     *     
     */
    public TecnologiaInfo getTecnologia() {
        return tecnologia;
    }

    /**
     * Sets the value of the tecnologia property.
     * 
     * @param value
     *     allowed object is
     *     {@link TecnologiaInfo }
     *     
     */
    public void setTecnologia(TecnologiaInfo value) {
        this.tecnologia = value;
    }

    /**
     * Gets the value of the activateVisita property.
     * 
	 * @return 
     */
    public boolean isActivateVisita() {
        return activateVisita;
    }

    /**
     * Sets the value of the activateVisita property.
     * 
	 * @param value
     */
    public void setActivateVisita(boolean value) {
        this.activateVisita = value;
    }

    /**
     * Gets the value of the tipoOrden property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOrden() {
        return tipoOrden;
    }

    /**
     * Sets the value of the tipoOrden property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOrden(String value) {
        this.tipoOrden = value;
    }

}
