
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrdenHermanaViewModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrdenHermanaViewModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NumeroOrden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RequiereVisita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CitaRecibida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoDeOrden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrdenHermanaViewModel", propOrder = {
    "numeroOrden",
    "requiereVisita",
    "citaRecibida",
    "tipoDeOrden"
})
public class OrdenHermanaViewModel {

	/**
	 *
	 */
	@XmlElement(name = "NumeroOrden")
    protected String numeroOrden;

	/**
	 *
	 */
	@XmlElement(name = "RequiereVisita")
    protected String requiereVisita;

	/**
	 *
	 */
	@XmlElement(name = "CitaRecibida")
    protected String citaRecibida;

	/**
	 *
	 */
	@XmlElement(name = "TipoDeOrden")
    protected String tipoDeOrden;

    /**
     * Gets the value of the numeroOrden property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroOrden() {
        return numeroOrden;
    }

    /**
     * Sets the value of the numeroOrden property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroOrden(String value) {
        this.numeroOrden = value;
    }

    /**
     * Gets the value of the requiereVisita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequiereVisita() {
        return requiereVisita;
    }

    /**
     * Sets the value of the requiereVisita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequiereVisita(String value) {
        this.requiereVisita = value;
    }

    /**
     * Gets the value of the citaRecibida property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCitaRecibida() {
        return citaRecibida;
    }

    /**
     * Sets the value of the citaRecibida property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCitaRecibida(String value) {
        this.citaRecibida = value;
    }

    /**
     * Gets the value of the tipoDeOrden property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDeOrden() {
        return tipoDeOrden;
    }

    /**
     * Sets the value of the tipoDeOrden property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDeOrden(String value) {
        this.tipoDeOrden = value;
    }

}
