
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfIntentoContactoViewModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfIntentoContactoViewModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntentoContactoViewModel" type="{http://tempuri.org/}IntentoContactoViewModel" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfIntentoContactoViewModel", propOrder = {
    "intentoContactoViewModel"
})
public class ArrayOfIntentoContactoViewModel {

	/**
	 *
	 */
	@XmlElement(name = "IntentoContactoViewModel", nillable = true)
    protected List<IntentoContactoViewModel> intentoContactoViewModel;

    /**
     * Gets the value of the intentoContactoViewModel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intentoContactoViewModel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntentoContactoViewModel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntentoContactoViewModel }
     * 
     * 
	 * @return 
     */
    public List<IntentoContactoViewModel> getIntentoContactoViewModel() {
        if (intentoContactoViewModel == null) {
            intentoContactoViewModel = new ArrayList<IntentoContactoViewModel>();
        }
        return this.intentoContactoViewModel;
    }

}
