/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.saydot.SaydotQ;
import java.util.List;
import org.tempuri.OrdenSimpleViewModel;
import org.tempuri.SaydotQService;
import org.tempuri.SaydotQServiceSoap;

/**
 * Clase ejecutable con métodos de pruebas para probar las diferentes
 * funcionalidades en este proyecto.
 *
 * @author amatos
 */
public class Main {

	static TransactionInterfacesPayload payload = new TransactionInterfacesPayload(
			new V_Transaction(), new Workflow_Step(), "{}"
	);
	static String numeroOS = "113540466";

	static Object getEstatusOS() throws Exception {
		SaydotQServiceSoap soap = new SaydotQService().getSaydotQServiceSoap();
		OrdenSimpleViewModel result = soap.getEstatusOS(numeroOS);
		return result;
	}

	static Object testSaydot() throws Exception {
		SaydotQ service = new SaydotQ();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces
				.stream()
				.filter(item -> item.getName().endsWith("consultarClientePorCuenta"))
				.findFirst().get();
		ObjectNode form = JSON.newObjectNode();
		String[] value = new String[]{"8095986131", "6789", "SCRIPTING"};
		for (int i = 0; i < inter.getInterface_field().size(); i++) {
			Interface_Field field = inter.getInterface_field().get(i);
			form.put(field.getName(), value[i]);
		}
		payload.setForm(form.toString());
		return service.callMethod(inter, payload, User.SYSTEM);
	}

	/**
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		InterfaceConfigs.setConfig();
		Object result = null;
//		result = getEstatusOS();
		result = testSaydot();
		System.out.println(JSON.toString(result));
	}

}
