package com.mstn.scripting.interfaces.saydot;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import java.lang.reflect.Method;

/**
 * Interfaz que agrega a Scripting la conexión a SaydotQ desde una transacción
 * en curso.
 *
 * @author amatos
 */
public class SaydotQ extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = SaydotQ.class;
		try {
			SOAP = new SaydotQServiceSoap();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public SaydotQ() {
		super(0, "saydot", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("saydot"));
		return inter;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("saydot").toString();
		super.test();
	}
}
