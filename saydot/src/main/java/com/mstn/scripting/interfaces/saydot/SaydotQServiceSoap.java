/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.saydot;

import com.mstn.scripting.core.Utils;
import org.tempuri.ArrayOfOrdenConsultaByCedula;
import org.tempuri.ArrayOfOrdenEstatusDomain;
import org.tempuri.ArrayOfOrdenEstatusM6;
import org.tempuri.ArrayOfString;
import org.tempuri.EstatusAveria;
import org.tempuri.ManejarEscalamientoResult;
import org.tempuri.OrdenDistritoInfo;
import org.tempuri.OrdenEstatusOMSViewModel;
import org.tempuri.OrdenEstatusSaydotViewModel;
import org.tempuri.OrdenFacilidadFibraInfo;
import org.tempuri.OrdenInfoGeneralViewModel;
import org.tempuri.OrdenSimpleViewModel;
import org.tempuri.OrderDetailsIcarus;
import org.tempuri.SWTrackingDetail;
import org.tempuri.SaydotQService;
import org.tempuri.StatusCode;

/**
 * Clase que crea una instancia del endpoint de SaydotQ.
 *
 * @author amatos
 */
public class SaydotQServiceSoap implements org.tempuri.SaydotQServiceSoap {

	private final org.tempuri.SaydotQServiceSoap base;

	/**
	 *
	 */
	public SaydotQServiceSoap() {
		try {
			this.base = SaydotQService.getInstance();
		} catch (Exception ex) {
			Utils.logException(this.getClass(), "Error invocando endpoint de SAD", ex);
			throw ex;
		}
	}

	@Override
	public OrdenSimpleViewModel getEstatusOS(String numeroOS) {
		return base.getEstatusOS(numeroOS);
	}

	@Override
	public OrdenDistritoInfo getDistritoOrdenCerrada(String numeroOrden) {
		return base.getDistritoOrdenCerrada(numeroOrden);
	}

	@Override
	public OrdenInfoGeneralViewModel getOrdenInfoGeneral(String numeroOrden) {
		return base.getOrdenInfoGeneral(numeroOrden);
	}

	@Override
	public OrdenEstatusSaydotViewModel getOrdenEstatusSaydot(String numeroOrden) {
		return base.getOrdenEstatusSaydot(numeroOrden);
	}

	@Override
	public OrdenEstatusOMSViewModel getOrdenEstatusOMS(String numeroOrden) {
		return base.getOrdenEstatusOMS(numeroOrden);
	}

	@Override
	public ArrayOfOrdenEstatusM6 getOrdenEstatusM6(ArrayOfString numeroOrden) {
		return base.getOrdenEstatusM6(numeroOrden);
	}

	@Override
	public String actualizarFacilidadOrden(String data) {
		return base.actualizarFacilidadOrden(data);
	}

	@Override
	public StatusCode registrarFacilidadOrden(String data) {
		return base.registrarFacilidadOrden(data);
	}

	@Override
	public StatusCode addFacilidadesFibra(OrdenFacilidadFibraInfo data) {
		return base.addFacilidadesFibra(data);
	}

	@Override
	public String registrarActualizacionEstatusOrden(String numeroOrden, String estatus) {
		return base.registrarActualizacionEstatusOrden(numeroOrden, estatus);
	}

	@Override
	public ArrayOfOrdenEstatusDomain getOrdenEstatusDomain(String numeroOrden) {
		return base.getOrdenEstatusDomain(numeroOrden);
	}

	@Override
	public OrderDetailsIcarus getOrdenEstatusIcarus(String numeroOrden) {
		return base.getOrdenEstatusIcarus(numeroOrden);
	}

	@Override
	public ArrayOfOrdenConsultaByCedula getOrdersByCedula(String cedulaIdentidad) {
		return base.getOrdersByCedula(cedulaIdentidad);
	}

	@Override
	public ManejarEscalamientoResult manejarEscalamiento(String noOrden, String numeroCaso, String cola, String estatus, String nota) {
		return base.manejarEscalamiento(noOrden, numeroCaso, cola, estatus, nota);
	}

	@Override
	public SWTrackingDetail getTrackingDetailByTicketID(String ticketID) {
		return base.getTrackingDetailByTicketID(ticketID);
	}

	@Override
	public EstatusAveria getEstatusAveria(String telefono, String caso) {
		return base.getEstatusAveria(telefono, caso);
	}

}
