/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interface200;

import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Interface_Option;
import com.mstn.scripting.core.models.Transaction_Result;
import java.util.Arrays;
import java.util.List;

/**
 * @author amatos
 */
public class Interface200 extends InterfaceConnectorBase {

	private final static int ID = 200;
	private final static String NAME = "prueba200";
	private final static String LABEL = "Prueba200";
	private final static String SERVER_URL = "google.com/api200";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	public Interface200() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Option> getOptions() {
		return Arrays.asList(
				//(id, id_interface, name, id_type, value, color, order_index)
				new Interface_Option(0, ID, "Retorna A", 1, "A", "#aaa", 1),
				new Interface_Option(0, ID, "Retorna Blue", 1, "7BF", "#7BF", 2),
				new Interface_Option(0, ID, "Retorna true", 1, "true", "#000", 0)
		);
	}

	@Override
	public List<Interface_Field> getFields() {
		return Arrays.asList(
				//(id, id_interface, name, label, id_type, order_index, default_value)
				new Interface_Field(1, ID, "title", "Titulo", 1, 1, null),
				new Interface_Field(2, ID, "edad", "Edad", 2, 2, "18"),
				new Interface_Field(3, ID, "fecha", "Fecha", 6, 2, null)
		);
	}

	@Override
	public List<Transaction_Result> getResults() {
		return Arrays.asList(
				//(id_interface, name)
				new Transaction_Result(ID, "interface100_title"),
				new Transaction_Result(ID, "interface100_edad"),
				new Transaction_Result(ID, "interface100_fecha")
		);
	}

	//@Override
	//public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload) throws IOException {
	//	return super.getForm(payload);
	//}
}
