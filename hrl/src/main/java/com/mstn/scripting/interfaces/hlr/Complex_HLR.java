/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.hlr;

import com.claro.hlr.ws.endpoint.HLREndPointServiceLocator;
import com.claro.hlr.ws.service.AuthenticationInfo;
import com.claro.hlr.ws.service.HlrService;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Transaction_Result;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import org.apache.axis.message.MessageElement;

/**
 * Interfaz que agrega a Scripting la conexión a Complex-HLR desde una
 * transacción en curso.
 *
 * @author amatos
 */
public class Complex_HLR extends InterfaceBase {

	public Complex_HLR() {
		super(0, "complexhlr");
		//showBaseLogs = false;
		IGNORED_METHODS.addAll(Arrays.asList(
				"_initOperationDesc1",
				"_initOperationDesc2",
				"_initOperationDesc3",
				"_initOperationDesc4",
				"_initOperationDesc5",
				"_initOperationDesc6",
				"_initOperationDesc7",
				"_initOperationDesc8",
				"createCall",
				"addBindings0",
				"addBindings1"));
	}

	@Override
	protected Object processINSTANCE(Object INSTANCE) {
		HlrService SOAP = null;
		try {
			SOAP = new HLREndPointServiceLocator().getHLREndPointPort();
		} catch (Exception ex) {
			Utils.logException(Complex_HLR.class, "Error instanciando endpoint", ex);
		}
		return SOAP;
	}
	
	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("complex_hlr"));
		return inter;
	}

	@Override
	protected List<Interface_Field> getInterfaceFieldFromClass(Interface inter, String paramName, String parentName, Class _class, boolean prefix) throws Exception {
		if (_class == AuthenticationInfo.class
				|| paramName.endsWith("_centralHlr")
				|| paramName.endsWith("_typeDesc")
				|| paramName.endsWith("___equalsCalc")
				|| paramName.endsWith("___hashCodeCalc")) {
			return Arrays.asList();
		}
		return super.getInterfaceFieldFromClass(inter, paramName, parentName, _class, prefix);
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("subscriber")) {
			field.setLabel("No. de Teléfono");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_PHONE);
		} else if (fieldName.endsWith("apnId")) {
			field.setLabel("ID APN");
		} else if (fieldName.endsWith("fnum")) {
			field.setLabel("FNUM");
		} else if (fieldName.endsWith("pdpId")) {
			field.setLabel("ID PDP");
		} else if (fieldName.endsWith("ip")) {
			field.setLabel("IP");
		} else if (fieldName.endsWith("oick")) {
			field.setLabel("OICK");
		} else if (fieldName.endsWith("pcug")) {
			field.setLabel("PCUG");
		} else if (fieldName.endsWith("stype")) {
			field.setLabel("STYPE");
		} else if (fieldName.endsWith("ic")) {
			field.setLabel("IC");
		} else if (fieldName.endsWith("index")) {
			field.setLabel("INDEX");
		} else if (fieldName.endsWith("imsi")) {
			field.setLabel("IMSI");
		}
		return field;
	}

	@Override
	protected Object getParamValueFromForm(Class paramType, String paramName) throws Exception {
		if (paramName.endsWith("_centralHlr")) {
			String centralHlr = InterfaceConfigs.get("complex_hlr_centralHlr");
			return Integer.parseInt(centralHlr);
		}
		return super.getParamValueFromForm(paramType, paramName);
	}

	@Override
	protected Object getParamObjectFromForm(String paramName, Class paramClass, Object paramInstance, Field paramField) throws Exception {
		if (paramClass == AuthenticationInfo.class) {
			AuthenticationInfo authInfo = new AuthenticationInfo(
					InterfaceConfigs.get("complex_hlr_user"),
					InterfaceConfigs.get("complex_hlr_password")
			);
			return authInfo;
		} else if (paramName.endsWith("_typeDesc")
				|| paramName.endsWith("___equalsCalc")
				|| paramName.endsWith("___hashCodeCalc")) {
			return paramField.get(paramInstance);
		}
		return super.getParamObjectFromForm(paramName, paramClass, paramInstance, paramField);
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromClass(Method method, Interface inter, String paramName, List<Class> inspectedClases) {
		Class _class = inspectedClases.get(inspectedClases.size() - 1);
		if (paramName.endsWith("_typeDesc")
				|| paramName.endsWith("___equalsCalc")
				|| paramName.endsWith("___hashCodeCalc")) {
			return Arrays.asList();
		} else if (_class == MessageElement.class) {
			String nameName = (paramName + "_name");
			String nameValue = (paramName + "_value");
			return Arrays.asList(
					new Transaction_Result(inter.getId(), nameName, nameName),
					new Transaction_Result(inter.getId(), nameValue, nameValue)
			);
		}
		return super.getTransactionResultsFromClass(method, inter, paramName, inspectedClases);
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromResponse(Object value, Interface inter, String paramName) throws Exception {
		if (paramName.endsWith("_typeDesc")
				|| paramName.endsWith("___equalsCalc")
				|| paramName.endsWith("___hashCodeCalc")) {
			return Arrays.asList();
		} else if (value != null && value.getClass() == MessageElement.class) {
			MessageElement node = (MessageElement) value;
			String nameName = (paramName + "_name");
			String nameValue = (paramName + "_value");
			String valueName = node.getName();
			String valueValue = node.getValue();
			return Arrays.asList(
					new Transaction_Result(0, inter.getId(), nameName, nameName, valueName, null),
					new Transaction_Result(0, inter.getId(), nameValue, nameValue, valueValue, null)
			);
		}
		return super.getTransactionResultsFromResponse(value, inter, paramName);
	}

	/**
	 *
	 * @throws Exception
	 */
	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("complex_hlr").toString();
		InterfaceConfigs.get("complex_hlr_user").toString();
		InterfaceConfigs.get("complex_hlr_password").toString();
		Integer.parseInt(InterfaceConfigs.get("complex_hlr_centralHlr"));
		super.test();
	}
}
