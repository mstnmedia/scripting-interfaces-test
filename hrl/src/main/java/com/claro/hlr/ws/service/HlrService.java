/**
 * HlrService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.claro.hlr.ws.service;

public interface HlrService extends java.rmi.Remote {

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCallFwOnBusyCFB(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addPrepaidRoamingVS_RSA_4(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteVoiceMailDCF(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyToCloseFlotaACCESS_IA(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyAccessToInCallsBAIC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyCallerIdCLIP(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyCamelSubsProfileCSP_39(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addAccessToOutCallsBAOC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyInSmsTS21(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addPrivateNumberServiceCLIR(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrSubscriberData getHLRSubscriberData(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addAccessToInCallsBAIC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCamelSubsProfileCSP_39(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution deletePcugToFlotaPCUG(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pcug) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyOutSmsTS22(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addRestrictionToEndCallsOBI(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCallerIdCLIP(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyToOpenFlotaACCESS_OIA(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addPcugToFlotaPCUG(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pcug) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrDictionary getHlrCodeDictionary() throws java.rmi.RemoteException, com.claro.hlr.ws.service.ServiceException;

	public com.claro.hlr.ws.service.OutHlrDictionary getHlrCodeDictionaryDesc(java.lang.String nomeclature) throws java.rmi.RemoteException, com.claro.hlr.ws.service.ServiceException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addFlotaCloseGroup(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String index, java.lang.String ic) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallFwUnconditionalCFU(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCallFwOnNotReachCFNRC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrSubscriberData getHLRSubscriberDataByImsi(com.claro.hlr.ws.service.AuthenticationInfo authenticationInfo, int centralHlr, java.lang.String imsi) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addRoamingPostpagoVS_RSA_2(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addThreeWayCallingMPTY(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addRoamingPostpagoVSD_RSA_0(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addPrepaidWithNotRoamingRSA_1(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addPrepaidRoamingV_RSA_3(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteOcsiTcsiCCH(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallFwOnNotReachCFNRC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addOriginalCamelSubsInfoOCSI(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyApnWithIp(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String apnId, java.lang.String pdpId, java.lang.String ip) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallFwOnBusyCFB(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCallWaitingCAW(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCallFwUnconditionalCFU(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addFlotaCategory(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String oick, java.lang.String stype) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallWaitingCAW(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallFwOnNotReplyCFNRY(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyAccessToOutCallsBAOC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyThreeWayCallingMPTY(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyRoamingOBR(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteApnWithPDPID(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pdpid) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyFixedCellREGSER(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pdpid) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCallFwOnNotReplyCFNRY(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addOutSmsTS22(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addRoamingOBR(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addInSmsTS21(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addApnWithIp(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String apnId, java.lang.String pdpId, java.lang.String ip) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addGprsNAM(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyHgiri(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String imsi) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyOICK(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String oick) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyTICK(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pdpid) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution refreshVlr(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addVoiceMail(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyGprsNAM(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifySTYPE(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String stype) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyPDPCP(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteFromHlr(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addApn(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String apnId, java.lang.String pdpId) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addRestrictionToOriginCallsOBO(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyAccessInternationalCallBOIC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyRestrictionToOriginCallsOBO_0(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyRestrictionToEndCallsOBI(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addAccessInternationalCallBOIC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyRestrictionToOriginCallsOBO_2(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyAccessInternationalCallBOIEXH(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addTerminatingCamelSubsInfoTCSI(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyPrivateNumberServiceCLIR(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;

	public com.claro.hlr.ws.service.OutHlrCommandExecution addAccessInternationalCallBOIEXH(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException;
}
