/**
 * LocationData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class LocationData  implements java.io.Serializable {
    private java.lang.String lmsid;

    private java.lang.String mscNumber;

    private java.lang.String msrn;

    private java.lang.String sgsnNumber;

    private java.lang.String sgsnPurged;

    private java.lang.String vlrAddress;

    private java.lang.String vlrPurged;

    public LocationData() {
    }

    public LocationData(
           java.lang.String lmsid,
           java.lang.String mscNumber,
           java.lang.String msrn,
           java.lang.String sgsnNumber,
           java.lang.String sgsnPurged,
           java.lang.String vlrAddress,
           java.lang.String vlrPurged) {
           this.lmsid = lmsid;
           this.mscNumber = mscNumber;
           this.msrn = msrn;
           this.sgsnNumber = sgsnNumber;
           this.sgsnPurged = sgsnPurged;
           this.vlrAddress = vlrAddress;
           this.vlrPurged = vlrPurged;
    }


    /**
     * Gets the lmsid value for this LocationData.
     * 
     * @return lmsid
     */
    public java.lang.String getLmsid() {
        return lmsid;
    }


    /**
     * Sets the lmsid value for this LocationData.
     * 
     * @param lmsid
     */
    public void setLmsid(java.lang.String lmsid) {
        this.lmsid = lmsid;
    }


    /**
     * Gets the mscNumber value for this LocationData.
     * 
     * @return mscNumber
     */
    public java.lang.String getMscNumber() {
        return mscNumber;
    }


    /**
     * Sets the mscNumber value for this LocationData.
     * 
     * @param mscNumber
     */
    public void setMscNumber(java.lang.String mscNumber) {
        this.mscNumber = mscNumber;
    }


    /**
     * Gets the msrn value for this LocationData.
     * 
     * @return msrn
     */
    public java.lang.String getMsrn() {
        return msrn;
    }


    /**
     * Sets the msrn value for this LocationData.
     * 
     * @param msrn
     */
    public void setMsrn(java.lang.String msrn) {
        this.msrn = msrn;
    }


    /**
     * Gets the sgsnNumber value for this LocationData.
     * 
     * @return sgsnNumber
     */
    public java.lang.String getSgsnNumber() {
        return sgsnNumber;
    }


    /**
     * Sets the sgsnNumber value for this LocationData.
     * 
     * @param sgsnNumber
     */
    public void setSgsnNumber(java.lang.String sgsnNumber) {
        this.sgsnNumber = sgsnNumber;
    }


    /**
     * Gets the sgsnPurged value for this LocationData.
     * 
     * @return sgsnPurged
     */
    public java.lang.String getSgsnPurged() {
        return sgsnPurged;
    }


    /**
     * Sets the sgsnPurged value for this LocationData.
     * 
     * @param sgsnPurged
     */
    public void setSgsnPurged(java.lang.String sgsnPurged) {
        this.sgsnPurged = sgsnPurged;
    }


    /**
     * Gets the vlrAddress value for this LocationData.
     * 
     * @return vlrAddress
     */
    public java.lang.String getVlrAddress() {
        return vlrAddress;
    }


    /**
     * Sets the vlrAddress value for this LocationData.
     * 
     * @param vlrAddress
     */
    public void setVlrAddress(java.lang.String vlrAddress) {
        this.vlrAddress = vlrAddress;
    }


    /**
     * Gets the vlrPurged value for this LocationData.
     * 
     * @return vlrPurged
     */
    public java.lang.String getVlrPurged() {
        return vlrPurged;
    }


    /**
     * Sets the vlrPurged value for this LocationData.
     * 
     * @param vlrPurged
     */
    public void setVlrPurged(java.lang.String vlrPurged) {
        this.vlrPurged = vlrPurged;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LocationData)) return false;
        LocationData other = (LocationData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.lmsid==null && other.getLmsid()==null) || 
             (this.lmsid!=null &&
              this.lmsid.equals(other.getLmsid()))) &&
            ((this.mscNumber==null && other.getMscNumber()==null) || 
             (this.mscNumber!=null &&
              this.mscNumber.equals(other.getMscNumber()))) &&
            ((this.msrn==null && other.getMsrn()==null) || 
             (this.msrn!=null &&
              this.msrn.equals(other.getMsrn()))) &&
            ((this.sgsnNumber==null && other.getSgsnNumber()==null) || 
             (this.sgsnNumber!=null &&
              this.sgsnNumber.equals(other.getSgsnNumber()))) &&
            ((this.sgsnPurged==null && other.getSgsnPurged()==null) || 
             (this.sgsnPurged!=null &&
              this.sgsnPurged.equals(other.getSgsnPurged()))) &&
            ((this.vlrAddress==null && other.getVlrAddress()==null) || 
             (this.vlrAddress!=null &&
              this.vlrAddress.equals(other.getVlrAddress()))) &&
            ((this.vlrPurged==null && other.getVlrPurged()==null) || 
             (this.vlrPurged!=null &&
              this.vlrPurged.equals(other.getVlrPurged())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLmsid() != null) {
            _hashCode += getLmsid().hashCode();
        }
        if (getMscNumber() != null) {
            _hashCode += getMscNumber().hashCode();
        }
        if (getMsrn() != null) {
            _hashCode += getMsrn().hashCode();
        }
        if (getSgsnNumber() != null) {
            _hashCode += getSgsnNumber().hashCode();
        }
        if (getSgsnPurged() != null) {
            _hashCode += getSgsnPurged().hashCode();
        }
        if (getVlrAddress() != null) {
            _hashCode += getVlrAddress().hashCode();
        }
        if (getVlrPurged() != null) {
            _hashCode += getVlrPurged().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LocationData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "locationData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lmsid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lmsid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mscNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mscNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msrn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msrn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sgsnNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sgsnNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sgsnPurged");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sgsnPurged"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrPurged");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrPurged"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
