/**
 * InSubscriberAndAuthenticationInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class InSubscriberAndAuthenticationInfo  implements java.io.Serializable {
    private com.claro.hlr.ws.service.AuthenticationInfo authenticationInfo;

    private int centralHlr;

    private java.lang.String subscriber;

    public InSubscriberAndAuthenticationInfo() {
    }

    public InSubscriberAndAuthenticationInfo(
           com.claro.hlr.ws.service.AuthenticationInfo authenticationInfo,
           int centralHlr,
           java.lang.String subscriber) {
           this.authenticationInfo = authenticationInfo;
           this.centralHlr = centralHlr;
           this.subscriber = subscriber;
    }


    /**
     * Gets the authenticationInfo value for this InSubscriberAndAuthenticationInfo.
     * 
     * @return authenticationInfo
     */
    public com.claro.hlr.ws.service.AuthenticationInfo getAuthenticationInfo() {
        return authenticationInfo;
    }


    /**
     * Sets the authenticationInfo value for this InSubscriberAndAuthenticationInfo.
     * 
     * @param authenticationInfo
     */
    public void setAuthenticationInfo(com.claro.hlr.ws.service.AuthenticationInfo authenticationInfo) {
        this.authenticationInfo = authenticationInfo;
    }


    /**
     * Gets the centralHlr value for this InSubscriberAndAuthenticationInfo.
     * 
     * @return centralHlr
     */
    public int getCentralHlr() {
        return centralHlr;
    }


    /**
     * Sets the centralHlr value for this InSubscriberAndAuthenticationInfo.
     * 
     * @param centralHlr
     */
    public void setCentralHlr(int centralHlr) {
        this.centralHlr = centralHlr;
    }


    /**
     * Gets the subscriber value for this InSubscriberAndAuthenticationInfo.
     * 
     * @return subscriber
     */
    public java.lang.String getSubscriber() {
        return subscriber;
    }


    /**
     * Sets the subscriber value for this InSubscriberAndAuthenticationInfo.
     * 
     * @param subscriber
     */
    public void setSubscriber(java.lang.String subscriber) {
        this.subscriber = subscriber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InSubscriberAndAuthenticationInfo)) return false;
        InSubscriberAndAuthenticationInfo other = (InSubscriberAndAuthenticationInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.authenticationInfo==null && other.getAuthenticationInfo()==null) || 
             (this.authenticationInfo!=null &&
              this.authenticationInfo.equals(other.getAuthenticationInfo()))) &&
            this.centralHlr == other.getCentralHlr() &&
            ((this.subscriber==null && other.getSubscriber()==null) || 
             (this.subscriber!=null &&
              this.subscriber.equals(other.getSubscriber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAuthenticationInfo() != null) {
            _hashCode += getAuthenticationInfo().hashCode();
        }
        _hashCode += getCentralHlr();
        if (getSubscriber() != null) {
            _hashCode += getSubscriber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InSubscriberAndAuthenticationInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authenticationInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AuthenticationInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "authenticationInfo"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("centralHlr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "centralHlr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
