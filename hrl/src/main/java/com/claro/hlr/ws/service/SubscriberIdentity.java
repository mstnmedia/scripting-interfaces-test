/**
 * SubscriberIdentity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class SubscriberIdentity  implements java.io.Serializable {
    private java.lang.String msisdn;

    private java.lang.String imsi;

    private java.lang.String state;

    private java.lang.String authd;

    private java.lang.String nam;

    private java.lang.String imeisv;

    public SubscriberIdentity() {
    }

    public SubscriberIdentity(
           java.lang.String msisdn,
           java.lang.String imsi,
           java.lang.String state,
           java.lang.String authd,
           java.lang.String nam,
           java.lang.String imeisv) {
           this.msisdn = msisdn;
           this.imsi = imsi;
           this.state = state;
           this.authd = authd;
           this.nam = nam;
           this.imeisv = imeisv;
    }


    /**
     * Gets the msisdn value for this SubscriberIdentity.
     * 
     * @return msisdn
     */
    public java.lang.String getMsisdn() {
        return msisdn;
    }


    /**
     * Sets the msisdn value for this SubscriberIdentity.
     * 
     * @param msisdn
     */
    public void setMsisdn(java.lang.String msisdn) {
        this.msisdn = msisdn;
    }


    /**
     * Gets the imsi value for this SubscriberIdentity.
     * 
     * @return imsi
     */
    public java.lang.String getImsi() {
        return imsi;
    }


    /**
     * Sets the imsi value for this SubscriberIdentity.
     * 
     * @param imsi
     */
    public void setImsi(java.lang.String imsi) {
        this.imsi = imsi;
    }


    /**
     * Gets the state value for this SubscriberIdentity.
     * 
     * @return state
     */
    public java.lang.String getState() {
        return state;
    }


    /**
     * Sets the state value for this SubscriberIdentity.
     * 
     * @param state
     */
    public void setState(java.lang.String state) {
        this.state = state;
    }


    /**
     * Gets the authd value for this SubscriberIdentity.
     * 
     * @return authd
     */
    public java.lang.String getAuthd() {
        return authd;
    }


    /**
     * Sets the authd value for this SubscriberIdentity.
     * 
     * @param authd
     */
    public void setAuthd(java.lang.String authd) {
        this.authd = authd;
    }


    /**
     * Gets the nam value for this SubscriberIdentity.
     * 
     * @return nam
     */
    public java.lang.String getNam() {
        return nam;
    }


    /**
     * Sets the nam value for this SubscriberIdentity.
     * 
     * @param nam
     */
    public void setNam(java.lang.String nam) {
        this.nam = nam;
    }


    /**
     * Gets the imeisv value for this SubscriberIdentity.
     * 
     * @return imeisv
     */
    public java.lang.String getImeisv() {
        return imeisv;
    }


    /**
     * Sets the imeisv value for this SubscriberIdentity.
     * 
     * @param imeisv
     */
    public void setImeisv(java.lang.String imeisv) {
        this.imeisv = imeisv;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubscriberIdentity)) return false;
        SubscriberIdentity other = (SubscriberIdentity) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.msisdn==null && other.getMsisdn()==null) || 
             (this.msisdn!=null &&
              this.msisdn.equals(other.getMsisdn()))) &&
            ((this.imsi==null && other.getImsi()==null) || 
             (this.imsi!=null &&
              this.imsi.equals(other.getImsi()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.authd==null && other.getAuthd()==null) || 
             (this.authd!=null &&
              this.authd.equals(other.getAuthd()))) &&
            ((this.nam==null && other.getNam()==null) || 
             (this.nam!=null &&
              this.nam.equals(other.getNam()))) &&
            ((this.imeisv==null && other.getImeisv()==null) || 
             (this.imeisv!=null &&
              this.imeisv.equals(other.getImeisv())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMsisdn() != null) {
            _hashCode += getMsisdn().hashCode();
        }
        if (getImsi() != null) {
            _hashCode += getImsi().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getAuthd() != null) {
            _hashCode += getAuthd().hashCode();
        }
        if (getNam() != null) {
            _hashCode += getNam().hashCode();
        }
        if (getImeisv() != null) {
            _hashCode += getImeisv().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubscriberIdentity.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "subscriberIdentity"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msisdn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msisdn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imsi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "imsi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "authd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nam");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imeisv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "imeisv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
