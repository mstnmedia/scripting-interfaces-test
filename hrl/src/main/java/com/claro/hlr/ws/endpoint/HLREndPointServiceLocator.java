/**
 * HLREndPointServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.claro.hlr.ws.endpoint;

import com.mstn.scripting.core.models.InterfaceConfigs;

public class HLREndPointServiceLocator
		extends org.apache.axis.client.Service
		implements com.claro.hlr.ws.endpoint.HLREndPointService {

	public HLREndPointServiceLocator() {
	}

	public HLREndPointServiceLocator(org.apache.axis.EngineConfiguration config) {
		super(config);
	}

	public HLREndPointServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
		super(wsdlLoc, sName);
	}

	// Use to get a proxy class for HLREndPointPort
	private java.lang.String HLREndPointPort_address = "";

	@Override
	public java.lang.String getHLREndPointPortAddress() {
		return InterfaceConfigs.get("complex_hlr");
	}

	public void setHLREndPointPortEndpointAddress(java.lang.String address) {
		HLREndPointPort_address = address;
	}

	// The WSDD service name defaults to the port name.
	private java.lang.String HLREndPointPortWSDDServiceName = "HLREndPointPort";

	public java.lang.String getHLREndPointPortWSDDServiceName() {
		return HLREndPointPortWSDDServiceName;
	}

	public void setHLREndPointPortWSDDServiceName(java.lang.String name) {
		HLREndPointPortWSDDServiceName = name;
	}

	public com.claro.hlr.ws.service.HlrService getHLREndPointPort() throws javax.xml.rpc.ServiceException {
		java.net.URL endpoint;
		try {
			endpoint = new java.net.URL(getHLREndPointPortAddress());
		} catch (java.net.MalformedURLException e) {
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getHLREndPointPort(endpoint);
	}

	public com.claro.hlr.ws.service.HlrService getHLREndPointPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
		try {
			com.claro.hlr.ws.endpoint.HLREndPointPortBindingStub _stub = new com.claro.hlr.ws.endpoint.HLREndPointPortBindingStub(portAddress, this);
			_stub.setPortName(getHLREndPointPortWSDDServiceName());
			return _stub;
		} catch (org.apache.axis.AxisFault e) {
			return null;
		}
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
		try {
			if (com.claro.hlr.ws.service.HlrService.class.isAssignableFrom(serviceEndpointInterface)) {
				com.claro.hlr.ws.endpoint.HLREndPointPortBindingStub _stub
						= new com.claro.hlr.ws.endpoint.HLREndPointPortBindingStub(
								new java.net.URL(getHLREndPointPortAddress()), this
						);
				_stub.setPortName(getHLREndPointPortWSDDServiceName());
				return _stub;
			}
		} catch (java.lang.Throwable t) {
			throw new javax.xml.rpc.ServiceException(t);
		}
		throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
		if (portName == null) {
			return getPort(serviceEndpointInterface);
		}
		java.lang.String inputPortName = portName.getLocalPart();
		if ("HLREndPointPort".equals(inputPortName)) {
			return getHLREndPointPort();
		} else {
			java.rmi.Remote _stub = getPort(serviceEndpointInterface);
			((org.apache.axis.client.Stub) _stub).setPortName(portName);
			return _stub;
		}
	}

	public javax.xml.namespace.QName getServiceName() {
		return new javax.xml.namespace.QName("http://endpoint.ws.hlr.claro.com/", "HLREndPointService");
	}

	private java.util.HashSet ports = null;

	public java.util.Iterator getPorts() {
		if (ports == null) {
			ports = new java.util.HashSet();
			ports.add(new javax.xml.namespace.QName("http://endpoint.ws.hlr.claro.com/", "HLREndPointPort"));
		}
		return ports.iterator();
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {

		if ("HLREndPointPort".equals(portName)) {
			setHLREndPointPortEndpointAddress(address);
		} else { // Unknown Port Name
			throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
		}
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
		setEndpointAddress(portName.getLocalPart(), address);
	}

}
