/**
 * ModifyApnWithIpResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class ModifyApnWithIpResponse  implements java.io.Serializable {
    private com.claro.hlr.ws.service.OutHlrCommandExecution outHlrCommandExecution;

    public ModifyApnWithIpResponse() {
    }

    public ModifyApnWithIpResponse(
           com.claro.hlr.ws.service.OutHlrCommandExecution outHlrCommandExecution) {
           this.outHlrCommandExecution = outHlrCommandExecution;
    }


    /**
     * Gets the outHlrCommandExecution value for this ModifyApnWithIpResponse.
     * 
     * @return outHlrCommandExecution
     */
    public com.claro.hlr.ws.service.OutHlrCommandExecution getOutHlrCommandExecution() {
        return outHlrCommandExecution;
    }


    /**
     * Sets the outHlrCommandExecution value for this ModifyApnWithIpResponse.
     * 
     * @param outHlrCommandExecution
     */
    public void setOutHlrCommandExecution(com.claro.hlr.ws.service.OutHlrCommandExecution outHlrCommandExecution) {
        this.outHlrCommandExecution = outHlrCommandExecution;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ModifyApnWithIpResponse)) return false;
        ModifyApnWithIpResponse other = (ModifyApnWithIpResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.outHlrCommandExecution==null && other.getOutHlrCommandExecution()==null) || 
             (this.outHlrCommandExecution!=null &&
              this.outHlrCommandExecution.equals(other.getOutHlrCommandExecution())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOutHlrCommandExecution() != null) {
            _hashCode += getOutHlrCommandExecution().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ModifyApnWithIpResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyApnWithIpResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outHlrCommandExecution");
        elemField.setXmlName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
