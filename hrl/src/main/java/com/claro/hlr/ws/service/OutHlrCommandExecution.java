/**
 * OutHlrCommandExecution.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class OutHlrCommandExecution  implements java.io.Serializable {
    private com.claro.hlr.ws.service.ErrorDetail errorDetail;

    private boolean success;

    private java.lang.String transactionId;

    public OutHlrCommandExecution() {
    }

    public OutHlrCommandExecution(
           com.claro.hlr.ws.service.ErrorDetail errorDetail,
           boolean success,
           java.lang.String transactionId) {
           this.errorDetail = errorDetail;
           this.success = success;
           this.transactionId = transactionId;
    }


    /**
     * Gets the errorDetail value for this OutHlrCommandExecution.
     * 
     * @return errorDetail
     */
    public com.claro.hlr.ws.service.ErrorDetail getErrorDetail() {
        return errorDetail;
    }


    /**
     * Sets the errorDetail value for this OutHlrCommandExecution.
     * 
     * @param errorDetail
     */
    public void setErrorDetail(com.claro.hlr.ws.service.ErrorDetail errorDetail) {
        this.errorDetail = errorDetail;
    }


    /**
     * Gets the success value for this OutHlrCommandExecution.
     * 
     * @return success
     */
    public boolean isSuccess() {
        return success;
    }


    /**
     * Sets the success value for this OutHlrCommandExecution.
     * 
     * @param success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }


    /**
     * Gets the transactionId value for this OutHlrCommandExecution.
     * 
     * @return transactionId
     */
    public java.lang.String getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this OutHlrCommandExecution.
     * 
     * @param transactionId
     */
    public void setTransactionId(java.lang.String transactionId) {
        this.transactionId = transactionId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OutHlrCommandExecution)) return false;
        OutHlrCommandExecution other = (OutHlrCommandExecution) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.errorDetail==null && other.getErrorDetail()==null) || 
             (this.errorDetail!=null &&
              this.errorDetail.equals(other.getErrorDetail()))) &&
            this.success == other.isSuccess() &&
            ((this.transactionId==null && other.getTransactionId()==null) || 
             (this.transactionId!=null &&
              this.transactionId.equals(other.getTransactionId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErrorDetail() != null) {
            _hashCode += getErrorDetail().hashCode();
        }
        _hashCode += (isSuccess() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getTransactionId() != null) {
            _hashCode += getTransactionId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OutHlrCommandExecution.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "errorDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "errorDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("success");
        elemField.setXmlName(new javax.xml.namespace.QName("", "success"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	@Override
	public String toString() {
		return "OutHlrCommandExecution [errorDetail=" + errorDetail + ", success=" + success + ", transactionId="
				+ transactionId + "]";
	}

    
}
