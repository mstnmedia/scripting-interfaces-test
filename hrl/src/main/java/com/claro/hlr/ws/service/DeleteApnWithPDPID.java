/**
 * DeleteApnWithPDPID.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class DeleteApnWithPDPID  implements java.io.Serializable {
    private com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo;

    private java.lang.String pdpid;

    public DeleteApnWithPDPID() {
    }

    public DeleteApnWithPDPID(
           com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo,
           java.lang.String pdpid) {
           this.inSubscriberAndAuthenticationInfo = inSubscriberAndAuthenticationInfo;
           this.pdpid = pdpid;
    }


    /**
     * Gets the inSubscriberAndAuthenticationInfo value for this DeleteApnWithPDPID.
     * 
     * @return inSubscriberAndAuthenticationInfo
     */
    public com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo getInSubscriberAndAuthenticationInfo() {
        return inSubscriberAndAuthenticationInfo;
    }


    /**
     * Sets the inSubscriberAndAuthenticationInfo value for this DeleteApnWithPDPID.
     * 
     * @param inSubscriberAndAuthenticationInfo
     */
    public void setInSubscriberAndAuthenticationInfo(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) {
        this.inSubscriberAndAuthenticationInfo = inSubscriberAndAuthenticationInfo;
    }


    /**
     * Gets the pdpid value for this DeleteApnWithPDPID.
     * 
     * @return pdpid
     */
    public java.lang.String getPdpid() {
        return pdpid;
    }


    /**
     * Sets the pdpid value for this DeleteApnWithPDPID.
     * 
     * @param pdpid
     */
    public void setPdpid(java.lang.String pdpid) {
        this.pdpid = pdpid;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeleteApnWithPDPID)) return false;
        DeleteApnWithPDPID other = (DeleteApnWithPDPID) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.inSubscriberAndAuthenticationInfo==null && other.getInSubscriberAndAuthenticationInfo()==null) || 
             (this.inSubscriberAndAuthenticationInfo!=null &&
              this.inSubscriberAndAuthenticationInfo.equals(other.getInSubscriberAndAuthenticationInfo()))) &&
            ((this.pdpid==null && other.getPdpid()==null) || 
             (this.pdpid!=null &&
              this.pdpid.equals(other.getPdpid())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInSubscriberAndAuthenticationInfo() != null) {
            _hashCode += getInSubscriberAndAuthenticationInfo().hashCode();
        }
        if (getPdpid() != null) {
            _hashCode += getPdpid().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeleteApnWithPDPID.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteApnWithPDPID"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inSubscriberAndAuthenticationInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pdpid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pdpid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
