package com.claro.hlr.ws.service;

public class HlrServiceProxy implements com.claro.hlr.ws.service.HlrService {
  private String _endpoint = null;
  private com.claro.hlr.ws.service.HlrService hlrService = null;
  
  public HlrServiceProxy() {
    _initHlrServiceProxy();
  }
  
  public HlrServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initHlrServiceProxy();
  }
  
  private void _initHlrServiceProxy() {
    try {
      hlrService = (new com.claro.hlr.ws.endpoint.HLREndPointServiceLocator()).getHLREndPointPort();
      if (hlrService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)hlrService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)hlrService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (hlrService != null)
      ((javax.xml.rpc.Stub)hlrService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.claro.hlr.ws.service.HlrService getHlrService() {
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService;
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addCallFwOnBusyCFB(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addCallFwOnBusyCFB(inSubscriberAndAuthenticationInfo, fnum);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addPrepaidRoamingVS_RSA_4(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addPrepaidRoamingVS_RSA_4(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution deleteVoiceMailDCF(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.deleteVoiceMailDCF(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyToCloseFlotaACCESS_IA(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyToCloseFlotaACCESS_IA(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyAccessToInCallsBAIC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyAccessToInCallsBAIC(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyCallerIdCLIP(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyCallerIdCLIP(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyCamelSubsProfileCSP_39(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyCamelSubsProfileCSP_39(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addAccessToOutCallsBAOC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addAccessToOutCallsBAOC(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyInSmsTS21(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyInSmsTS21(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addPrivateNumberServiceCLIR(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addPrivateNumberServiceCLIR(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrSubscriberData getHLRSubscriberData(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.getHLRSubscriberData(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addAccessToInCallsBAIC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addAccessToInCallsBAIC(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addCamelSubsProfileCSP_39(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addCamelSubsProfileCSP_39(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution deletePcugToFlotaPCUG(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pcug) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.deletePcugToFlotaPCUG(inSubscriberAndAuthenticationInfo, pcug);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyOutSmsTS22(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyOutSmsTS22(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addRestrictionToEndCallsOBI(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addRestrictionToEndCallsOBI(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addCallerIdCLIP(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addCallerIdCLIP(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyToOpenFlotaACCESS_OIA(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyToOpenFlotaACCESS_OIA(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addPcugToFlotaPCUG(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pcug) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addPcugToFlotaPCUG(inSubscriberAndAuthenticationInfo, pcug);
  }
  
  public com.claro.hlr.ws.service.OutHlrDictionary getHlrCodeDictionary() throws java.rmi.RemoteException, com.claro.hlr.ws.service.ServiceException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.getHlrCodeDictionary();
  }
  
  public com.claro.hlr.ws.service.OutHlrDictionary getHlrCodeDictionaryDesc(java.lang.String nomeclature) throws java.rmi.RemoteException, com.claro.hlr.ws.service.ServiceException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.getHlrCodeDictionaryDesc(nomeclature);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addFlotaCloseGroup(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String index, java.lang.String ic) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addFlotaCloseGroup(inSubscriberAndAuthenticationInfo, index, ic);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallFwUnconditionalCFU(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.deleteCallFwUnconditionalCFU(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addCallFwOnNotReachCFNRC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addCallFwOnNotReachCFNRC(inSubscriberAndAuthenticationInfo, fnum);
  }
  
  public com.claro.hlr.ws.service.OutHlrSubscriberData getHLRSubscriberDataByImsi(com.claro.hlr.ws.service.AuthenticationInfo authenticationInfo, int centralHlr, java.lang.String imsi) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.getHLRSubscriberDataByImsi(authenticationInfo, centralHlr, imsi);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addRoamingPostpagoVS_RSA_2(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addRoamingPostpagoVS_RSA_2(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addThreeWayCallingMPTY(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addThreeWayCallingMPTY(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addRoamingPostpagoVSD_RSA_0(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addRoamingPostpagoVSD_RSA_0(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addPrepaidWithNotRoamingRSA_1(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addPrepaidWithNotRoamingRSA_1(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addPrepaidRoamingV_RSA_3(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addPrepaidRoamingV_RSA_3(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution deleteOcsiTcsiCCH(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.deleteOcsiTcsiCCH(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallFwOnNotReachCFNRC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.deleteCallFwOnNotReachCFNRC(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addOriginalCamelSubsInfoOCSI(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addOriginalCamelSubsInfoOCSI(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyApnWithIp(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String apnId, java.lang.String pdpId, java.lang.String ip) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyApnWithIp(inSubscriberAndAuthenticationInfo, apnId, pdpId, ip);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallFwOnBusyCFB(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.deleteCallFwOnBusyCFB(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addCallWaitingCAW(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addCallWaitingCAW(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addCallFwUnconditionalCFU(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addCallFwUnconditionalCFU(inSubscriberAndAuthenticationInfo, fnum);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addFlotaCategory(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String oick, java.lang.String stype) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addFlotaCategory(inSubscriberAndAuthenticationInfo, oick, stype);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallWaitingCAW(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.deleteCallWaitingCAW(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallFwOnNotReplyCFNRY(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.deleteCallFwOnNotReplyCFNRY(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyAccessToOutCallsBAOC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyAccessToOutCallsBAOC(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyThreeWayCallingMPTY(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyThreeWayCallingMPTY(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyRoamingOBR(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyRoamingOBR(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution deleteApnWithPDPID(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pdpid) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.deleteApnWithPDPID(inSubscriberAndAuthenticationInfo, pdpid);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyFixedCellREGSER(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pdpid) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyFixedCellREGSER(inSubscriberAndAuthenticationInfo, pdpid);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addCallFwOnNotReplyCFNRY(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addCallFwOnNotReplyCFNRY(inSubscriberAndAuthenticationInfo, fnum);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addOutSmsTS22(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addOutSmsTS22(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addRoamingOBR(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addRoamingOBR(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addInSmsTS21(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addInSmsTS21(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addApnWithIp(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String apnId, java.lang.String pdpId, java.lang.String ip) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addApnWithIp(inSubscriberAndAuthenticationInfo, apnId, pdpId, ip);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addGprsNAM(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addGprsNAM(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyHgiri(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String imsi) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyHgiri(inSubscriberAndAuthenticationInfo, imsi);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyOICK(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String oick) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyOICK(inSubscriberAndAuthenticationInfo, oick);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyTICK(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pdpid) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyTICK(inSubscriberAndAuthenticationInfo, pdpid);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution refreshVlr(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.refreshVlr(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addVoiceMail(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addVoiceMail(inSubscriberAndAuthenticationInfo, fnum);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyGprsNAM(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyGprsNAM(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifySTYPE(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String stype) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifySTYPE(inSubscriberAndAuthenticationInfo, stype);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyPDPCP(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyPDPCP(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution deleteFromHlr(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.deleteFromHlr(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addApn(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String apnId, java.lang.String pdpId) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addApn(inSubscriberAndAuthenticationInfo, apnId, pdpId);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addRestrictionToOriginCallsOBO(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addRestrictionToOriginCallsOBO(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyAccessInternationalCallBOIC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyAccessInternationalCallBOIC(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyRestrictionToOriginCallsOBO_0(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyRestrictionToOriginCallsOBO_0(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyRestrictionToEndCallsOBI(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyRestrictionToEndCallsOBI(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addAccessInternationalCallBOIC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addAccessInternationalCallBOIC(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyRestrictionToOriginCallsOBO_2(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyRestrictionToOriginCallsOBO_2(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyAccessInternationalCallBOIEXH(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyAccessInternationalCallBOIEXH(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addTerminatingCamelSubsInfoTCSI(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addTerminatingCamelSubsInfoTCSI(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution modifyPrivateNumberServiceCLIR(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.modifyPrivateNumberServiceCLIR(inSubscriberAndAuthenticationInfo);
  }
  
  public com.claro.hlr.ws.service.OutHlrCommandExecution addAccessInternationalCallBOIEXH(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException{
    if (hlrService == null)
      _initHlrServiceProxy();
    return hlrService.addAccessInternationalCallBOIEXH(inSubscriberAndAuthenticationInfo);
  }
  
  
}