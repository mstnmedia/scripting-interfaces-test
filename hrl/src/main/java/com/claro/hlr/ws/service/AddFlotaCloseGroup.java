/**
 * AddFlotaCloseGroup.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class AddFlotaCloseGroup  implements java.io.Serializable {
    private com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo;

    private java.lang.String index;

    private java.lang.String ic;

    public AddFlotaCloseGroup() {
    }

    public AddFlotaCloseGroup(
           com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo,
           java.lang.String index,
           java.lang.String ic) {
           this.inSubscriberAndAuthenticationInfo = inSubscriberAndAuthenticationInfo;
           this.index = index;
           this.ic = ic;
    }


    /**
     * Gets the inSubscriberAndAuthenticationInfo value for this AddFlotaCloseGroup.
     * 
     * @return inSubscriberAndAuthenticationInfo
     */
    public com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo getInSubscriberAndAuthenticationInfo() {
        return inSubscriberAndAuthenticationInfo;
    }


    /**
     * Sets the inSubscriberAndAuthenticationInfo value for this AddFlotaCloseGroup.
     * 
     * @param inSubscriberAndAuthenticationInfo
     */
    public void setInSubscriberAndAuthenticationInfo(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) {
        this.inSubscriberAndAuthenticationInfo = inSubscriberAndAuthenticationInfo;
    }


    /**
     * Gets the index value for this AddFlotaCloseGroup.
     * 
     * @return index
     */
    public java.lang.String getIndex() {
        return index;
    }


    /**
     * Sets the index value for this AddFlotaCloseGroup.
     * 
     * @param index
     */
    public void setIndex(java.lang.String index) {
        this.index = index;
    }


    /**
     * Gets the ic value for this AddFlotaCloseGroup.
     * 
     * @return ic
     */
    public java.lang.String getIc() {
        return ic;
    }


    /**
     * Sets the ic value for this AddFlotaCloseGroup.
     * 
     * @param ic
     */
    public void setIc(java.lang.String ic) {
        this.ic = ic;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddFlotaCloseGroup)) return false;
        AddFlotaCloseGroup other = (AddFlotaCloseGroup) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.inSubscriberAndAuthenticationInfo==null && other.getInSubscriberAndAuthenticationInfo()==null) || 
             (this.inSubscriberAndAuthenticationInfo!=null &&
              this.inSubscriberAndAuthenticationInfo.equals(other.getInSubscriberAndAuthenticationInfo()))) &&
            ((this.index==null && other.getIndex()==null) || 
             (this.index!=null &&
              this.index.equals(other.getIndex()))) &&
            ((this.ic==null && other.getIc()==null) || 
             (this.ic!=null &&
              this.ic.equals(other.getIc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInSubscriberAndAuthenticationInfo() != null) {
            _hashCode += getInSubscriberAndAuthenticationInfo().hashCode();
        }
        if (getIndex() != null) {
            _hashCode += getIndex().hashCode();
        }
        if (getIc() != null) {
            _hashCode += getIc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddFlotaCloseGroup.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addFlotaCloseGroup"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inSubscriberAndAuthenticationInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("index");
        elemField.setXmlName(new javax.xml.namespace.QName("", "index"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
