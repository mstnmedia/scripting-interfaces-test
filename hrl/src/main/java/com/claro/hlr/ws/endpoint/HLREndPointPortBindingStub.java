/**
 * HLREndPointPortBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.claro.hlr.ws.endpoint;

public class HLREndPointPortBindingStub
		extends org.apache.axis.client.Stub
		implements com.claro.hlr.ws.service.HlrService {

	private java.util.Vector cachedSerClasses = new java.util.Vector();
	private java.util.Vector cachedSerQNames = new java.util.Vector();
	private java.util.Vector cachedSerFactories = new java.util.Vector();
	private java.util.Vector cachedDeserFactories = new java.util.Vector();

	static org.apache.axis.description.OperationDesc[] _operations;

	static {
		_operations = new org.apache.axis.description.OperationDesc[71];
		_initOperationDesc1();
		_initOperationDesc2();
		_initOperationDesc3();
		_initOperationDesc4();
		_initOperationDesc5();
		_initOperationDesc6();
		_initOperationDesc7();
		_initOperationDesc8();
	}

	private static void _initOperationDesc1() {
		org.apache.axis.description.OperationDesc oper;
		org.apache.axis.description.ParameterDesc param;
		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addCallFwOnBusyCFB");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "fnum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[0] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addPrepaidRoamingVS_RSA_4");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[1] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("deleteVoiceMailDCF");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[2] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyToCloseFlotaACCESS_IA");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[3] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyAccessToInCallsBAIC");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[4] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyCallerIdCLIP");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[5] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyCamelSubsProfileCSP_39");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[6] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addAccessToOutCallsBAOC");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[7] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyInSmsTS21");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[8] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addPrivateNumberServiceCLIR");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[9] = oper;

	}

	private static void _initOperationDesc2() {
		org.apache.axis.description.OperationDesc oper;
		org.apache.axis.description.ParameterDesc param;
		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("getHLRSubscriberData");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrSubscriberData"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrSubscriberData.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "OutHlrSubscriberData"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[10] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addAccessToInCallsBAIC");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[11] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addCamelSubsProfileCSP_39");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[12] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("deletePcugToFlotaPCUG");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "pcug"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[13] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyOutSmsTS22");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[14] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addRestrictionToEndCallsOBI");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[15] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addCallerIdCLIP");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[16] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyToOpenFlotaACCESS_OIA");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[17] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addPcugToFlotaPCUG");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "pcug"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[18] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("getHlrCodeDictionary");
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrDictionary"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrDictionary.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "HlrDictionary"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		oper.addFault(new org.apache.axis.description.FaultDesc(
				new javax.xml.namespace.QName("http://exceptions.service.ws.hlr.claro.com/", "ServiceException"),
				"com.claro.hlr.ws.service.ServiceException",
				new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "ServiceException"),
				true
		));
		_operations[19] = oper;

	}

	private static void _initOperationDesc3() {
		org.apache.axis.description.OperationDesc oper;
		org.apache.axis.description.ParameterDesc param;
		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("getHlrCodeDictionaryDesc");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "nomeclature"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrDictionary"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrDictionary.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "HlrDictionary"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		oper.addFault(new org.apache.axis.description.FaultDesc(
				new javax.xml.namespace.QName("http://exceptions.service.ws.hlr.claro.com/", "ServiceException"),
				"com.claro.hlr.ws.service.ServiceException",
				new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "ServiceException"),
				true
		));
		_operations[20] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addFlotaCloseGroup");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "index"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ic"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[21] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("deleteCallFwUnconditionalCFU");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[22] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addCallFwOnNotReachCFNRC");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "fnum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[23] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("getHLRSubscriberDataByImsi");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "AuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "authenticationInfo"), com.claro.hlr.ws.service.AuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "centralHlr"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "imsi"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrSubscriberData"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrSubscriberData.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "OutHlrSubscriberData"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[24] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addRoamingPostpagoVS_RSA_2");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[25] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addThreeWayCallingMPTY");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[26] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addRoamingPostpagoVSD_RSA_0");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[27] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addPrepaidWithNotRoamingRSA_1");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[28] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addPrepaidRoamingV_RSA_3");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[29] = oper;

	}

	private static void _initOperationDesc4() {
		org.apache.axis.description.OperationDesc oper;
		org.apache.axis.description.ParameterDesc param;
		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("deleteOcsiTcsiCCH");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[30] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("deleteCallFwOnNotReachCFNRC");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[31] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addOriginalCamelSubsInfoOCSI");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[32] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyApnWithIp");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "apnId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "pdpId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ip"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[33] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("deleteCallFwOnBusyCFB");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[34] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addCallWaitingCAW");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[35] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addCallFwUnconditionalCFU");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "fnum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[36] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addFlotaCategory");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "oick"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "stype"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[37] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("deleteCallWaitingCAW");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[38] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("deleteCallFwOnNotReplyCFNRY");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[39] = oper;

	}

	private static void _initOperationDesc5() {
		org.apache.axis.description.OperationDesc oper;
		org.apache.axis.description.ParameterDesc param;
		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyAccessToOutCallsBAOC");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[40] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyThreeWayCallingMPTY");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[41] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyRoamingOBR");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[42] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("deleteApnWithPDPID");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "pdpid"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[43] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyFixedCellREGSER");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "pdpid"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[44] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addCallFwOnNotReplyCFNRY");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "fnum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[45] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addOutSmsTS22");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[46] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addRoamingOBR");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[47] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addInSmsTS21");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[48] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addApnWithIp");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "apnId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "pdpId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ip"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[49] = oper;

	}

	private static void _initOperationDesc6() {
		org.apache.axis.description.OperationDesc oper;
		org.apache.axis.description.ParameterDesc param;
		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addGprsNAM");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[50] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyHgiri");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "imsi"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[51] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyOICK");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "oick"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[52] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyTICK");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "pdpid"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[53] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("refreshVlr");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[54] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addVoiceMail");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "fnum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[55] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyGprsNAM");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[56] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifySTYPE");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "stype"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[57] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyPDPCP");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[58] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("deleteFromHlr");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[59] = oper;

	}

	private static void _initOperationDesc7() {
		org.apache.axis.description.OperationDesc oper;
		org.apache.axis.description.ParameterDesc param;
		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addApn");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "apnId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "pdpId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[60] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addRestrictionToOriginCallsOBO");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[61] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyAccessInternationalCallBOIC");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[62] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyRestrictionToOriginCallsOBO_0");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[63] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyRestrictionToEndCallsOBI");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[64] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addAccessInternationalCallBOIC");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[65] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyRestrictionToOriginCallsOBO_2");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[66] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyAccessInternationalCallBOIEXH");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[67] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addTerminatingCamelSubsInfoTCSI");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[68] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("modifyPrivateNumberServiceCLIR");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[69] = oper;

	}

	private static void _initOperationDesc8() {
		org.apache.axis.description.OperationDesc oper;
		org.apache.axis.description.ParameterDesc param;
		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("addAccessInternationalCallBOIEXH");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"), com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class, false, false);
		param.setOmittable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution"));
		oper.setReturnClass(com.claro.hlr.ws.service.OutHlrCommandExecution.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "OutHlrCommandExecution"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[70] = oper;

	}

	public HLREndPointPortBindingStub() throws org.apache.axis.AxisFault {
		this(null);
	}

	public HLREndPointPortBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
		this(service);
		super.cachedEndpoint = endpointURL;
	}

	public HLREndPointPortBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
		if (service == null) {
			super.service = new org.apache.axis.client.Service();
		} else {
			super.service = service;
		}
		((org.apache.axis.client.Service) super.service).setTypeMappingVersion("1.2");
		java.lang.Class cls;
		javax.xml.namespace.QName qName;
		javax.xml.namespace.QName qName2;
		java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
		java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
		java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
		java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
		java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
		java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
		java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
		java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
		java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
		java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
		addBindings0();
		addBindings1();
	}

	private void addBindings0() {
		java.lang.Class cls;
		javax.xml.namespace.QName qName;
		javax.xml.namespace.QName qName2;
		java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
		java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
		java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
		java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
		java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
		java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
		java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
		java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
		java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
		java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", ">>outHlrSubscriberData>SupplementaryServiceData>entry");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.OutHlrSubscriberDataSupplementaryServiceDataEntry.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", ">outHlrDictionary>ListItems");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.OutHlrDictionaryListItems.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", ">outHlrSubscriberData>PermanentSubscriberData");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.OutHlrSubscriberDataPermanentSubscriberData.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", ">outHlrSubscriberData>SupplementaryServiceData");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.OutHlrSubscriberDataSupplementaryServiceDataEntry[].class;
		cachedSerClasses.add(cls);
		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", ">>outHlrSubscriberData>SupplementaryServiceData>entry");
		qName2 = new javax.xml.namespace.QName("", "entry");
		cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
		cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addAccessInternationalCallBOIC");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddAccessInternationalCallBOIC.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addAccessInternationalCallBOICResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddAccessInternationalCallBOICResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addAccessInternationalCallBOIEXH");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddAccessInternationalCallBOIEXH.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addAccessInternationalCallBOIEXHResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddAccessInternationalCallBOIEXHResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addAccessToInCallsBAIC");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddAccessToInCallsBAIC.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addAccessToInCallsBAICResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddAccessToInCallsBAICResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addAccessToOutCallsBAOC");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddAccessToOutCallsBAOC.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addAccessToOutCallsBAOCResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddAccessToOutCallsBAOCResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addApn");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddApn.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addApnResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddApnResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addApnWithIp");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddApnWithIp.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addApnWithIpResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddApnWithIpResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallerIdCLIP");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCallerIdCLIP.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallerIdCLIPResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCallerIdCLIPResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallFwOnBusyCFB");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCallFwOnBusyCFB.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallFwOnBusyCFBResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCallFwOnBusyCFBResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallFwOnNotReachCFNRC");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCallFwOnNotReachCFNRC.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallFwOnNotReachCFNRCResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCallFwOnNotReachCFNRCResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallFwOnNotReplyCFNRY");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCallFwOnNotReplyCFNRY.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallFwOnNotReplyCFNRYResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCallFwOnNotReplyCFNRYResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallFwUnconditionalCFU");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCallFwUnconditionalCFU.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallFwUnconditionalCFUResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCallFwUnconditionalCFUResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallWaitingCAW");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCallWaitingCAW.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallWaitingCAWResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCallWaitingCAWResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCamelSubsProfileCSP_39");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCamelSubsProfileCSP_39.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCamelSubsProfileCSP_39Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddCamelSubsProfileCSP_39Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addFlotaCategory");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddFlotaCategory.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addFlotaCategoryResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddFlotaCategoryResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addFlotaCloseGroup");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddFlotaCloseGroup.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addFlotaCloseGroupResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddFlotaCloseGroupResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addGprsNAM");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddGprsNAM.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addGprsNAMResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddGprsNAMResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addInSmsTS21");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddInSmsTS21.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addInSmsTS21Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddInSmsTS21Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addOriginalCamelSubsInfoOCSI");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddOriginalCamelSubsInfoOCSI.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addOriginalCamelSubsInfoOCSIResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddOriginalCamelSubsInfoOCSIResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addOutSmsTS22");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddOutSmsTS22.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addOutSmsTS22Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddOutSmsTS22Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPcugToFlotaPCUG");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddPcugToFlotaPCUG.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPcugToFlotaPCUGResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddPcugToFlotaPCUGResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPrepaidRoamingV_RSA_3");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddPrepaidRoamingV_RSA_3.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPrepaidRoamingV_RSA_3Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddPrepaidRoamingV_RSA_3Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPrepaidRoamingVS_RSA_4");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddPrepaidRoamingVS_RSA_4.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPrepaidRoamingVS_RSA_4Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddPrepaidRoamingVS_RSA_4Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPrepaidWithNotRoamingRSA_1");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddPrepaidWithNotRoamingRSA_1.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPrepaidWithNotRoamingRSA_1Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddPrepaidWithNotRoamingRSA_1Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPrivateNumberServiceCLIR");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddPrivateNumberServiceCLIR.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPrivateNumberServiceCLIRResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddPrivateNumberServiceCLIRResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRestrictionToEndCallsOBI");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddRestrictionToEndCallsOBI.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRestrictionToEndCallsOBIResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddRestrictionToEndCallsOBIResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRestrictionToOriginCallsOBO");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddRestrictionToOriginCallsOBO.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRestrictionToOriginCallsOBOResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddRestrictionToOriginCallsOBOResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRoamingOBR");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddRoamingOBR.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRoamingOBRResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddRoamingOBRResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRoamingPostpagoVS_RSA_2");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddRoamingPostpagoVS_RSA_2.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRoamingPostpagoVS_RSA_2Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddRoamingPostpagoVS_RSA_2Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRoamingPostpagoVSD_RSA_0");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddRoamingPostpagoVSD_RSA_0.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRoamingPostpagoVSD_RSA_0Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddRoamingPostpagoVSD_RSA_0Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addTerminatingCamelSubsInfoTCSI");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddTerminatingCamelSubsInfoTCSI.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addTerminatingCamelSubsInfoTCSIResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddTerminatingCamelSubsInfoTCSIResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addThreeWayCallingMPTY");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddThreeWayCallingMPTY.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addThreeWayCallingMPTYResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddThreeWayCallingMPTYResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addVoiceMail");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddVoiceMail.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addVoiceMailResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AddVoiceMailResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "apnRow");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ApnRow.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "authenticationInfo");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.AuthenticationInfo.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "bsg");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.Bsg.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "bsgList");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.Bsg[].class;
		cachedSerClasses.add(cls);
		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "bsg");
		qName2 = new javax.xml.namespace.QName("", "bsgRow");
		cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
		cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteApnWithPDPID");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteApnWithPDPID.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteApnWithPDPIDResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteApnWithPDPIDResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallFwOnBusyCFB");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteCallFwOnBusyCFB.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallFwOnBusyCFBResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteCallFwOnBusyCFBResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallFwOnNotReachCFNRC");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteCallFwOnNotReachCFNRC.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallFwOnNotReachCFNRCResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteCallFwOnNotReachCFNRCResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallFwOnNotReplyCFNRY");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteCallFwOnNotReplyCFNRY.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallFwOnNotReplyCFNRYResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteCallFwOnNotReplyCFNRYResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallFwUnconditionalCFU");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteCallFwUnconditionalCFU.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallFwUnconditionalCFUResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteCallFwUnconditionalCFUResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallWaitingCAW");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteCallWaitingCAW.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallWaitingCAWResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteCallWaitingCAWResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteFromHlr");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteFromHlr.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteFromHlrResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteFromHlrResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteOcsiTcsiCCH");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteOcsiTcsiCCH.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteOcsiTcsiCCHResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteOcsiTcsiCCHResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deletePcugToFlotaPCUG");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeletePcugToFlotaPCUG.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deletePcugToFlotaPCUGResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeletePcugToFlotaPCUGResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteVoiceMailDCF");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteVoiceMailDCF.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteVoiceMailDCFResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.DeleteVoiceMailDCFResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "errorDetail");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ErrorDetail.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHlrCodeDictionary");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.GetHlrCodeDictionary.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHlrCodeDictionaryDesc");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.GetHlrCodeDictionaryDesc.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHlrCodeDictionaryDescResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.GetHlrCodeDictionaryDescResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHlrCodeDictionaryResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.GetHlrCodeDictionaryResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHLRSubscriberData");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.GetHLRSubscriberData.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHLRSubscriberDataByImsi");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.GetHLRSubscriberDataByImsi.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHLRSubscriberDataByImsiResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.GetHLRSubscriberDataByImsiResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

	}

	private void addBindings1() {
		java.lang.Class cls;
		javax.xml.namespace.QName qName;
		javax.xml.namespace.QName qName2;
		java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
		java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
		java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
		java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
		java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
		java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
		java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
		java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
		java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
		java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHLRSubscriberDataResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.GetHLRSubscriberDataResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "locationData");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.LocationData.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyAccessInternationalCallBOIC");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyAccessInternationalCallBOIC.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyAccessInternationalCallBOICResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyAccessInternationalCallBOICResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyAccessInternationalCallBOIEXH");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyAccessInternationalCallBOIEXH.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyAccessInternationalCallBOIEXHResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyAccessInternationalCallBOIEXHResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyAccessToInCallsBAIC");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyAccessToInCallsBAIC.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyAccessToInCallsBAICResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyAccessToInCallsBAICResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyAccessToOutCallsBAOC");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyAccessToOutCallsBAOC.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyAccessToOutCallsBAOCResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyAccessToOutCallsBAOCResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyApnWithIp");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyApnWithIp.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyApnWithIpResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyApnWithIpResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyCallerIdCLIP");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyCallerIdCLIP.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyCallerIdCLIPResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyCallerIdCLIPResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyCamelSubsProfileCSP_39");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyCamelSubsProfileCSP_39.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyCamelSubsProfileCSP_39Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyCamelSubsProfileCSP_39Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyFixedCellREGSER");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyFixedCellREGSER.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyFixedCellREGSERResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyFixedCellREGSERResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyGprsNAM");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyGprsNAM.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyGprsNAMResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyGprsNAMResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyHgiri");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyHgiri.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyHgiriResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyHgiriResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyInSmsTS21");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyInSmsTS21.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyInSmsTS21Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyInSmsTS21Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyOICK");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyOICK.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyOICKResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyOICKResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyOutSmsTS22");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyOutSmsTS22.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyOutSmsTS22Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyOutSmsTS22Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyPDPCP");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyPDPCP.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyPDPCPResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyPDPCPResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyPrivateNumberServiceCLIR");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyPrivateNumberServiceCLIR.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyPrivateNumberServiceCLIRResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyPrivateNumberServiceCLIRResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyRestrictionToEndCallsOBI");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyRestrictionToEndCallsOBI.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyRestrictionToEndCallsOBIResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyRestrictionToEndCallsOBIResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyRestrictionToOriginCallsOBO_0");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyRestrictionToOriginCallsOBO_0.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyRestrictionToOriginCallsOBO_0Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyRestrictionToOriginCallsOBO_0Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyRestrictionToOriginCallsOBO_2");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyRestrictionToOriginCallsOBO_2.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyRestrictionToOriginCallsOBO_2Response");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyRestrictionToOriginCallsOBO_2Response.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyRoamingOBR");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyRoamingOBR.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyRoamingOBRResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyRoamingOBRResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifySTYPE");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifySTYPE.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifySTYPEResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifySTYPEResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyThreeWayCallingMPTY");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyThreeWayCallingMPTY.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyThreeWayCallingMPTYResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyThreeWayCallingMPTYResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyTICK");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyTICK.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyTICKResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyTICKResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyToCloseFlotaACCESS_IA");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyToCloseFlotaACCESS_IA.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyToCloseFlotaACCESS_IAResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyToCloseFlotaACCESS_IAResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyToOpenFlotaACCESS_OIA");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyToOpenFlotaACCESS_OIA.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyToOpenFlotaACCESS_OIAResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ModifyToOpenFlotaACCESS_OIAResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrCommandExecution");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.OutHlrCommandExecution.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrDictionary");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.OutHlrDictionary.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrSubscriberData");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.OutHlrSubscriberData.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "packetDataProtocolContextData");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ApnRow[].class;
		cachedSerClasses.add(cls);
		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "apnRow");
		qName2 = new javax.xml.namespace.QName("", "apnRow");
		cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
		cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "refreshVlr");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.RefreshVlr.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "refreshVlrResponse");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.RefreshVlrResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "ServiceException");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.ServiceException.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "subscriberIdentity");
		cachedSerQNames.add(qName);
		cls = com.claro.hlr.ws.service.SubscriberIdentity.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

	}

	protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
		try {
			org.apache.axis.client.Call _call = super._createCall();
			if (super.maintainSessionSet) {
				_call.setMaintainSession(super.maintainSession);
			}
			if (super.cachedUsername != null) {
				_call.setUsername(super.cachedUsername);
			}
			if (super.cachedPassword != null) {
				_call.setPassword(super.cachedPassword);
			}
			if (super.cachedEndpoint != null) {
				_call.setTargetEndpointAddress(super.cachedEndpoint);
			}
			if (super.cachedTimeout != null) {
				_call.setTimeout(super.cachedTimeout);
			}
			if (super.cachedPortName != null) {
				_call.setPortName(super.cachedPortName);
			}
			java.util.Enumeration keys = super.cachedProperties.keys();
			while (keys.hasMoreElements()) {
				java.lang.String key = (java.lang.String) keys.nextElement();
				_call.setProperty(key, super.cachedProperties.get(key));
			}
			// All the type mapping information is registered
			// when the first call is made.
			// The type mapping information is actually registered in
			// the TypeMappingRegistry of the service, which
			// is the reason why registration is only needed for the first call.
			synchronized (this) {
				if (firstCall()) {
					// must set encoding style before registering serializers
					_call.setEncodingStyle(null);
					for (int i = 0; i < cachedSerFactories.size(); ++i) {
						java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
						javax.xml.namespace.QName qName
								= (javax.xml.namespace.QName) cachedSerQNames.get(i);
						java.lang.Object x = cachedSerFactories.get(i);
						if (x instanceof Class) {
							java.lang.Class sf = (java.lang.Class) cachedSerFactories.get(i);
							java.lang.Class df = (java.lang.Class) cachedDeserFactories.get(i);
							_call.registerTypeMapping(cls, qName, sf, df, false);
						} else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
							org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory) cachedSerFactories.get(i);
							org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory) cachedDeserFactories.get(i);
							_call.registerTypeMapping(cls, qName, sf, df, false);
						}
					}
				}
			}
			return _call;
		} catch (java.lang.Throwable _t) {
			throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCallFwOnBusyCFB(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[0]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addCallFwOnBusyCFB");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallFwOnBusyCFB"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, fnum});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addPrepaidRoamingVS_RSA_4(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[1]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addPrepaidRoamingVS_RSA_4");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPrepaidRoamingVS_RSA_4"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteVoiceMailDCF(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[2]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/deleteVoiceMailDCF");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteVoiceMailDCF"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyToCloseFlotaACCESS_IA(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[3]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyToCloseFlotaACCESS_IA");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyToCloseFlotaACCESS_IA"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyAccessToInCallsBAIC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[4]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyAccessToInCallsBAIC");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyAccessToInCallsBAIC"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyCallerIdCLIP(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[5]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyCallerIdCLIP");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyCallerIdCLIP"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyCamelSubsProfileCSP_39(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[6]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyCamelSubsProfileCSP_39");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyCamelSubsProfileCSP_39"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addAccessToOutCallsBAOC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[7]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addAccessToOutCallsBAOC");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addAccessToOutCallsBAOC"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyInSmsTS21(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[8]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyInSmsTS21");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyInSmsTS21"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addPrivateNumberServiceCLIR(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[9]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addPrivateNumberServiceCLIR");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPrivateNumberServiceCLIR"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrSubscriberData getHLRSubscriberData(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[10]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/getHLRSubscriberData");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHLRSubscriberData"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrSubscriberData) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrSubscriberData) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrSubscriberData.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addAccessToInCallsBAIC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[11]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addAccessToInCallsBAIC");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addAccessToInCallsBAIC"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCamelSubsProfileCSP_39(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[12]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addCamelSubsProfileCSP_39");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCamelSubsProfileCSP_39"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution deletePcugToFlotaPCUG(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pcug) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[13]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/deletePcugToFlotaPCUG");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deletePcugToFlotaPCUG"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, pcug});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyOutSmsTS22(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[14]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyOutSmsTS22");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyOutSmsTS22"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addRestrictionToEndCallsOBI(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[15]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addRestrictionToEndCallsOBI");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRestrictionToEndCallsOBI"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCallerIdCLIP(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[16]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addCallerIdCLIP");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallerIdCLIP"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyToOpenFlotaACCESS_OIA(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[17]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyToOpenFlotaACCESS_OIA");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyToOpenFlotaACCESS_OIA"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addPcugToFlotaPCUG(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pcug) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[18]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addPcugToFlotaPCUG");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPcugToFlotaPCUG"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, pcug});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrDictionary getHlrCodeDictionary() throws java.rmi.RemoteException, com.claro.hlr.ws.service.ServiceException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[19]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/getHlrCodeDictionary");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHlrCodeDictionary"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrDictionary) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrDictionary) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrDictionary.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			if (axisFaultException.detail != null) {
				if (axisFaultException.detail instanceof java.rmi.RemoteException) {
					throw (java.rmi.RemoteException) axisFaultException.detail;
				}
				if (axisFaultException.detail instanceof com.claro.hlr.ws.service.ServiceException) {
					throw (com.claro.hlr.ws.service.ServiceException) axisFaultException.detail;
				}
			}
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrDictionary getHlrCodeDictionaryDesc(java.lang.String nomeclature) throws java.rmi.RemoteException, com.claro.hlr.ws.service.ServiceException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[20]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/getHlrCodeDictionaryDesc");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHlrCodeDictionaryDesc"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{nomeclature});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrDictionary) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrDictionary) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrDictionary.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			if (axisFaultException.detail != null) {
				if (axisFaultException.detail instanceof java.rmi.RemoteException) {
					throw (java.rmi.RemoteException) axisFaultException.detail;
				}
				if (axisFaultException.detail instanceof com.claro.hlr.ws.service.ServiceException) {
					throw (com.claro.hlr.ws.service.ServiceException) axisFaultException.detail;
				}
			}
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addFlotaCloseGroup(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String index, java.lang.String ic) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[21]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addFlotaCloseGroup");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addFlotaCloseGroup"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, index, ic});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallFwUnconditionalCFU(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[22]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/deleteCallFwUnconditionalCFU");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallFwUnconditionalCFU"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCallFwOnNotReachCFNRC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[23]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addCallFwOnNotReachCFNRC");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallFwOnNotReachCFNRC"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, fnum});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrSubscriberData getHLRSubscriberDataByImsi(com.claro.hlr.ws.service.AuthenticationInfo authenticationInfo, int centralHlr, java.lang.String imsi) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[24]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/getHLRSubscriberDataByImsi");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHLRSubscriberDataByImsi"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{authenticationInfo, new java.lang.Integer(centralHlr), imsi});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrSubscriberData) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrSubscriberData) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrSubscriberData.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addRoamingPostpagoVS_RSA_2(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[25]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addRoamingPostpagoVS_RSA_2");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRoamingPostpagoVS_RSA_2"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addThreeWayCallingMPTY(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[26]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addThreeWayCallingMPTY");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addThreeWayCallingMPTY"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addRoamingPostpagoVSD_RSA_0(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[27]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addRoamingPostpagoVSD_RSA_0");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRoamingPostpagoVSD_RSA_0"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addPrepaidWithNotRoamingRSA_1(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[28]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addPrepaidWithNotRoamingRSA_1");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPrepaidWithNotRoamingRSA_1"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addPrepaidRoamingV_RSA_3(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[29]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addPrepaidRoamingV_RSA_3");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addPrepaidRoamingV_RSA_3"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteOcsiTcsiCCH(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[30]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/deleteOcsiTcsiCCH");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteOcsiTcsiCCH"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallFwOnNotReachCFNRC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[31]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/deleteCallFwOnNotReachCFNRC");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallFwOnNotReachCFNRC"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addOriginalCamelSubsInfoOCSI(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[32]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addOriginalCamelSubsInfoOCSI");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addOriginalCamelSubsInfoOCSI"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyApnWithIp(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String apnId, java.lang.String pdpId, java.lang.String ip) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[33]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyApnWithIp");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyApnWithIp"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, apnId, pdpId, ip});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallFwOnBusyCFB(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[34]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/deleteCallFwOnBusyCFB");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallFwOnBusyCFB"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCallWaitingCAW(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[35]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addCallWaitingCAW");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallWaitingCAW"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCallFwUnconditionalCFU(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[36]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addCallFwUnconditionalCFU");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallFwUnconditionalCFU"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, fnum});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addFlotaCategory(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String oick, java.lang.String stype) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[37]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addFlotaCategory");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addFlotaCategory"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, oick, stype});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallWaitingCAW(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[38]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/deleteCallWaitingCAW");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallWaitingCAW"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteCallFwOnNotReplyCFNRY(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[39]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/deleteCallFwOnNotReplyCFNRY");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteCallFwOnNotReplyCFNRY"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyAccessToOutCallsBAOC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[40]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyAccessToOutCallsBAOC");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyAccessToOutCallsBAOC"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyThreeWayCallingMPTY(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[41]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyThreeWayCallingMPTY");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyThreeWayCallingMPTY"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyRoamingOBR(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[42]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyRoamingOBR");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyRoamingOBR"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteApnWithPDPID(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pdpid) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[43]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/deleteApnWithPDPID");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteApnWithPDPID"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, pdpid});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyFixedCellREGSER(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pdpid) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[44]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyFixedCellREGSER");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyFixedCellREGSER"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, pdpid});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addCallFwOnNotReplyCFNRY(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[45]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addCallFwOnNotReplyCFNRY");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addCallFwOnNotReplyCFNRY"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, fnum});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addOutSmsTS22(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[46]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addOutSmsTS22");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addOutSmsTS22"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addRoamingOBR(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[47]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addRoamingOBR");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRoamingOBR"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addInSmsTS21(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[48]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addInSmsTS21");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addInSmsTS21"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addApnWithIp(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String apnId, java.lang.String pdpId, java.lang.String ip) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[49]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addApnWithIp");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addApnWithIp"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, apnId, pdpId, ip});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addGprsNAM(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[50]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addGprsNAM");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addGprsNAM"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyHgiri(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String imsi) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[51]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyHgiri");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyHgiri"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, imsi});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyOICK(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String oick) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[52]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyOICK");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyOICK"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, oick});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyTICK(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String pdpid) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[53]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyTICK");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyTICK"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, pdpid});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution refreshVlr(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[54]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/refreshVlr");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "refreshVlr"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addVoiceMail(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String fnum) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[55]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addVoiceMail");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addVoiceMail"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, fnum});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyGprsNAM(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[56]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyGprsNAM");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyGprsNAM"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifySTYPE(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String stype) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[57]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifySTYPE");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifySTYPE"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, stype});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyPDPCP(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[58]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyPDPCP");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyPDPCP"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution deleteFromHlr(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[59]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/deleteFromHlr");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deleteFromHlr"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addApn(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo, java.lang.String apnId, java.lang.String pdpId) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[60]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addApn");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addApn"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo, apnId, pdpId});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addRestrictionToOriginCallsOBO(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[61]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addRestrictionToOriginCallsOBO");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addRestrictionToOriginCallsOBO"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyAccessInternationalCallBOIC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[62]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyAccessInternationalCallBOIC");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyAccessInternationalCallBOIC"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyRestrictionToOriginCallsOBO_0(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[63]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyRestrictionToOriginCallsOBO_0");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyRestrictionToOriginCallsOBO_0"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyRestrictionToEndCallsOBI(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[64]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyRestrictionToEndCallsOBI");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyRestrictionToEndCallsOBI"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addAccessInternationalCallBOIC(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[65]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addAccessInternationalCallBOIC");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addAccessInternationalCallBOIC"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyRestrictionToOriginCallsOBO_2(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[66]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyRestrictionToOriginCallsOBO_2");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyRestrictionToOriginCallsOBO_2"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyAccessInternationalCallBOIEXH(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[67]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyAccessInternationalCallBOIEXH");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyAccessInternationalCallBOIEXH"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addTerminatingCamelSubsInfoTCSI(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[68]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addTerminatingCamelSubsInfoTCSI");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addTerminatingCamelSubsInfoTCSI"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution modifyPrivateNumberServiceCLIR(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[69]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/modifyPrivateNumberServiceCLIR");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyPrivateNumberServiceCLIR"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.claro.hlr.ws.service.OutHlrCommandExecution addAccessInternationalCallBOIEXH(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[70]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://service.ws.hlr.claro.com/addAccessInternationalCallBOIEXH");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addAccessInternationalCallBOIEXH"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[]{inSubscriberAndAuthenticationInfo});

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.claro.hlr.ws.service.OutHlrCommandExecution) org.apache.axis.utils.JavaUtils.convert(_resp, com.claro.hlr.ws.service.OutHlrCommandExecution.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

}
