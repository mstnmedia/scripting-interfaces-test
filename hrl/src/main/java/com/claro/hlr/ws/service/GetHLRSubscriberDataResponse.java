/**
 * GetHLRSubscriberDataResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class GetHLRSubscriberDataResponse  implements java.io.Serializable {
    private com.claro.hlr.ws.service.OutHlrSubscriberData outHlrSubscriberData;

    public GetHLRSubscriberDataResponse() {
    }

    public GetHLRSubscriberDataResponse(
           com.claro.hlr.ws.service.OutHlrSubscriberData outHlrSubscriberData) {
           this.outHlrSubscriberData = outHlrSubscriberData;
    }


    /**
     * Gets the outHlrSubscriberData value for this GetHLRSubscriberDataResponse.
     * 
     * @return outHlrSubscriberData
     */
    public com.claro.hlr.ws.service.OutHlrSubscriberData getOutHlrSubscriberData() {
        return outHlrSubscriberData;
    }


    /**
     * Sets the outHlrSubscriberData value for this GetHLRSubscriberDataResponse.
     * 
     * @param outHlrSubscriberData
     */
    public void setOutHlrSubscriberData(com.claro.hlr.ws.service.OutHlrSubscriberData outHlrSubscriberData) {
        this.outHlrSubscriberData = outHlrSubscriberData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetHLRSubscriberDataResponse)) return false;
        GetHLRSubscriberDataResponse other = (GetHLRSubscriberDataResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.outHlrSubscriberData==null && other.getOutHlrSubscriberData()==null) || 
             (this.outHlrSubscriberData!=null &&
              this.outHlrSubscriberData.equals(other.getOutHlrSubscriberData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOutHlrSubscriberData() != null) {
            _hashCode += getOutHlrSubscriberData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetHLRSubscriberDataResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHLRSubscriberDataResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outHlrSubscriberData");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OutHlrSubscriberData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrSubscriberData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
