/**
 * HLREndPointService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.endpoint;

public interface HLREndPointService extends javax.xml.rpc.Service {
    public java.lang.String getHLREndPointPortAddress();

    public com.claro.hlr.ws.service.HlrService getHLREndPointPort() throws javax.xml.rpc.ServiceException;

    public com.claro.hlr.ws.service.HlrService getHLREndPointPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
