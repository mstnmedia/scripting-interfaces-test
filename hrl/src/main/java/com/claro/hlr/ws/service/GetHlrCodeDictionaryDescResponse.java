/**
 * GetHlrCodeDictionaryDescResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class GetHlrCodeDictionaryDescResponse  implements java.io.Serializable {
    private com.claro.hlr.ws.service.OutHlrDictionary hlrDictionary;

    public GetHlrCodeDictionaryDescResponse() {
    }

    public GetHlrCodeDictionaryDescResponse(
           com.claro.hlr.ws.service.OutHlrDictionary hlrDictionary) {
           this.hlrDictionary = hlrDictionary;
    }


    /**
     * Gets the hlrDictionary value for this GetHlrCodeDictionaryDescResponse.
     * 
     * @return hlrDictionary
     */
    public com.claro.hlr.ws.service.OutHlrDictionary getHlrDictionary() {
        return hlrDictionary;
    }


    /**
     * Sets the hlrDictionary value for this GetHlrCodeDictionaryDescResponse.
     * 
     * @param hlrDictionary
     */
    public void setHlrDictionary(com.claro.hlr.ws.service.OutHlrDictionary hlrDictionary) {
        this.hlrDictionary = hlrDictionary;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetHlrCodeDictionaryDescResponse)) return false;
        GetHlrCodeDictionaryDescResponse other = (GetHlrCodeDictionaryDescResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.hlrDictionary==null && other.getHlrDictionary()==null) || 
             (this.hlrDictionary!=null &&
              this.hlrDictionary.equals(other.getHlrDictionary())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHlrDictionary() != null) {
            _hashCode += getHlrDictionary().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetHlrCodeDictionaryDescResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "getHlrCodeDictionaryDescResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hlrDictionary");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HlrDictionary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrDictionary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
