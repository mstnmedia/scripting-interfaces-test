/**
 * ModifyApnWithIp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class ModifyApnWithIp  implements java.io.Serializable {
    private com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo;

    private java.lang.String apnId;

    private java.lang.String pdpId;

    private java.lang.String ip;

    public ModifyApnWithIp() {
    }

    public ModifyApnWithIp(
           com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo,
           java.lang.String apnId,
           java.lang.String pdpId,
           java.lang.String ip) {
           this.inSubscriberAndAuthenticationInfo = inSubscriberAndAuthenticationInfo;
           this.apnId = apnId;
           this.pdpId = pdpId;
           this.ip = ip;
    }


    /**
     * Gets the inSubscriberAndAuthenticationInfo value for this ModifyApnWithIp.
     * 
     * @return inSubscriberAndAuthenticationInfo
     */
    public com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo getInSubscriberAndAuthenticationInfo() {
        return inSubscriberAndAuthenticationInfo;
    }


    /**
     * Sets the inSubscriberAndAuthenticationInfo value for this ModifyApnWithIp.
     * 
     * @param inSubscriberAndAuthenticationInfo
     */
    public void setInSubscriberAndAuthenticationInfo(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) {
        this.inSubscriberAndAuthenticationInfo = inSubscriberAndAuthenticationInfo;
    }


    /**
     * Gets the apnId value for this ModifyApnWithIp.
     * 
     * @return apnId
     */
    public java.lang.String getApnId() {
        return apnId;
    }


    /**
     * Sets the apnId value for this ModifyApnWithIp.
     * 
     * @param apnId
     */
    public void setApnId(java.lang.String apnId) {
        this.apnId = apnId;
    }


    /**
     * Gets the pdpId value for this ModifyApnWithIp.
     * 
     * @return pdpId
     */
    public java.lang.String getPdpId() {
        return pdpId;
    }


    /**
     * Sets the pdpId value for this ModifyApnWithIp.
     * 
     * @param pdpId
     */
    public void setPdpId(java.lang.String pdpId) {
        this.pdpId = pdpId;
    }


    /**
     * Gets the ip value for this ModifyApnWithIp.
     * 
     * @return ip
     */
    public java.lang.String getIp() {
        return ip;
    }


    /**
     * Sets the ip value for this ModifyApnWithIp.
     * 
     * @param ip
     */
    public void setIp(java.lang.String ip) {
        this.ip = ip;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ModifyApnWithIp)) return false;
        ModifyApnWithIp other = (ModifyApnWithIp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.inSubscriberAndAuthenticationInfo==null && other.getInSubscriberAndAuthenticationInfo()==null) || 
             (this.inSubscriberAndAuthenticationInfo!=null &&
              this.inSubscriberAndAuthenticationInfo.equals(other.getInSubscriberAndAuthenticationInfo()))) &&
            ((this.apnId==null && other.getApnId()==null) || 
             (this.apnId!=null &&
              this.apnId.equals(other.getApnId()))) &&
            ((this.pdpId==null && other.getPdpId()==null) || 
             (this.pdpId!=null &&
              this.pdpId.equals(other.getPdpId()))) &&
            ((this.ip==null && other.getIp()==null) || 
             (this.ip!=null &&
              this.ip.equals(other.getIp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInSubscriberAndAuthenticationInfo() != null) {
            _hashCode += getInSubscriberAndAuthenticationInfo().hashCode();
        }
        if (getApnId() != null) {
            _hashCode += getApnId().hashCode();
        }
        if (getPdpId() != null) {
            _hashCode += getPdpId().hashCode();
        }
        if (getIp() != null) {
            _hashCode += getIp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ModifyApnWithIp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "modifyApnWithIp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inSubscriberAndAuthenticationInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apnId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pdpId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pdpId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ip");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
