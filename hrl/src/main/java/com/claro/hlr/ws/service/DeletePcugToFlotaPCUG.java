/**
 * DeletePcugToFlotaPCUG.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class DeletePcugToFlotaPCUG  implements java.io.Serializable {
    private com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo;

    private java.lang.String pcug;

    public DeletePcugToFlotaPCUG() {
    }

    public DeletePcugToFlotaPCUG(
           com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo,
           java.lang.String pcug) {
           this.inSubscriberAndAuthenticationInfo = inSubscriberAndAuthenticationInfo;
           this.pcug = pcug;
    }


    /**
     * Gets the inSubscriberAndAuthenticationInfo value for this DeletePcugToFlotaPCUG.
     * 
     * @return inSubscriberAndAuthenticationInfo
     */
    public com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo getInSubscriberAndAuthenticationInfo() {
        return inSubscriberAndAuthenticationInfo;
    }


    /**
     * Sets the inSubscriberAndAuthenticationInfo value for this DeletePcugToFlotaPCUG.
     * 
     * @param inSubscriberAndAuthenticationInfo
     */
    public void setInSubscriberAndAuthenticationInfo(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) {
        this.inSubscriberAndAuthenticationInfo = inSubscriberAndAuthenticationInfo;
    }


    /**
     * Gets the pcug value for this DeletePcugToFlotaPCUG.
     * 
     * @return pcug
     */
    public java.lang.String getPcug() {
        return pcug;
    }


    /**
     * Sets the pcug value for this DeletePcugToFlotaPCUG.
     * 
     * @param pcug
     */
    public void setPcug(java.lang.String pcug) {
        this.pcug = pcug;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeletePcugToFlotaPCUG)) return false;
        DeletePcugToFlotaPCUG other = (DeletePcugToFlotaPCUG) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.inSubscriberAndAuthenticationInfo==null && other.getInSubscriberAndAuthenticationInfo()==null) || 
             (this.inSubscriberAndAuthenticationInfo!=null &&
              this.inSubscriberAndAuthenticationInfo.equals(other.getInSubscriberAndAuthenticationInfo()))) &&
            ((this.pcug==null && other.getPcug()==null) || 
             (this.pcug!=null &&
              this.pcug.equals(other.getPcug())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInSubscriberAndAuthenticationInfo() != null) {
            _hashCode += getInSubscriberAndAuthenticationInfo().hashCode();
        }
        if (getPcug() != null) {
            _hashCode += getPcug().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeletePcugToFlotaPCUG.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "deletePcugToFlotaPCUG"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inSubscriberAndAuthenticationInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pcug");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pcug"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
