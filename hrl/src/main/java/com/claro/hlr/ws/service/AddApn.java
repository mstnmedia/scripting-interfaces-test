/**
 * AddApn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class AddApn  implements java.io.Serializable {
    private com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo;

    private java.lang.String apnId;

    private java.lang.String pdpId;

    public AddApn() {
    }

    public AddApn(
           com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo,
           java.lang.String apnId,
           java.lang.String pdpId) {
           this.inSubscriberAndAuthenticationInfo = inSubscriberAndAuthenticationInfo;
           this.apnId = apnId;
           this.pdpId = pdpId;
    }


    /**
     * Gets the inSubscriberAndAuthenticationInfo value for this AddApn.
     * 
     * @return inSubscriberAndAuthenticationInfo
     */
    public com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo getInSubscriberAndAuthenticationInfo() {
        return inSubscriberAndAuthenticationInfo;
    }


    /**
     * Sets the inSubscriberAndAuthenticationInfo value for this AddApn.
     * 
     * @param inSubscriberAndAuthenticationInfo
     */
    public void setInSubscriberAndAuthenticationInfo(com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo inSubscriberAndAuthenticationInfo) {
        this.inSubscriberAndAuthenticationInfo = inSubscriberAndAuthenticationInfo;
    }


    /**
     * Gets the apnId value for this AddApn.
     * 
     * @return apnId
     */
    public java.lang.String getApnId() {
        return apnId;
    }


    /**
     * Sets the apnId value for this AddApn.
     * 
     * @param apnId
     */
    public void setApnId(java.lang.String apnId) {
        this.apnId = apnId;
    }


    /**
     * Gets the pdpId value for this AddApn.
     * 
     * @return pdpId
     */
    public java.lang.String getPdpId() {
        return pdpId;
    }


    /**
     * Sets the pdpId value for this AddApn.
     * 
     * @param pdpId
     */
    public void setPdpId(java.lang.String pdpId) {
        this.pdpId = pdpId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddApn)) return false;
        AddApn other = (AddApn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.inSubscriberAndAuthenticationInfo==null && other.getInSubscriberAndAuthenticationInfo()==null) || 
             (this.inSubscriberAndAuthenticationInfo!=null &&
              this.inSubscriberAndAuthenticationInfo.equals(other.getInSubscriberAndAuthenticationInfo()))) &&
            ((this.apnId==null && other.getApnId()==null) || 
             (this.apnId!=null &&
              this.apnId.equals(other.getApnId()))) &&
            ((this.pdpId==null && other.getPdpId()==null) || 
             (this.pdpId!=null &&
              this.pdpId.equals(other.getPdpId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInSubscriberAndAuthenticationInfo() != null) {
            _hashCode += getInSubscriberAndAuthenticationInfo().hashCode();
        }
        if (getApnId() != null) {
            _hashCode += getApnId().hashCode();
        }
        if (getPdpId() != null) {
            _hashCode += getPdpId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddApn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "addApn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inSubscriberAndAuthenticationInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "InSubscriberAndAuthenticationInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "inSubscriberAndAuthenticationInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apnId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pdpId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pdpId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
