/**
 * OutHlrSubscriberData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

import java.util.Arrays;

public class OutHlrSubscriberData  implements java.io.Serializable {
    private java.lang.String transactionId;

    private boolean success;

    private int centralHlr;

    private com.claro.hlr.ws.service.SubscriberIdentity subscriberIdentity;

    private com.claro.hlr.ws.service.OutHlrSubscriberDataPermanentSubscriberData permanentSubscriberData;

    private java.lang.String amsisdn;

    private java.lang.String bs;

    private java.lang.String bc;

    private com.claro.hlr.ws.service.OutHlrSubscriberDataSupplementaryServiceDataEntry[] supplementaryServiceData;

    private com.claro.hlr.ws.service.LocationData locationData;

    private com.claro.hlr.ws.service.ApnRow[] pdpContextData;

    private com.claro.hlr.ws.service.ErrorDetail errorDetail;

    public OutHlrSubscriberData() {
    }

    public OutHlrSubscriberData(
           java.lang.String transactionId,
           boolean success,
           int centralHlr,
           com.claro.hlr.ws.service.SubscriberIdentity subscriberIdentity,
           com.claro.hlr.ws.service.OutHlrSubscriberDataPermanentSubscriberData permanentSubscriberData,
           java.lang.String amsisdn,
           java.lang.String bs,
           java.lang.String bc,
           com.claro.hlr.ws.service.OutHlrSubscriberDataSupplementaryServiceDataEntry[] supplementaryServiceData,
           com.claro.hlr.ws.service.LocationData locationData,
           com.claro.hlr.ws.service.ApnRow[] pdpContextData,
           com.claro.hlr.ws.service.ErrorDetail errorDetail) {
           this.transactionId = transactionId;
           this.success = success;
           this.centralHlr = centralHlr;
           this.subscriberIdentity = subscriberIdentity;
           this.permanentSubscriberData = permanentSubscriberData;
           this.amsisdn = amsisdn;
           this.bs = bs;
           this.bc = bc;
           this.supplementaryServiceData = supplementaryServiceData;
           this.locationData = locationData;
           this.pdpContextData = pdpContextData;
           this.errorDetail = errorDetail;
    }


    /**
     * Gets the transactionId value for this OutHlrSubscriberData.
     * 
     * @return transactionId
     */
    public java.lang.String getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this OutHlrSubscriberData.
     * 
     * @param transactionId
     */
    public void setTransactionId(java.lang.String transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the success value for this OutHlrSubscriberData.
     * 
     * @return success
     */
    public boolean isSuccess() {
        return success;
    }


    /**
     * Sets the success value for this OutHlrSubscriberData.
     * 
     * @param success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }


    /**
     * Gets the centralHlr value for this OutHlrSubscriberData.
     * 
     * @return centralHlr
     */
    public int getCentralHlr() {
        return centralHlr;
    }


    /**
     * Sets the centralHlr value for this OutHlrSubscriberData.
     * 
     * @param centralHlr
     */
    public void setCentralHlr(int centralHlr) {
        this.centralHlr = centralHlr;
    }


    /**
     * Gets the subscriberIdentity value for this OutHlrSubscriberData.
     * 
     * @return subscriberIdentity
     */
    public com.claro.hlr.ws.service.SubscriberIdentity getSubscriberIdentity() {
        return subscriberIdentity;
    }


    /**
     * Sets the subscriberIdentity value for this OutHlrSubscriberData.
     * 
     * @param subscriberIdentity
     */
    public void setSubscriberIdentity(com.claro.hlr.ws.service.SubscriberIdentity subscriberIdentity) {
        this.subscriberIdentity = subscriberIdentity;
    }


    /**
     * Gets the permanentSubscriberData value for this OutHlrSubscriberData.
     * 
     * @return permanentSubscriberData
     */
    public com.claro.hlr.ws.service.OutHlrSubscriberDataPermanentSubscriberData getPermanentSubscriberData() {
        return permanentSubscriberData;
    }


    /**
     * Sets the permanentSubscriberData value for this OutHlrSubscriberData.
     * 
     * @param permanentSubscriberData
     */
    public void setPermanentSubscriberData(com.claro.hlr.ws.service.OutHlrSubscriberDataPermanentSubscriberData permanentSubscriberData) {
        this.permanentSubscriberData = permanentSubscriberData;
    }


    /**
     * Gets the amsisdn value for this OutHlrSubscriberData.
     * 
     * @return amsisdn
     */
    public java.lang.String getAmsisdn() {
        return amsisdn;
    }


    /**
     * Sets the amsisdn value for this OutHlrSubscriberData.
     * 
     * @param amsisdn
     */
    public void setAmsisdn(java.lang.String amsisdn) {
        this.amsisdn = amsisdn;
    }


    /**
     * Gets the bs value for this OutHlrSubscriberData.
     * 
     * @return bs
     */
    public java.lang.String getBs() {
        return bs;
    }


    /**
     * Sets the bs value for this OutHlrSubscriberData.
     * 
     * @param bs
     */
    public void setBs(java.lang.String bs) {
        this.bs = bs;
    }


    /**
     * Gets the bc value for this OutHlrSubscriberData.
     * 
     * @return bc
     */
    public java.lang.String getBc() {
        return bc;
    }


    /**
     * Sets the bc value for this OutHlrSubscriberData.
     * 
     * @param bc
     */
    public void setBc(java.lang.String bc) {
        this.bc = bc;
    }


    /**
     * Gets the supplementaryServiceData value for this OutHlrSubscriberData.
     * 
     * @return supplementaryServiceData
     */
    public com.claro.hlr.ws.service.OutHlrSubscriberDataSupplementaryServiceDataEntry[] getSupplementaryServiceData() {
        return supplementaryServiceData;
    }


    /**
     * Sets the supplementaryServiceData value for this OutHlrSubscriberData.
     * 
     * @param supplementaryServiceData
     */
    public void setSupplementaryServiceData(com.claro.hlr.ws.service.OutHlrSubscriberDataSupplementaryServiceDataEntry[] supplementaryServiceData) {
        this.supplementaryServiceData = supplementaryServiceData;
    }


    /**
     * Gets the locationData value for this OutHlrSubscriberData.
     * 
     * @return locationData
     */
    public com.claro.hlr.ws.service.LocationData getLocationData() {
        return locationData;
    }


    /**
     * Sets the locationData value for this OutHlrSubscriberData.
     * 
     * @param locationData
     */
    public void setLocationData(com.claro.hlr.ws.service.LocationData locationData) {
        this.locationData = locationData;
    }


    /**
     * Gets the pdpContextData value for this OutHlrSubscriberData.
     * 
     * @return pdpContextData
     */
    public com.claro.hlr.ws.service.ApnRow[] getPdpContextData() {
        return pdpContextData;
    }


    /**
     * Sets the pdpContextData value for this OutHlrSubscriberData.
     * 
     * @param pdpContextData
     */
    public void setPdpContextData(com.claro.hlr.ws.service.ApnRow[] pdpContextData) {
        this.pdpContextData = pdpContextData;
    }


    /**
     * Gets the errorDetail value for this OutHlrSubscriberData.
     * 
     * @return errorDetail
     */
    public com.claro.hlr.ws.service.ErrorDetail getErrorDetail() {
        return errorDetail;
    }


    /**
     * Sets the errorDetail value for this OutHlrSubscriberData.
     * 
     * @param errorDetail
     */
    public void setErrorDetail(com.claro.hlr.ws.service.ErrorDetail errorDetail) {
        this.errorDetail = errorDetail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OutHlrSubscriberData)) return false;
        OutHlrSubscriberData other = (OutHlrSubscriberData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.transactionId==null && other.getTransactionId()==null) || 
             (this.transactionId!=null &&
              this.transactionId.equals(other.getTransactionId()))) &&
            this.success == other.isSuccess() &&
            this.centralHlr == other.getCentralHlr() &&
            ((this.subscriberIdentity==null && other.getSubscriberIdentity()==null) || 
             (this.subscriberIdentity!=null &&
              this.subscriberIdentity.equals(other.getSubscriberIdentity()))) &&
            ((this.permanentSubscriberData==null && other.getPermanentSubscriberData()==null) || 
             (this.permanentSubscriberData!=null &&
              this.permanentSubscriberData.equals(other.getPermanentSubscriberData()))) &&
            ((this.amsisdn==null && other.getAmsisdn()==null) || 
             (this.amsisdn!=null &&
              this.amsisdn.equals(other.getAmsisdn()))) &&
            ((this.bs==null && other.getBs()==null) || 
             (this.bs!=null &&
              this.bs.equals(other.getBs()))) &&
            ((this.bc==null && other.getBc()==null) || 
             (this.bc!=null &&
              this.bc.equals(other.getBc()))) &&
            ((this.supplementaryServiceData==null && other.getSupplementaryServiceData()==null) || 
             (this.supplementaryServiceData!=null &&
              java.util.Arrays.equals(this.supplementaryServiceData, other.getSupplementaryServiceData()))) &&
            ((this.locationData==null && other.getLocationData()==null) || 
             (this.locationData!=null &&
              this.locationData.equals(other.getLocationData()))) &&
            ((this.pdpContextData==null && other.getPdpContextData()==null) || 
             (this.pdpContextData!=null &&
              java.util.Arrays.equals(this.pdpContextData, other.getPdpContextData()))) &&
            ((this.errorDetail==null && other.getErrorDetail()==null) || 
             (this.errorDetail!=null &&
              this.errorDetail.equals(other.getErrorDetail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTransactionId() != null) {
            _hashCode += getTransactionId().hashCode();
        }
        _hashCode += (isSuccess() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += getCentralHlr();
        if (getSubscriberIdentity() != null) {
            _hashCode += getSubscriberIdentity().hashCode();
        }
        if (getPermanentSubscriberData() != null) {
            _hashCode += getPermanentSubscriberData().hashCode();
        }
        if (getAmsisdn() != null) {
            _hashCode += getAmsisdn().hashCode();
        }
        if (getBs() != null) {
            _hashCode += getBs().hashCode();
        }
        if (getBc() != null) {
            _hashCode += getBc().hashCode();
        }
        if (getSupplementaryServiceData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSupplementaryServiceData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSupplementaryServiceData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLocationData() != null) {
            _hashCode += getLocationData().hashCode();
        }
        if (getPdpContextData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPdpContextData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPdpContextData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getErrorDetail() != null) {
            _hashCode += getErrorDetail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OutHlrSubscriberData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "outHlrSubscriberData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("success");
        elemField.setXmlName(new javax.xml.namespace.QName("", "success"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("centralHlr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "centralHlr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriberIdentity");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriberIdentity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "subscriberIdentity"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("permanentSubscriberData");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PermanentSubscriberData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", ">outHlrSubscriberData>PermanentSubscriberData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amsisdn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "amsisdn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("supplementaryServiceData");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SupplementaryServiceData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", ">>outHlrSubscriberData>SupplementaryServiceData>entry"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "entry"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locationData");
        elemField.setXmlName(new javax.xml.namespace.QName("", "locationData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "locationData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pdpContextData");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pdpContextData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "apnRow"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "apnRow"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "errorDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "errorDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	@Override
	public String toString() {
		return "OutHlrSubscriberData [transactionId=" + transactionId + ", success=" + success + ", centralHlr="
				+ centralHlr + ", subscriberIdentity=" + subscriberIdentity + ", permanentSubscriberData="
				+ permanentSubscriberData + ", amsisdn=" + amsisdn + ", bs=" + bs + ", bc=" + bc
				+ ", supplementaryServiceData=" + Arrays.toString(supplementaryServiceData) + ", locationData="
				+ locationData + ", pdpContextData=" + Arrays.toString(pdpContextData) + ", errorDetail=" + errorDetail
				+ ", __equalsCalc=" + __equalsCalc + ", __hashCodeCalc=" + __hashCodeCalc + "]";
	}
    
    

}
