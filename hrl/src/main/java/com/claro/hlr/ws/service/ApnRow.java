/**
 * ApnRow.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.claro.hlr.ws.service;

public class ApnRow  implements java.io.Serializable {
    private java.lang.String apnId;

    private java.lang.String eqosid;

    private java.lang.String pdpAdd;

    private java.lang.String pdpch;

    private java.lang.String pdpid;

    private java.lang.String pdpty;

    private java.lang.String vpaa;

    public ApnRow() {
    }

    public ApnRow(
           java.lang.String apnId,
           java.lang.String eqosid,
           java.lang.String pdpAdd,
           java.lang.String pdpch,
           java.lang.String pdpid,
           java.lang.String pdpty,
           java.lang.String vpaa) {
           this.apnId = apnId;
           this.eqosid = eqosid;
           this.pdpAdd = pdpAdd;
           this.pdpch = pdpch;
           this.pdpid = pdpid;
           this.pdpty = pdpty;
           this.vpaa = vpaa;
    }


    /**
     * Gets the apnId value for this ApnRow.
     * 
     * @return apnId
     */
    public java.lang.String getApnId() {
        return apnId;
    }


    /**
     * Sets the apnId value for this ApnRow.
     * 
     * @param apnId
     */
    public void setApnId(java.lang.String apnId) {
        this.apnId = apnId;
    }


    /**
     * Gets the eqosid value for this ApnRow.
     * 
     * @return eqosid
     */
    public java.lang.String getEqosid() {
        return eqosid;
    }


    /**
     * Sets the eqosid value for this ApnRow.
     * 
     * @param eqosid
     */
    public void setEqosid(java.lang.String eqosid) {
        this.eqosid = eqosid;
    }


    /**
     * Gets the pdpAdd value for this ApnRow.
     * 
     * @return pdpAdd
     */
    public java.lang.String getPdpAdd() {
        return pdpAdd;
    }


    /**
     * Sets the pdpAdd value for this ApnRow.
     * 
     * @param pdpAdd
     */
    public void setPdpAdd(java.lang.String pdpAdd) {
        this.pdpAdd = pdpAdd;
    }


    /**
     * Gets the pdpch value for this ApnRow.
     * 
     * @return pdpch
     */
    public java.lang.String getPdpch() {
        return pdpch;
    }


    /**
     * Sets the pdpch value for this ApnRow.
     * 
     * @param pdpch
     */
    public void setPdpch(java.lang.String pdpch) {
        this.pdpch = pdpch;
    }


    /**
     * Gets the pdpid value for this ApnRow.
     * 
     * @return pdpid
     */
    public java.lang.String getPdpid() {
        return pdpid;
    }


    /**
     * Sets the pdpid value for this ApnRow.
     * 
     * @param pdpid
     */
    public void setPdpid(java.lang.String pdpid) {
        this.pdpid = pdpid;
    }


    /**
     * Gets the pdpty value for this ApnRow.
     * 
     * @return pdpty
     */
    public java.lang.String getPdpty() {
        return pdpty;
    }


    /**
     * Sets the pdpty value for this ApnRow.
     * 
     * @param pdpty
     */
    public void setPdpty(java.lang.String pdpty) {
        this.pdpty = pdpty;
    }


    /**
     * Gets the vpaa value for this ApnRow.
     * 
     * @return vpaa
     */
    public java.lang.String getVpaa() {
        return vpaa;
    }


    /**
     * Sets the vpaa value for this ApnRow.
     * 
     * @param vpaa
     */
    public void setVpaa(java.lang.String vpaa) {
        this.vpaa = vpaa;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ApnRow)) return false;
        ApnRow other = (ApnRow) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.apnId==null && other.getApnId()==null) || 
             (this.apnId!=null &&
              this.apnId.equals(other.getApnId()))) &&
            ((this.eqosid==null && other.getEqosid()==null) || 
             (this.eqosid!=null &&
              this.eqosid.equals(other.getEqosid()))) &&
            ((this.pdpAdd==null && other.getPdpAdd()==null) || 
             (this.pdpAdd!=null &&
              this.pdpAdd.equals(other.getPdpAdd()))) &&
            ((this.pdpch==null && other.getPdpch()==null) || 
             (this.pdpch!=null &&
              this.pdpch.equals(other.getPdpch()))) &&
            ((this.pdpid==null && other.getPdpid()==null) || 
             (this.pdpid!=null &&
              this.pdpid.equals(other.getPdpid()))) &&
            ((this.pdpty==null && other.getPdpty()==null) || 
             (this.pdpty!=null &&
              this.pdpty.equals(other.getPdpty()))) &&
            ((this.vpaa==null && other.getVpaa()==null) || 
             (this.vpaa!=null &&
              this.vpaa.equals(other.getVpaa())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApnId() != null) {
            _hashCode += getApnId().hashCode();
        }
        if (getEqosid() != null) {
            _hashCode += getEqosid().hashCode();
        }
        if (getPdpAdd() != null) {
            _hashCode += getPdpAdd().hashCode();
        }
        if (getPdpch() != null) {
            _hashCode += getPdpch().hashCode();
        }
        if (getPdpid() != null) {
            _hashCode += getPdpid().hashCode();
        }
        if (getPdpty() != null) {
            _hashCode += getPdpty().hashCode();
        }
        if (getVpaa() != null) {
            _hashCode += getVpaa().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ApnRow.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.ws.hlr.claro.com/", "apnRow"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apnId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eqosid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eqosid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pdpAdd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pdpAdd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pdpch");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pdpch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pdpid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pdpid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pdpty");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pdpty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vpaa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vpaa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
