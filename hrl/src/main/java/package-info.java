/**
 * Este proyecto contiene la interfaz que conecta con Complex-HLR.
 * Esta interfaz agrega a Scripting la posibilidad de que los usuarios puedan 
 * interactuar con el sistema Complex-HLR para dar soportes al mundo de servicios
 * móviles dentro de una transacción en curso.
 */
