/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.claro.hlr.ws.endpoint.HLREndPointServiceLocator;
import com.claro.hlr.ws.service.AuthenticationInfo;
import com.claro.hlr.ws.service.HlrService;
import com.claro.hlr.ws.service.InSubscriberAndAuthenticationInfo;
import com.claro.hlr.ws.service.OutHlrCommandExecution;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.hlr.Complex_HLR;
import java.util.Arrays;
import java.util.List;

/**
 * Clase ejecutable con métodos de pruebas para probar las diferentes
 * funcionalidades en este proyecto.
 *
 * @author amatos
 */
public class Main {

	/**
	 * @param args the command line arguments
	 * @throws java.lang.Exception
	 */
	static TransactionInterfacesPayload payload = new TransactionInterfacesPayload(
			new V_Transaction(), new Workflow_Step(), "{}"
	);
	static HlrService SOAP = null;

	static {
		InterfaceConfigs.setConfig();
		try {
			SOAP = new HLREndPointServiceLocator().getHLREndPointPort();
		} catch (Exception ex) {
			Utils.logException(Main.class, "Error getting instance SOAP", ex);
		}
	}

	static Object getHLRSubscriberData() throws Exception {
		InSubscriberAndAuthenticationInfo request = new InSubscriberAndAuthenticationInfo();
		AuthenticationInfo authInfo = new AuthenticationInfo();
		authInfo.setUser("scripting");
		authInfo.setPassword("scripting");
		request.setAuthenticationInfo(authInfo);
		request.setCentralHlr(1);
		request.setSubscriber("8297604403");
		return SOAP.getHLRSubscriberData(request);
	}

	static Object addApn() throws Exception {
		InSubscriberAndAuthenticationInfo request = new InSubscriberAndAuthenticationInfo();
		AuthenticationInfo authInfo = new AuthenticationInfo();
		authInfo.setUser("scripting");
		authInfo.setPassword("scripting");
		request.setAuthenticationInfo(authInfo);
		request.setCentralHlr(1);
		request.setSubscriber("8297604403");
		OutHlrCommandExecution response = SOAP.addApn(request, "33", "4");
		return response;
	}

	static Object deleteApn() throws Exception {
		InSubscriberAndAuthenticationInfo request = new InSubscriberAndAuthenticationInfo();
		AuthenticationInfo authInfo = new AuthenticationInfo();
		authInfo.setUser("scripting");
		authInfo.setPassword("scripting");
		request.setAuthenticationInfo(authInfo);
		request.setCentralHlr(1);
		request.setSubscriber("8297604403");
		OutHlrCommandExecution response = SOAP.deleteApnWithPDPID(request, "4");
		return response;
	}

	static Object testAddApn() throws Exception {
		Complex_HLR service = new Complex_HLR();
		Interface inter = service.getInterfaces(true).stream()
				.filter(item -> item.getName().endsWith("addApn"))
				.findFirst().get();

		ObjectNode form = JSON.newObjectNode();
		List<Object> values = Arrays.asList("8297604403", "33", "4");
		for (int i = 0; i < inter.getInterface_field().size(); i++) {
			Interface_Field field = inter.getInterface_field().get(i);
			form.putPOJO(field.getName(), values.get(i));
		}
		payload.setForm(form.toString());

		return service.callMethod(inter, payload, User.SYSTEM);
	}

	static Object testDeleteApn() throws Exception {
		Complex_HLR service = new Complex_HLR();
		Interface inter = service.getInterfaces(true).stream()
				.filter(item -> item.getName().endsWith("deleteApnWithPDPID"))
				.findFirst().get();

		ObjectNode form = JSON.newObjectNode();
		List<Object> values = Arrays.asList("8297604403", "4");
		for (int i = 0; i < inter.getInterface_field().size(); i++) {
			Interface_Field field = inter.getInterface_field().get(i);
			form.putPOJO(field.getName(), values.get(i));
		}
		payload.setForm(form.toString());

		return service.callMethod(inter, payload, User.SYSTEM);
	}

	static Object testGetHLRSubscriberData() throws Exception {
		Complex_HLR service = new Complex_HLR();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces.stream()
				.filter(item -> item.getName().endsWith("getHLRSubscriberData"))
				.findFirst().get();

		ObjectNode form = JSON.newObjectNode();
		List<Object> values = Arrays.asList("8297604403");
		for (int i = 0; i < inter.getInterface_field().size(); i++) {
			Interface_Field field = inter.getInterface_field().get(i);
			form.putPOJO(field.getName(), values.get(i));
		}
		payload.setForm(form.toString());

		return service.callMethod(inter, payload, User.SYSTEM);
	}

	public static void main(String[] args) throws Exception {
//		Object result = "";
//		Object result = getHLRSubscriberData();
		Object result = testGetHLRSubscriberData();
//		Object result = addApn();
//		Object result = deleteApn();
//		Object result = testAddApn();
//		Object result = testDeleteApn();

		System.out.println(JSON.toString(result));
	}
}
