/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.casehistory;

/**
 *
 * @author amatos
 */
public class Record {

	public String id;
	public String actividad;
	public String fecha;
	public String usuario;
	public String infoAdicional;
	public String llamadaEntrante;
	public String notaAdministrativa;
	public String reason;
	public String notaCierre;

}
