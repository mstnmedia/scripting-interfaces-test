package com.mstn.scripting.interfaces.casehistory;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;

/**
 * Interfaz que agrega a Scripting la conexión a SAD desde una transacción en
 * curso.
 *
 * @author amatos
 */
public class CaseHistoryDyn extends InterfaceBase {

	static Procedure SOAP = new Procedure();

	public CaseHistoryDyn() {
		super(0, "casehistory", SOAP);
		IGNORED_METHODS.add("getConnection");
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("crm_casehistory_connectionString"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("caseID") || fieldName.endsWith("arg0")) {
			field.setLabel("ID de caso");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		}
		return field;
	}

	@Override
	protected Object invokeMethod() throws Exception {
		return super.invokeMethod();
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("crm_casehistory_connectionString").toString();
		InterfaceConfigs.get("crm_casehistory_user").toString();
		InterfaceConfigs.get("crm_casehistory_password").toString();
		if (Utils.isNullOrWhiteSpaces(InterfaceConfigs.get("crm_casehistory_date_format"))) {
			Utils.logWarn(this.getClass(),
					"No se encontró un valor para 'crm_casehistory_date_format' en el archivo de configuración. "
					+ "Utilizará el formato por defecto: '" + Procedure.DEFAULT_DATE_FORMAT + "'.");
		}
		super.test();
	}
}
