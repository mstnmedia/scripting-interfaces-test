/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.casehistory;

import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.InterfaceConfigs;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import oracle.jdbc.internal.OracleTypes;

/**
 * Clase que envuelve a una instancia del servicio de direcciones de SAD y hace
 * públicos métodos específicos, los mismos que los usuarios tendrán
 * disponibles.
 *
 * @author amatos
 */
public class Procedure {

	//	static final String SOME_NAME = "{call schema_name.org_name_pkg.return_something(?,?)}";
	static final String SOME_NAME = "{call INTERFACES.PKG_SCRIPTING.ConsultaHistCasoById(?,?)}";
	static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy hh:mm:ss aa";

	public Procedure() {
	}

	static public Connection getConnection() throws SQLException {
		String connectionString = InterfaceConfigs.get("crm_casehistory_connectionString");
		String user = InterfaceConfigs.get("crm_casehistory_user");
		String password = InterfaceConfigs.get("crm_casehistory_password");
		Connection conn = DriverManager.getConnection(connectionString, user, password);
		return conn;
	}

	static public List<Record> get(String caseID) throws Exception {
		try {
			Connection conn = getConnection();
			CallableStatement stmt = null;
			ResultSet rset = null;
			stmt = conn.prepareCall(SOME_NAME);//We have declared this at the very top
			stmt.setString(1, caseID);//Passing CompanyID here
			stmt.registerOutParameter(2, OracleTypes.CURSOR);//Refcursor selects the row based upon query results provided in Package.
			stmt.execute();
			rset = (ResultSet) stmt.getObject(2);
			List<Record> records = new ArrayList<>();
			Record record = null;
			int id = 0;
			while (rset.next()) {
				record = new Record();
//				record.id = String.valueOf(id++);
				record.actividad = rset.getString("ACTIVIDAD");
				String dateFormat = InterfaceConfigs.get("crm_casehistory_date_format", DEFAULT_DATE_FORMAT);
				String strFecha = rset.getString("FECHA");
				Date fecha = new SimpleDateFormat(dateFormat).parse(strFecha);
				record.fecha = DATE.toDateTimeString(fecha);
				record.usuario = rset.getString("USUARIO");
				record.infoAdicional = rset.getString("INFO_ADICIONAL");
				record.llamadaEntrante = rset.getString("LLAMADA_ENTRANTE");
				record.notaAdministrativa = rset.getString("NOTA_ADMINISTRATIVA");
				record.reason = rset.getString("REASON");
				record.notaCierre = rset.getString("NOTACIERRE");
				records.add(record);
			}
			conn.close();
			for (int i = records.size() - 1; i >= 0; i--) {
				records.get(i).id = String.valueOf(id++);
			}
			return records;
		} catch (Exception ex) {
			Utils.logException(Procedure.class, "Error consultando procedimiento del histórico de casos", ex);
			throw ex;
		}
	}

}
