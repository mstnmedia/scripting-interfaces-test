package com.mstn.scripting.interfaces.casehistory;

import _do.com.claro.soa.services.customer.getcrmticketbycredentials.GetCrmTicketByCredentials;
import _do.com.claro.soa.services.customer.getcrmticketbycredentials.GetCrmTicketByCredentials_Service;
import _do.com.claro.soa.services.customer.getcrmticketbycredentials.GetRequest;
import _do.com.claro.soa.services.customer.getcrmticketbycredentials.GetResponse;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Log;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import com.tcs.iesb.crm.casemgmt.CaseManagementService;
import com.tcs.iesb.crm.casemgmt.CaseManagementService_Service;
import com.tcs.iesb.crm.casemgmt.types.GetCaseDetailsByIdRequest;
import com.tcs.iesb.crm.casemgmt.types.GetCaseDetailsByIdResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

public class CaseHistorySta extends InterfaceConnectorBase {

	private final static int ID = Interfaces.CASE_HISTORIES_ID;
	private final static String NAME = "case_histories";
	private final static String LABEL = "CRM - Histórico del caso";
	private final static String SERVER_URL = "crm.org";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	private List caseHistories = new ArrayList();
	private GetCaseDetailsByIdResponse caseDetails = null;
	public final static String CASE_ID = "case_id";

	public CaseHistorySta() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Field> getFields() {
		List<Interface_Field> list = new ArrayList();
		list.add(new Interface_Field(1, ID, CASE_ID, "ID del caso", InterfaceFieldTypes.NUMBER, 1, ""));
		return list;
	}

	@Override
	public List<Transaction_Result> getResults() throws Exception {
		String prefix = NAME + "_";
		String strCaseHistories = JSON.toString(caseHistories);
		String strCaseDetails = null;
		if (caseDetails != null && (caseDetails.getFailureReason() == null || "-1".equals(caseDetails.getFailureReason().getReasonCode()))) {
			strCaseDetails = JSON.toString(caseDetails.getCaseDetails());
		}
		return Arrays.asList(
				//(id, id_center, id_transaction, id_workflow_step, id_interface, name, label, value, props)
				new Transaction_Result(1, 0, 0, 0, ID, prefix + "caseHistories", "Histórico del caso", strCaseHistories, "{\"notSave\":true}"),
				new Transaction_Result(2, 0, 0, 0, ID, prefix + "caseDetails", "Detalle del caso", strCaseDetails, "{\"notSave\":true}")
		);
	}

	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) {
		TransactionInterfacesResponse response = new TransactionInterfacesResponse();
		ObjectNode json = JSON.newObjectNode();
		response.setJson(json);
		LogDao log = InterfaceConfigs.dbi == null ? getEmptyDao() : InterfaceConfigs.dbi.onDemand(LogDao.class);
		try {
			Interface inter = getInterface();
			response.setInterface(inter);
			ObjectNode form = JSON.toNode(payload.getForm());

			String caseID = JSON.getString(form, CASE_ID);
			boolean validID = Utils.nonNullOrEmpty(caseID);

			if (!validID) {
				form.remove(CASE_ID);
				setAsMissingRequest(response, json, form);
			} else {
				json.put(CASE_ID, caseID);
				long userID = user == null ? 0 : user.getId();
				String userIP = user == null ? null : user.getIp();
				try {
					caseHistories = Procedure.get(caseID);
				} catch (Exception ex) {
					log.insert(userID, "CRM Casos", Long.parseLong(caseID),
							"Error consultando histórico del caso",
							ex.getMessage(), JSON.toString(ex), userIP);
					throw ex;
				}

				String token = null;
				try {
					GetCrmTicketByCredentials credentials = GetCrmTicketByCredentials_Service.getInstance();
					BindingProvider bp = (BindingProvider) credentials;
					String username = ((user == null || user.getEmployee() == null) ? ""
							: (user.getEmployee().getSource() == null || "CLARO".equals(user.getEmployee().getSource())
							? "TD" : "CT")) + userID;
					String password = "";
					List<Handler> handlerChain = bp.getBinding().getHandlerChain();
					handlerChain.add(new SOAPHeaderHandler(username, password));
					bp.getBinding().setHandlerChain(handlerChain);
					GetRequest params = new GetRequest();
					params.setIn("Lo que sea");
					GetResponse tokenResponse = credentials.get(params);
					token = tokenResponse.getTicket();
				} catch (Exception ex) {
					log.insert(userID, "CRM Casos", Long.parseLong(caseID),
							"Error obteniendo token de usuario",
							ex.getMessage(), JSON.toString(ex), userIP);
					throw ex;
				}

				try {
					CaseManagementService casemgmt = CaseManagementService_Service.getInstance();
					GetCaseDetailsByIdRequest params = new GetCaseDetailsByIdRequest();
					params.setTicket(token);
					params.setCaseId(caseID);
					caseDetails = casemgmt.getCaseDetailsById(params);
				} catch (Exception ex) {
					log.insert(userID, "CRM Casos", Long.parseLong(caseID),
							"Error consultando detalle del caso",
							ex.getMessage(), JSON.toString(ex), userIP);
					throw ex;
				}

				List<Transaction_Result> results = getResults();
				inter.setInterface_results(results);
				response.setValue(InterfaceOptions.SUCCESS);
				response.setAction(TransactionInterfaceActions.NEXT_STEP);
			}
		} catch (Exception ex) {
			setAsExceptionResponse(response, ex);
		}
		return response;
	}

	private LogDao getEmptyDao() {
		return new LogDao() {
			@Override
			public long getCount(String where, WhereClause whereClause) {
				return 0;
			}

			@Override
			public List<Log> getAll(String where, WhereClause whereClause) {
				return null;
			}

			@Override
			public Log get(long id) {
				return null;
			}

			@Override
			public int insert(Log item) {
				return 0;
			}

			@Override
			public int delete(int id) {
				return 0;
			}
		};
	}

	@Override
	@SuppressWarnings({"RedundantStringToString", "ResultOfMethodCallIgnored"})
	public boolean test() throws Exception {
		InterfaceConfigs.get("crm_ticketbycredentials").toString();
		InterfaceConfigs.get("crm_casemgmt").toString();
		if (Utils.isNullOrWhiteSpaces(InterfaceConfigs.get("crm_casehistory_date_format"))) {
			Utils.logWarn(this.getClass(),
					"No se encontró un valor para 'crm_casehistory_date_format' en el archivo de configuración. "
					+ "Utilizará el formato por defecto: '" + Procedure.DEFAULT_DATE_FORMAT + "'.");
		}
		return super.test();
	}

}
