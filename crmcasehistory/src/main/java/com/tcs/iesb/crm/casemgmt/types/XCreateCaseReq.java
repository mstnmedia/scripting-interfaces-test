
package com.tcs.iesb.crm.casemgmt.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for XCreateCaseReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XCreateCaseReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="phoneStation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="caseTypeLevel1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="severity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="queueName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contractId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="parentCaseId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="autoDispatch" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="visitPeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contactMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="caseTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="caseTypeLevel3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="parentChild" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="contactPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contactLastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="technicianID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accountId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriberNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="visitDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="contactInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contactFirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="caseTypeLevel2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XCreateCaseReq", propOrder = {
    "phoneStation",
    "caseTypeLevel1",
    "severity",
    "queueName",
    "contractId",
    "parentCaseId",
    "productType",
    "autoDispatch",
    "visitPeriod",
    "contactMode",
    "caseTitle",
    "priority",
    "caseTypeLevel3",
    "parentChild",
    "contactPhone",
    "notes",
    "contactLastName",
    "technicianID",
    "accountId",
    "subscriberNumber",
    "visitDate",
    "contactInfo",
    "contactFirstName",
    "caseTypeLevel2"
})
public class XCreateCaseReq {

    @XmlElement(required = true, nillable = true)
    protected String phoneStation;
    @XmlElement(required = true, nillable = true)
    protected String caseTypeLevel1;
    @XmlElement(required = true, nillable = true)
    protected String severity;
    @XmlElement(required = true, nillable = true)
    protected String queueName;
    @XmlElement(required = true, nillable = true)
    protected String contractId;
    @XmlElement(required = true, nillable = true)
    protected String parentCaseId;
    @XmlElement(required = true, nillable = true)
    protected String productType;
    protected boolean autoDispatch;
    @XmlElement(required = true, nillable = true)
    protected String visitPeriod;
    @XmlElement(required = true, nillable = true)
    protected String contactMode;
    @XmlElement(required = true, nillable = true)
    protected String caseTitle;
    @XmlElement(required = true, nillable = true)
    protected String priority;
    @XmlElement(required = true, nillable = true)
    protected String caseTypeLevel3;
    protected int parentChild;
    @XmlElement(required = true, nillable = true)
    protected String contactPhone;
    @XmlElement(required = true, nillable = true)
    protected String notes;
    @XmlElement(required = true, nillable = true)
    protected String contactLastName;
    @XmlElement(required = true, nillable = true)
    protected String technicianID;
    @XmlElement(required = true, nillable = true)
    protected String accountId;
    @XmlElement(required = true, nillable = true)
    protected String subscriberNumber;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar visitDate;
    @XmlElement(required = true, nillable = true)
    protected String contactInfo;
    @XmlElement(required = true, nillable = true)
    protected String contactFirstName;
    @XmlElement(required = true, nillable = true)
    protected String caseTypeLevel2;

    /**
     * Gets the value of the phoneStation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneStation() {
        return phoneStation;
    }

    /**
     * Sets the value of the phoneStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneStation(String value) {
        this.phoneStation = value;
    }

    /**
     * Gets the value of the caseTypeLevel1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTypeLevel1() {
        return caseTypeLevel1;
    }

    /**
     * Sets the value of the caseTypeLevel1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTypeLevel1(String value) {
        this.caseTypeLevel1 = value;
    }

    /**
     * Gets the value of the severity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeverity() {
        return severity;
    }

    /**
     * Sets the value of the severity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeverity(String value) {
        this.severity = value;
    }

    /**
     * Gets the value of the queueName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueueName() {
        return queueName;
    }

    /**
     * Sets the value of the queueName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueueName(String value) {
        this.queueName = value;
    }

    /**
     * Gets the value of the contractId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractId() {
        return contractId;
    }

    /**
     * Sets the value of the contractId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractId(String value) {
        this.contractId = value;
    }

    /**
     * Gets the value of the parentCaseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentCaseId() {
        return parentCaseId;
    }

    /**
     * Sets the value of the parentCaseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentCaseId(String value) {
        this.parentCaseId = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the autoDispatch property.
     * 
     */
    public boolean isAutoDispatch() {
        return autoDispatch;
    }

    /**
     * Sets the value of the autoDispatch property.
     * 
     */
    public void setAutoDispatch(boolean value) {
        this.autoDispatch = value;
    }

    /**
     * Gets the value of the visitPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitPeriod() {
        return visitPeriod;
    }

    /**
     * Sets the value of the visitPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitPeriod(String value) {
        this.visitPeriod = value;
    }

    /**
     * Gets the value of the contactMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactMode() {
        return contactMode;
    }

    /**
     * Sets the value of the contactMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactMode(String value) {
        this.contactMode = value;
    }

    /**
     * Gets the value of the caseTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTitle() {
        return caseTitle;
    }

    /**
     * Sets the value of the caseTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTitle(String value) {
        this.caseTitle = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Gets the value of the caseTypeLevel3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTypeLevel3() {
        return caseTypeLevel3;
    }

    /**
     * Sets the value of the caseTypeLevel3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTypeLevel3(String value) {
        this.caseTypeLevel3 = value;
    }

    /**
     * Gets the value of the parentChild property.
     * 
     */
    public int getParentChild() {
        return parentChild;
    }

    /**
     * Sets the value of the parentChild property.
     * 
     */
    public void setParentChild(int value) {
        this.parentChild = value;
    }

    /**
     * Gets the value of the contactPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * Sets the value of the contactPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPhone(String value) {
        this.contactPhone = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the contactLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactLastName() {
        return contactLastName;
    }

    /**
     * Sets the value of the contactLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactLastName(String value) {
        this.contactLastName = value;
    }

    /**
     * Gets the value of the technicianID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnicianID() {
        return technicianID;
    }

    /**
     * Sets the value of the technicianID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnicianID(String value) {
        this.technicianID = value;
    }

    /**
     * Gets the value of the accountId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountId(String value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the subscriberNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberNumber() {
        return subscriberNumber;
    }

    /**
     * Sets the value of the subscriberNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberNumber(String value) {
        this.subscriberNumber = value;
    }

    /**
     * Gets the value of the visitDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVisitDate() {
        return visitDate;
    }

    /**
     * Sets the value of the visitDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVisitDate(XMLGregorianCalendar value) {
        this.visitDate = value;
    }

    /**
     * Gets the value of the contactInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactInfo() {
        return contactInfo;
    }

    /**
     * Sets the value of the contactInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactInfo(String value) {
        this.contactInfo = value;
    }

    /**
     * Gets the value of the contactFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactFirstName() {
        return contactFirstName;
    }

    /**
     * Sets the value of the contactFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactFirstName(String value) {
        this.contactFirstName = value;
    }

    /**
     * Gets the value of the caseTypeLevel2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTypeLevel2() {
        return caseTypeLevel2;
    }

    /**
     * Sets the value of the caseTypeLevel2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTypeLevel2(String value) {
        this.caseTypeLevel2 = value;
    }

}
