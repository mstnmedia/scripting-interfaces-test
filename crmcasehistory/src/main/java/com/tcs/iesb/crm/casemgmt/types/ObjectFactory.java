
package com.tcs.iesb.crm.casemgmt.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.tcs.iesb.crm.casemgmt.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SearchCaseRequest_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "SearchCaseRequest");
    private final static QName _UpdateCaseResponse_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "UpdateCaseResponse");
    private final static QName _DispatchCaseRequest_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "DispatchCaseRequest");
    private final static QName _ParentChildCaseResponse_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "ParentChildCaseResponse");
    private final static QName _AddCaseNotesResponse_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "AddCaseNotesResponse");
    private final static QName _GetCaseDetailsByIdRequest_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "GetCaseDetailsByIdRequest");
    private final static QName _DispatchCaseResponse_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "DispatchCaseResponse");
    private final static QName _GetCaseDetailsByIdResponse_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "GetCaseDetailsByIdResponse");
    private final static QName _CreateCaseRequest_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "CreateCaseRequest");
    private final static QName _SearchCaseResponse_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "SearchCaseResponse");
    private final static QName _CreateCaseResponse_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "CreateCaseResponse");
    private final static QName _AddCaseNotesRequest_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "AddCaseNotesRequest");
    private final static QName _CloseCaseResponse_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "CloseCaseResponse");
    private final static QName _CloseCaseRequest_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "CloseCaseRequest");
    private final static QName _ParentChildCaseRequest_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "ParentChildCaseRequest");
    private final static QName _UpdateCaseRequest_QNAME = new QName("http://iesb.tcs.com/crm/casemgmt/types/", "UpdateCaseRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tcs.iesb.crm.casemgmt.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UpdateCaseRequest }
     * 
     */
    public UpdateCaseRequest createUpdateCaseRequest() {
        return new UpdateCaseRequest();
    }

    /**
     * Create an instance of {@link CloseCaseRequest }
     * 
     */
    public CloseCaseRequest createCloseCaseRequest() {
        return new CloseCaseRequest();
    }

    /**
     * Create an instance of {@link DispatchCaseResponse }
     * 
     */
    public DispatchCaseResponse createDispatchCaseResponse() {
        return new DispatchCaseResponse();
    }

    /**
     * Create an instance of {@link XDispatchCaseReq }
     * 
     */
    public XDispatchCaseReq createXDispatchCaseReq() {
        return new XDispatchCaseReq();
    }

    /**
     * Create an instance of {@link ParentChildCaseResponse }
     * 
     */
    public ParentChildCaseResponse createParentChildCaseResponse() {
        return new ParentChildCaseResponse();
    }

    /**
     * Create an instance of {@link SearchCaseRequest }
     * 
     */
    public SearchCaseRequest createSearchCaseRequest() {
        return new SearchCaseRequest();
    }

    /**
     * Create an instance of {@link XGetCaseDetailsByIdResp }
     * 
     */
    public XGetCaseDetailsByIdResp createXGetCaseDetailsByIdResp() {
        return new XGetCaseDetailsByIdResp();
    }

    /**
     * Create an instance of {@link FailureReason }
     * 
     */
    public FailureReason createFailureReason() {
        return new FailureReason();
    }

    /**
     * Create an instance of {@link XSearchCaseResp }
     * 
     */
    public XSearchCaseResp createXSearchCaseResp() {
        return new XSearchCaseResp();
    }

    /**
     * Create an instance of {@link XCloseCaseReq }
     * 
     */
    public XCloseCaseReq createXCloseCaseReq() {
        return new XCloseCaseReq();
    }

    /**
     * Create an instance of {@link XFlexAttribute }
     * 
     */
    public XFlexAttribute createXFlexAttribute() {
        return new XFlexAttribute();
    }

    /**
     * Create an instance of {@link GetCaseDetailsByIdRequest }
     * 
     */
    public GetCaseDetailsByIdRequest createGetCaseDetailsByIdRequest() {
        return new GetCaseDetailsByIdRequest();
    }

    /**
     * Create an instance of {@link XDispatchCaseStatusResp }
     * 
     */
    public XDispatchCaseStatusResp createXDispatchCaseStatusResp() {
        return new XDispatchCaseStatusResp();
    }

    /**
     * Create an instance of {@link XParentChildCaseReq }
     * 
     */
    public XParentChildCaseReq createXParentChildCaseReq() {
        return new XParentChildCaseReq();
    }

    /**
     * Create an instance of {@link ParentChildCaseRequest }
     * 
     */
    public ParentChildCaseRequest createParentChildCaseRequest() {
        return new ParentChildCaseRequest();
    }

    /**
     * Create an instance of {@link XUpdateCaseReq }
     * 
     */
    public XUpdateCaseReq createXUpdateCaseReq() {
        return new XUpdateCaseReq();
    }

    /**
     * Create an instance of {@link XCreateCaseReq }
     * 
     */
    public XCreateCaseReq createXCreateCaseReq() {
        return new XCreateCaseReq();
    }

    /**
     * Create an instance of {@link XCreateCaseResp }
     * 
     */
    public XCreateCaseResp createXCreateCaseResp() {
        return new XCreateCaseResp();
    }

    /**
     * Create an instance of {@link CreateCaseRequest }
     * 
     */
    public CreateCaseRequest createCreateCaseRequest() {
        return new CreateCaseRequest();
    }

    /**
     * Create an instance of {@link XAddCaseNotesReq }
     * 
     */
    public XAddCaseNotesReq createXAddCaseNotesReq() {
        return new XAddCaseNotesReq();
    }

    /**
     * Create an instance of {@link DispatchCaseRequest }
     * 
     */
    public DispatchCaseRequest createDispatchCaseRequest() {
        return new DispatchCaseRequest();
    }

    /**
     * Create an instance of {@link XSearchCaseReq }
     * 
     */
    public XSearchCaseReq createXSearchCaseReq() {
        return new XSearchCaseReq();
    }

    /**
     * Create an instance of {@link SearchCaseResponse }
     * 
     */
    public SearchCaseResponse createSearchCaseResponse() {
        return new SearchCaseResponse();
    }

    /**
     * Create an instance of {@link CreateCaseResponse }
     * 
     */
    public CreateCaseResponse createCreateCaseResponse() {
        return new CreateCaseResponse();
    }

    /**
     * Create an instance of {@link XParentChildCaseResp }
     * 
     */
    public XParentChildCaseResp createXParentChildCaseResp() {
        return new XParentChildCaseResp();
    }

    /**
     * Create an instance of {@link AddCaseNotesRequest }
     * 
     */
    public AddCaseNotesRequest createAddCaseNotesRequest() {
        return new AddCaseNotesRequest();
    }

    /**
     * Create an instance of {@link GetCaseDetailsByIdResponse }
     * 
     */
    public GetCaseDetailsByIdResponse createGetCaseDetailsByIdResponse() {
        return new GetCaseDetailsByIdResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCaseRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "SearchCaseRequest")
    public JAXBElement<SearchCaseRequest> createSearchCaseRequest(SearchCaseRequest value) {
        return new JAXBElement<SearchCaseRequest>(_SearchCaseRequest_QNAME, SearchCaseRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FailureReason }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "UpdateCaseResponse")
    public JAXBElement<FailureReason> createUpdateCaseResponse(FailureReason value) {
        return new JAXBElement<FailureReason>(_UpdateCaseResponse_QNAME, FailureReason.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DispatchCaseRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "DispatchCaseRequest")
    public JAXBElement<DispatchCaseRequest> createDispatchCaseRequest(DispatchCaseRequest value) {
        return new JAXBElement<DispatchCaseRequest>(_DispatchCaseRequest_QNAME, DispatchCaseRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParentChildCaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "ParentChildCaseResponse")
    public JAXBElement<ParentChildCaseResponse> createParentChildCaseResponse(ParentChildCaseResponse value) {
        return new JAXBElement<ParentChildCaseResponse>(_ParentChildCaseResponse_QNAME, ParentChildCaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FailureReason }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "AddCaseNotesResponse")
    public JAXBElement<FailureReason> createAddCaseNotesResponse(FailureReason value) {
        return new JAXBElement<FailureReason>(_AddCaseNotesResponse_QNAME, FailureReason.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCaseDetailsByIdRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "GetCaseDetailsByIdRequest")
    public JAXBElement<GetCaseDetailsByIdRequest> createGetCaseDetailsByIdRequest(GetCaseDetailsByIdRequest value) {
        return new JAXBElement<GetCaseDetailsByIdRequest>(_GetCaseDetailsByIdRequest_QNAME, GetCaseDetailsByIdRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DispatchCaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "DispatchCaseResponse")
    public JAXBElement<DispatchCaseResponse> createDispatchCaseResponse(DispatchCaseResponse value) {
        return new JAXBElement<DispatchCaseResponse>(_DispatchCaseResponse_QNAME, DispatchCaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCaseDetailsByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "GetCaseDetailsByIdResponse")
    public JAXBElement<GetCaseDetailsByIdResponse> createGetCaseDetailsByIdResponse(GetCaseDetailsByIdResponse value) {
        return new JAXBElement<GetCaseDetailsByIdResponse>(_GetCaseDetailsByIdResponse_QNAME, GetCaseDetailsByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCaseRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "CreateCaseRequest")
    public JAXBElement<CreateCaseRequest> createCreateCaseRequest(CreateCaseRequest value) {
        return new JAXBElement<CreateCaseRequest>(_CreateCaseRequest_QNAME, CreateCaseRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "SearchCaseResponse")
    public JAXBElement<SearchCaseResponse> createSearchCaseResponse(SearchCaseResponse value) {
        return new JAXBElement<SearchCaseResponse>(_SearchCaseResponse_QNAME, SearchCaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "CreateCaseResponse")
    public JAXBElement<CreateCaseResponse> createCreateCaseResponse(CreateCaseResponse value) {
        return new JAXBElement<CreateCaseResponse>(_CreateCaseResponse_QNAME, CreateCaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCaseNotesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "AddCaseNotesRequest")
    public JAXBElement<AddCaseNotesRequest> createAddCaseNotesRequest(AddCaseNotesRequest value) {
        return new JAXBElement<AddCaseNotesRequest>(_AddCaseNotesRequest_QNAME, AddCaseNotesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FailureReason }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "CloseCaseResponse")
    public JAXBElement<FailureReason> createCloseCaseResponse(FailureReason value) {
        return new JAXBElement<FailureReason>(_CloseCaseResponse_QNAME, FailureReason.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseCaseRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "CloseCaseRequest")
    public JAXBElement<CloseCaseRequest> createCloseCaseRequest(CloseCaseRequest value) {
        return new JAXBElement<CloseCaseRequest>(_CloseCaseRequest_QNAME, CloseCaseRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParentChildCaseRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "ParentChildCaseRequest")
    public JAXBElement<ParentChildCaseRequest> createParentChildCaseRequest(ParentChildCaseRequest value) {
        return new JAXBElement<ParentChildCaseRequest>(_ParentChildCaseRequest_QNAME, ParentChildCaseRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCaseRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iesb.tcs.com/crm/casemgmt/types/", name = "UpdateCaseRequest")
    public JAXBElement<UpdateCaseRequest> createUpdateCaseRequest(UpdateCaseRequest value) {
        return new JAXBElement<UpdateCaseRequest>(_UpdateCaseRequest_QNAME, UpdateCaseRequest.class, null, value);
    }

}
