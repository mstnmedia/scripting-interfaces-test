
package com.tcs.iesb.crm.casemgmt.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateCaseResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateCaseResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CaseDetails" type="{http://iesb.tcs.com/crm/casemgmt/types/}XCreateCaseResp"/>
 *         &lt;element name="FailureReason" type="{http://iesb.tcs.com/crm/casemgmt/types/}FailureReason"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateCaseResponse", propOrder = {
    "caseDetails",
    "failureReason"
})
public class CreateCaseResponse {

    @XmlElement(name = "CaseDetails", required = true, nillable = true)
    protected XCreateCaseResp caseDetails;
    @XmlElement(name = "FailureReason", required = true, nillable = true)
    protected FailureReason failureReason;

    /**
     * Gets the value of the caseDetails property.
     * 
     * @return
     *     possible object is
     *     {@link XCreateCaseResp }
     *     
     */
    public XCreateCaseResp getCaseDetails() {
        return caseDetails;
    }

    /**
     * Sets the value of the caseDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link XCreateCaseResp }
     *     
     */
    public void setCaseDetails(XCreateCaseResp value) {
        this.caseDetails = value;
    }

    /**
     * Gets the value of the failureReason property.
     * 
     * @return
     *     possible object is
     *     {@link FailureReason }
     *     
     */
    public FailureReason getFailureReason() {
        return failureReason;
    }

    /**
     * Sets the value of the failureReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailureReason }
     *     
     */
    public void setFailureReason(FailureReason value) {
        this.failureReason = value;
    }

}
