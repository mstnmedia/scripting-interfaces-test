
package com.tcs.iesb.crm.casemgmt.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XCloseCaseReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XCloseCaseReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rootCause" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rootSubCause" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="caseId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="adjustmentAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="technicianId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="maintenanceContract" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="adjustmentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="resolution" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dispositionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dispositionSubCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XCloseCaseReq", propOrder = {
    "rootCause",
    "rootSubCause",
    "caseId",
    "adjustmentAmount",
    "technicianId",
    "notes",
    "maintenanceContract",
    "adjustmentType",
    "status",
    "resolution",
    "dispositionCode",
    "dispositionSubCode"
})
public class XCloseCaseReq {

    @XmlElement(required = true, nillable = true)
    protected String rootCause;
    @XmlElement(required = true, nillable = true)
    protected String rootSubCause;
    @XmlElement(required = true, nillable = true)
    protected String caseId;
    protected double adjustmentAmount;
    @XmlElement(required = true, nillable = true)
    protected String technicianId;
    @XmlElement(required = true, nillable = true)
    protected String notes;
    protected boolean maintenanceContract;
    @XmlElement(required = true, nillable = true)
    protected String adjustmentType;
    @XmlElement(required = true, nillable = true)
    protected String status;
    @XmlElement(required = true, nillable = true)
    protected String resolution;
    @XmlElement(required = true, nillable = true)
    protected String dispositionCode;
    @XmlElement(required = true, nillable = true)
    protected String dispositionSubCode;

    /**
     * Gets the value of the rootCause property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRootCause() {
        return rootCause;
    }

    /**
     * Sets the value of the rootCause property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRootCause(String value) {
        this.rootCause = value;
    }

    /**
     * Gets the value of the rootSubCause property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRootSubCause() {
        return rootSubCause;
    }

    /**
     * Sets the value of the rootSubCause property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRootSubCause(String value) {
        this.rootSubCause = value;
    }

    /**
     * Gets the value of the caseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseId() {
        return caseId;
    }

    /**
     * Sets the value of the caseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseId(String value) {
        this.caseId = value;
    }

    /**
     * Gets the value of the adjustmentAmount property.
     * 
     */
    public double getAdjustmentAmount() {
        return adjustmentAmount;
    }

    /**
     * Sets the value of the adjustmentAmount property.
     * 
     */
    public void setAdjustmentAmount(double value) {
        this.adjustmentAmount = value;
    }

    /**
     * Gets the value of the technicianId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnicianId() {
        return technicianId;
    }

    /**
     * Sets the value of the technicianId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnicianId(String value) {
        this.technicianId = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the maintenanceContract property.
     * 
     */
    public boolean isMaintenanceContract() {
        return maintenanceContract;
    }

    /**
     * Sets the value of the maintenanceContract property.
     * 
     */
    public void setMaintenanceContract(boolean value) {
        this.maintenanceContract = value;
    }

    /**
     * Gets the value of the adjustmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdjustmentType() {
        return adjustmentType;
    }

    /**
     * Sets the value of the adjustmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdjustmentType(String value) {
        this.adjustmentType = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the resolution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResolution() {
        return resolution;
    }

    /**
     * Sets the value of the resolution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResolution(String value) {
        this.resolution = value;
    }

    /**
     * Gets the value of the dispositionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispositionCode() {
        return dispositionCode;
    }

    /**
     * Sets the value of the dispositionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispositionCode(String value) {
        this.dispositionCode = value;
    }

    /**
     * Gets the value of the dispositionSubCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispositionSubCode() {
        return dispositionSubCode;
    }

    /**
     * Sets the value of the dispositionSubCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispositionSubCode(String value) {
        this.dispositionSubCode = value;
    }

}
