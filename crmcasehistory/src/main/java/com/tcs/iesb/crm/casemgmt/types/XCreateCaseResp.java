
package com.tcs.iesb.crm.casemgmt.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for XCreateCaseResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XCreateCaseResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="caseId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SLAFixTargetDt" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="SLAFixTargetTime" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XCreateCaseResp", propOrder = {
    "caseId",
    "slaFixTargetDt",
    "slaFixTargetTime"
})
public class XCreateCaseResp {

    @XmlElement(required = true, nillable = true)
    protected String caseId;
    @XmlElement(name = "SLAFixTargetDt", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar slaFixTargetDt;
    @XmlElement(name = "SLAFixTargetTime")
    protected double slaFixTargetTime;

    /**
     * Gets the value of the caseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseId() {
        return caseId;
    }

    /**
     * Sets the value of the caseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseId(String value) {
        this.caseId = value;
    }

    /**
     * Gets the value of the slaFixTargetDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSLAFixTargetDt() {
        return slaFixTargetDt;
    }

    /**
     * Sets the value of the slaFixTargetDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSLAFixTargetDt(XMLGregorianCalendar value) {
        this.slaFixTargetDt = value;
    }

    /**
     * Gets the value of the slaFixTargetTime property.
     * 
     */
    public double getSLAFixTargetTime() {
        return slaFixTargetTime;
    }

    /**
     * Sets the value of the slaFixTargetTime property.
     * 
     */
    public void setSLAFixTargetTime(double value) {
        this.slaFixTargetTime = value;
    }

}
