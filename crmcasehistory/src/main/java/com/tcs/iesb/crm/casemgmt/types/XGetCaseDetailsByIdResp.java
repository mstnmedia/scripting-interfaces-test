
package com.tcs.iesb.crm.casemgmt.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for XGetCaseDetailsByIdResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XGetCaseDetailsByIdResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="caseTypeLevel1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SLAFixTargetDt" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="severity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="condition" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contractId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="parentCaseId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="visitPeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SLAFixTargetTime" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="contactMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="queue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="caseTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="caseTypeLevel3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="parentChild" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="originator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accountId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriberNumberStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriberNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="visitDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="contactInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="maintenanceContractIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="createDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="history" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="flexAttributes" type="{http://iesb.tcs.com/crm/casemgmt/types/}XFlexAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="caseTypeLevel2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="closeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XGetCaseDetailsByIdResp", propOrder = {
    "caseTypeLevel1",
    "slaFixTargetDt",
    "severity",
    "condition",
    "contractId",
    "parentCaseId",
    "productType",
    "visitPeriod",
    "slaFixTargetTime",
    "contactMode",
    "queue",
    "caseTitle",
    "caseTypeLevel3",
    "priority",
    "parentChild",
    "user",
    "originator",
    "status",
    "accountId",
    "subscriberNumberStatus",
    "subscriberNumber",
    "visitDate",
    "contactInfo",
    "maintenanceContractIndicator",
    "createDate",
    "history",
    "flexAttributes",
    "caseTypeLevel2",
    "closeDate"
})
public class XGetCaseDetailsByIdResp {

    @XmlElement(required = true, nillable = true)
    protected String caseTypeLevel1;
    @XmlElement(name = "SLAFixTargetDt", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar slaFixTargetDt;
    @XmlElement(required = true, nillable = true)
    protected String severity;
    @XmlElement(required = true, nillable = true)
    protected String condition;
    @XmlElement(required = true, nillable = true)
    protected String contractId;
    @XmlElement(required = true, nillable = true)
    protected String parentCaseId;
    @XmlElement(required = true, nillable = true)
    protected String productType;
    @XmlElement(required = true, nillable = true)
    protected String visitPeriod;
    @XmlElement(name = "SLAFixTargetTime")
    protected double slaFixTargetTime;
    @XmlElement(required = true, nillable = true)
    protected String contactMode;
    @XmlElement(required = true, nillable = true)
    protected String queue;
    @XmlElement(required = true, nillable = true)
    protected String caseTitle;
    @XmlElement(required = true, nillable = true)
    protected String caseTypeLevel3;
    @XmlElement(required = true, nillable = true)
    protected String priority;
    protected int parentChild;
    @XmlElement(required = true, nillable = true)
    protected String user;
    @XmlElement(required = true, nillable = true)
    protected String originator;
    @XmlElement(required = true, nillable = true)
    protected String status;
    @XmlElement(required = true, nillable = true)
    protected String accountId;
    @XmlElement(required = true, nillable = true)
    protected String subscriberNumberStatus;
    @XmlElement(required = true, nillable = true)
    protected String subscriberNumber;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar visitDate;
    @XmlElement(required = true, nillable = true)
    protected String contactInfo;
    protected boolean maintenanceContractIndicator;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDate;
    @XmlElement(required = true, nillable = true)
    protected String history;
    @XmlElement(nillable = true)
    protected List<XFlexAttribute> flexAttributes;
    @XmlElement(required = true, nillable = true)
    protected String caseTypeLevel2;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar closeDate;

    /**
     * Gets the value of the caseTypeLevel1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTypeLevel1() {
        return caseTypeLevel1;
    }

    /**
     * Sets the value of the caseTypeLevel1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTypeLevel1(String value) {
        this.caseTypeLevel1 = value;
    }

    /**
     * Gets the value of the slaFixTargetDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSLAFixTargetDt() {
        return slaFixTargetDt;
    }

    /**
     * Sets the value of the slaFixTargetDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSLAFixTargetDt(XMLGregorianCalendar value) {
        this.slaFixTargetDt = value;
    }

    /**
     * Gets the value of the severity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeverity() {
        return severity;
    }

    /**
     * Sets the value of the severity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeverity(String value) {
        this.severity = value;
    }

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCondition(String value) {
        this.condition = value;
    }

    /**
     * Gets the value of the contractId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractId() {
        return contractId;
    }

    /**
     * Sets the value of the contractId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractId(String value) {
        this.contractId = value;
    }

    /**
     * Gets the value of the parentCaseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentCaseId() {
        return parentCaseId;
    }

    /**
     * Sets the value of the parentCaseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentCaseId(String value) {
        this.parentCaseId = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the visitPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitPeriod() {
        return visitPeriod;
    }

    /**
     * Sets the value of the visitPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitPeriod(String value) {
        this.visitPeriod = value;
    }

    /**
     * Gets the value of the slaFixTargetTime property.
     * 
     */
    public double getSLAFixTargetTime() {
        return slaFixTargetTime;
    }

    /**
     * Sets the value of the slaFixTargetTime property.
     * 
     */
    public void setSLAFixTargetTime(double value) {
        this.slaFixTargetTime = value;
    }

    /**
     * Gets the value of the contactMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactMode() {
        return contactMode;
    }

    /**
     * Sets the value of the contactMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactMode(String value) {
        this.contactMode = value;
    }

    /**
     * Gets the value of the queue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueue() {
        return queue;
    }

    /**
     * Sets the value of the queue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueue(String value) {
        this.queue = value;
    }

    /**
     * Gets the value of the caseTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTitle() {
        return caseTitle;
    }

    /**
     * Sets the value of the caseTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTitle(String value) {
        this.caseTitle = value;
    }

    /**
     * Gets the value of the caseTypeLevel3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTypeLevel3() {
        return caseTypeLevel3;
    }

    /**
     * Sets the value of the caseTypeLevel3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTypeLevel3(String value) {
        this.caseTypeLevel3 = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Gets the value of the parentChild property.
     * 
     */
    public int getParentChild() {
        return parentChild;
    }

    /**
     * Sets the value of the parentChild property.
     * 
     */
    public void setParentChild(int value) {
        this.parentChild = value;
    }

    /**
     * Gets the value of the user property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUser(String value) {
        this.user = value;
    }

    /**
     * Gets the value of the originator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginator() {
        return originator;
    }

    /**
     * Sets the value of the originator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginator(String value) {
        this.originator = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the accountId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountId(String value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the subscriberNumberStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberNumberStatus() {
        return subscriberNumberStatus;
    }

    /**
     * Sets the value of the subscriberNumberStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberNumberStatus(String value) {
        this.subscriberNumberStatus = value;
    }

    /**
     * Gets the value of the subscriberNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberNumber() {
        return subscriberNumber;
    }

    /**
     * Sets the value of the subscriberNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberNumber(String value) {
        this.subscriberNumber = value;
    }

    /**
     * Gets the value of the visitDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVisitDate() {
        return visitDate;
    }

    /**
     * Sets the value of the visitDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVisitDate(XMLGregorianCalendar value) {
        this.visitDate = value;
    }

    /**
     * Gets the value of the contactInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactInfo() {
        return contactInfo;
    }

    /**
     * Sets the value of the contactInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactInfo(String value) {
        this.contactInfo = value;
    }

    /**
     * Gets the value of the maintenanceContractIndicator property.
     * 
     */
    public boolean isMaintenanceContractIndicator() {
        return maintenanceContractIndicator;
    }

    /**
     * Sets the value of the maintenanceContractIndicator property.
     * 
     */
    public void setMaintenanceContractIndicator(boolean value) {
        this.maintenanceContractIndicator = value;
    }

    /**
     * Gets the value of the createDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDate(XMLGregorianCalendar value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the history property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistory() {
        return history;
    }

    /**
     * Sets the value of the history property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistory(String value) {
        this.history = value;
    }

    /**
     * Gets the value of the flexAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flexAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlexAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XFlexAttribute }
     * 
     * 
     */
    public List<XFlexAttribute> getFlexAttributes() {
        if (flexAttributes == null) {
            flexAttributes = new ArrayList<XFlexAttribute>();
        }
        return this.flexAttributes;
    }

    /**
     * Gets the value of the caseTypeLevel2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTypeLevel2() {
        return caseTypeLevel2;
    }

    /**
     * Sets the value of the caseTypeLevel2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTypeLevel2(String value) {
        this.caseTypeLevel2 = value;
    }

    /**
     * Gets the value of the closeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCloseDate() {
        return closeDate;
    }

    /**
     * Sets the value of the closeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCloseDate(XMLGregorianCalendar value) {
        this.closeDate = value;
    }

}
