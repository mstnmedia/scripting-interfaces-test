
package com.tcs.iesb.crm.casemgmt.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for XUpdateCaseReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XUpdateCaseReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="visitPeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="visitDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="phoneStation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="caseId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="technicianId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FlexAttributes" type="{http://iesb.tcs.com/crm/casemgmt/types/}XFlexAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XUpdateCaseReq", propOrder = {
    "visitPeriod",
    "visitDate",
    "phoneStation",
    "caseId",
    "technicianId",
    "status",
    "flexAttributes"
})
public class XUpdateCaseReq {

    @XmlElement(required = true, nillable = true)
    protected String visitPeriod;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar visitDate;
    @XmlElement(required = true, nillable = true)
    protected String phoneStation;
    @XmlElement(required = true, nillable = true)
    protected String caseId;
    @XmlElement(required = true, nillable = true)
    protected String technicianId;
    @XmlElement(required = true, nillable = true)
    protected String status;
    @XmlElement(name = "FlexAttributes", nillable = true)
    protected List<XFlexAttribute> flexAttributes;

    /**
     * Gets the value of the visitPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitPeriod() {
        return visitPeriod;
    }

    /**
     * Sets the value of the visitPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitPeriod(String value) {
        this.visitPeriod = value;
    }

    /**
     * Gets the value of the visitDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVisitDate() {
        return visitDate;
    }

    /**
     * Sets the value of the visitDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVisitDate(XMLGregorianCalendar value) {
        this.visitDate = value;
    }

    /**
     * Gets the value of the phoneStation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneStation() {
        return phoneStation;
    }

    /**
     * Sets the value of the phoneStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneStation(String value) {
        this.phoneStation = value;
    }

    /**
     * Gets the value of the caseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseId() {
        return caseId;
    }

    /**
     * Sets the value of the caseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseId(String value) {
        this.caseId = value;
    }

    /**
     * Gets the value of the technicianId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnicianId() {
        return technicianId;
    }

    /**
     * Sets the value of the technicianId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnicianId(String value) {
        this.technicianId = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the flexAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flexAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlexAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XFlexAttribute }
     * 
     * 
     */
    public List<XFlexAttribute> getFlexAttributes() {
        if (flexAttributes == null) {
            flexAttributes = new ArrayList<XFlexAttribute>();
        }
        return this.flexAttributes;
    }

}
