/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.Employee;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.casehistory.Procedure;
import com.mstn.scripting.interfaces.casehistory.CaseHistoryDyn;
import com.mstn.scripting.interfaces.casehistory.CaseHistorySta;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Clase ejecutable con métodos de pruebas para probar las diferentes
 * funcionalidades en este proyecto.
 *
 * @author amatos
 */
public class Main {

	static TransactionInterfacesPayload payload = new TransactionInterfacesPayload(new V_Transaction(), new Workflow_Step(), "{}");
//	static String caseID = "10002557";
	static String caseID = "10002558";

	static Object procedure() throws Exception {
		TimeZone.setDefault(TimeZone.getTimeZone("America/Caracas"));
		Locale.setDefault(Locale.forLanguageTag("es-DO"));
		Procedure procedure = new Procedure();
		Object records = procedure.get(caseID);
		return records;
	}

	static Object testCaseHistoryDyn() throws Exception {
		CaseHistoryDyn service = new CaseHistoryDyn();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces
				.stream()
				.filter(item -> item.getName().endsWith("get"))
				.findFirst().get();
		ObjectNode form = JSON.newObjectNode();
		String[] value = new String[]{caseID};
		for (int i = 0; i < inter.getInterface_field().size(); i++) {
			Interface_Field field = inter.getInterface_field().get(i);
			form.put(field.getName(), value[i]);
		}
		payload.setForm(form.toString());
		TransactionInterfacesResponse response = service.callMethod(inter, payload, null);
		return response;
	}

	static Object testCaseHistorySta() throws Exception {
		CaseHistorySta service = new CaseHistorySta();
		ObjectNode form = JSON.newObjectNode().put(service.CASE_ID, caseID);
		payload.setForm(form.toString());
		int userID = 319290;
		Employee employee = new Employee(userID, 0, 0, 0, null, "", "", "", "", "OPITEL");
		User user = new User(userID, "", "", null, employee, null, null, "172.23.45.67", new Date(), 0);
		TransactionInterfacesResponse response = service.getForm(payload, user);
		return response;
	}

	public static void main(String[] args) throws Exception {
		InterfaceConfigs.setConfig();
		Object result = null;
//		result = procedure();
//		result = testCaseHistoryDyn();
		result = testCaseHistorySta();

		System.out.println(JSON.toString(result));
	}

}
