
package _do.com.claro.soa.model.customer;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InteractionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InteractionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CALL"/>
 *     &lt;enumeration value="EMAIL"/>
 *     &lt;enumeration value="FAX"/>
 *     &lt;enumeration value="IVR"/>
 *     &lt;enumeration value="LETTER"/>
 *     &lt;enumeration value="OTHERS"/>
 *     &lt;enumeration value="WEB"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InteractionType")
@XmlEnum
public enum InteractionType {

    CALL,
    EMAIL,
    FAX,
    IVR,
    LETTER,
    OTHERS,
    WEB;

    public String value() {
        return name();
    }

    public static InteractionType fromValue(String v) {
        return valueOf(v);
    }

}
