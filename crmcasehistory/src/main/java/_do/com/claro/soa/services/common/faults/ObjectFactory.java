
package _do.com.claro.soa.services.common.faults;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro.soa.services.common.faults package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro.soa.services.common.faults
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UnsupportedCustomerType }
     * 
     */
    public UnsupportedCustomerType createUnsupportedCustomerType() {
        return new UnsupportedCustomerType();
    }

    /**
     * Create an instance of {@link UnsupportedCustomerId }
     * 
     */
    public UnsupportedCustomerId createUnsupportedCustomerId() {
        return new UnsupportedCustomerId();
    }

    /**
     * Create an instance of {@link UserAuthenticationError }
     * 
     */
    public UserAuthenticationError createUserAuthenticationError() {
        return new UserAuthenticationError();
    }

    /**
     * Create an instance of {@link IllegalArgument }
     * 
     */
    public IllegalArgument createIllegalArgument() {
        return new IllegalArgument();
    }

}
