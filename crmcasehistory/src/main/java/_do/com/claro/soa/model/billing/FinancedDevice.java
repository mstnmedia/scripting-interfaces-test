
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.generic.TelephoneNumber;


/**
 * 
 *         @Created SOA-260
 *         Represents information concerning the financing of a device
 *         This entity is represented by the following fields:
 * 
 *         -telephoneNumber: Indicates the subscriberNumber tied to the financing.
 *         -billingAccountNumber: Represents the BAN tied to the financing.
 *         -itemID: Represents the id of the financed device.
 *         -deviceDescription: A short description a the financed device.
 *         -serialNumber: A serial that represents a device.
 *         -serialType: represents the type of serialNumber(e.x: SIM,IMEI,etc)
 *         -financing: Represents the payment information about the financed device.
 * 
 *         @modified SOA-303:
 *         -Added billingAccountNumber field.
 *       
 * 
 * <p>Java class for FinancedDevice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancedDevice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/generic}telephoneNumber"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}billingAccountNumber"/>
 *         &lt;element name="itemID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="deviceDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serialType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}financingPaymentsDetails"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancedDevice", propOrder = {
    "telephoneNumber",
    "billingAccountNumber",
    "itemID",
    "deviceDescription",
    "serialNumber",
    "serialType",
    "financingPaymentsDetails"
})
public class FinancedDevice {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/generic", required = true)
    protected TelephoneNumber telephoneNumber;
    @XmlElement(required = true, nillable = true)
    protected String billingAccountNumber;
    @XmlElement(required = true)
    protected String itemID;
    @XmlElement(required = true)
    protected String deviceDescription;
    @XmlElement(required = true)
    protected String serialNumber;
    @XmlElement(required = true)
    protected String serialType;
    @XmlElement(required = true)
    protected FinancingPaymentDetails financingPaymentsDetails;

    /**
     * Gets the value of the telephoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneNumber }
     *     
     */
    public TelephoneNumber getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * Sets the value of the telephoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneNumber }
     *     
     */
    public void setTelephoneNumber(TelephoneNumber value) {
        this.telephoneNumber = value;
    }

    /**
     * Gets the value of the billingAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingAccountNumber() {
        return billingAccountNumber;
    }

    /**
     * Sets the value of the billingAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingAccountNumber(String value) {
        this.billingAccountNumber = value;
    }

    /**
     * Gets the value of the itemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemID() {
        return itemID;
    }

    /**
     * Sets the value of the itemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemID(String value) {
        this.itemID = value;
    }

    /**
     * Gets the value of the deviceDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceDescription() {
        return deviceDescription;
    }

    /**
     * Sets the value of the deviceDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceDescription(String value) {
        this.deviceDescription = value;
    }

    /**
     * Gets the value of the serialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets the value of the serialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

    /**
     * Gets the value of the serialType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialType() {
        return serialType;
    }

    /**
     * Sets the value of the serialType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialType(String value) {
        this.serialType = value;
    }

    /**
     * Gets the value of the financingPaymentsDetails property.
     * 
     * @return
     *     possible object is
     *     {@link FinancingPaymentDetails }
     *     
     */
    public FinancingPaymentDetails getFinancingPaymentsDetails() {
        return financingPaymentsDetails;
    }

    /**
     * Sets the value of the financingPaymentsDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancingPaymentDetails }
     *     
     */
    public void setFinancingPaymentsDetails(FinancingPaymentDetails value) {
        this.financingPaymentsDetails = value;
    }

}
