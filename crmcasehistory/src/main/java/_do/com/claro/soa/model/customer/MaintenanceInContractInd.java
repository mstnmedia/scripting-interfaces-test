
package _do.com.claro.soa.model.customer;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MaintenanceInContractInd.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MaintenanceInContractInd">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TRUE"/>
 *     &lt;enumeration value="FALSE"/>
 *     &lt;enumeration value="NOT_AVAILABLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MaintenanceInContractInd")
@XmlEnum
public enum MaintenanceInContractInd {

    TRUE,
    FALSE,
    NOT_AVAILABLE;

    public String value() {
        return name();
    }

    public static MaintenanceInContractInd fromValue(String v) {
        return valueOf(v);
    }

}
