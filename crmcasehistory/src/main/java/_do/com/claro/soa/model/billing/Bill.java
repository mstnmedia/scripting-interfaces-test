
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 *         @Created: SOA-187
 *         Represents the information contained in a bill
 *       
 * 
 * <p>Java class for Bill complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Bill">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sequenceNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="billingAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="balanceAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="billDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="coverageStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="coverageEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="billCloseDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="billCloseDay" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="billType" type="{http://www.claro.com.do/soa/model/billing}BillType"/>
 *         &lt;element name="confirmationStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="previousBalanceAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="paymentsReceivedAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="creditsOrAjustAppliedAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="pastDueAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="currentChargesOrCreditsAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="totalDueAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="billsAverageAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="dueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cycleCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cycleRunMonth" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cycleRunYear" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Bill", propOrder = {
    "sequenceNumber",
    "billingAccountNumber",
    "balanceAmount",
    "billDate",
    "coverageStartDate",
    "coverageEndDate",
    "billCloseDate",
    "billCloseDay",
    "billType",
    "confirmationStatus",
    "previousBalanceAmount",
    "paymentsReceivedAmount",
    "creditsOrAjustAppliedAmount",
    "pastDueAmount",
    "currentChargesOrCreditsAmount",
    "totalDueAmount",
    "billsAverageAmount",
    "dueDate",
    "email",
    "cycleCode",
    "cycleRunMonth",
    "cycleRunYear"
})
public class Bill {

    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer sequenceNumber;
    @XmlElement(required = true, nillable = true)
    protected String billingAccountNumber;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double balanceAmount;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar billDate;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar coverageStartDate;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar coverageEndDate;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar billCloseDate;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer billCloseDay;
    @XmlElement(required = true)
    protected BillType billType;
    @XmlElement(required = true, nillable = true)
    protected String confirmationStatus;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double previousBalanceAmount;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double paymentsReceivedAmount;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double creditsOrAjustAppliedAmount;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double pastDueAmount;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double currentChargesOrCreditsAmount;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double totalDueAmount;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double billsAverageAmount;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dueDate;
    @XmlElement(required = true, nillable = true)
    protected String email;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer cycleCode;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer cycleRunMonth;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer cycleRunYear;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSequenceNumber(Integer value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the billingAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingAccountNumber() {
        return billingAccountNumber;
    }

    /**
     * Sets the value of the billingAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingAccountNumber(String value) {
        this.billingAccountNumber = value;
    }

    /**
     * Gets the value of the balanceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBalanceAmount() {
        return balanceAmount;
    }

    /**
     * Sets the value of the balanceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBalanceAmount(Double value) {
        this.balanceAmount = value;
    }

    /**
     * Gets the value of the billDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBillDate() {
        return billDate;
    }

    /**
     * Sets the value of the billDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBillDate(XMLGregorianCalendar value) {
        this.billDate = value;
    }

    /**
     * Gets the value of the coverageStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverageStartDate() {
        return coverageStartDate;
    }

    /**
     * Sets the value of the coverageStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverageStartDate(XMLGregorianCalendar value) {
        this.coverageStartDate = value;
    }

    /**
     * Gets the value of the coverageEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverageEndDate() {
        return coverageEndDate;
    }

    /**
     * Sets the value of the coverageEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverageEndDate(XMLGregorianCalendar value) {
        this.coverageEndDate = value;
    }

    /**
     * Gets the value of the billCloseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBillCloseDate() {
        return billCloseDate;
    }

    /**
     * Sets the value of the billCloseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBillCloseDate(XMLGregorianCalendar value) {
        this.billCloseDate = value;
    }

    /**
     * Gets the value of the billCloseDay property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBillCloseDay() {
        return billCloseDay;
    }

    /**
     * Sets the value of the billCloseDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBillCloseDay(Integer value) {
        this.billCloseDay = value;
    }

    /**
     * Gets the value of the billType property.
     * 
     * @return
     *     possible object is
     *     {@link BillType }
     *     
     */
    public BillType getBillType() {
        return billType;
    }

    /**
     * Sets the value of the billType property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillType }
     *     
     */
    public void setBillType(BillType value) {
        this.billType = value;
    }

    /**
     * Gets the value of the confirmationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmationStatus() {
        return confirmationStatus;
    }

    /**
     * Sets the value of the confirmationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmationStatus(String value) {
        this.confirmationStatus = value;
    }

    /**
     * Gets the value of the previousBalanceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPreviousBalanceAmount() {
        return previousBalanceAmount;
    }

    /**
     * Sets the value of the previousBalanceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPreviousBalanceAmount(Double value) {
        this.previousBalanceAmount = value;
    }

    /**
     * Gets the value of the paymentsReceivedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPaymentsReceivedAmount() {
        return paymentsReceivedAmount;
    }

    /**
     * Sets the value of the paymentsReceivedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPaymentsReceivedAmount(Double value) {
        this.paymentsReceivedAmount = value;
    }

    /**
     * Gets the value of the creditsOrAjustAppliedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCreditsOrAjustAppliedAmount() {
        return creditsOrAjustAppliedAmount;
    }

    /**
     * Sets the value of the creditsOrAjustAppliedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCreditsOrAjustAppliedAmount(Double value) {
        this.creditsOrAjustAppliedAmount = value;
    }

    /**
     * Gets the value of the pastDueAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPastDueAmount() {
        return pastDueAmount;
    }

    /**
     * Sets the value of the pastDueAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPastDueAmount(Double value) {
        this.pastDueAmount = value;
    }

    /**
     * Gets the value of the currentChargesOrCreditsAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCurrentChargesOrCreditsAmount() {
        return currentChargesOrCreditsAmount;
    }

    /**
     * Sets the value of the currentChargesOrCreditsAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCurrentChargesOrCreditsAmount(Double value) {
        this.currentChargesOrCreditsAmount = value;
    }

    /**
     * Gets the value of the totalDueAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTotalDueAmount() {
        return totalDueAmount;
    }

    /**
     * Sets the value of the totalDueAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTotalDueAmount(Double value) {
        this.totalDueAmount = value;
    }

    /**
     * Gets the value of the billsAverageAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBillsAverageAmount() {
        return billsAverageAmount;
    }

    /**
     * Sets the value of the billsAverageAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBillsAverageAmount(Double value) {
        this.billsAverageAmount = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDueDate(XMLGregorianCalendar value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the cycleCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCycleCode() {
        return cycleCode;
    }

    /**
     * Sets the value of the cycleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCycleCode(Integer value) {
        this.cycleCode = value;
    }

    /**
     * Gets the value of the cycleRunMonth property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCycleRunMonth() {
        return cycleRunMonth;
    }

    /**
     * Sets the value of the cycleRunMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCycleRunMonth(Integer value) {
        this.cycleRunMonth = value;
    }

    /**
     * Gets the value of the cycleRunYear property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCycleRunYear() {
        return cycleRunYear;
    }

    /**
     * Sets the value of the cycleRunYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCycleRunYear(Integer value) {
        this.cycleRunYear = value;
    }

}
