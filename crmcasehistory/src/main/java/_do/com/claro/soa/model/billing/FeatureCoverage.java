
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for featureCoverage.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="featureCoverage">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EHA"/>
 *     &lt;enumeration value="HOME"/>
 *     &lt;enumeration value="LSA"/>
 *     &lt;enumeration value="TOLL"/>
 *     &lt;enumeration value="ROAM"/>
 *     &lt;enumeration value="TRUE-HOME"/>
 *     &lt;enumeration value="LOCAL-TOLL"/>
 *     &lt;enumeration value="ROAM-TOLL"/>
 *     &lt;enumeration value="ROAM-LOCAL-TOLL"/>
 *     &lt;enumeration value="TOLL-DISCOUNT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "featureCoverage")
@XmlEnum
public enum FeatureCoverage {

    EHA("EHA"),
    HOME("HOME"),
    LSA("LSA"),
    TOLL("TOLL"),
    ROAM("ROAM"),
    @XmlEnumValue("TRUE-HOME")
    TRUE_HOME("TRUE-HOME"),
    @XmlEnumValue("LOCAL-TOLL")
    LOCAL_TOLL("LOCAL-TOLL"),
    @XmlEnumValue("ROAM-TOLL")
    ROAM_TOLL("ROAM-TOLL"),
    @XmlEnumValue("ROAM-LOCAL-TOLL")
    ROAM_LOCAL_TOLL("ROAM-LOCAL-TOLL"),
    @XmlEnumValue("TOLL-DISCOUNT")
    TOLL_DISCOUNT("TOLL-DISCOUNT");
    private final String value;

    FeatureCoverage(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FeatureCoverage fromValue(String v) {
        for (FeatureCoverage c: FeatureCoverage.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
