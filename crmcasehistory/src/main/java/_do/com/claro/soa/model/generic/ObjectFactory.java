
package _do.com.claro.soa.model.generic;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro.soa.model.generic package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Money_QNAME = new QName("http://www.claro.com.do/soa/model/generic", "money");
    private final static QName _SmsPlanUsage_QNAME = new QName("http://www.claro.com.do/soa/model/generic", "smsPlanUsage");
    private final static QName _CustomerDevice_QNAME = new QName("http://www.claro.com.do/soa/model/generic", "customerDevice");
    private final static QName _PhoneString_QNAME = new QName("http://www.claro.com.do/soa/model/generic", "phoneString");
    private final static QName _VoicePlanUsage_QNAME = new QName("http://www.claro.com.do/soa/model/generic", "voicePlanUsage");
    private final static QName _TelephoneNumber_QNAME = new QName("http://www.claro.com.do/soa/model/generic", "telephoneNumber");
    private final static QName _Gender_QNAME = new QName("http://www.claro.com.do/soa/model/generic", "gender");
    private final static QName _FaxString_QNAME = new QName("http://www.claro.com.do/soa/model/generic", "faxString");
    private final static QName _Phone_QNAME = new QName("http://www.claro.com.do/soa/model/generic", "phone");
    private final static QName _SimCard_QNAME = new QName("http://www.claro.com.do/soa/model/generic", "simCard");
    private final static QName _Balance_QNAME = new QName("http://www.claro.com.do/soa/model/generic", "balance");
    private final static QName _Fax_QNAME = new QName("http://www.claro.com.do/soa/model/generic", "fax");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro.soa.model.generic
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VoicePlanUsage }
     * 
     */
    public VoicePlanUsage createVoicePlanUsage() {
        return new VoicePlanUsage();
    }

    /**
     * Create an instance of {@link SmsPlanUsage }
     * 
     */
    public SmsPlanUsage createSmsPlanUsage() {
        return new SmsPlanUsage();
    }

    /**
     * Create an instance of {@link Frequency }
     * 
     */
    public Frequency createFrequency() {
        return new Frequency();
    }

    /**
     * Create an instance of {@link CustomerDevice }
     * 
     */
    public CustomerDevice createCustomerDevice() {
        return new CustomerDevice();
    }

    /**
     * Create an instance of {@link Money }
     * 
     */
    public Money createMoney() {
        return new Money();
    }

    /**
     * Create an instance of {@link SimCard }
     * 
     */
    public SimCard createSimCard() {
        return new SimCard();
    }

    /**
     * Create an instance of {@link TelephoneNumber }
     * 
     */
    public TelephoneNumber createTelephoneNumber() {
        return new TelephoneNumber();
    }

    /**
     * Create an instance of {@link Balance }
     * 
     */
    public Balance createBalance() {
        return new Balance();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Money }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/generic", name = "money")
    public JAXBElement<Money> createMoney(Money value) {
        return new JAXBElement<Money>(_Money_QNAME, Money.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SmsPlanUsage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/generic", name = "smsPlanUsage")
    public JAXBElement<SmsPlanUsage> createSmsPlanUsage(SmsPlanUsage value) {
        return new JAXBElement<SmsPlanUsage>(_SmsPlanUsage_QNAME, SmsPlanUsage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerDevice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/generic", name = "customerDevice")
    public JAXBElement<CustomerDevice> createCustomerDevice(CustomerDevice value) {
        return new JAXBElement<CustomerDevice>(_CustomerDevice_QNAME, CustomerDevice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/generic", name = "phoneString")
    public JAXBElement<String> createPhoneString(String value) {
        return new JAXBElement<String>(_PhoneString_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VoicePlanUsage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/generic", name = "voicePlanUsage")
    public JAXBElement<VoicePlanUsage> createVoicePlanUsage(VoicePlanUsage value) {
        return new JAXBElement<VoicePlanUsage>(_VoicePlanUsage_QNAME, VoicePlanUsage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TelephoneNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/generic", name = "telephoneNumber")
    public JAXBElement<TelephoneNumber> createTelephoneNumber(TelephoneNumber value) {
        return new JAXBElement<TelephoneNumber>(_TelephoneNumber_QNAME, TelephoneNumber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Gender }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/generic", name = "gender")
    public JAXBElement<Gender> createGender(Gender value) {
        return new JAXBElement<Gender>(_Gender_QNAME, Gender.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/generic", name = "faxString")
    public JAXBElement<String> createFaxString(String value) {
        return new JAXBElement<String>(_FaxString_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TelephoneNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/generic", name = "phone")
    public JAXBElement<TelephoneNumber> createPhone(TelephoneNumber value) {
        return new JAXBElement<TelephoneNumber>(_Phone_QNAME, TelephoneNumber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/generic", name = "simCard")
    public JAXBElement<SimCard> createSimCard(SimCard value) {
        return new JAXBElement<SimCard>(_SimCard_QNAME, SimCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Balance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/generic", name = "balance")
    public JAXBElement<Balance> createBalance(Balance value) {
        return new JAXBElement<Balance>(_Balance_QNAME, Balance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TelephoneNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/generic", name = "fax")
    public JAXBElement<TelephoneNumber> createFax(TelephoneNumber value) {
        return new JAXBElement<TelephoneNumber>(_Fax_QNAME, TelephoneNumber.class, null, value);
    }

}
