
package _do.com.claro.soa.model.customer;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EntityType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EntityType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DEFAULT"/>
 *     &lt;enumeration value="CASE"/>
 *     &lt;enumeration value="SUBCASE"/>
 *     &lt;enumeration value="CONTACT"/>
 *     &lt;enumeration value="OPORTUNITY"/>
 *     &lt;enumeration value="ACTION_ITEM"/>
 *     &lt;enumeration value="LITERATURE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EntityType")
@XmlEnum
public enum EntityType {

    DEFAULT,
    CASE,
    SUBCASE,
    CONTACT,
    OPORTUNITY,
    ACTION_ITEM,
    LITERATURE;

    public String value() {
        return name();
    }

    public static EntityType fromValue(String v) {
        return valueOf(v);
    }

}
