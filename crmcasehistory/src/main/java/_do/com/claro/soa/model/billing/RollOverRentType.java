
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RollOverRentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RollOverRentType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MONEY"/>
 *     &lt;enumeration value="MINUTES"/>
 *     &lt;enumeration value="DATA"/>
 *     &lt;enumeration value="OTHER"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RollOverRentType")
@XmlEnum
public enum RollOverRentType {

    MONEY,
    MINUTES,
    DATA,
    OTHER;

    public String value() {
        return name();
    }

    public static RollOverRentType fromValue(String v) {
        return valueOf(v);
    }

}
