
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import _do.com.claro.soa.model.customer.CustomerID;
import _do.com.claro.soa.model.generic.TelephoneNumber;
import _do.com.claro.soa.model.product.Product;


/**
 * 
 *         @Created: Genesis...
 *         Entity that returns all the information asociated to a subscription.
 *         Ex: Status, Owner Account, billing plan, productType.
 *         
 *         @Modified: SOA-238
 *         Including futureBillingPlan in case that applies for a mobile subscription.
 * 		
 *         @Modified: SOA-337
 *         Remove payOnBillAvailability
 * 		
 *         @Modified: SOA-368
 *         Adding Rollover RentType
 *       
 * 
 * <p>Java class for Subscription complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Subscription">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}customerID"/>
 *         &lt;element name="taxID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}subscriberNo"/>
 *         &lt;element name="subscriptionStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="status" type="{http://www.claro.com.do/soa/model/billing}SubscriptionStatus"/>
 *         &lt;element name="statusLastUpdate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}billingPlan"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}futureBillingPlan"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/product}product" minOccurs="0"/>
 *         &lt;element name="billingAccountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rolloverRentType" type="{http://www.claro.com.do/soa/model/billing}RollOverRentType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Subscription", propOrder = {
    "customerID",
    "taxID",
    "subscriberNo",
    "subscriptionStartDate",
    "status",
    "statusLastUpdate",
    "billingPlan",
    "futureBillingPlan",
    "product",
    "billingAccountNo",
    "rolloverRentType"
})
public class Subscription {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/customer", required = true)
    protected CustomerID customerID;
    @XmlElement(required = true, nillable = true)
    protected String taxID;
    @XmlElement(required = true, nillable = true)
    protected TelephoneNumber subscriberNo;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar subscriptionStartDate;
    @XmlElement(required = true)
    protected SubscriptionStatus status;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusLastUpdate;
    @XmlElement(required = true, nillable = true)
    protected BillingPlan billingPlan;
    @XmlElement(required = true, nillable = true)
    protected FutureBillingPlan futureBillingPlan;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/product", nillable = true)
    protected Product product;
    @XmlElement(required = true, nillable = true)
    protected String billingAccountNo;
    @XmlElement(required = true)
    protected RollOverRentType rolloverRentType;

    /**
     * Gets the value of the customerID property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerID }
     *     
     */
    public CustomerID getCustomerID() {
        return customerID;
    }

    /**
     * Sets the value of the customerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerID }
     *     
     */
    public void setCustomerID(CustomerID value) {
        this.customerID = value;
    }

    /**
     * Gets the value of the taxID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxID() {
        return taxID;
    }

    /**
     * Sets the value of the taxID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxID(String value) {
        this.taxID = value;
    }

    /**
     * Gets the value of the subscriberNo property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneNumber }
     *     
     */
    public TelephoneNumber getSubscriberNo() {
        return subscriberNo;
    }

    /**
     * Sets the value of the subscriberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneNumber }
     *     
     */
    public void setSubscriberNo(TelephoneNumber value) {
        this.subscriberNo = value;
    }

    /**
     * Gets the value of the subscriptionStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSubscriptionStartDate() {
        return subscriptionStartDate;
    }

    /**
     * Sets the value of the subscriptionStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSubscriptionStartDate(XMLGregorianCalendar value) {
        this.subscriptionStartDate = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriptionStatus }
     *     
     */
    public SubscriptionStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriptionStatus }
     *     
     */
    public void setStatus(SubscriptionStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the statusLastUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatusLastUpdate() {
        return statusLastUpdate;
    }

    /**
     * Sets the value of the statusLastUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatusLastUpdate(XMLGregorianCalendar value) {
        this.statusLastUpdate = value;
    }

    /**
     * Gets the value of the billingPlan property.
     * 
     * @return
     *     possible object is
     *     {@link BillingPlan }
     *     
     */
    public BillingPlan getBillingPlan() {
        return billingPlan;
    }

    /**
     * Sets the value of the billingPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingPlan }
     *     
     */
    public void setBillingPlan(BillingPlan value) {
        this.billingPlan = value;
    }

    /**
     * Gets the value of the futureBillingPlan property.
     * 
     * @return
     *     possible object is
     *     {@link FutureBillingPlan }
     *     
     */
    public FutureBillingPlan getFutureBillingPlan() {
        return futureBillingPlan;
    }

    /**
     * Sets the value of the futureBillingPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link FutureBillingPlan }
     *     
     */
    public void setFutureBillingPlan(FutureBillingPlan value) {
        this.futureBillingPlan = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link Product }
     *     
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link Product }
     *     
     */
    public void setProduct(Product value) {
        this.product = value;
    }

    /**
     * Gets the value of the billingAccountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingAccountNo() {
        return billingAccountNo;
    }

    /**
     * Sets the value of the billingAccountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingAccountNo(String value) {
        this.billingAccountNo = value;
    }

    /**
     * Gets the value of the rolloverRentType property.
     * 
     * @return
     *     possible object is
     *     {@link RollOverRentType }
     *     
     */
    public RollOverRentType getRolloverRentType() {
        return rolloverRentType;
    }

    /**
     * Sets the value of the rolloverRentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RollOverRentType }
     *     
     */
    public void setRolloverRentType(RollOverRentType value) {
        this.rolloverRentType = value;
    }

}
