
package _do.com.claro.soa.model.customer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerID complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerID">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}transientID" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerID", propOrder = {
    "transientID"
})
public class CustomerID {

    protected TransientID transientID;

    /**
     * Gets the value of the transientID property.
     * 
     * @return
     *     possible object is
     *     {@link TransientID }
     *     
     */
    public TransientID getTransientID() {
        return transientID;
    }

    /**
     * Sets the value of the transientID property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransientID }
     *     
     */
    public void setTransientID(TransientID value) {
        this.transientID = value;
    }

}
