
package _do.com.claro.soa.model.product;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ComponentStatusName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ComponentStatusName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACTIVE"/>
 *     &lt;enumeration value="SUSPENDED"/>
 *     &lt;enumeration value="CEASED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ComponentStatusName")
@XmlEnum
public enum ComponentStatusName {

    ACTIVE,
    SUSPENDED,
    CEASED;

    public String value() {
        return name();
    }

    public static ComponentStatusName fromValue(String v) {
        return valueOf(v);
    }

}
