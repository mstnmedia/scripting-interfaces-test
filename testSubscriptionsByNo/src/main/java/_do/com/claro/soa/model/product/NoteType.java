
package _do.com.claro.soa.model.product;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NoteType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NoteType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TECHNICAL"/>
 *     &lt;enumeration value="SALES"/>
 *     &lt;enumeration value="NOTE"/>
 *     &lt;enumeration value="FREE_TEXT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "NoteType")
@XmlEnum
public enum NoteType {

    TECHNICAL,
    SALES,
    NOTE,
    FREE_TEXT;

    public String value() {
        return name();
    }

    public static NoteType fromValue(String v) {
        return valueOf(v);
    }

}
