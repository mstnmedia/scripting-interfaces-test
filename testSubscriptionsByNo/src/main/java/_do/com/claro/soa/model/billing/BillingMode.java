
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillingMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BillingMode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PREPAID"/>
 *     &lt;enumeration value="POSTPAID"/>
 *     &lt;enumeration value="MIXED"/>
 *     &lt;enumeration value="CELLPHONE_FLEET"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BillingMode")
@XmlEnum
public enum BillingMode {

    PREPAID,
    POSTPAID,
    MIXED,
    CELLPHONE_FLEET;

    public String value() {
        return name();
    }

    public static BillingMode fromValue(String v) {
        return valueOf(v);
    }

}
