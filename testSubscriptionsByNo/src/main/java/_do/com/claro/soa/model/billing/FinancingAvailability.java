
package _do.com.claro.soa.model.billing;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.generic.Money;


/**
 * 
 *         @Created SOA-303:
 *         Represents the financing availability details for this ban, this type is composed by:
 * 
 *         -activeFinancings: Used to represents how many active financings a BAN has.
 *         -amountAvailableForFinancing: The total amount that can be financed to a BAN.
 *       
 * 
 * <p>Java class for FinancingAvailability complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancingAvailability">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="activeFinancings" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="amountAvailableForFinancing" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancingAvailability", propOrder = {
    "activeFinancings",
    "amountAvailableForFinancing"
})
public class FinancingAvailability {

    @XmlElement(required = true, nillable = true)
    protected BigInteger activeFinancings;
    @XmlElement(required = true, nillable = true)
    protected Money amountAvailableForFinancing;

    /**
     * Gets the value of the activeFinancings property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getActiveFinancings() {
        return activeFinancings;
    }

    /**
     * Sets the value of the activeFinancings property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setActiveFinancings(BigInteger value) {
        this.activeFinancings = value;
    }

    /**
     * Gets the value of the amountAvailableForFinancing property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getAmountAvailableForFinancing() {
        return amountAvailableForFinancing;
    }

    /**
     * Sets the value of the amountAvailableForFinancing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setAmountAvailableForFinancing(Money value) {
        this.amountAvailableForFinancing = value;
    }

}
