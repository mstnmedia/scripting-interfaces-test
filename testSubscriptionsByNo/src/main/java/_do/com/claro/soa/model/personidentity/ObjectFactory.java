
package _do.com.claro.soa.model.personidentity;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro.soa.model.personidentity package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IdentityCardID_QNAME = new QName("http://www.claro.com.do/soa/model/personidentity", "identityCardID");
    private final static QName _WorkPermit_QNAME = new QName("http://www.claro.com.do/soa/model/personidentity", "workPermit");
    private final static QName _IdentityCard_QNAME = new QName("http://www.claro.com.do/soa/model/personidentity", "identityCard");
    private final static QName _Other_QNAME = new QName("http://www.claro.com.do/soa/model/personidentity", "other");
    private final static QName _PersonID_QNAME = new QName("http://www.claro.com.do/soa/model/personidentity", "personID");
    private final static QName _Names_QNAME = new QName("http://www.claro.com.do/soa/model/personidentity", "names");
    private final static QName _TaxID_QNAME = new QName("http://www.claro.com.do/soa/model/personidentity", "taxID");
    private final static QName _Passport_QNAME = new QName("http://www.claro.com.do/soa/model/personidentity", "passport");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro.soa.model.personidentity
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IdentityCard }
     * 
     */
    public IdentityCard createIdentityCard() {
        return new IdentityCard();
    }

    /**
     * Create an instance of {@link Passport }
     * 
     */
    public Passport createPassport() {
        return new Passport();
    }

    /**
     * Create an instance of {@link WorkPermit }
     * 
     */
    public WorkPermit createWorkPermit() {
        return new WorkPermit();
    }

    /**
     * Create an instance of {@link IdentityCardID }
     * 
     */
    public IdentityCardID createIdentityCardID() {
        return new IdentityCardID();
    }

    /**
     * Create an instance of {@link TaxID }
     * 
     */
    public TaxID createTaxID() {
        return new TaxID();
    }

    /**
     * Create an instance of {@link PersonID }
     * 
     */
    public PersonID createPersonID() {
        return new PersonID();
    }

    /**
     * Create an instance of {@link Names }
     * 
     */
    public Names createNames() {
        return new Names();
    }

    /**
     * Create an instance of {@link Other }
     * 
     */
    public Other createOther() {
        return new Other();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentityCardID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/personidentity", name = "identityCardID")
    public JAXBElement<IdentityCardID> createIdentityCardID(IdentityCardID value) {
        return new JAXBElement<IdentityCardID>(_IdentityCardID_QNAME, IdentityCardID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WorkPermit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/personidentity", name = "workPermit")
    public JAXBElement<WorkPermit> createWorkPermit(WorkPermit value) {
        return new JAXBElement<WorkPermit>(_WorkPermit_QNAME, WorkPermit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentityCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/personidentity", name = "identityCard")
    public JAXBElement<IdentityCard> createIdentityCard(IdentityCard value) {
        return new JAXBElement<IdentityCard>(_IdentityCard_QNAME, IdentityCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Other }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/personidentity", name = "other")
    public JAXBElement<Other> createOther(Other value) {
        return new JAXBElement<Other>(_Other_QNAME, Other.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/personidentity", name = "personID")
    public JAXBElement<PersonID> createPersonID(PersonID value) {
        return new JAXBElement<PersonID>(_PersonID_QNAME, PersonID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Names }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/personidentity", name = "names")
    public JAXBElement<Names> createNames(Names value) {
        return new JAXBElement<Names>(_Names_QNAME, Names.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/personidentity", name = "taxID")
    public JAXBElement<TaxID> createTaxID(TaxID value) {
        return new JAXBElement<TaxID>(_TaxID_QNAME, TaxID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Passport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/personidentity", name = "passport")
    public JAXBElement<Passport> createPassport(Passport value) {
        return new JAXBElement<Passport>(_Passport_QNAME, Passport.class, null, value);
    }

}
