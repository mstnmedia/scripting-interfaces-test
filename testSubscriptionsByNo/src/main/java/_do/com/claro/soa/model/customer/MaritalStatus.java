
package _do.com.claro.soa.model.customer;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MaritalStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MaritalStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MARRIED"/>
 *     &lt;enumeration value="SINGLE"/>
 *     &lt;enumeration value="DIVORCED"/>
 *     &lt;enumeration value="WIDOWED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MaritalStatus")
@XmlEnum
public enum MaritalStatus {

    MARRIED,
    SINGLE,
    DIVORCED,
    WIDOWED;

    public String value() {
        return name();
    }

    public static MaritalStatus fromValue(String v) {
        return valueOf(v);
    }

}
