
package _do.com.claro.soa.model.customer;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import _do.com.claro.soa.model.employee.Employee;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro.soa.model.customer package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CustomerType_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "customerType");
    private final static QName _InteractionType_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "interactionType");
    private final static QName _Contact_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "contact");
    private final static QName _MaintenanceInContractInd_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "maintenanceInContractInd");
    private final static QName _AlternateContactMethod_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "alternateContactMethod");
    private final static QName _Notes_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "notes");
    private final static QName _CustomerID_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "customerID");
    private final static QName _Topic_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "topic");
    private final static QName _Diagnosis_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "diagnosis");
    private final static QName _Attachment_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "attachment");
    private final static QName _Subcase_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "subcase");
    private final static QName _Note_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "note");
    private final static QName _CaseContacts_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "caseContacts");
    private final static QName _AlternateContactMethods_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "alternateContactMethods");
    private final static QName _Interactions_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "interactions");
    private final static QName _Interaction_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "interaction");
    private final static QName _FlexibleAttributes_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "flexibleAttributes");
    private final static QName _Customer_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "customer");
    private final static QName _AccountExecutive_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "accountExecutive");
    private final static QName _Cases_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "cases");
    private final static QName _InteractionDirection_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "interactionDirection");
    private final static QName _CaseContact_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "caseContact");
    private final static QName _Case_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "case");
    private final static QName _AccountExecutives_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "accountExecutives");
    private final static QName _ActionItems_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "actionItems");
    private final static QName _Channel_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "channel");
    private final static QName _AccountOfficer_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "accountOfficer");
    private final static QName _ActionItem_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "actionItem");
    private final static QName _FlexibleAttribute_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "flexibleAttribute");
    private final static QName _Subcases_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "subcases");
    private final static QName _AlternateContactMethodType_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "alternateContactMethodType");
    private final static QName _AdditionalContact_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "additionalContact");
    private final static QName _Diagnoses_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "diagnoses");
    private final static QName _CustomerSegment_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "customerSegment");
    private final static QName _Customers_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "customers");
    private final static QName _Attachments_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "attachments");
    private final static QName _TransientID_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "transientID");
    private final static QName _AdditionalContacts_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "additionalContacts");
    private final static QName _Contacts_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "contacts");
    private final static QName _Creator_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "creator");
    private final static QName _Topics_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "topics");
    private final static QName _LetterPrintableInd_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "letterPrintableInd");
    private final static QName _Closer_QNAME = new QName("http://www.claro.com.do/soa/model/customer", "closer");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro.soa.model.customer
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Interaction }
     * 
     */
    public Interaction createInteraction() {
        return new Interaction();
    }

    /**
     * Create an instance of {@link SubCase }
     * 
     */
    public SubCase createSubCase() {
        return new SubCase();
    }

    /**
     * Create an instance of {@link ActionItem }
     * 
     */
    public ActionItem createActionItem() {
        return new ActionItem();
    }

    /**
     * Create an instance of {@link _do.com.claro.soa.model.customer.Creator }
     * 
     */
    public _do.com.claro.soa.model.customer.Creator createCreator() {
        return new _do.com.claro.soa.model.customer.Creator();
    }

    /**
     * Create an instance of {@link Cases }
     * 
     */
    public Cases createCases() {
        return new Cases();
    }

    /**
     * Create an instance of {@link Customers }
     * 
     */
    public Customers createCustomers() {
        return new Customers();
    }

    /**
     * Create an instance of {@link ActionItems }
     * 
     */
    public ActionItems createActionItems() {
        return new ActionItems();
    }

    /**
     * Create an instance of {@link AccountExecutive }
     * 
     */
    public AccountExecutive createAccountExecutive() {
        return new AccountExecutive();
    }

    /**
     * Create an instance of {@link Closer }
     * 
     */
    public Closer createCloser() {
        return new Closer();
    }

    /**
     * Create an instance of {@link Case }
     * 
     */
    public Case createCase() {
        return new Case();
    }

    /**
     * Create an instance of {@link AdditionalContact }
     * 
     */
    public AdditionalContact createAdditionalContact() {
        return new AdditionalContact();
    }

    /**
     * Create an instance of {@link Attachment }
     * 
     */
    public Attachment createAttachment() {
        return new Attachment();
    }

    /**
     * Create an instance of {@link Attachments }
     * 
     */
    public Attachments createAttachments() {
        return new Attachments();
    }

    /**
     * Create an instance of {@link Interactions }
     * 
     */
    public Interactions createInteractions() {
        return new Interactions();
    }

    /**
     * Create an instance of {@link Diagnoses }
     * 
     */
    public Diagnoses createDiagnoses() {
        return new Diagnoses();
    }

    /**
     * Create an instance of {@link Note }
     * 
     */
    public Note createNote() {
        return new Note();
    }

    /**
     * Create an instance of {@link CaseContacts }
     * 
     */
    public CaseContacts createCaseContacts() {
        return new CaseContacts();
    }

    /**
     * Create an instance of {@link FlexibleAttributes }
     * 
     */
    public FlexibleAttributes createFlexibleAttributes() {
        return new FlexibleAttributes();
    }

    /**
     * Create an instance of {@link SubCases }
     * 
     */
    public SubCases createSubCases() {
        return new SubCases();
    }

    /**
     * Create an instance of {@link Contacts }
     * 
     */
    public Contacts createContacts() {
        return new Contacts();
    }

    /**
     * Create an instance of {@link CaseContact }
     * 
     */
    public CaseContact createCaseContact() {
        return new CaseContact();
    }

    /**
     * Create an instance of {@link Interaction.Creator }
     * 
     */
    public Interaction.Creator createInteractionCreator() {
        return new Interaction.Creator();
    }

    /**
     * Create an instance of {@link Notes }
     * 
     */
    public Notes createNotes() {
        return new Notes();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link AlternateContactMethods }
     * 
     */
    public AlternateContactMethods createAlternateContactMethods() {
        return new AlternateContactMethods();
    }

    /**
     * Create an instance of {@link Topics }
     * 
     */
    public Topics createTopics() {
        return new Topics();
    }

    /**
     * Create an instance of {@link AdditionalContacts }
     * 
     */
    public AdditionalContacts createAdditionalContacts() {
        return new AdditionalContacts();
    }

    /**
     * Create an instance of {@link CustomerSegment }
     * 
     */
    public CustomerSegment createCustomerSegment() {
        return new CustomerSegment();
    }

    /**
     * Create an instance of {@link CustomerID }
     * 
     */
    public CustomerID createCustomerID() {
        return new CustomerID();
    }

    /**
     * Create an instance of {@link Channel }
     * 
     */
    public Channel createChannel() {
        return new Channel();
    }

    /**
     * Create an instance of {@link FlexibleAttribute }
     * 
     */
    public FlexibleAttribute createFlexibleAttribute() {
        return new FlexibleAttribute();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link Topic }
     * 
     */
    public Topic createTopic() {
        return new Topic();
    }

    /**
     * Create an instance of {@link TransientID }
     * 
     */
    public TransientID createTransientID() {
        return new TransientID();
    }

    /**
     * Create an instance of {@link AccountExecutives }
     * 
     */
    public AccountExecutives createAccountExecutives() {
        return new AccountExecutives();
    }

    /**
     * Create an instance of {@link Diagnosis }
     * 
     */
    public Diagnosis createDiagnosis() {
        return new Diagnosis();
    }

    /**
     * Create an instance of {@link AlternateContactMethod }
     * 
     */
    public AlternateContactMethod createAlternateContactMethod() {
        return new AlternateContactMethod();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "customerType")
    public JAXBElement<CustomerType> createCustomerType(CustomerType value) {
        return new JAXBElement<CustomerType>(_CustomerType_QNAME, CustomerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InteractionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "interactionType")
    public JAXBElement<InteractionType> createInteractionType(InteractionType value) {
        return new JAXBElement<InteractionType>(_InteractionType_QNAME, InteractionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Contact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "contact")
    public JAXBElement<Contact> createContact(Contact value) {
        return new JAXBElement<Contact>(_Contact_QNAME, Contact.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintenanceInContractInd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "maintenanceInContractInd")
    public JAXBElement<MaintenanceInContractInd> createMaintenanceInContractInd(MaintenanceInContractInd value) {
        return new JAXBElement<MaintenanceInContractInd>(_MaintenanceInContractInd_QNAME, MaintenanceInContractInd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlternateContactMethod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "alternateContactMethod")
    public JAXBElement<AlternateContactMethod> createAlternateContactMethod(AlternateContactMethod value) {
        return new JAXBElement<AlternateContactMethod>(_AlternateContactMethod_QNAME, AlternateContactMethod.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Notes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "notes")
    public JAXBElement<Notes> createNotes(Notes value) {
        return new JAXBElement<Notes>(_Notes_QNAME, Notes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "customerID")
    public JAXBElement<CustomerID> createCustomerID(CustomerID value) {
        return new JAXBElement<CustomerID>(_CustomerID_QNAME, CustomerID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Topic }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "topic")
    public JAXBElement<Topic> createTopic(Topic value) {
        return new JAXBElement<Topic>(_Topic_QNAME, Topic.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Diagnosis }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "diagnosis")
    public JAXBElement<Diagnosis> createDiagnosis(Diagnosis value) {
        return new JAXBElement<Diagnosis>(_Diagnosis_QNAME, Diagnosis.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Attachment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "attachment")
    public JAXBElement<Attachment> createAttachment(Attachment value) {
        return new JAXBElement<Attachment>(_Attachment_QNAME, Attachment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubCase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "subcase")
    public JAXBElement<SubCase> createSubcase(SubCase value) {
        return new JAXBElement<SubCase>(_Subcase_QNAME, SubCase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Note }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "note")
    public JAXBElement<Note> createNote(Note value) {
        return new JAXBElement<Note>(_Note_QNAME, Note.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CaseContacts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "caseContacts")
    public JAXBElement<CaseContacts> createCaseContacts(CaseContacts value) {
        return new JAXBElement<CaseContacts>(_CaseContacts_QNAME, CaseContacts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlternateContactMethods }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "alternateContactMethods")
    public JAXBElement<AlternateContactMethods> createAlternateContactMethods(AlternateContactMethods value) {
        return new JAXBElement<AlternateContactMethods>(_AlternateContactMethods_QNAME, AlternateContactMethods.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Interactions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "interactions")
    public JAXBElement<Interactions> createInteractions(Interactions value) {
        return new JAXBElement<Interactions>(_Interactions_QNAME, Interactions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Interaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "interaction")
    public JAXBElement<Interaction> createInteraction(Interaction value) {
        return new JAXBElement<Interaction>(_Interaction_QNAME, Interaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FlexibleAttributes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "flexibleAttributes")
    public JAXBElement<FlexibleAttributes> createFlexibleAttributes(FlexibleAttributes value) {
        return new JAXBElement<FlexibleAttributes>(_FlexibleAttributes_QNAME, FlexibleAttributes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Customer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "customer")
    public JAXBElement<Customer> createCustomer(Customer value) {
        return new JAXBElement<Customer>(_Customer_QNAME, Customer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountExecutive }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "accountExecutive")
    public JAXBElement<AccountExecutive> createAccountExecutive(AccountExecutive value) {
        return new JAXBElement<AccountExecutive>(_AccountExecutive_QNAME, AccountExecutive.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cases }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "cases")
    public JAXBElement<Cases> createCases(Cases value) {
        return new JAXBElement<Cases>(_Cases_QNAME, Cases.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InteractionDirection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "interactionDirection")
    public JAXBElement<InteractionDirection> createInteractionDirection(InteractionDirection value) {
        return new JAXBElement<InteractionDirection>(_InteractionDirection_QNAME, InteractionDirection.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CaseContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "caseContact")
    public JAXBElement<CaseContact> createCaseContact(CaseContact value) {
        return new JAXBElement<CaseContact>(_CaseContact_QNAME, CaseContact.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Case }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "case")
    public JAXBElement<Case> createCase(Case value) {
        return new JAXBElement<Case>(_Case_QNAME, Case.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountExecutives }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "accountExecutives")
    public JAXBElement<AccountExecutives> createAccountExecutives(AccountExecutives value) {
        return new JAXBElement<AccountExecutives>(_AccountExecutives_QNAME, AccountExecutives.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActionItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "actionItems")
    public JAXBElement<ActionItems> createActionItems(ActionItems value) {
        return new JAXBElement<ActionItems>(_ActionItems_QNAME, ActionItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Channel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "channel")
    public JAXBElement<Channel> createChannel(Channel value) {
        return new JAXBElement<Channel>(_Channel_QNAME, Channel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Employee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "accountOfficer")
    public JAXBElement<Employee> createAccountOfficer(Employee value) {
        return new JAXBElement<Employee>(_AccountOfficer_QNAME, Employee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActionItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "actionItem")
    public JAXBElement<ActionItem> createActionItem(ActionItem value) {
        return new JAXBElement<ActionItem>(_ActionItem_QNAME, ActionItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FlexibleAttribute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "flexibleAttribute")
    public JAXBElement<FlexibleAttribute> createFlexibleAttribute(FlexibleAttribute value) {
        return new JAXBElement<FlexibleAttribute>(_FlexibleAttribute_QNAME, FlexibleAttribute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubCases }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "subcases")
    public JAXBElement<SubCases> createSubcases(SubCases value) {
        return new JAXBElement<SubCases>(_Subcases_QNAME, SubCases.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlternateContactMethodType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "alternateContactMethodType")
    public JAXBElement<AlternateContactMethodType> createAlternateContactMethodType(AlternateContactMethodType value) {
        return new JAXBElement<AlternateContactMethodType>(_AlternateContactMethodType_QNAME, AlternateContactMethodType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdditionalContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "additionalContact")
    public JAXBElement<AdditionalContact> createAdditionalContact(AdditionalContact value) {
        return new JAXBElement<AdditionalContact>(_AdditionalContact_QNAME, AdditionalContact.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Diagnoses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "diagnoses")
    public JAXBElement<Diagnoses> createDiagnoses(Diagnoses value) {
        return new JAXBElement<Diagnoses>(_Diagnoses_QNAME, Diagnoses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerSegment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "customerSegment")
    public JAXBElement<CustomerSegment> createCustomerSegment(CustomerSegment value) {
        return new JAXBElement<CustomerSegment>(_CustomerSegment_QNAME, CustomerSegment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Customers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "customers")
    public JAXBElement<Customers> createCustomers(Customers value) {
        return new JAXBElement<Customers>(_Customers_QNAME, Customers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Attachments }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "attachments")
    public JAXBElement<Attachments> createAttachments(Attachments value) {
        return new JAXBElement<Attachments>(_Attachments_QNAME, Attachments.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransientID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "transientID")
    public JAXBElement<TransientID> createTransientID(TransientID value) {
        return new JAXBElement<TransientID>(_TransientID_QNAME, TransientID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdditionalContacts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "additionalContacts")
    public JAXBElement<AdditionalContacts> createAdditionalContacts(AdditionalContacts value) {
        return new JAXBElement<AdditionalContacts>(_AdditionalContacts_QNAME, AdditionalContacts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Contacts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "contacts")
    public JAXBElement<Contacts> createContacts(Contacts value) {
        return new JAXBElement<Contacts>(_Contacts_QNAME, Contacts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link _do.com.claro.soa.model.customer.Creator }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "creator")
    public JAXBElement<_do.com.claro.soa.model.customer.Creator> createCreator(_do.com.claro.soa.model.customer.Creator value) {
        return new JAXBElement<_do.com.claro.soa.model.customer.Creator>(_Creator_QNAME, _do.com.claro.soa.model.customer.Creator.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Topics }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "topics")
    public JAXBElement<Topics> createTopics(Topics value) {
        return new JAXBElement<Topics>(_Topics_QNAME, Topics.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LetterPrintableInd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "letterPrintableInd")
    public JAXBElement<LetterPrintableInd> createLetterPrintableInd(LetterPrintableInd value) {
        return new JAXBElement<LetterPrintableInd>(_LetterPrintableInd_QNAME, LetterPrintableInd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Closer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/customer", name = "closer")
    public JAXBElement<Closer> createCloser(Closer value) {
        return new JAXBElement<Closer>(_Closer_QNAME, Closer.class, null, value);
    }

}
