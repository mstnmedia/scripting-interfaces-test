
package _do.com.claro.soa.model.address;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro.soa.model.address package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Address_QNAME = new QName("http://www.claro.com.do/soa/model/address", "address");
    private final static QName _AddressDetails_QNAME = new QName("http://www.claro.com.do/soa/model/address", "addressDetails");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro.soa.model.address
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddressDetails }
     * 
     */
    public AddressDetails createAddressDetails() {
        return new AddressDetails();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/address", name = "address")
    public JAXBElement<Address> createAddress(Address value) {
        return new JAXBElement<Address>(_Address_QNAME, Address.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddressDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/address", name = "addressDetails")
    public JAXBElement<AddressDetails> createAddressDetails(AddressDetails value) {
        return new JAXBElement<AddressDetails>(_AddressDetails_QNAME, AddressDetails.class, null, value);
    }

}
