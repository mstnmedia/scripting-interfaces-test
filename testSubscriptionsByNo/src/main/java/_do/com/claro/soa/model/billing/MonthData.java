
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         @Created SOA-235
 *       
 * 
 * <p>Java class for MonthData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MonthData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="jan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="feb" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mar" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="apr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="may" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="jun" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="jul" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="aug" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sep" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="oct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nov" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dec" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MonthData", propOrder = {
    "jan",
    "feb",
    "mar",
    "apr",
    "may",
    "jun",
    "jul",
    "aug",
    "sep",
    "oct",
    "nov",
    "dec"
})
public class MonthData {

    @XmlElement(required = true, nillable = true)
    protected String jan;
    @XmlElement(required = true, nillable = true)
    protected String feb;
    @XmlElement(required = true, nillable = true)
    protected String mar;
    @XmlElement(required = true, nillable = true)
    protected String apr;
    @XmlElement(required = true, nillable = true)
    protected String may;
    @XmlElement(required = true, nillable = true)
    protected String jun;
    @XmlElement(required = true, nillable = true)
    protected String jul;
    @XmlElement(required = true, nillable = true)
    protected String aug;
    @XmlElement(required = true, nillable = true)
    protected String sep;
    @XmlElement(required = true, nillable = true)
    protected String oct;
    @XmlElement(required = true, nillable = true)
    protected String nov;
    @XmlElement(required = true, nillable = true)
    protected String dec;

    /**
     * Gets the value of the jan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJan() {
        return jan;
    }

    /**
     * Sets the value of the jan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJan(String value) {
        this.jan = value;
    }

    /**
     * Gets the value of the feb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeb() {
        return feb;
    }

    /**
     * Sets the value of the feb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeb(String value) {
        this.feb = value;
    }

    /**
     * Gets the value of the mar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMar() {
        return mar;
    }

    /**
     * Sets the value of the mar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMar(String value) {
        this.mar = value;
    }

    /**
     * Gets the value of the apr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApr() {
        return apr;
    }

    /**
     * Sets the value of the apr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApr(String value) {
        this.apr = value;
    }

    /**
     * Gets the value of the may property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMay() {
        return may;
    }

    /**
     * Sets the value of the may property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMay(String value) {
        this.may = value;
    }

    /**
     * Gets the value of the jun property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJun() {
        return jun;
    }

    /**
     * Sets the value of the jun property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJun(String value) {
        this.jun = value;
    }

    /**
     * Gets the value of the jul property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJul() {
        return jul;
    }

    /**
     * Sets the value of the jul property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJul(String value) {
        this.jul = value;
    }

    /**
     * Gets the value of the aug property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAug() {
        return aug;
    }

    /**
     * Sets the value of the aug property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAug(String value) {
        this.aug = value;
    }

    /**
     * Gets the value of the sep property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSep() {
        return sep;
    }

    /**
     * Sets the value of the sep property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSep(String value) {
        this.sep = value;
    }

    /**
     * Gets the value of the oct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOct() {
        return oct;
    }

    /**
     * Sets the value of the oct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOct(String value) {
        this.oct = value;
    }

    /**
     * Gets the value of the nov property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNov() {
        return nov;
    }

    /**
     * Sets the value of the nov property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNov(String value) {
        this.nov = value;
    }

    /**
     * Gets the value of the dec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDec() {
        return dec;
    }

    /**
     * Sets the value of the dec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDec(String value) {
        this.dec = value;
    }

}
