
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import _do.com.claro.soa.model.personidentity.Names;
import _do.com.claro.soa.model.personidentity.PersonID;


/**
 * 
 *         @Created: SOA-213
 *         Type used to present a billing account guarantor. 
 *         In some cases a customer is not solvent to have a Service with Claro and a guarantor assume
 *         the responsibility for this account.
 *       
 * 
 * <p>Java class for Guarantor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Guarantor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}billingAccountNumber"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/personidentity}names"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/personidentity}personID"/>
 *         &lt;element name="masterServiceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Guarantor", propOrder = {
    "billingAccountNumber",
    "names",
    "personID",
    "masterServiceDate"
})
public class Guarantor {

    @XmlElement(required = true, nillable = true)
    protected String billingAccountNumber;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/personidentity", required = true)
    protected Names names;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/personidentity", required = true)
    protected PersonID personID;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar masterServiceDate;

    /**
     * Gets the value of the billingAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingAccountNumber() {
        return billingAccountNumber;
    }

    /**
     * Sets the value of the billingAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingAccountNumber(String value) {
        this.billingAccountNumber = value;
    }

    /**
     * Gets the value of the names property.
     * 
     * @return
     *     possible object is
     *     {@link Names }
     *     
     */
    public Names getNames() {
        return names;
    }

    /**
     * Sets the value of the names property.
     * 
     * @param value
     *     allowed object is
     *     {@link Names }
     *     
     */
    public void setNames(Names value) {
        this.names = value;
    }

    /**
     * Gets the value of the personID property.
     * 
     * @return
     *     possible object is
     *     {@link PersonID }
     *     
     */
    public PersonID getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonID }
     *     
     */
    public void setPersonID(PersonID value) {
        this.personID = value;
    }

    /**
     * Gets the value of the masterServiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMasterServiceDate() {
        return masterServiceDate;
    }

    /**
     * Sets the value of the masterServiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMasterServiceDate(XMLGregorianCalendar value) {
        this.masterServiceDate = value;
    }

}
