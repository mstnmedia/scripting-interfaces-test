
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChargeAndCreditType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ChargeAndCreditType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INMEDIATE_CHARGE"/>
 *     &lt;enumeration value="ONE_TIME_CHARGE"/>
 *     &lt;enumeration value="RECURRENT_CHARGE"/>
 *     &lt;enumeration value="ADVANCED_RECURRENT_CHARGE"/>
 *     &lt;enumeration value="RECURRENT_CHARGE_PRORRATED"/>
 *     &lt;enumeration value="USAGE_CHARGE"/>
 *     &lt;enumeration value="NOT_AVAILABLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ChargeAndCreditType")
@XmlEnum
public enum ChargeAndCreditType {


    /**
     * 
     *             Charge made directly to the customer account, this type of charges and credit does not appears in the
     *             pending charges and credit section.
     *           
     * 
     */
    INMEDIATE_CHARGE,
    ONE_TIME_CHARGE,
    RECURRENT_CHARGE,
    ADVANCED_RECURRENT_CHARGE,
    RECURRENT_CHARGE_PRORRATED,
    USAGE_CHARGE,
    NOT_AVAILABLE;

    public String value() {
        return name();
    }

    public static ChargeAndCreditType fromValue(String v) {
        return valueOf(v);
    }

}
