package com.mstn.scripting;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import _do.com.claro.soa.model.generic.TelephoneNumber;
import _do.com.claro.soa.services.billing.getsubscriptionsbysubscriberno.GetRequest;
import _do.com.claro.soa.services.billing.getsubscriptionsbysubscriberno.GetResponse;
import _do.com.claro.soa.services.billing.getsubscriptionsbysubscriberno.GetSubscriptionsBySubscriberNo;
import _do.com.claro.soa.services.billing.getsubscriptionsbysubscriberno.GetSubscriptionsBySubscriberNo_Service;
import java.net.URL;
import java.util.Date;
import javax.xml.namespace.QName;

/**
 *
 * @author amatos
 */
public class Main {

	static GetSubscriptionsBySubscriberNo soap;

	static {
		GetSubscriptionsBySubscriberNo_Service service = new GetSubscriptionsBySubscriberNo_Service();
//		URL url = com.mstn.scripting.Main.class.getResource("/wsdl/GetSubscriptionsBySubscriberNo.wsdl");
//		QName qName = new QName("http://www.claro.com.do/soa/services/billing/GetSubscriptionsBySubscriberNo", "GetSubscriptionsBySubscriberNo");
//		GetSubscriptionsBySubscriberNo_Service service = new GetSubscriptionsBySubscriberNo_Service(url, qName);
		soap = service.getGetSubscriptionsBySubscriberNoSOAP();
	}

	static public Object getSubscriptionsByNo() throws Exception {
		long time1 = new Date().getTime();
		long time2 = new Date().getTime();
		System.out.println("Time 2: " + (time2 - time1));
		GetRequest request = new GetRequest();
		TelephoneNumber phone = new TelephoneNumber();
		phone.setNpa("");
		phone.setNxx("");
		phone.setStationCode("8092974255");
		request.setTelephoneNumber(phone);
		long time3 = new Date().getTime();
		System.out.println("Time 3: " + (time3 - time2));
		GetResponse response = soap.get(request);
		long time4 = new Date().getTime();
		System.out.println("Time 4: " + (time4 - time3));
		return response;
	}

	//	static public Object testToken() throws Exception {
	//		CRM_GetCrmTicketByCredentials base = new CRM_GetCrmTicketByCredentials();
	//		List<Interface> interfaces = base.getInterfaces(true);
	//		Interface inter = interfaces.stream()
	//				.filter(i -> i.getName().endsWith("get"))
	//				.findFirst()
	//				.get();
	//		ObjectNode form = JSON.newObjectNode()
	//				.put("crmticketbycredentials_get_arg0_in", "319343")
	//				.put("test", "es");
	//		payload.setForm(form.toString());
	//		TransactionInterfacesResponse response = base.callMethod(inter, payload, User.SYSTEM);
	//		return response;
	//	}
	public static void main(String[] args) throws Exception {
		long start = new Date().getTime();
		Object result = null;
		result = getSubscriptionsByNo();
		long end = new Date().getTime();
		System.out.println("Total: " + (end - start));
		System.out.println(result);
	}

}
