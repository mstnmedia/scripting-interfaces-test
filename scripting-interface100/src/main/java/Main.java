/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.internal.Scripting_Category;
import com.mstn.scripting.interfaces.internal.Scripting_CustomForm;
import com.mstn.scripting.interfaces.internal.Scripting_ListSelector;
import com.mstn.scripting.interfaces.internal.Scripting_Mail;
import com.mstn.scripting.interfaces.internal.Scripting_Results;
import com.mstn.scripting.interfaces.internal.Scripting_Time;
import java.util.Arrays;
import java.util.List;

/**
 * Clase ejecutable con métodos de pruebas para probar las diferentes
 * funcionalidades en este proyecto.
 *
 * @author amatos
 */
public class Main {

	/**
	 * @param args the command line arguments
	 */
	static TransactionInterfacesPayload payload = new TransactionInterfacesPayload(
			new V_Transaction(),
			new Workflow_Step(),
			"{}"
	);

	static Object testJSON() throws JsonProcessingException {
		List items = Arrays.asList("dfsdf", 2, 34, "efdsf");
		Object result = JSON.newObjectNode()
				.put("asignProp", "id")
				.put("url", "/transactions/category")
				.set("items", JSON.toJsonNode(items))
				.toString();
		Object jsonNode = JSON.newJsonNode();
		Object jsonObject = JSON.newObjectNode();
		Object jsonArray = JSON.newArrayNode();
		return result;
	}

	static Object testMail() throws Exception {
		Scripting_Mail service = new Scripting_Mail();
		String to = "[\"amatos@mstn.com\",\"arsenys_rodriguez@claro.com.do\"]";
		String subject = "My Subject: prueba correos multiples $transaction.getId()";
		String content = "<b>Hello</b> world from Scripting! \n"
				+ "ID Transaction: $transaction.id \n"
				+ "ID Transaction: $transaction.getId()\n "
				+ "ID Transaction: $transaction_id_transaction\n ";
		String form = JSON.newObjectNode()
				.put("mail_sender_arg0", to)
				.put("mail_sender_to", to)
				.put("mail_sender_arg1", subject)
				.put("mail_sender_subject", subject)
				.put("mail_sender_arg2", content)
				.put("mail_sender_content", content)
				.put("mail_sender_arg3", "")
				.toString();
		payload.setForm(form);
		Interface inter = service.getInterfaces(true).get(0);
		return service.callMethod(inter, payload, User.SYSTEM);
	}

	static Object testReplaceInText() throws Exception {
		Scripting_Results service = new Scripting_Results();
		String name = "var1";
		String text = "<b>Hello</b> world from Scripting! \n"
				+ "ID Transaction: $transaction.id \n"
				+ "ID Transaction: $transaction.getId()\n "
				+ "ID Transaction: $transaction_id_transaction\n ";
		String form = JSON.newObjectNode()
				.put("results_replaceInText_arg0", text)
				.put("results_replaceInText_text", text)
				.put("results_replaceInText_name", name)
				.put("results_replaceInText_arg1", name)
				.put("mail_sender_arg3", "")
				.toString();
		payload.setForm(form);
		Interface inter = service.getInterfaces(true).get(1);
		TransactionInterfacesResponse response = service.callMethod(inter, payload, User.SYSTEM);
		return response;
	}

	static Object testListType() throws Exception {
		Scripting_Category service = new Scripting_Category();
		payload.setForm("{\"category_asigner_arg0\":15}");
		Interface inter = service.getInterfaces(true).get(0);
		return service.callMethod(inter, payload, User.SYSTEM);
	}

	static Object testListSelector() throws Exception {
		Scripting_ListSelector service = new Scripting_ListSelector();
		Interface inter = service.getInterfaces(true).get(0);
		ObjectNode form = JSON.newObjectNode()
				.put(inter.getName() + "_jsonArray", "[{\"id\": 0,\"id_interface\": 0,\"name\": \"list_selector_arg0\",\"label\": \"list_selector_arg0\",\"id_type\": 1,\"order_index\": 0,\"required\": true,\"readonly\": false,\"default_value\": null,\"grid\": null,\"source\": null,\"onvalidate\": null,\"onchange\": null,\"onfocus\": null,\"onblur\": null,\"interface\": null},            {\"id\": 0,\"id_interface\": 0,\"name\": \"list_selector_arg1\",\"label\": \"list_selector_arg1\",\"id_type\": 1,\"order_index\": 0,\"required\": true,\"readonly\": false,\"default_value\": null,\"grid\": null,\"source\": null,\"onvalidate\": null,\"onchange\": null,\"onfocus\": null,\"onblur\": null,\"interface\": null}]")
				.put(inter.getName() + "_label", "Campos")
				.put(inter.getName() + "_item", "{\"id\": 0,\"id_interface\": 0,\"name\": \"list_selector_arg1\",\"label\": \"list_selector_arg1\",\"id_type\": 1,\"order_index\": 0,\"required\": true,\"readonly\": false,\"default_value\": null,\"grid\": null,\"source\": null,\"onvalidate\": null,\"onchange\": null,\"onfocus\": null,\"onblur\": null,\"interface\": null}")
				.put(inter.getName() + "_test", "");
		payload.setForm(form.toString());
		TransactionInterfacesResponse response = service.callMethod(inter, payload, User.SYSTEM);
		return response;
	}

	static Object testTime() throws Exception {
		Scripting_Time service = new Scripting_Time();
		List<Interface> interfaces = service.getInterfaces(true);
		int interIndex = 2;
		Interface inter = interfaces.get(interIndex);
		ObjectNode form = JSON.newObjectNode()
				.put(inter.getName() + "_arg0", "2015-01-05T12:35:55")
				.put(inter.getName() + "_date", "2015-01-05T12:35:55")
				.put(inter.getName() + "_arg1", "DD/MMM/YYYY")
				.put(inter.getName() + "_format", "DD/MMM/YYYY")
				.put(inter.getName() + "_arg2", "es-DO")
				.put(inter.getName() + "_locale", "es-DO")
				.put(inter.getName() + "_test", "");
		payload.setForm(form.toString());
		return service.callMethod(inter, payload, User.SYSTEM);
	}

	static Object testCustomForm() throws Exception {
		Scripting_CustomForm service = new Scripting_CustomForm();
		List<Interface> interfaces = service.getInterfaces(true);
		int interIndex = 0;
		Interface inter = interfaces.get(interIndex);
		List<Scripting_CustomForm.CustomField> customFields = Arrays.asList(
				new Scripting_CustomForm.CustomField("name", "Nombre", InterfaceFieldTypes.TEXT, true, "")
		//				new Scripting_CustomForm.CustomField("age", "Edad", InterfaceFieldTypes.NUMBER, true, ""),
		//				new Scripting_CustomForm.CustomField("birthdate", "Fecha de nacimiento", InterfaceFieldTypes.DATE, true, ""),
		//				new Scripting_CustomForm.CustomField("integer", "List Integer", InterfaceFieldTypes.OPTIONS, false, "{\"items\": [1,2,4,5]}")
		);
		ObjectNode form = JSON.newObjectNode()
				.put(Scripting_CustomForm.CUSTOM_FIELDS, JSON.toJsonNode(customFields).toString())
				.put(inter.getName() + "_name", "Abel Matos")
				//				.put(inter.getName() + "_age", 21)
				//				.put(inter.getName() + "_birthdate", DATE.toISODate(new Date()))
				//				.put(inter.getName() + "_integer", "")
				//				.put(inter.getName() + "_test", "")
				.remove(Arrays.asList("edd34"));
		payload.setForm(form.toString());
		return service.callMethod(inter, payload, User.SYSTEM);
	}

	/**
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Object result = null;
//		result = testJSON();
//		result = testListType();
//		result = String.valueOf(new Date());
//		result = testMail();
		result = testListSelector();
//		result = testTime();
//		result = String.valueOf(Locale.forLanguageTag("es-DO"));
//		result = TransactionEndCauses.WRONG_CALLER.getClass().getEnumConstants();
//		result = testCustomForm();
//		result = testReplaceInText();

		System.out.println(JSON.toString(result));
	}
}
