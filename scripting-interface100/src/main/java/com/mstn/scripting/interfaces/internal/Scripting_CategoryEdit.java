/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.core.models.V_Transaction;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Interfaz que asigna una categoría indicada a transacción actual.
 *
 * @author amatos
 * @deprecated
 */
public class Scripting_CategoryEdit extends InterfaceConnectorBase {

	private final static int ID = 110;
	private final static String NAME = "scripting_category_edit";
	private final static String LABEL = "Scripting - Modificar la categoría";
	private final static String SERVER_URL = "localhost";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	private V_Transaction tempTransaction;

	/**
	 *
	 */
	public Scripting_CategoryEdit() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	private final String ID_CATEGORY = Interfaces.ID_CATEGORY;

	@Override
	public List<Interface_Field> getFields() {
		//(id, id_interface, name, label, id_type, source, order_index, default_value)
		Interface_Field field = new Interface_Field(1, ID, ID_CATEGORY, "Categoría", InterfaceFieldTypes.AUTOCOMPLETE, true);
		try {
			String source = JSON.newObjectNode()
					.put("getItemValue", "name")
					.put("asignProp", "id")
					.put("url", InterfaceConfigs.microserviceConfig.getApiURL() + "/transactions/category")
					.toString();
			field.setSource(source);
		} catch (Exception ex) {
		}
		return Arrays.asList(field);
	}

	@Override
	protected void getDataForResults() throws Exception {
		tempTransaction = transaction;
	}

	@Override
	public List<Transaction_Result> getResults() {
		boolean empty = tempTransaction == null;
		if (!empty) {
			String id_category = JSON.getString(form, ID_CATEGORY);
			HashMap<String, String> transactionChanges = response.getTransactionChanges();
			transactionChanges.put("id_category", id_category);
		}
		return Arrays.asList();
	}
}
