/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.MAIL;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.Velocity;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * Interfaz que envía correos de texto HTML a los destinos indicados.
 * Esta interfaz permite interpolación en el texto del correo.
 *
 * @author amatos
 */
public class Scripting_Mail extends InterfaceBase {

	static Getter SOAP = new Scripting_Mail.Getter();

	/**
	 *
	 */
	public Scripting_Mail() {
		super(0, "mail", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		Interface item = super.getInterface(method, interID, interName, withChildren);
		item.setLabel("Enviar correo");
		item.setServer_url(InterfaceConfigs.mailer != null
				? InterfaceConfigs.mailer.getSession().getProperty("mail.smtp.host")
				: "Mailer no está inicializado con una sessión");
		return item;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if ("mail_sender".equals(inter.getName())) {
			if (fieldName.endsWith("to")) {
				field.setLabel("Para");
			} else if (fieldName.endsWith(LIST_TEMPLATE)) {
				field.setLabel("Correos");
				field.setId_type(InterfaceFieldTypes.EMAIL);
			} else if (fieldName.endsWith("subject")) {
				field.setLabel("Asunto");
				field.setId_type(InterfaceFieldTypes.TEXT);
			} else if (fieldName.endsWith("content")) {
				field.setLabel("Contenido");
				field.setId_type(InterfaceFieldTypes.HTML);
			} else if (fieldName.endsWith("attachmentNames") || fieldName.endsWith("arg3")) {
				field.setLabel("Nombres de archivos para adjuntar");
				field.setId_type(InterfaceFieldTypes.TAGS);
				field.setRequired(false);
			}
		}
		return field;
	}

	@Override
	protected Object getParamValueFromForm(Class paramType, String paramName) throws Exception {
		Object value = super.getParamValueFromForm(paramType, paramName);
		if ("mail_sender".equals(inter.getName()) && (paramName.endsWith("subject") || paramName.endsWith("content"))) {
			String localValue = Utils.coalesce((String) value, "");
			try {
				Map<String, Object> values = mapResults(mapppedResults, true);
				localValue = Velocity.interpolate(localValue, values);
			} catch (Exception ex) {
				Utils.logException(this.getClass(), "Error interpolando " + paramName, ex);
			}
			return localValue;
		}
		return value;
	}

	/**
	 *
	 * @throws Exception
	 */
	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.mailer.toString();
		InterfaceConfigs.get("mailFromName").toString();
		InterfaceConfigs.get("mailFromEmail").toString();
		InterfaceConfigs.microserviceConfig.getContentsFolder().toString();
		super.test();
	}

	/**
	 *
	 */
	static public class Getter {

		/**
		 *
		 * @param to
		 * @param subject
		 * @param content
		 * @param attachmentNames
		 */
		public void sender(List<String> to, String subject, String content, String attachmentNames) {
			MAIL.send(String.join(",", to), subject, content, attachmentNames);
		}
	}
}
