/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Transaction_Result;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Interfaz que muestra un formulario con un campo tipo lista desplegable con
 * los elementos tomados desde una variable de la transacción actual para que el
 * usuario seleccione un elemento de la lista. Cuando el usuario envíe el
 * formulario se tomará el elemento seleccionado de la lista y se guardará como
 * variables para la transacción actual.
 *
 * @author amatos
 */
public class Scripting_ListSelector extends InterfaceBase {

	static String LABEL = "list_selector_label";
	static String JSON_ARRAY = "list_selector_jsonArray";
	static String ITEM = "list_selector_item";
	static Getter SOAP = new Scripting_ListSelector.Getter();

	/**
	 *
	 */
	public Scripting_ListSelector() {
		super(0, "list", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		Interface item = super.getInterface(method, interID, interName, withChildren);
		item.setLabel("Crear variables de un elemento en una lista");
		item.setServer_url("localhost");
		item.setKeep_results(true);
		return item;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("label")) {
			field.setLabel("Título del campo a mostrar");
		} else if (fieldName.endsWith("jsonArray")) {
			field.setLabel("Nombre de la variable con la lista");
		} else if (fieldName.endsWith("item")) {
			field.setLabel("JSON del elemento seleccionado");
			field.setRequired(false);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_JSON);
		}
		return field;
	}

	@Override
	protected boolean isValidForm() {
		return JSON.hasValue(form, ITEM);
	}

	@Override
	protected List<Interface_Field> getStepFields() throws Exception {
		List<Interface_Field> stepFields = new ArrayList();
		String label = JSON.getString(form, LABEL, "");
		Interface_Field userField = new Interface_Field(0, inter.getId(), ITEM, label, InterfaceFieldTypes.OPTIONS, true);

		List<String> items = new ArrayList();
		String jsonArray = JSON.getString(form, JSON_ARRAY, "");
		ArrayNode itemsA = JSON.toArrayNode(jsonArray);
		itemsA.forEach(jsonNode -> {
			items.add(jsonNode.toString());
		});
		JsonNode source = JSON.newObjectNode()
				.put("emptyOption", "SelectDisabled")
				.set("items", JSON.toJsonNode(items));
		userField.setSource(source.toString());
		stepFields.add(userField);
		return stepFields;
	}

	@Override
	protected Object invokeMethod() throws Exception {
		String json = JSON.getString(form, ITEM, "");
		JsonNode item = JSON.toJsonNode(json);
		return item;
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromResponse(Object value, Interface inter) throws Exception {
		String paramName = inter.getName();
		JsonNode item = (JsonNode) value;
		List<Transaction_Result> transactionResults = getTransactionResultsFromResponse(item, inter, paramName);
		return transactionResults;
	}

	/**
	 *
	 * @param value
	 * @param inter
	 * @param paramName
	 * @return
	 * @throws Exception
	 */
	protected List<Transaction_Result> getTransactionResultsFromResponse(JsonNode value, Interface inter, String paramName) throws Exception {
		List<Transaction_Result> transactionResults = new ArrayList<>();
		baseLog("ResultsFromResponse: " + paramName);
		if (value == null || value.isNull() || value.isMissingNode()) {
			Transaction_Result result = getTransactionResultFromResponse(null, inter, paramName);
			transactionResults.add(result);
		} else if (value.isValueNode()) {
			Transaction_Result result = getTransactionResultFromResponse(value.asText(), inter, paramName);
			transactionResults.add(result);
		} else if (value.isArray()) {
			for (int i = 0; i < value.size(); i++) {
				JsonNode child = value.get(i);
				String childName = paramName + i;
				transactionResults.addAll(getTransactionResultsFromResponse(child, inter, childName));
			}
		} else if (value.isObject()) {
			for (Iterator<Map.Entry<String, JsonNode>> it = value.fields(); it.hasNext();) {
				Map.Entry<String, JsonNode> entry = it.next();
				String childName = paramName + "_" + entry.getKey();
				JsonNode child = entry.getValue();
				transactionResults.addAll(getTransactionResultsFromResponse(child, inter, childName));
			}
		}
		return transactionResults;
	}

	/**
	 *
	 */
	static public class Getter {

		/**
		 *
		 * @param label
		 * @param jsonArray
		 * @param item
		 * @return
		 * @throws Exception
		 */
		public Object selector(String label, String jsonArray, String item) throws Exception {
			return "";
		}
	}
}
