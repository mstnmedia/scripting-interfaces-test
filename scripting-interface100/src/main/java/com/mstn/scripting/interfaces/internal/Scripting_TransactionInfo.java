/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.core.models.V_Transaction;
import java.util.ArrayList;
import java.util.List;

/**
 * Interfaz que contiene las variables de con información de la transacción
 * actual
 *
 * @author amatos
 * @deprecated
 */
public class Scripting_TransactionInfo extends InterfaceConnectorBase {

	private final static int ID = Interfaces.TRANSACTION_INFO_INTERFACE_ID;
	private final static String NAME = Interfaces.TRANSACTION_INFO_INTERFACE_NAME;
	private final static String LABEL = "Scripting - Información de la Transacción";
	private final static String SERVER_URL = "localhost";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	private V_Transaction tempTransaction;

	/**
	 *
	 */
	public Scripting_TransactionInfo() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	protected void getDataForResults() throws Exception {
		tempTransaction = transaction;
	}

	@Override
	public List<Transaction_Result> getResults() {
		boolean empty = tempTransaction == null;

		List<Transaction_Result> transactionResults = new ArrayList(
				V_Transaction.getResults(tempTransaction)
		);

		String caller_name = empty ? "" : mapppedResults.getOrDefault(prefix + "caller_name", "");
		String onAlert = empty ? "" : mapppedResults.getOrDefault(prefix + "on_alert", "");
		transactionResults.add(
				new Transaction_Result(20, ID, prefix + "caller_name", "Nombre de la persona en línea", caller_name, "")
		);
		transactionResults.add(
				new Transaction_Result(21, ID, prefix + "on_alert", "Creada en alerta", onAlert, "")
		);

		return transactionResults;
	}
}
