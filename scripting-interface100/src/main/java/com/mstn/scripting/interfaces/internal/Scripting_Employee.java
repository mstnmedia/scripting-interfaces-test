/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.db.common.EmployeeService;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.models.Employee;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Transaction_Result;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Interfaz que agrega información del empleado indicado a la transacción
 * actual.
 *
 * @author amatos
 */
public class Scripting_Employee extends InterfaceBase {

	static Getter SOAP = new Scripting_Employee.Getter();
	static final String FIELD_NAME = Interfaces.EMPLOYEE_INFO_INTERFACE_NAME + "_id_employee";

	/**
	 *
	 */
	public Scripting_Employee() {
		super(0, "employee", SOAP);
		INTERFACES_ID.put(Interfaces.EMPLOYEE_INFO_INTERFACE_NAME, Interfaces.EMPLOYEE_INFO_INTERFACE_ID);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		Interface item = super.getInterface(method, interID, interName, withChildren);
		item.setLabel("Información del empleado");
		item.setServer_url(InterfaceConfigs.microserviceConfig != null && InterfaceConfigs.microserviceConfig.getDataSourceFactory() != null
				? InterfaceConfigs.microserviceConfig.getDataSourceFactory().getUrl()
				: "DataSource is not setted.");
		return item;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (FIELD_NAME.equals(fieldName)) {
			field.setLabel("Empleado");
			field.setId_type(InterfaceFieldTypes.AUTOCOMPLETE);
			field.setSource(
					JSON.newObjectNode()
							.put("getItemValue", "id|name")
							.put("asignProp", "id")
							.put("url",
									InterfaceConfigs.microserviceConfig.getApiBrowserURL()
									+ "/employees/employee")
							.toString());
		}
		return field;
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromClass(Method method, Interface inter) {
		if (Utils.stringAreEquals("employee_info", inter.getName())) {
			return getTransactionVariables(null, inter);
		}
		return super.getTransactionResultsFromClass(method, inter);
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromResponse(Object value, Interface inter) throws Exception {
		if (Utils.stringAreEquals("employee_info", inter.getName())) {
			return getTransactionVariables((Employee) value, inter);
		}
		return super.getTransactionResultsFromResponse(value, inter);
	}

	List<Transaction_Result> getTransactionVariables(Employee employee, Interface inter) {
		List<Transaction_Result> transactionResults = new ArrayList(
				Employee.getResults(employee)
		);
		return transactionResults;
	}

	/**
	 *
	 */
	static public class Getter {

		/**
		 *
		 * @param id_employee
		 * @return
		 * @throws Exception
		 */
		public Employee info(int id_employee) throws Exception {
			EmployeeService employeeService = InterfaceConfigs.dbi.onDemand(EmployeeService.class);
			Employee employee = employeeService.get(id_employee);
			employee.setParent(employeeService.get(employee.getId_parent()));
			return employee;
		}
	}
}
