/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.db.common.EmployeeService;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Employee;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Transaction_Result;
import java.util.Arrays;
import java.util.List;

/**
 * Interfaz que agrega información del empleado indicado a la transacción
 * actual.
 *
 * @author amatos
 * @deprecated
 */
public class Scripting_EmployeeInfo extends InterfaceConnectorBase {

	private static final int ID = 106;
	private static final String NAME = "scripting_employee_info";
	private static final String LABEL = "Scripting - Información del Empleado";
	private static final String SERVER_URL = "server";
	private static final String VALUE_STEP = "returned";
	private static final boolean DELETED = false;

	Employee employee;

	/**
	 *
	 */
	public Scripting_EmployeeInfo() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	private static final String EMPLOYEE_ID = "id_employee";

	@Override
	public List<Interface_Field> getFields() {
		return Arrays.asList(
				//(id, id_interface, name, label, id_type, order_index, default_value, required, read_only)
				new Interface_Field(1, ID, EMPLOYEE_ID, "Tarjeta del Empleado", InterfaceFieldTypes.NUMBER, 1, null, true, false)
		);
	}

	@Override
	protected void getDataForResults() throws Exception {
		EmployeeService employeeService = InterfaceConfigs.dbi.onDemand(EmployeeService.class);
		int id_employee = form.get(EMPLOYEE_ID).asInt();
		employee = employeeService.get(id_employee);
		employee.setParent(employeeService.get(employee.getId_parent()));
	}

	@Override
	public List<Transaction_Result> getResults() {
		return Employee.getResults(employee);
	}

//	@Override
//	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload) throws Exception {
//		TransactionInterfacesResponse response = new TransactionInterfacesResponse();
//
//		Interface inter = getInterface();
//		ObjectNode form = JSON.stringToNode(payload.getForm());
//
//		if (form.has("id_employee")) {
//			response.setAction(TransactionInterfaceActions.NEXT_STEP);
//			List<Transaction_Result> results = getResults();
//
//			inter.setInterface_results(results);
//			response.setInterface(inter);
//
//			String value = "true";
//			response.setValue(value);
//		} else {
//			response.setAction(TransactionInterfaceActions.ERROR);
//			response.setValue("false");
//		}
//		return response;
//	}
}
