/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Transaction_Result;
import java.util.Arrays;
import java.util.List;

/**
 * Interfaces que permiten verificar el valor de una variable de la transacción
 * actual.
 *
 * @author amatos
 * @deprecated
 */
public class Scripting_ReadResults extends InterfaceConnectorBase {

	private final static int ID = 108;
	private final static String NAME = "scripting_read_results";
	private final static String LABEL = "Scripting - Leer resultados";
	private final static String SERVER_URL = "server";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	/**
	 *
	 */
	public Scripting_ReadResults() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Field> getFields() {
		return Arrays.asList();
	}

	@Override
	public List<Transaction_Result> getResults() {
		return Arrays.asList();
	}

}
