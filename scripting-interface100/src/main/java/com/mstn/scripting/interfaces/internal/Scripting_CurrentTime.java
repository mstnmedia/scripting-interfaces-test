/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Interfaz que agrega información del tiempo a la transacción actual.
 *
 * @author amatos
 * @deprecated
 */
public class Scripting_CurrentTime extends InterfaceConnectorBase {

	private final static int ID = 105;
	private final static String NAME = "scripting_current_time";
	private final static String LABEL = "Scripting - Tiempo del servidor";
	private final static String SERVER_URL = "server";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	/**
	 *
	 */
	public Scripting_CurrentTime() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Field> getFields() {
		return new ArrayList<>();
	}

	@Override
	public List<Transaction_Result> getResults() {
		Date date = Date.from(Instant.now());
		LocalDate now = LocalDate.now();
		String prefix = NAME + "_";
		return Arrays.asList(
				//(id_interface, name)
				new Transaction_Result(0, 0, 0, 0, ID, prefix + "fulldate", "DateTime with Zone", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(date), ""),
				new Transaction_Result(1, 0, 0, 0, ID, prefix + "datetime", "DateTime", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date), ""),
				new Transaction_Result(2, 0, 0, 0, ID, prefix + "date", "Date", new SimpleDateFormat("yyyy-MM-dd").format(date), ""),
				new Transaction_Result(3, 0, 0, 0, ID, prefix + "time", "Time", new SimpleDateFormat("HH:mm:ss").format(date), ""),
				new Transaction_Result(5, 0, 0, 0, ID, prefix + "ticks", "Ticks", String.valueOf(date.getTime()), ""),
				new Transaction_Result(6, 0, 0, 0, ID, prefix + "year_number", "Year number", String.valueOf(now.getYear()), ""),
				new Transaction_Result(7, 0, 0, 0, ID, prefix + "week_number", "Number of week", String.valueOf(now.get(ChronoField.ALIGNED_WEEK_OF_YEAR)), ""),
				new Transaction_Result(8, 0, 0, 0, ID, prefix + "month_number", "Month Number", String.valueOf(now.getMonthValue()), ""),
				new Transaction_Result(9, 0, 0, 0, ID, prefix + "month_name", "Month Name", String.valueOf(now.getMonth().name()), ""),
				new Transaction_Result(10, 0, 0, 0, ID, prefix + "day_number", "Day", String.valueOf(now.getDayOfMonth()), ""),
				new Transaction_Result(11, 0, 0, 0, ID, prefix + "day_week", "Day number in week", String.valueOf(now.getDayOfWeek().getValue()), ""),
				new Transaction_Result(12, 0, 0, 0, ID, prefix + "day_name", "Day Name", String.valueOf(now.getDayOfWeek().name()), "")
		);
	}

	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) throws Exception {
		TransactionInterfacesResponse response = new TransactionInterfacesResponse();
		response.setAction(TransactionInterfaceActions.NEXT_STEP);

		inter = getInterface();
		results = getResults();
		inter.setInterface_results(results);
		response.setInterface(inter);

		String value = "true";
		response.setValue(value);
		return response;
	}
}
