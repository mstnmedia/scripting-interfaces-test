/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Transaction_Result;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Interfaz que agrega información del tiempo a la transacción actual y
 * formatear variables de fechas.
 *
 * @author amatos
 */
public class Scripting_Time extends InterfaceBase {

	static Getter SOAP = new Scripting_Time.Getter();

	/**
	 *
	 */
	public Scripting_Time() {
		super(0, "time", SOAP);
		INTERFACES_ID.put("time_current", -3);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		Interface item = super.getInterface(method, interID, interName, withChildren);
		item.setServer_url("localhost");
		switch (item.getName()) {
			case "time_current":
				item.setLabel("Obtener fecha y tiempo actual");
				break;
			case "time_formatter":
				item.setLabel("Formatear fecha y tiempo");
				break;
			case "time_formatterWithLocale":
				item.setLabel("Formatear fecha y tiempo con región");
				break;
		}
		return item;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("date")) {
			field.setLabel("Fecha");
		} else if (fieldName.endsWith("format")) {
			field.setLabel("Formato");
		} else if (fieldName.endsWith("locale")) {
			field.setLabel("Región");
		}
		return field;
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromClass(Method method, Interface inter) {
		if ("time_current".equals(inter.getName())) {
			return currentResults(inter, Date.from(Instant.now()));
		}
		return super.getTransactionResultsFromClass(method, inter);
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromResponse(Object value, Interface inter) throws Exception {
		if ("time_current".equals(inter.getName())) {
			return currentResults(inter, Date.from(Instant.now()));
		}
		return super.getTransactionResultsFromResponse(value, inter);
	}

	static List<Transaction_Result> currentResults(Interface inter, Date date) {
		LocalDate now = LocalDate.now();
		String prefix = inter.getName() + "_";
		int ID = inter.getId();
		return Arrays.asList(
				//(id_interface, name)
				new Transaction_Result(0, ID, prefix + "datetimezone", "DateTime with Zone", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(date), ""),
				new Transaction_Result(0, ID, prefix + "datetime", "DateTime", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date), ""),
				new Transaction_Result(0, ID, prefix + "date", "Date", new SimpleDateFormat("yyyy-MM-dd").format(date), ""),
				new Transaction_Result(0, ID, prefix + "time", "Time", new SimpleDateFormat("HH:mm:ss").format(date), ""),
				new Transaction_Result(0, ID, prefix + "ticks", "Ticks", String.valueOf(date.getTime()), ""),
				new Transaction_Result(0, ID, prefix + "year_number", "Year number", String.valueOf(now.getYear()), ""),
				new Transaction_Result(0, ID, prefix + "week_number", "Number of week", String.valueOf(now.get(ChronoField.ALIGNED_WEEK_OF_YEAR)), ""),
				new Transaction_Result(0, ID, prefix + "month_number", "Month Number", String.valueOf(now.getMonthValue()), ""),
				new Transaction_Result(0, ID, prefix + "month_name", "Month Name", DATE.format(date, "MMMM"), ""),
				new Transaction_Result(0, ID, prefix + "day_number", "Day", String.valueOf(now.getDayOfMonth()), ""),
				new Transaction_Result(0, ID, prefix + "day_week", "Day number in week", String.valueOf(now.getDayOfWeek().getValue()), ""),
				new Transaction_Result(0, ID, prefix + "day_name", "Day Name", DATE.format(date, "EEEE"), "")
		);
	}

	/**
	 *
	 */
	static public class Getter {

		/**
		 *
		 * @return @throws Exception
		 */
		public Date current() throws Exception {
			return new Date();
		}

		/**
		 *
		 * @param date
		 * @param format
		 * @return
		 */
		public String formatter(Date date, String format) {
			return DATE.format(date, format);
		}

		/**
		 *
		 * @param date
		 * @param format
		 * @param locale
		 * @return
		 */
		public String formatterWithLocale(Date date, String format, Locale locale) {
			return DATE.format(date, format, locale);
		}

	}
}
