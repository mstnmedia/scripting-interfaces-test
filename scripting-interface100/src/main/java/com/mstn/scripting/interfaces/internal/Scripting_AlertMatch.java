/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.db.common.AlertTriggeredDataService;
import com.mstn.scripting.core.db.common.AlertTriggeredService;
import com.mstn.scripting.core.db.common.AlertTriggeredTransactionService;
import com.mstn.scripting.core.models.Alert_Triggered;
import com.mstn.scripting.core.models.Alert_Triggered_Data;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Transaction_Result;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Interfaz que obtiene con cuáles alertas disparadas coinciden la transacción
 * actual.
 *
 * @author amatos
 */
public class Scripting_AlertMatch extends InterfaceBase {

	static Getter SOAP = new Scripting_AlertMatch.Getter();

	/**
	 *
	 */
	public Scripting_AlertMatch() {
		super(0, "alertmatch", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		Interface item = super.getInterface(method, interID, interName, withChildren);
		item.setLabel("Comparar con alertas disparadas");
		item.setServer_url(InterfaceConfigs.microserviceConfig != null && InterfaceConfigs.microserviceConfig.getDataSourceFactory() != null
				? InterfaceConfigs.microserviceConfig.getDataSourceFactory().getUrl()
				: "DataSource is not setted.");
		return item;
	}

	@Override
	protected Object invokeMethod() throws Exception {
		AlertTriggeredDataService dataService = InterfaceConfigs.dbi.onDemand(AlertTriggeredDataService.class);
		AlertTriggeredTransactionService transactionService = InterfaceConfigs.dbi.onDemand(AlertTriggeredTransactionService.class);
		AlertTriggeredService alertTriggeredService = InterfaceConfigs.dbi.onDemand(AlertTriggeredService.class)
				.setDataService(dataService)
				.setTransactionService(transactionService);
		List<Alert_Triggered> triggereds = alertTriggeredService.getAll(
				new WhereClause(new Where("state", 1))
		);

		List<Transaction_Result> transactionResults = Utils.coalesce(transaction.getTransaction_results(), new ArrayList());
		Map<String, String> mapResults = new HashMap();
		for (Transaction_Result result : transactionResults) {
			mapResults.put(result.getName(), Utils.coalesce(result.getValue(), Transaction_Result.NULL_VALUE));
		}
		// TODO: Probar el comportamiento cuando dispara una alerta de valores nulos o vacíos
		List<Alert_Triggered> matches = triggereds.stream()
				.filter(triggered -> {
					if (Utils.isNullOrEmpty(triggered.getData())) {
						return false;
					}
					for (Alert_Triggered_Data data : triggered.getData()) {
						if (!Utils.stringAreEquals(data.getValue(), mapResults.get(data.getName()))) {
							return false;
						}
					}
					return true;
				})
				.collect(Collectors.toList());
		return new Result(matches.size() > 0, matches);
	}

	/**
	 *
	 */
	static public class Getter {

		/**
		 *
		 * @return @throws Exception
		 */
		public Result match() throws Exception {
			return null;
		}
	}

	/**
	 *
	 */
	static public class Result {

		/**
		 *
		 */
		public boolean match;

		/**
		 *
		 */
		public List<Alert_Triggered> triggereds = new ArrayList();

		/**
		 *
		 * @param match
		 * @param triggereds
		 */
		public Result(boolean match, List<Alert_Triggered> triggereds) {
			this.match = match;
			this.triggereds = triggereds;
		}

	}
}
