/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.Velocity;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Transaction_Result;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Interfaces que permiten verificar el valor de una variable de la transacción
 * actual o crear nuevas variables a partir de los valores actuales.
 *
 * @author amatos
 */
public class Scripting_Results extends InterfaceBase {

	static Getter SOAP = new Scripting_Results.Getter();

	/**
	 *
	 */
	public Scripting_Results() {
		super(0, "results", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		Interface item = super.getInterface(method, interID, interName, withChildren);
		item.setServer_url("localhost");
		if (item.getName().endsWith("check")) {
			item.setLabel("Leer resultados");
		} else if (item.getName().endsWith("replaceInText")) {
			item.setLabel("Sustituir variables en texto");
			item.setKeep_results(true);
		}
		return item;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (inter.getName().endsWith("replaceInText")) {
			if (fieldName.endsWith("text") || fieldName.endsWith("arg0")) {
				field.setLabel("Plantilla");
				field.setId_type(InterfaceFieldTypes.TEXTAREA);
			} else if (fieldName.endsWith("name") || fieldName.endsWith("arg1")) {
				field.setLabel("Nombre agregado a variable");
				field.setRequired(false);
			}
		}
		return field;
	}

	@Override
	protected Object getParamValueFromForm(Class paramType, String paramName) throws Exception {
		Object value = super.getParamValueFromForm(paramType, paramName);
		if (inter.getName().endsWith("replaceInText")) {
			if (paramName.endsWith("text") || paramName.endsWith("arg0")) {
				String localValue = Utils.coalesce((String) value, "");
				try {
					Map<String, Object> values = mapResults(mapppedResults, true);
					localValue = Velocity.interpolate(localValue, values);
				} catch (Exception ex) {
					Utils.logException(this.getClass(), "Error interpolando " + paramName, ex);
				}
				return localValue;
			}
		}
		return value;
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromClass(Method method, Interface inter) {
		if (inter.getName().endsWith("check")) {
			return Arrays.asList();
		}
		return super.getTransactionResultsFromClass(method, inter);
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromResponse(Object value, Interface inter) throws Exception {
		if (inter.getName().endsWith("check")) {
			return Arrays.asList();
		}
		List<Transaction_Result> localResults = super.getTransactionResultsFromResponse(value, inter);
		if (inter.getName().endsWith("replaceInText") && !localResults.isEmpty()) {
			Transaction_Result result = localResults.get(0);
			if (result != null) {
				String name = JSON.getString(form, "results_replaceInText_name", "");
				if (Utils.stringNonNullOrEmpty(name)) {
					result.setName(result.getName() + "_" + name);
					result.setLabel(name);
				}
			}
		}
		return localResults;
	}

	/**
	 *
	 */
	static public class Getter {

		/**
		 *
		 * @throws Exception
		 */
		public void check() throws Exception {
		}

		/**
		 *
		 * @param text
		 * @param name
		 * @return
		 */
		public String replaceInText(String text, String name) {
			return text;
		}
	}
}
