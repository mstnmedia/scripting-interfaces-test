/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.CategoryTypes;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.core.models.V_Transaction;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Interfaz que contiene las variables de con información de la transacción
 * actual.
 *
 * @author amatos
 */
public class Scripting_Transaction extends InterfaceBase {

	static Getter SOAP = new Scripting_Transaction.Getter();

	/**
	 *
	 */
	public Scripting_Transaction() {
		super(0, "transaction", SOAP);
		INTERFACES_ID.put("transaction_info", Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		Interface item = super.getInterface(method, interID, interName, withChildren);
		item.setLabel("Información de la transacción");
		item.setServer_url("localhost");
		item.setKeep_results(true);
		return item;
	}

	@Override
	protected Object invokeMethod() throws Exception {
		if (Utils.stringAreEquals("transaction_info", inter.getName())) {
			return transaction;
		}
		return super.invokeMethod();
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromClass(Method method, Interface inter) {
		if (Utils.stringAreEquals("transaction_info", inter.getName())) {
			return getTransactionVariables(null, inter);
		}
		return super.getTransactionResultsFromClass(method, inter);
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromResponse(Object value, Interface inter) throws Exception {
		if (Utils.stringAreEquals("transaction_info", inter.getName())) {
			return getTransactionVariables((V_Transaction) value, inter);
		}
		return super.getTransactionResultsFromResponse(value, inter);
	}

	List<Transaction_Result> getTransactionVariables(V_Transaction transaction, Interface inter) {
		String prefix = inter.getName() + "_";
		int ID = inter.getId();

		boolean empty = transaction == null;
		List<Transaction_Result> transactionResults = new ArrayList(
				V_Transaction.getResults(transaction)
		);
		String CALLER_NAME = prefix + "caller_name";
		String ON_ALERT = prefix + "on_alert";
		String caller_name = empty ? "" : getTransactionResultValue(CALLER_NAME);
		String onAlert = empty ? "" : getTransactionResultValue(ON_ALERT);
		transactionResults.add(new Transaction_Result(0, ID, CALLER_NAME, "Nombre de la persona en línea", caller_name, ""));
		transactionResults.add(new Transaction_Result(0, ID, ON_ALERT, "Creada en alerta", onAlert, ""));

		String Type1ID = CategoryTypes.TYPE1_ID;
		String Type1Name = CategoryTypes.TYPE1_NAME;
		String Type2ID = CategoryTypes.TYPE2_ID;
		String Type2Name = CategoryTypes.TYPE2_NAME;
		String Type3ID = CategoryTypes.TYPE3_ID;
		String Type3Name = CategoryTypes.TYPE3_NAME;

		String type1ID = empty ? "" : getTransactionResultValue(Type1ID);
		String type1Name = empty ? "" : getTransactionResultValue(Type1Name);
		String type2ID = empty ? "" : getTransactionResultValue(Type2ID);
		String type2Name = empty ? "" : getTransactionResultValue(Type2Name);
		String type3ID = empty ? "" : getTransactionResultValue(Type3ID);
		String type3Name = empty ? "" : getTransactionResultValue(Type3Name);

		transactionResults.add(new Transaction_Result(0, ID, Type1ID, CategoryTypes.TYPE1_ID_LABEL, type1ID, ""));
		transactionResults.add(new Transaction_Result(0, ID, Type1Name, CategoryTypes.TYPE1_NAME_LABEL, type1Name, ""));
		transactionResults.add(new Transaction_Result(0, ID, Type2ID, CategoryTypes.TYPE2_ID_LABEL, type2ID, ""));
		transactionResults.add(new Transaction_Result(0, ID, Type2Name, CategoryTypes.TYPE2_NAME_LABEL, type2Name, ""));
		transactionResults.add(new Transaction_Result(0, ID, Type3ID, CategoryTypes.TYPE3_ID_LABEL, type3ID, ""));
		transactionResults.add(new Transaction_Result(0, ID, Type3Name, CategoryTypes.TYPE3_NAME_LABEL, type3Name, ""));

		return transactionResults;
	}

	/**
	 *
	 */
	static public class Getter {

		/**
		 *
		 * @return @throws Exception
		 */
		public V_Transaction info() throws Exception {
			return null;
		}
	}
}
