/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.Velocity;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Transaction_Result;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Interfaz que muestra un formulario personalizado al usuario y guarda los
 * datos ingresados como variables.
 *
 * @author amatos
 */
public class Scripting_CustomForm extends InterfaceBase {

	static final String NAME = "customform";

	/**
	 *
	 */
	static public final String CUSTOM_FIELDS = NAME + "_save_list_fields";
	static Getter SOAP = new Scripting_CustomForm.Getter();

	/**
	 *
	 */
	public Scripting_CustomForm() {
		super(0, NAME, SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		Interface item = super.getInterface(method, interID, interName, withChildren);
		item.setLabel("Guardar formulario personalizado");
		item.setServer_url("localhost");
		item.setKeep_results(true);
		return item;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("name")) {
			field.setLabel("Nombre");
		} else if (fieldName.endsWith("label")) {
			field.setLabel("Título");
		} else if (fieldName.endsWith("type")) {
			field.setLabel("Tipo");
			field.setId_type(InterfaceFieldTypes.OPTIONS);
			JsonNode source = JSON.newObjectNode()
					.put("emptyOption", "SelectDisabled")
					.set("items", JSON.newArrayNode()
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.AUTOCOMPLETE).put("label", "Autocomplete"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.BOOLEAN).put("label", "Booleano"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.RADIO).put("label", "Botón de radio"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.CHECKBOX).put("label", "Caja de verificación"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.EMAIL).put("label", "Correo"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.PASSWORD).put("label", "Contraseña"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.LINK).put("label", "Enlace"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.TAGS).put("label", "Etiquetas"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.DATE).put("label", "Fecha"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.DATETIME).put("label", "Fecha y tiempo"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.HTML).put("label", "HTML"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.OPTIONS).put("label", "Lista desplegable"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.NUMBER).put("label", "Númerico"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.HIDDEN).put("label", "Oculto"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.TEXT).put("label", "Texto"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.TEXTAREA).put("label", "Texto multilínea"))
							.add(JSON.newObjectNode().put("value", InterfaceFieldTypes.TIME).put("label", "Tiempo"))
					);
			field.setSource(source.toString());
		} else if (fieldName.endsWith("required")) {
			field.setLabel("Requerido");
		} else if (fieldName.endsWith("source")) {
			field.setLabel("Fuente");
			field.setId_type(InterfaceFieldTypes.TEXTAREA);
			field.setRequired(false);
			//field.setOnvalidate(InterfaceFieldTypes.VALIDATE_JSON);
		}

		return field;
	}

	@Override
	protected List<Interface_Field> getStepFields() throws Exception {
		String jsonCustomFields = JSON.getString(form, CUSTOM_FIELDS, "[]");
		List<CustomField> customFields = JSON.toList(jsonCustomFields, CustomField.class);
		List<Interface_Field> stepFields = new ArrayList();
		for (CustomField field : customFields) {
			String name = inter.getName() + "_" + field.name;
			String localValue = Utils.coalesce((String) field.source, "");
			try {
				Map<String, Object> values = mapResults(mapppedResults, true);
				localValue = Velocity.interpolate(localValue, values);
			} catch (Exception ex) {
				Utils.logException(this.getClass(), "Error interpolando source del campo " + field.name, ex);
			}
			stepFields.add(new Interface_Field(0, inter.getId(),
					name, field.label, field.type,
					localValue, 0, field.required
			));
		}
		return stepFields;
	}

	@Override
	protected Object invokeMethod() throws Exception {
		ObjectNode newForm = JSON.newObjectNode();
		form.fields().forEachRemaining(entry -> {
			if (!FORM_GENERATIONDATE.equals(entry.getKey())) {
				String name = entry.getKey().replace(inter.getName() + "_", "");
				newForm.set(name, entry.getValue());
			}
		});
		return newForm;
	}

	@Override
	protected Transaction_Result getTransactionResultFromResponse(Object value, Interface inter, String name) {
		Transaction_Result result = super.getTransactionResultFromResponse(value, inter, name);
		try {
			List<Interface_Field> fields = getStepFields();
			fields.add(new Interface_Field(0, 0, CUSTOM_FIELDS, "Campos", 0, true));
			for (Interface_Field field : fields) {
				if (Utils.stringAreEquals(name, field.getName())) {
					result.setLabel(field.getLabel());
					break;
				}
			}
		} catch (Exception ex) {
			Logger.getLogger(Scripting_CustomForm.class.getName()).log(Level.SEVERE, null, ex);
		}
		return result;
	}

	/**
	 *
	 */
	static public class Getter {

		/**
		 *
		 * @param list
		 * @return
		 * @throws Exception
		 */
		public Object save(CFList list) throws Exception {
			return null;
		}

	}

	/**
	 *
	 */
	static public class CustomField {

		/**
		 *
		 */
		public String name;

		/**
		 *
		 */
		public String label;

		/**
		 *
		 */
		public int type;

		/**
		 *
		 */
		public boolean required;

		/**
		 *
		 */
		public String source;

		/**
		 *
		 */
		public CustomField() {
		}

		/**
		 *
		 * @param name
		 * @param label
		 * @param type
		 * @param required
		 * @param source
		 */
		public CustomField(String name, String label, int type, boolean required, String source) {
			this.name = name;
			this.label = label;
			this.type = type;
			this.required = required;
			this.source = source;
		}

	}

	/**
	 *
	 */
	static public class CFList {

		/**
		 *
		 */
		public List<CustomField> fields;
	}
}
