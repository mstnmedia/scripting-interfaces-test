/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Interfaz que asigna una categoría indicada a transacción actual.
 *
 * @author amatos
 */
public class Scripting_Category extends InterfaceBase {

	static Asigner soap = new Scripting_Category.Asigner();
	final String FIELD_NAME = "category_asigner_id_category";

	/**
	 *
	 */
	public Scripting_Category() {
		super(0, "category", soap);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		Interface item = super.getInterface(method, interID, interName, withChildren);
		item.setLabel("Asignar categoría");
		item.setServer_url("localhost");
		return item;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (FIELD_NAME.equals(fieldName)) {
			field.setLabel("Categoría");
			field.setId_type(InterfaceFieldTypes.AUTOCOMPLETE);
			field.setSource(
					JSON.newObjectNode()
							.put("getItemValue", "id|name|parent_name")
							.put("asignProp", "id")
							.put("url",
									InterfaceConfigs.microserviceConfig.getApiBrowserURL()
									+ "/transactions/category")
							.toString());
		}
		return field;
	}

	@Override
	protected Object invokeMethod() throws Exception {
		HashMap<String, String> transactionChanges = response.getTransactionChanges();
		String id_category = JSON.getString(form, FIELD_NAME);
		baseLog(FIELD_NAME + ": " + id_category);
		transactionChanges.put("id_category", id_category);
		return true;
	}

	/**
	 *
	 */
	static public class Asigner {

		/**
		 *
		 * @param id_category
		 * @throws Exception
		 */
		public void asigner(int id_category) throws Exception {
		}
	}
}
