package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import java.util.Date;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class VwHistoricoGraficosOLT   {
  
  private Integer id = null;
  private Integer idFacilidades = null;
  private Integer velocidadConfiguradaDN = null;
  private Integer velocidadConfiguradaUP = null;
  private Double oNTTxPower = null;
  private Double oNTRxPower = null;
  private Double oLTRxONTOpticalPower = null;
  private Integer oNTBiasCurrent = null;
  private Date fecha = null;
  private String telefono = null;
  private String nodeAdress = null;
  private String nodeName = null;
  private Double oNTVoltage = null;
  private Integer oNTTemp = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Id")
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IdFacilidades")
  public Integer getIdFacilidades() {
    return idFacilidades;
  }
  public void setIdFacilidades(Integer idFacilidades) {
    this.idFacilidades = idFacilidades;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("velocidadConfigurada_DN")
  public Integer getVelocidadConfiguradaDN() {
    return velocidadConfiguradaDN;
  }
  public void setVelocidadConfiguradaDN(Integer velocidadConfiguradaDN) {
    this.velocidadConfiguradaDN = velocidadConfiguradaDN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("velocidadConfigurada_UP")
  public Integer getVelocidadConfiguradaUP() {
    return velocidadConfiguradaUP;
  }
  public void setVelocidadConfiguradaUP(Integer velocidadConfiguradaUP) {
    this.velocidadConfiguradaUP = velocidadConfiguradaUP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTTxPower")
  public Double getONTTxPower() {
    return oNTTxPower;
  }
  public void setONTTxPower(Double oNTTxPower) {
    this.oNTTxPower = oNTTxPower;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTRxPower")
  public Double getONTRxPower() {
    return oNTRxPower;
  }
  public void setONTRxPower(Double oNTRxPower) {
    this.oNTRxPower = oNTRxPower;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OLTRxONTOpticalPower")
  public Double getOLTRxONTOpticalPower() {
    return oLTRxONTOpticalPower;
  }
  public void setOLTRxONTOpticalPower(Double oLTRxONTOpticalPower) {
    this.oLTRxONTOpticalPower = oLTRxONTOpticalPower;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTBiasCurrent")
  public Integer getONTBiasCurrent() {
    return oNTBiasCurrent;
  }
  public void setONTBiasCurrent(Integer oNTBiasCurrent) {
    this.oNTBiasCurrent = oNTBiasCurrent;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("fecha")
  public Date getFecha() {
    return fecha;
  }
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Telefono")
  public String getTelefono() {
    return telefono;
  }
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("NodeAdress")
  public String getNodeAdress() {
    return nodeAdress;
  }
  public void setNodeAdress(String nodeAdress) {
    this.nodeAdress = nodeAdress;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("NodeName")
  public String getNodeName() {
    return nodeName;
  }
  public void setNodeName(String nodeName) {
    this.nodeName = nodeName;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTVoltage")
  public Double getONTVoltage() {
    return oNTVoltage;
  }
  public void setONTVoltage(Double oNTVoltage) {
    this.oNTVoltage = oNTVoltage;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTTemp")
  public Integer getONTTemp() {
    return oNTTemp;
  }
  public void setONTTemp(Integer oNTTemp) {
    this.oNTTemp = oNTTemp;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class VwHistoricoGraficosOLT {\n");
    
    sb.append("    id: ").append(StringUtil.toIndentedString(id)).append("\n");
    sb.append("    idFacilidades: ").append(StringUtil.toIndentedString(idFacilidades)).append("\n");
    sb.append("    velocidadConfiguradaDN: ").append(StringUtil.toIndentedString(velocidadConfiguradaDN)).append("\n");
    sb.append("    velocidadConfiguradaUP: ").append(StringUtil.toIndentedString(velocidadConfiguradaUP)).append("\n");
    sb.append("    oNTTxPower: ").append(StringUtil.toIndentedString(oNTTxPower)).append("\n");
    sb.append("    oNTRxPower: ").append(StringUtil.toIndentedString(oNTRxPower)).append("\n");
    sb.append("    oLTRxONTOpticalPower: ").append(StringUtil.toIndentedString(oLTRxONTOpticalPower)).append("\n");
    sb.append("    oNTBiasCurrent: ").append(StringUtil.toIndentedString(oNTBiasCurrent)).append("\n");
    sb.append("    fecha: ").append(StringUtil.toIndentedString(fecha)).append("\n");
    sb.append("    telefono: ").append(StringUtil.toIndentedString(telefono)).append("\n");
    sb.append("    nodeAdress: ").append(StringUtil.toIndentedString(nodeAdress)).append("\n");
    sb.append("    nodeName: ").append(StringUtil.toIndentedString(nodeName)).append("\n");
    sb.append("    oNTVoltage: ").append(StringUtil.toIndentedString(oNTVoltage)).append("\n");
    sb.append("    oNTTemp: ").append(StringUtil.toIndentedString(oNTTemp)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
