package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import java.util.Date;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class VwHistoricoResultDslam   {
  
  private String telefono = null;
  private String nodeAddress = null;
  private String nodeName = null;
  private String usuario = null;
  private String ani = null;
  private String tarjeta = null;
  private Integer codigoTecnico = null;
  private String tipo = null;
  private Integer osTicket = null;
  private Integer id = null;
  private Boolean condadministrativa = null;
  private Boolean condoperativa = null;
  private Boolean atenuacion = null;
  private Boolean desbalance = null;
  private Boolean velocidad = null;
  private Boolean configuracion = null;
  private Boolean ruido = null;
  private Boolean ip = null;
  private Date fecha = null;
  private Integer idDiagnostico = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Telefono")
  public String getTelefono() {
    return telefono;
  }
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("NodeAddress")
  public String getNodeAddress() {
    return nodeAddress;
  }
  public void setNodeAddress(String nodeAddress) {
    this.nodeAddress = nodeAddress;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("NodeName")
  public String getNodeName() {
    return nodeName;
  }
  public void setNodeName(String nodeName) {
    this.nodeName = nodeName;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Usuario")
  public String getUsuario() {
    return usuario;
  }
  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Ani")
  public String getAni() {
    return ani;
  }
  public void setAni(String ani) {
    this.ani = ani;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Tarjeta")
  public String getTarjeta() {
    return tarjeta;
  }
  public void setTarjeta(String tarjeta) {
    this.tarjeta = tarjeta;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("CodigoTecnico")
  public Integer getCodigoTecnico() {
    return codigoTecnico;
  }
  public void setCodigoTecnico(Integer codigoTecnico) {
    this.codigoTecnico = codigoTecnico;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Tipo")
  public String getTipo() {
    return tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OsTicket")
  public Integer getOsTicket() {
    return osTicket;
  }
  public void setOsTicket(Integer osTicket) {
    this.osTicket = osTicket;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Id")
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Condadministrativa")
  public Boolean getCondadministrativa() {
    return condadministrativa;
  }
  public void setCondadministrativa(Boolean condadministrativa) {
    this.condadministrativa = condadministrativa;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Condoperativa")
  public Boolean getCondoperativa() {
    return condoperativa;
  }
  public void setCondoperativa(Boolean condoperativa) {
    this.condoperativa = condoperativa;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Atenuacion")
  public Boolean getAtenuacion() {
    return atenuacion;
  }
  public void setAtenuacion(Boolean atenuacion) {
    this.atenuacion = atenuacion;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Desbalance")
  public Boolean getDesbalance() {
    return desbalance;
  }
  public void setDesbalance(Boolean desbalance) {
    this.desbalance = desbalance;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad")
  public Boolean getVelocidad() {
    return velocidad;
  }
  public void setVelocidad(Boolean velocidad) {
    this.velocidad = velocidad;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Configuracion")
  public Boolean getConfiguracion() {
    return configuracion;
  }
  public void setConfiguracion(Boolean configuracion) {
    this.configuracion = configuracion;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Ruido")
  public Boolean getRuido() {
    return ruido;
  }
  public void setRuido(Boolean ruido) {
    this.ruido = ruido;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Ip")
  public Boolean getIp() {
    return ip;
  }
  public void setIp(Boolean ip) {
    this.ip = ip;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Fecha")
  public Date getFecha() {
    return fecha;
  }
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IdDiagnostico")
  public Integer getIdDiagnostico() {
    return idDiagnostico;
  }
  public void setIdDiagnostico(Integer idDiagnostico) {
    this.idDiagnostico = idDiagnostico;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class VwHistoricoResultDslam {\n");
    
    sb.append("    telefono: ").append(StringUtil.toIndentedString(telefono)).append("\n");
    sb.append("    nodeAddress: ").append(StringUtil.toIndentedString(nodeAddress)).append("\n");
    sb.append("    nodeName: ").append(StringUtil.toIndentedString(nodeName)).append("\n");
    sb.append("    usuario: ").append(StringUtil.toIndentedString(usuario)).append("\n");
    sb.append("    ani: ").append(StringUtil.toIndentedString(ani)).append("\n");
    sb.append("    tarjeta: ").append(StringUtil.toIndentedString(tarjeta)).append("\n");
    sb.append("    codigoTecnico: ").append(StringUtil.toIndentedString(codigoTecnico)).append("\n");
    sb.append("    tipo: ").append(StringUtil.toIndentedString(tipo)).append("\n");
    sb.append("    osTicket: ").append(StringUtil.toIndentedString(osTicket)).append("\n");
    sb.append("    id: ").append(StringUtil.toIndentedString(id)).append("\n");
    sb.append("    condadministrativa: ").append(StringUtil.toIndentedString(condadministrativa)).append("\n");
    sb.append("    condoperativa: ").append(StringUtil.toIndentedString(condoperativa)).append("\n");
    sb.append("    atenuacion: ").append(StringUtil.toIndentedString(atenuacion)).append("\n");
    sb.append("    desbalance: ").append(StringUtil.toIndentedString(desbalance)).append("\n");
    sb.append("    velocidad: ").append(StringUtil.toIndentedString(velocidad)).append("\n");
    sb.append("    configuracion: ").append(StringUtil.toIndentedString(configuracion)).append("\n");
    sb.append("    ruido: ").append(StringUtil.toIndentedString(ruido)).append("\n");
    sb.append("    ip: ").append(StringUtil.toIndentedString(ip)).append("\n");
    sb.append("    fecha: ").append(StringUtil.toIndentedString(fecha)).append("\n");
    sb.append("    idDiagnostico: ").append(StringUtil.toIndentedString(idDiagnostico)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
