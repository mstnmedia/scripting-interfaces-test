package sacs_v1.model;

import sacs_v1.invoker.StringUtil;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Facilidad del elemento de red
 *
 */
@ApiModel(description = "Facilidad del elemento de red")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class FacilityViewModel {

	private String vendorName = null;
	private String nodeName = null;
	private String nodeAddress = null;
	private Boolean isFiber = null;

	/**
	 * Vendor Ej: ALCATEL, HUAWEI.
	 *
	 */
	@ApiModelProperty(value = "Vendor Ej: ALCATEL, HUAWEI.")
	@JsonProperty("VendorName")
	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * Nombre del dispositivo a utilizar.Ej: GHLAYUCA01
	 *
	 */
	@ApiModelProperty(value = "Nombre del dispositivo a utilizar.Ej: GHLAYUCA01")
	@JsonProperty("NodeName")
	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	/**
	 * Puerto: SLOT 13-PUERTO 0-21
	 *
	 */
	@ApiModelProperty(value = "Puerto: SLOT 13-PUERTO 0-21")
	@JsonProperty("NodeAddress")
	public String getNodeAddress() {
		return nodeAddress;
	}

	public void setNodeAddress(String nodeAddress) {
		this.nodeAddress = nodeAddress;
	}

	/**
	 * Indica si es fibra optica o no.
	 *
	 */
	@ApiModelProperty(value = "Indica si es fibra optica o no.")
	@JsonProperty("IsFiber")
	public Boolean getIsFiber() {
		return isFiber;
	}

	public void setIsFiber(Boolean isFiber) {
		this.isFiber = isFiber;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class FacilityViewModel {\n");

		sb.append("    vendorName: ").append(StringUtil.toIndentedString(vendorName)).append("\n");
		sb.append("    nodeName: ").append(StringUtil.toIndentedString(nodeName)).append("\n");
		sb.append("    nodeAddress: ").append(StringUtil.toIndentedString(nodeAddress)).append("\n");
		sb.append("    isFiber: ").append(StringUtil.toIndentedString(isFiber)).append("\n");
		sb.append("}");
		return sb.toString();
	}
}
