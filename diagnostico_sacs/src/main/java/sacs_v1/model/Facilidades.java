package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import java.util.Date;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class Facilidades   {
  
  private Integer id = null;
  private String telefono = null;
  private String nodeAddress = null;
  private String nodeName = null;
  private String equipmentName = null;
  private String circuitDesignId = null;
  private String vendorName = null;
  private String type = null;
  private String circuitId = null;
  private String clliCode = null;
  private String status = null;
  private String tipo = null;
  private String tipoReferencia = null;
  private String referencia = null;
  private String usuario = null;
  private String sistema = null;
  private Date fecha = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Id")
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Telefono")
  public String getTelefono() {
    return telefono;
  }
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("NodeAddress")
  public String getNodeAddress() {
    return nodeAddress;
  }
  public void setNodeAddress(String nodeAddress) {
    this.nodeAddress = nodeAddress;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("NodeName")
  public String getNodeName() {
    return nodeName;
  }
  public void setNodeName(String nodeName) {
    this.nodeName = nodeName;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("EquipmentName")
  public String getEquipmentName() {
    return equipmentName;
  }
  public void setEquipmentName(String equipmentName) {
    this.equipmentName = equipmentName;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("CircuitDesignId")
  public String getCircuitDesignId() {
    return circuitDesignId;
  }
  public void setCircuitDesignId(String circuitDesignId) {
    this.circuitDesignId = circuitDesignId;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VendorName")
  public String getVendorName() {
    return vendorName;
  }
  public void setVendorName(String vendorName) {
    this.vendorName = vendorName;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Type")
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("CircuitId")
  public String getCircuitId() {
    return circuitId;
  }
  public void setCircuitId(String circuitId) {
    this.circuitId = circuitId;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ClliCode")
  public String getClliCode() {
    return clliCode;
  }
  public void setClliCode(String clliCode) {
    this.clliCode = clliCode;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Status")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Tipo")
  public String getTipo() {
    return tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("TipoReferencia")
  public String getTipoReferencia() {
    return tipoReferencia;
  }
  public void setTipoReferencia(String tipoReferencia) {
    this.tipoReferencia = tipoReferencia;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Referencia")
  public String getReferencia() {
    return referencia;
  }
  public void setReferencia(String referencia) {
    this.referencia = referencia;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Usuario")
  public String getUsuario() {
    return usuario;
  }
  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Sistema")
  public String getSistema() {
    return sistema;
  }
  public void setSistema(String sistema) {
    this.sistema = sistema;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Fecha")
  public Date getFecha() {
    return fecha;
  }
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Facilidades {\n");
    
    sb.append("    id: ").append(StringUtil.toIndentedString(id)).append("\n");
    sb.append("    telefono: ").append(StringUtil.toIndentedString(telefono)).append("\n");
    sb.append("    nodeAddress: ").append(StringUtil.toIndentedString(nodeAddress)).append("\n");
    sb.append("    nodeName: ").append(StringUtil.toIndentedString(nodeName)).append("\n");
    sb.append("    equipmentName: ").append(StringUtil.toIndentedString(equipmentName)).append("\n");
    sb.append("    circuitDesignId: ").append(StringUtil.toIndentedString(circuitDesignId)).append("\n");
    sb.append("    vendorName: ").append(StringUtil.toIndentedString(vendorName)).append("\n");
    sb.append("    type: ").append(StringUtil.toIndentedString(type)).append("\n");
    sb.append("    circuitId: ").append(StringUtil.toIndentedString(circuitId)).append("\n");
    sb.append("    clliCode: ").append(StringUtil.toIndentedString(clliCode)).append("\n");
    sb.append("    status: ").append(StringUtil.toIndentedString(status)).append("\n");
    sb.append("    tipo: ").append(StringUtil.toIndentedString(tipo)).append("\n");
    sb.append("    tipoReferencia: ").append(StringUtil.toIndentedString(tipoReferencia)).append("\n");
    sb.append("    referencia: ").append(StringUtil.toIndentedString(referencia)).append("\n");
    sb.append("    usuario: ").append(StringUtil.toIndentedString(usuario)).append("\n");
    sb.append("    sistema: ").append(StringUtil.toIndentedString(sistema)).append("\n");
    sb.append("    fecha: ").append(StringUtil.toIndentedString(fecha)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
