package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import sacs_v1.model.RadiusViewModel;
import java.util.*;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Representa los datos utilizados en RADIUS.
 **/
@ApiModel(description = "Representa los datos utilizados en RADIUS.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class RadiusReturnViewModel   {
  
  private List<RadiusViewModel> sessionList = new ArrayList<RadiusViewModel>();
  private Integer count = null;

  
  /**
   * Listado de sesiones que ha tenido el usuario.
   **/
  @ApiModelProperty(value = "Listado de sesiones que ha tenido el usuario.")
  @JsonProperty("SessionList")
  public List<RadiusViewModel> getSessionList() {
    return sessionList;
  }
  public void setSessionList(List<RadiusViewModel> sessionList) {
    this.sessionList = sessionList;
  }

  
  /**
   * Conteo de conexiones y desconexiones en un día.
   **/
  @ApiModelProperty(value = "Conteo de conexiones y desconexiones en un día.")
  @JsonProperty("Count")
  public Integer getCount() {
    return count;
  }
  public void setCount(Integer count) {
    this.count = count;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class RadiusReturnViewModel {\n");
    
    sb.append("    sessionList: ").append(StringUtil.toIndentedString(sessionList)).append("\n");
    sb.append("    count: ").append(StringUtil.toIndentedString(count)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
