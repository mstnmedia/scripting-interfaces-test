package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import sacs_v1.model.VwHistoricoGraficosDslam;
import java.util.*;
import sacs_v1.model.VwHistoricoGraficosOLT;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Representa el histórico en forma numérica para el graficar el comportamiento del sistema.
 **/
@ApiModel(description = "Representa el histórico en forma numérica para el graficar el comportamiento del sistema.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class HistoricoGraficoViewModel   {
  
  private List<VwHistoricoGraficosOLT> historicoGraficoOlt = new ArrayList<VwHistoricoGraficosOLT>();
  private List<VwHistoricoGraficosDslam> historicoGraficoDslam = new ArrayList<VwHistoricoGraficosDslam>();

  
  /**
   * Histórico del gráfico OLT.
   **/
  @ApiModelProperty(value = "Histórico del gráfico OLT.")
  @JsonProperty("HistoricoGraficoOlt")
  public List<VwHistoricoGraficosOLT> getHistoricoGraficoOlt() {
    return historicoGraficoOlt;
  }
  public void setHistoricoGraficoOlt(List<VwHistoricoGraficosOLT> historicoGraficoOlt) {
    this.historicoGraficoOlt = historicoGraficoOlt;
  }

  
  /**
   * Histórico del gráfico DSLAM.
   **/
  @ApiModelProperty(value = "Histórico del gráfico DSLAM.")
  @JsonProperty("HistoricoGraficoDslam")
  public List<VwHistoricoGraficosDslam> getHistoricoGraficoDslam() {
    return historicoGraficoDslam;
  }
  public void setHistoricoGraficoDslam(List<VwHistoricoGraficosDslam> historicoGraficoDslam) {
    this.historicoGraficoDslam = historicoGraficoDslam;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class HistoricoGraficoViewModel {\n");
    
    sb.append("    historicoGraficoOlt: ").append(StringUtil.toIndentedString(historicoGraficoOlt)).append("\n");
    sb.append("    historicoGraficoDslam: ").append(StringUtil.toIndentedString(historicoGraficoDslam)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
