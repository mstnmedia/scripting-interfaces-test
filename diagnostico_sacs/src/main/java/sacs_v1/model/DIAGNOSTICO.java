package sacs_v1.model;

import sacs_v1.invoker.StringUtil;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class DIAGNOSTICO   {
  
  private Integer ID = null;
  private Integer ID_FACILIDADES = null;
  private Boolean condAdministrativa = null;
  private Boolean condOperativa = null;
  private Boolean atenuacion = null;
  private Boolean atenuacionSubida = null;
  private Boolean atenuacionBajada = null;
  private Boolean atenuacionSubidaLoop = null;
  private Boolean atenuacionBajadaLoop = null;
  private Boolean desbalance = null;
  private Boolean velocidad = null;
  private Boolean serviceProfile = null;
  private Boolean configuracion = null;
  private Boolean ruido = null;
  private Boolean ruidoBajada = null;
  private Boolean ruidoSubida = null;
  private Boolean pVC35 = null;
  private Boolean pVC33 = null;
  private Boolean autenticadorIPTV = null;
  private Boolean IP = null;
  private Boolean IP_VIDEO = null;
  private Boolean VLAN = null;
  private Boolean VLAN_IPTV = null;
  private Boolean testResult = null;
  private Boolean margenEstabilidad = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ID")
  public Integer getID() {
    return ID;
  }
  public void setID(Integer ID) {
    this.ID = ID;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ID_FACILIDADES")
  public Integer getIDFACILIDADES() {
    return ID_FACILIDADES;
  }
  public void setIDFACILIDADES(Integer ID_FACILIDADES) {
    this.ID_FACILIDADES = ID_FACILIDADES;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("CondAdministrativa")
  public Boolean getCondAdministrativa() {
    return condAdministrativa;
  }
  public void setCondAdministrativa(Boolean condAdministrativa) {
    this.condAdministrativa = condAdministrativa;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("CondOperativa")
  public Boolean getCondOperativa() {
    return condOperativa;
  }
  public void setCondOperativa(Boolean condOperativa) {
    this.condOperativa = condOperativa;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Atenuacion")
  public Boolean getAtenuacion() {
    return atenuacion;
  }
  public void setAtenuacion(Boolean atenuacion) {
    this.atenuacion = atenuacion;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Atenuacion_Subida")
  public Boolean getAtenuacionSubida() {
    return atenuacionSubida;
  }
  public void setAtenuacionSubida(Boolean atenuacionSubida) {
    this.atenuacionSubida = atenuacionSubida;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Atenuacion_Bajada")
  public Boolean getAtenuacionBajada() {
    return atenuacionBajada;
  }
  public void setAtenuacionBajada(Boolean atenuacionBajada) {
    this.atenuacionBajada = atenuacionBajada;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Atenuacion_Subida_Loop")
  public Boolean getAtenuacionSubidaLoop() {
    return atenuacionSubidaLoop;
  }
  public void setAtenuacionSubidaLoop(Boolean atenuacionSubidaLoop) {
    this.atenuacionSubidaLoop = atenuacionSubidaLoop;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Atenuacion_Bajada_Loop")
  public Boolean getAtenuacionBajadaLoop() {
    return atenuacionBajadaLoop;
  }
  public void setAtenuacionBajadaLoop(Boolean atenuacionBajadaLoop) {
    this.atenuacionBajadaLoop = atenuacionBajadaLoop;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Desbalance")
  public Boolean getDesbalance() {
    return desbalance;
  }
  public void setDesbalance(Boolean desbalance) {
    this.desbalance = desbalance;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad")
  public Boolean getVelocidad() {
    return velocidad;
  }
  public void setVelocidad(Boolean velocidad) {
    this.velocidad = velocidad;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Service_Profile")
  public Boolean getServiceProfile() {
    return serviceProfile;
  }
  public void setServiceProfile(Boolean serviceProfile) {
    this.serviceProfile = serviceProfile;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Configuracion")
  public Boolean getConfiguracion() {
    return configuracion;
  }
  public void setConfiguracion(Boolean configuracion) {
    this.configuracion = configuracion;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Ruido")
  public Boolean getRuido() {
    return ruido;
  }
  public void setRuido(Boolean ruido) {
    this.ruido = ruido;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Ruido_Bajada")
  public Boolean getRuidoBajada() {
    return ruidoBajada;
  }
  public void setRuidoBajada(Boolean ruidoBajada) {
    this.ruidoBajada = ruidoBajada;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Ruido_Subida")
  public Boolean getRuidoSubida() {
    return ruidoSubida;
  }
  public void setRuidoSubida(Boolean ruidoSubida) {
    this.ruidoSubida = ruidoSubida;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("PVC35")
  public Boolean getPVC35() {
    return pVC35;
  }
  public void setPVC35(Boolean pVC35) {
    this.pVC35 = pVC35;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("PVC33")
  public Boolean getPVC33() {
    return pVC33;
  }
  public void setPVC33(Boolean pVC33) {
    this.pVC33 = pVC33;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("AutenticadorIPTV")
  public Boolean getAutenticadorIPTV() {
    return autenticadorIPTV;
  }
  public void setAutenticadorIPTV(Boolean autenticadorIPTV) {
    this.autenticadorIPTV = autenticadorIPTV;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IP")
  public Boolean getIP() {
    return IP;
  }
  public void setIP(Boolean IP) {
    this.IP = IP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IP_VIDEO")
  public Boolean getIPVIDEO() {
    return IP_VIDEO;
  }
  public void setIPVIDEO(Boolean IP_VIDEO) {
    this.IP_VIDEO = IP_VIDEO;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VLAN")
  public Boolean getVLAN() {
    return VLAN;
  }
  public void setVLAN(Boolean VLAN) {
    this.VLAN = VLAN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VLAN_IPTV")
  public Boolean getVLANIPTV() {
    return VLAN_IPTV;
  }
  public void setVLANIPTV(Boolean VLAN_IPTV) {
    this.VLAN_IPTV = VLAN_IPTV;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("TestResult")
  public Boolean getTestResult() {
    return testResult;
  }
  public void setTestResult(Boolean testResult) {
    this.testResult = testResult;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("MargenEstabilidad")
  public Boolean getMargenEstabilidad() {
    return margenEstabilidad;
  }
  public void setMargenEstabilidad(Boolean margenEstabilidad) {
    this.margenEstabilidad = margenEstabilidad;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class DIAGNOSTICO {\n");
    
    sb.append("    ID: ").append(StringUtil.toIndentedString(ID)).append("\n");
    sb.append("    ID_FACILIDADES: ").append(StringUtil.toIndentedString(ID_FACILIDADES)).append("\n");
    sb.append("    condAdministrativa: ").append(StringUtil.toIndentedString(condAdministrativa)).append("\n");
    sb.append("    condOperativa: ").append(StringUtil.toIndentedString(condOperativa)).append("\n");
    sb.append("    atenuacion: ").append(StringUtil.toIndentedString(atenuacion)).append("\n");
    sb.append("    atenuacionSubida: ").append(StringUtil.toIndentedString(atenuacionSubida)).append("\n");
    sb.append("    atenuacionBajada: ").append(StringUtil.toIndentedString(atenuacionBajada)).append("\n");
    sb.append("    atenuacionSubidaLoop: ").append(StringUtil.toIndentedString(atenuacionSubidaLoop)).append("\n");
    sb.append("    atenuacionBajadaLoop: ").append(StringUtil.toIndentedString(atenuacionBajadaLoop)).append("\n");
    sb.append("    desbalance: ").append(StringUtil.toIndentedString(desbalance)).append("\n");
    sb.append("    velocidad: ").append(StringUtil.toIndentedString(velocidad)).append("\n");
    sb.append("    serviceProfile: ").append(StringUtil.toIndentedString(serviceProfile)).append("\n");
    sb.append("    configuracion: ").append(StringUtil.toIndentedString(configuracion)).append("\n");
    sb.append("    ruido: ").append(StringUtil.toIndentedString(ruido)).append("\n");
    sb.append("    ruidoBajada: ").append(StringUtil.toIndentedString(ruidoBajada)).append("\n");
    sb.append("    ruidoSubida: ").append(StringUtil.toIndentedString(ruidoSubida)).append("\n");
    sb.append("    pVC35: ").append(StringUtil.toIndentedString(pVC35)).append("\n");
    sb.append("    pVC33: ").append(StringUtil.toIndentedString(pVC33)).append("\n");
    sb.append("    autenticadorIPTV: ").append(StringUtil.toIndentedString(autenticadorIPTV)).append("\n");
    sb.append("    IP: ").append(StringUtil.toIndentedString(IP)).append("\n");
    sb.append("    IP_VIDEO: ").append(StringUtil.toIndentedString(IP_VIDEO)).append("\n");
    sb.append("    VLAN: ").append(StringUtil.toIndentedString(VLAN)).append("\n");
    sb.append("    VLAN_IPTV: ").append(StringUtil.toIndentedString(VLAN_IPTV)).append("\n");
    sb.append("    testResult: ").append(StringUtil.toIndentedString(testResult)).append("\n");
    sb.append("    margenEstabilidad: ").append(StringUtil.toIndentedString(margenEstabilidad)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
