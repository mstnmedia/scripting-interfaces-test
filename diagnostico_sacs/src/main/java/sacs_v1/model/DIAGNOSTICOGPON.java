package sacs_v1.model;

import sacs_v1.invoker.StringUtil;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class DIAGNOSTICOGPON   {
  
  private Integer ID = null;
  private Integer ID_FACILIDADES = null;
  private Boolean oLTAdmState = null;
  private Boolean oLTOperState = null;
  private Boolean oNTAdmState = null;
  private Boolean oNTOperState = null;
  private Boolean oNTRxPower = null;
  private Boolean oNTTxPower = null;
  private Boolean oNTBiasCurrent = null;
  private Boolean oNTTemp = null;
  private Boolean oNTVoltage = null;
  private Boolean oLTRxONTOpticalPower = null;
  private Boolean velConfigurada = null;
  private Boolean velConfiguradaUP = null;
  private Boolean velConfiguradaDN = null;
  private Boolean password = null;
  private Boolean IPTV = null;
  private Boolean ADSL = null;
  private Boolean VOIP = null;
  private Boolean iPTVTxRx = null;
  private Boolean iPTVServicePortStatus = null;
  private Boolean aDSLServicePortStatus = null;
  private Boolean vOIPServicePortStatus = null;
  private Boolean IP_IPTV = null;
  private Boolean IP_ADSL = null;
  private Boolean IP_VOIP = null;
  private Boolean testResult = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ID")
  public Integer getID() {
    return ID;
  }
  public void setID(Integer ID) {
    this.ID = ID;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ID_FACILIDADES")
  public Integer getIDFACILIDADES() {
    return ID_FACILIDADES;
  }
  public void setIDFACILIDADES(Integer ID_FACILIDADES) {
    this.ID_FACILIDADES = ID_FACILIDADES;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OLTAdmState")
  public Boolean getOLTAdmState() {
    return oLTAdmState;
  }
  public void setOLTAdmState(Boolean oLTAdmState) {
    this.oLTAdmState = oLTAdmState;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OLTOperState")
  public Boolean getOLTOperState() {
    return oLTOperState;
  }
  public void setOLTOperState(Boolean oLTOperState) {
    this.oLTOperState = oLTOperState;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTAdmState")
  public Boolean getONTAdmState() {
    return oNTAdmState;
  }
  public void setONTAdmState(Boolean oNTAdmState) {
    this.oNTAdmState = oNTAdmState;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTOperState")
  public Boolean getONTOperState() {
    return oNTOperState;
  }
  public void setONTOperState(Boolean oNTOperState) {
    this.oNTOperState = oNTOperState;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTRxPower")
  public Boolean getONTRxPower() {
    return oNTRxPower;
  }
  public void setONTRxPower(Boolean oNTRxPower) {
    this.oNTRxPower = oNTRxPower;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTTxPower")
  public Boolean getONTTxPower() {
    return oNTTxPower;
  }
  public void setONTTxPower(Boolean oNTTxPower) {
    this.oNTTxPower = oNTTxPower;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTBiasCurrent")
  public Boolean getONTBiasCurrent() {
    return oNTBiasCurrent;
  }
  public void setONTBiasCurrent(Boolean oNTBiasCurrent) {
    this.oNTBiasCurrent = oNTBiasCurrent;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTTemp")
  public Boolean getONTTemp() {
    return oNTTemp;
  }
  public void setONTTemp(Boolean oNTTemp) {
    this.oNTTemp = oNTTemp;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTVoltage")
  public Boolean getONTVoltage() {
    return oNTVoltage;
  }
  public void setONTVoltage(Boolean oNTVoltage) {
    this.oNTVoltage = oNTVoltage;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OLTRxONTOpticalPower")
  public Boolean getOLTRxONTOpticalPower() {
    return oLTRxONTOpticalPower;
  }
  public void setOLTRxONTOpticalPower(Boolean oLTRxONTOpticalPower) {
    this.oLTRxONTOpticalPower = oLTRxONTOpticalPower;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VelConfigurada")
  public Boolean getVelConfigurada() {
    return velConfigurada;
  }
  public void setVelConfigurada(Boolean velConfigurada) {
    this.velConfigurada = velConfigurada;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VelConfiguradaUP")
  public Boolean getVelConfiguradaUP() {
    return velConfiguradaUP;
  }
  public void setVelConfiguradaUP(Boolean velConfiguradaUP) {
    this.velConfiguradaUP = velConfiguradaUP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VelConfiguradaDN")
  public Boolean getVelConfiguradaDN() {
    return velConfiguradaDN;
  }
  public void setVelConfiguradaDN(Boolean velConfiguradaDN) {
    this.velConfiguradaDN = velConfiguradaDN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Password")
  public Boolean getPassword() {
    return password;
  }
  public void setPassword(Boolean password) {
    this.password = password;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IPTV")
  public Boolean getIPTV() {
    return IPTV;
  }
  public void setIPTV(Boolean IPTV) {
    this.IPTV = IPTV;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ADSL")
  public Boolean getADSL() {
    return ADSL;
  }
  public void setADSL(Boolean ADSL) {
    this.ADSL = ADSL;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VOIP")
  public Boolean getVOIP() {
    return VOIP;
  }
  public void setVOIP(Boolean VOIP) {
    this.VOIP = VOIP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IPTV_Tx_Rx")
  public Boolean getIPTVTxRx() {
    return iPTVTxRx;
  }
  public void setIPTVTxRx(Boolean iPTVTxRx) {
    this.iPTVTxRx = iPTVTxRx;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IPTV_ServicePort_Status")
  public Boolean getIPTVServicePortStatus() {
    return iPTVServicePortStatus;
  }
  public void setIPTVServicePortStatus(Boolean iPTVServicePortStatus) {
    this.iPTVServicePortStatus = iPTVServicePortStatus;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ADSL_ServicePort_Status")
  public Boolean getADSLServicePortStatus() {
    return aDSLServicePortStatus;
  }
  public void setADSLServicePortStatus(Boolean aDSLServicePortStatus) {
    this.aDSLServicePortStatus = aDSLServicePortStatus;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VOIP_ServicePort_Status")
  public Boolean getVOIPServicePortStatus() {
    return vOIPServicePortStatus;
  }
  public void setVOIPServicePortStatus(Boolean vOIPServicePortStatus) {
    this.vOIPServicePortStatus = vOIPServicePortStatus;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IP_IPTV")
  public Boolean getIPIPTV() {
    return IP_IPTV;
  }
  public void setIPIPTV(Boolean IP_IPTV) {
    this.IP_IPTV = IP_IPTV;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IP_ADSL")
  public Boolean getIPADSL() {
    return IP_ADSL;
  }
  public void setIPADSL(Boolean IP_ADSL) {
    this.IP_ADSL = IP_ADSL;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IP_VOIP")
  public Boolean getIPVOIP() {
    return IP_VOIP;
  }
  public void setIPVOIP(Boolean IP_VOIP) {
    this.IP_VOIP = IP_VOIP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("TestResult")
  public Boolean getTestResult() {
    return testResult;
  }
  public void setTestResult(Boolean testResult) {
    this.testResult = testResult;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class DIAGNOSTICOGPON {\n");
    
    sb.append("    ID: ").append(StringUtil.toIndentedString(ID)).append("\n");
    sb.append("    ID_FACILIDADES: ").append(StringUtil.toIndentedString(ID_FACILIDADES)).append("\n");
    sb.append("    oLTAdmState: ").append(StringUtil.toIndentedString(oLTAdmState)).append("\n");
    sb.append("    oLTOperState: ").append(StringUtil.toIndentedString(oLTOperState)).append("\n");
    sb.append("    oNTAdmState: ").append(StringUtil.toIndentedString(oNTAdmState)).append("\n");
    sb.append("    oNTOperState: ").append(StringUtil.toIndentedString(oNTOperState)).append("\n");
    sb.append("    oNTRxPower: ").append(StringUtil.toIndentedString(oNTRxPower)).append("\n");
    sb.append("    oNTTxPower: ").append(StringUtil.toIndentedString(oNTTxPower)).append("\n");
    sb.append("    oNTBiasCurrent: ").append(StringUtil.toIndentedString(oNTBiasCurrent)).append("\n");
    sb.append("    oNTTemp: ").append(StringUtil.toIndentedString(oNTTemp)).append("\n");
    sb.append("    oNTVoltage: ").append(StringUtil.toIndentedString(oNTVoltage)).append("\n");
    sb.append("    oLTRxONTOpticalPower: ").append(StringUtil.toIndentedString(oLTRxONTOpticalPower)).append("\n");
    sb.append("    velConfigurada: ").append(StringUtil.toIndentedString(velConfigurada)).append("\n");
    sb.append("    velConfiguradaUP: ").append(StringUtil.toIndentedString(velConfiguradaUP)).append("\n");
    sb.append("    velConfiguradaDN: ").append(StringUtil.toIndentedString(velConfiguradaDN)).append("\n");
    sb.append("    password: ").append(StringUtil.toIndentedString(password)).append("\n");
    sb.append("    IPTV: ").append(StringUtil.toIndentedString(IPTV)).append("\n");
    sb.append("    ADSL: ").append(StringUtil.toIndentedString(ADSL)).append("\n");
    sb.append("    VOIP: ").append(StringUtil.toIndentedString(VOIP)).append("\n");
    sb.append("    iPTVTxRx: ").append(StringUtil.toIndentedString(iPTVTxRx)).append("\n");
    sb.append("    iPTVServicePortStatus: ").append(StringUtil.toIndentedString(iPTVServicePortStatus)).append("\n");
    sb.append("    aDSLServicePortStatus: ").append(StringUtil.toIndentedString(aDSLServicePortStatus)).append("\n");
    sb.append("    vOIPServicePortStatus: ").append(StringUtil.toIndentedString(vOIPServicePortStatus)).append("\n");
    sb.append("    IP_IPTV: ").append(StringUtil.toIndentedString(IP_IPTV)).append("\n");
    sb.append("    IP_ADSL: ").append(StringUtil.toIndentedString(IP_ADSL)).append("\n");
    sb.append("    IP_VOIP: ").append(StringUtil.toIndentedString(IP_VOIP)).append("\n");
    sb.append("    testResult: ").append(StringUtil.toIndentedString(testResult)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
