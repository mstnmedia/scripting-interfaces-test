package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import sacs_v1.model.VwHistoricoResultOlt;
import java.util.*;
import sacs_v1.model.VwHistoricoResultDslam;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Representa la información que corresponde al histórico de diagnósticos.
 **/
@ApiModel(description = "Representa la información que corresponde al histórico de diagnósticos.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class HistoricoViewModel   {
  
  private List<VwHistoricoResultOlt> historicoResultOlt = new ArrayList<VwHistoricoResultOlt>();
  private List<VwHistoricoResultDslam> historicoResultDslam = new ArrayList<VwHistoricoResultDslam>();

  
  /**
   * Histórico de diagnóstico en caso de que sea OLT.
   **/
  @ApiModelProperty(value = "Histórico de diagnóstico en caso de que sea OLT.")
  @JsonProperty("HistoricoResultOlt")
  public List<VwHistoricoResultOlt> getHistoricoResultOlt() {
    return historicoResultOlt;
  }
  public void setHistoricoResultOlt(List<VwHistoricoResultOlt> historicoResultOlt) {
    this.historicoResultOlt = historicoResultOlt;
  }

  
  /**
   * Histórico de diágnostico en caso de que sea DSLAM.
   **/
  @ApiModelProperty(value = "Histórico de diágnostico en caso de que sea DSLAM.")
  @JsonProperty("HistoricoResultDslam")
  public List<VwHistoricoResultDslam> getHistoricoResultDslam() {
    return historicoResultDslam;
  }
  public void setHistoricoResultDslam(List<VwHistoricoResultDslam> historicoResultDslam) {
    this.historicoResultDslam = historicoResultDslam;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class HistoricoViewModel {\n");
    
    sb.append("    historicoResultOlt: ").append(StringUtil.toIndentedString(historicoResultOlt)).append("\n");
    sb.append("    historicoResultDslam: ").append(StringUtil.toIndentedString(historicoResultDslam)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
