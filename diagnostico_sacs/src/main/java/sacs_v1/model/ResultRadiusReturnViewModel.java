package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import sacs_v1.model.RadiusReturnViewModel;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Resultado al ejecutar operación.
 *
 */
@ApiModel(description = "Resultado al ejecutar operación.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class ResultRadiusReturnViewModel {

	public enum StatusEnum {
		CONTINUE("Continue"),
		SWITCHINGPROTOCOLS("SwitchingProtocols"),
		OK("OK"),
		CREATED("Created"),
		ACCEPTED("Accepted"),
		NONAUTHORITATIVEINFORMATION("NonAuthoritativeInformation"),
		NOCONTENT("NoContent"),
		RESETCONTENT("ResetContent"),
		PARTIALCONTENT("PartialContent"),
		MULTIPLECHOICES("MultipleChoices"),
		AMBIGUOUS("Ambiguous"),
		MOVEDPERMANENTLY("MovedPermanently"),
		MOVED("Moved"),
		FOUND("Found"),
		REDIRECT("Redirect"),
		SEEOTHER("SeeOther"),
		REDIRECTMETHOD("RedirectMethod"),
		NOTMODIFIED("NotModified"),
		USEPROXY("UseProxy"),
		UNUSED("Unused"),
		TEMPORARYREDIRECT("TemporaryRedirect"),
		REDIRECTKEEPVERB("RedirectKeepVerb"),
		BADREQUEST("BadRequest"),
		UNAUTHORIZED("Unauthorized"),
		PAYMENTREQUIRED("PaymentRequired"),
		FORBIDDEN("Forbidden"),
		NOTFOUND("NotFound"),
		METHODNOTALLOWED("MethodNotAllowed"),
		NOTACCEPTABLE("NotAcceptable"),
		PROXYAUTHENTICATIONREQUIRED("ProxyAuthenticationRequired"),
		REQUESTTIMEOUT("RequestTimeout"),
		CONFLICT("Conflict"),
		GONE("Gone"),
		LENGTHREQUIRED("LengthRequired"),
		PRECONDITIONFAILED("PreconditionFailed"),
		REQUESTENTITYTOOLARGE("RequestEntityTooLarge"),
		REQUESTURITOOLONG("RequestUriTooLong"),
		UNSUPPORTEDMEDIATYPE("UnsupportedMediaType"),
		REQUESTEDRANGENOTSATISFIABLE("RequestedRangeNotSatisfiable"),
		EXPECTATIONFAILED("ExpectationFailed"),
		UPGRADEREQUIRED("UpgradeRequired"),
		INTERNALSERVERERROR("InternalServerError"),
		NOTIMPLEMENTED("NotImplemented"),
		BADGATEWAY("BadGateway"),
		SERVICEUNAVAILABLE("ServiceUnavailable"),
		GATEWAYTIMEOUT("GatewayTimeout"),
		HTTPVERSIONNOTSUPPORTED("HttpVersionNotSupported");

		private String value;

		StatusEnum(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	private int status = 0;
	private String message = null;
	private RadiusReturnViewModel value = null;

	/**
	 * Código http que representa el estatus de la operación.
   *
	 */
	@ApiModelProperty(value = "Código http que representa el estatus de la operación.")
	@JsonProperty("Status")
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Mensaje que indica el resultado de la operación o si ocurre un error.
   *
	 */
	@ApiModelProperty(value = "Mensaje que indica el resultado de la operación o si ocurre un error.")
	@JsonProperty("Message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Valor genérico representa el resultado devuelto por las consultas.
   *
	 */
	@ApiModelProperty(value = "Valor genérico representa el resultado devuelto por las consultas.")
	@JsonProperty("Value")
	public RadiusReturnViewModel getValue() {
		return value;
	}

	public void setValue(RadiusReturnViewModel value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ResultRadiusReturnViewModel {\n");

		sb.append("    status: ").append(StringUtil.toIndentedString(status)).append("\n");
		sb.append("    message: ").append(StringUtil.toIndentedString(message)).append("\n");
		sb.append("    value: ").append(StringUtil.toIndentedString(value)).append("\n");
		sb.append("}");
		return sb.toString();
	}
}
