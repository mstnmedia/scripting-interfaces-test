package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import sacs_v1.model.DIAGNOSTICOGPON;
import sacs_v1.model.FACILITY;
import sacs_v1.model.SERVICEPORT;
import java.util.*;
import sacs_v1.model.Facilidades;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class GPONINFO   {
  
  private Integer ID = null;
  private Integer ID_FACILIDADES = null;
  private FACILITY facilida = null;
  private Boolean oLTAdmState = null;
  private Boolean oLTOperState = null;
  private Boolean oNTAdmState = null;
  private Boolean oNTOperState = null;
  private Double oLTPortTxPower = null;
  private Double oLTPortRxPower = null;
  private String oLTUptime = null;
  private String oLTFirmware = null;
  private Double oNTRxPower = null;
  private Double oNTTxPower = null;
  private Integer oNTBiasCurrent = null;
  private Integer oNTTemp = null;
  private Double oNTVoltage = null;
  private Integer oNTDistancia = null;
  private String oNTMemUsage = null;
  private String oNTCPUUsage = null;
  private Integer dataUsageDS = null;
  private Integer dataUsageUS = null;
  private String oNTLastDown = null;
  private String oNTLastDownCause = null;
  private String oNTUpTime = null;
  private String currentThroughput = null;
  private String oNTDescription = null;
  private String oNTSerialNumber = null;
  private String oNTLineProfile = null;
  private String oNTVendor = null;
  private String oNTVersion = null;
  private String oNTSoftwareVersion = null;
  private String ontProductDescription = null;
  private String oNTBatteryStatus = null;
  private Integer velocidadContratadaDN = null;
  private Integer velocidadContratadaUP = null;
  private Double velocidadConfiguradaDN = null;
  private Double velocidadConfiguradaUP = null;
  private String oLTPassword = null;
  private List<SERVICEPORT> servicePort = new ArrayList<SERVICEPORT>();
  private Double oLTRxONTOpticalPower = null;
  private String oNTOnlineDuration = null;
  private String oNTLastDownTime = null;
  private DIAGNOSTICOGPON diagnosticoGpon = null;
  private Facilidades facilidades = null;
  private Boolean migradoTripleA = null;
  private String velocidadTripleAUp = null;
  private String velocidadTripleADn = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ID")
  public Integer getID() {
    return ID;
  }
  public void setID(Integer ID) {
    this.ID = ID;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ID_FACILIDADES")
  public Integer getIDFACILIDADES() {
    return ID_FACILIDADES;
  }
  public void setIDFACILIDADES(Integer ID_FACILIDADES) {
    this.ID_FACILIDADES = ID_FACILIDADES;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Facilida")
  public FACILITY getFacilida() {
    return facilida;
  }
  public void setFacilida(FACILITY facilida) {
    this.facilida = facilida;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OLTAdmState")
  public Boolean getOLTAdmState() {
    return oLTAdmState;
  }
  public void setOLTAdmState(Boolean oLTAdmState) {
    this.oLTAdmState = oLTAdmState;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OLTOperState")
  public Boolean getOLTOperState() {
    return oLTOperState;
  }
  public void setOLTOperState(Boolean oLTOperState) {
    this.oLTOperState = oLTOperState;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTAdmState")
  public Boolean getONTAdmState() {
    return oNTAdmState;
  }
  public void setONTAdmState(Boolean oNTAdmState) {
    this.oNTAdmState = oNTAdmState;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTOperState")
  public Boolean getONTOperState() {
    return oNTOperState;
  }
  public void setONTOperState(Boolean oNTOperState) {
    this.oNTOperState = oNTOperState;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OLTPortTxPower")
  public Double getOLTPortTxPower() {
    return oLTPortTxPower;
  }
  public void setOLTPortTxPower(Double oLTPortTxPower) {
    this.oLTPortTxPower = oLTPortTxPower;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OLTPortRxPower")
  public Double getOLTPortRxPower() {
    return oLTPortRxPower;
  }
  public void setOLTPortRxPower(Double oLTPortRxPower) {
    this.oLTPortRxPower = oLTPortRxPower;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OLTUptime")
  public String getOLTUptime() {
    return oLTUptime;
  }
  public void setOLTUptime(String oLTUptime) {
    this.oLTUptime = oLTUptime;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OLTFirmware")
  public String getOLTFirmware() {
    return oLTFirmware;
  }
  public void setOLTFirmware(String oLTFirmware) {
    this.oLTFirmware = oLTFirmware;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTRxPower")
  public Double getONTRxPower() {
    return oNTRxPower;
  }
  public void setONTRxPower(Double oNTRxPower) {
    this.oNTRxPower = oNTRxPower;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTTxPower")
  public Double getONTTxPower() {
    return oNTTxPower;
  }
  public void setONTTxPower(Double oNTTxPower) {
    this.oNTTxPower = oNTTxPower;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTBiasCurrent")
  public Integer getONTBiasCurrent() {
    return oNTBiasCurrent;
  }
  public void setONTBiasCurrent(Integer oNTBiasCurrent) {
    this.oNTBiasCurrent = oNTBiasCurrent;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTTemp")
  public Integer getONTTemp() {
    return oNTTemp;
  }
  public void setONTTemp(Integer oNTTemp) {
    this.oNTTemp = oNTTemp;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTVoltage")
  public Double getONTVoltage() {
    return oNTVoltage;
  }
  public void setONTVoltage(Double oNTVoltage) {
    this.oNTVoltage = oNTVoltage;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTDistancia")
  public Integer getONTDistancia() {
    return oNTDistancia;
  }
  public void setONTDistancia(Integer oNTDistancia) {
    this.oNTDistancia = oNTDistancia;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTMemUsage")
  public String getONTMemUsage() {
    return oNTMemUsage;
  }
  public void setONTMemUsage(String oNTMemUsage) {
    this.oNTMemUsage = oNTMemUsage;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTCPUUsage")
  public String getONTCPUUsage() {
    return oNTCPUUsage;
  }
  public void setONTCPUUsage(String oNTCPUUsage) {
    this.oNTCPUUsage = oNTCPUUsage;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("DataUsageDS")
  public Integer getDataUsageDS() {
    return dataUsageDS;
  }
  public void setDataUsageDS(Integer dataUsageDS) {
    this.dataUsageDS = dataUsageDS;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("DataUsageUS")
  public Integer getDataUsageUS() {
    return dataUsageUS;
  }
  public void setDataUsageUS(Integer dataUsageUS) {
    this.dataUsageUS = dataUsageUS;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTLastDown")
  public String getONTLastDown() {
    return oNTLastDown;
  }
  public void setONTLastDown(String oNTLastDown) {
    this.oNTLastDown = oNTLastDown;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTLastDownCause")
  public String getONTLastDownCause() {
    return oNTLastDownCause;
  }
  public void setONTLastDownCause(String oNTLastDownCause) {
    this.oNTLastDownCause = oNTLastDownCause;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTUpTime")
  public String getONTUpTime() {
    return oNTUpTime;
  }
  public void setONTUpTime(String oNTUpTime) {
    this.oNTUpTime = oNTUpTime;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("CurrentThroughput")
  public String getCurrentThroughput() {
    return currentThroughput;
  }
  public void setCurrentThroughput(String currentThroughput) {
    this.currentThroughput = currentThroughput;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTDescription")
  public String getONTDescription() {
    return oNTDescription;
  }
  public void setONTDescription(String oNTDescription) {
    this.oNTDescription = oNTDescription;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTSerialNumber")
  public String getONTSerialNumber() {
    return oNTSerialNumber;
  }
  public void setONTSerialNumber(String oNTSerialNumber) {
    this.oNTSerialNumber = oNTSerialNumber;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTLineProfile")
  public String getONTLineProfile() {
    return oNTLineProfile;
  }
  public void setONTLineProfile(String oNTLineProfile) {
    this.oNTLineProfile = oNTLineProfile;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTVendor")
  public String getONTVendor() {
    return oNTVendor;
  }
  public void setONTVendor(String oNTVendor) {
    this.oNTVendor = oNTVendor;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTVersion")
  public String getONTVersion() {
    return oNTVersion;
  }
  public void setONTVersion(String oNTVersion) {
    this.oNTVersion = oNTVersion;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTSoftwareVersion")
  public String getONTSoftwareVersion() {
    return oNTSoftwareVersion;
  }
  public void setONTSoftwareVersion(String oNTSoftwareVersion) {
    this.oNTSoftwareVersion = oNTSoftwareVersion;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OntProductDescription")
  public String getOntProductDescription() {
    return ontProductDescription;
  }
  public void setOntProductDescription(String ontProductDescription) {
    this.ontProductDescription = ontProductDescription;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTBatteryStatus")
  public String getONTBatteryStatus() {
    return oNTBatteryStatus;
  }
  public void setONTBatteryStatus(String oNTBatteryStatus) {
    this.oNTBatteryStatus = oNTBatteryStatus;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_Contratada_DN")
  public Integer getVelocidadContratadaDN() {
    return velocidadContratadaDN;
  }
  public void setVelocidadContratadaDN(Integer velocidadContratadaDN) {
    this.velocidadContratadaDN = velocidadContratadaDN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_Contratada_UP")
  public Integer getVelocidadContratadaUP() {
    return velocidadContratadaUP;
  }
  public void setVelocidadContratadaUP(Integer velocidadContratadaUP) {
    this.velocidadContratadaUP = velocidadContratadaUP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_Configurada_DN")
  public Double getVelocidadConfiguradaDN() {
    return velocidadConfiguradaDN;
  }
  public void setVelocidadConfiguradaDN(Double velocidadConfiguradaDN) {
    this.velocidadConfiguradaDN = velocidadConfiguradaDN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_Configurada_UP")
  public Double getVelocidadConfiguradaUP() {
    return velocidadConfiguradaUP;
  }
  public void setVelocidadConfiguradaUP(Double velocidadConfiguradaUP) {
    this.velocidadConfiguradaUP = velocidadConfiguradaUP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OLTPassword")
  public String getOLTPassword() {
    return oLTPassword;
  }
  public void setOLTPassword(String oLTPassword) {
    this.oLTPassword = oLTPassword;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Service_Port")
  public List<SERVICEPORT> getServicePort() {
    return servicePort;
  }
  public void setServicePort(List<SERVICEPORT> servicePort) {
    this.servicePort = servicePort;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("OLTRxONTOpticalPower")
  public Double getOLTRxONTOpticalPower() {
    return oLTRxONTOpticalPower;
  }
  public void setOLTRxONTOpticalPower(Double oLTRxONTOpticalPower) {
    this.oLTRxONTOpticalPower = oLTRxONTOpticalPower;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTOnlineDuration")
  public String getONTOnlineDuration() {
    return oNTOnlineDuration;
  }
  public void setONTOnlineDuration(String oNTOnlineDuration) {
    this.oNTOnlineDuration = oNTOnlineDuration;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTLastDownTime")
  public String getONTLastDownTime() {
    return oNTLastDownTime;
  }
  public void setONTLastDownTime(String oNTLastDownTime) {
    this.oNTLastDownTime = oNTLastDownTime;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("DiagnosticoGpon")
  public DIAGNOSTICOGPON getDiagnosticoGpon() {
    return diagnosticoGpon;
  }
  public void setDiagnosticoGpon(DIAGNOSTICOGPON diagnosticoGpon) {
    this.diagnosticoGpon = diagnosticoGpon;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("facilidades")
  public Facilidades getFacilidades() {
    return facilidades;
  }
  public void setFacilidades(Facilidades facilidades) {
    this.facilidades = facilidades;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("MigradoTripleA")
  public Boolean getMigradoTripleA() {
    return migradoTripleA;
  }
  public void setMigradoTripleA(Boolean migradoTripleA) {
    this.migradoTripleA = migradoTripleA;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VelocidadTripleA_Up")
  public String getVelocidadTripleAUp() {
    return velocidadTripleAUp;
  }
  public void setVelocidadTripleAUp(String velocidadTripleAUp) {
    this.velocidadTripleAUp = velocidadTripleAUp;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VelocidadTripleA_Dn")
  public String getVelocidadTripleADn() {
    return velocidadTripleADn;
  }
  public void setVelocidadTripleADn(String velocidadTripleADn) {
    this.velocidadTripleADn = velocidadTripleADn;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class GPONINFO {\n");
    
    sb.append("    ID: ").append(StringUtil.toIndentedString(ID)).append("\n");
    sb.append("    ID_FACILIDADES: ").append(StringUtil.toIndentedString(ID_FACILIDADES)).append("\n");
    sb.append("    facilida: ").append(StringUtil.toIndentedString(facilida)).append("\n");
    sb.append("    oLTAdmState: ").append(StringUtil.toIndentedString(oLTAdmState)).append("\n");
    sb.append("    oLTOperState: ").append(StringUtil.toIndentedString(oLTOperState)).append("\n");
    sb.append("    oNTAdmState: ").append(StringUtil.toIndentedString(oNTAdmState)).append("\n");
    sb.append("    oNTOperState: ").append(StringUtil.toIndentedString(oNTOperState)).append("\n");
    sb.append("    oLTPortTxPower: ").append(StringUtil.toIndentedString(oLTPortTxPower)).append("\n");
    sb.append("    oLTPortRxPower: ").append(StringUtil.toIndentedString(oLTPortRxPower)).append("\n");
    sb.append("    oLTUptime: ").append(StringUtil.toIndentedString(oLTUptime)).append("\n");
    sb.append("    oLTFirmware: ").append(StringUtil.toIndentedString(oLTFirmware)).append("\n");
    sb.append("    oNTRxPower: ").append(StringUtil.toIndentedString(oNTRxPower)).append("\n");
    sb.append("    oNTTxPower: ").append(StringUtil.toIndentedString(oNTTxPower)).append("\n");
    sb.append("    oNTBiasCurrent: ").append(StringUtil.toIndentedString(oNTBiasCurrent)).append("\n");
    sb.append("    oNTTemp: ").append(StringUtil.toIndentedString(oNTTemp)).append("\n");
    sb.append("    oNTVoltage: ").append(StringUtil.toIndentedString(oNTVoltage)).append("\n");
    sb.append("    oNTDistancia: ").append(StringUtil.toIndentedString(oNTDistancia)).append("\n");
    sb.append("    oNTMemUsage: ").append(StringUtil.toIndentedString(oNTMemUsage)).append("\n");
    sb.append("    oNTCPUUsage: ").append(StringUtil.toIndentedString(oNTCPUUsage)).append("\n");
    sb.append("    dataUsageDS: ").append(StringUtil.toIndentedString(dataUsageDS)).append("\n");
    sb.append("    dataUsageUS: ").append(StringUtil.toIndentedString(dataUsageUS)).append("\n");
    sb.append("    oNTLastDown: ").append(StringUtil.toIndentedString(oNTLastDown)).append("\n");
    sb.append("    oNTLastDownCause: ").append(StringUtil.toIndentedString(oNTLastDownCause)).append("\n");
    sb.append("    oNTUpTime: ").append(StringUtil.toIndentedString(oNTUpTime)).append("\n");
    sb.append("    currentThroughput: ").append(StringUtil.toIndentedString(currentThroughput)).append("\n");
    sb.append("    oNTDescription: ").append(StringUtil.toIndentedString(oNTDescription)).append("\n");
    sb.append("    oNTSerialNumber: ").append(StringUtil.toIndentedString(oNTSerialNumber)).append("\n");
    sb.append("    oNTLineProfile: ").append(StringUtil.toIndentedString(oNTLineProfile)).append("\n");
    sb.append("    oNTVendor: ").append(StringUtil.toIndentedString(oNTVendor)).append("\n");
    sb.append("    oNTVersion: ").append(StringUtil.toIndentedString(oNTVersion)).append("\n");
    sb.append("    oNTSoftwareVersion: ").append(StringUtil.toIndentedString(oNTSoftwareVersion)).append("\n");
    sb.append("    ontProductDescription: ").append(StringUtil.toIndentedString(ontProductDescription)).append("\n");
    sb.append("    oNTBatteryStatus: ").append(StringUtil.toIndentedString(oNTBatteryStatus)).append("\n");
    sb.append("    velocidadContratadaDN: ").append(StringUtil.toIndentedString(velocidadContratadaDN)).append("\n");
    sb.append("    velocidadContratadaUP: ").append(StringUtil.toIndentedString(velocidadContratadaUP)).append("\n");
    sb.append("    velocidadConfiguradaDN: ").append(StringUtil.toIndentedString(velocidadConfiguradaDN)).append("\n");
    sb.append("    velocidadConfiguradaUP: ").append(StringUtil.toIndentedString(velocidadConfiguradaUP)).append("\n");
    sb.append("    oLTPassword: ").append(StringUtil.toIndentedString(oLTPassword)).append("\n");
    sb.append("    servicePort: ").append(StringUtil.toIndentedString(servicePort)).append("\n");
    sb.append("    oLTRxONTOpticalPower: ").append(StringUtil.toIndentedString(oLTRxONTOpticalPower)).append("\n");
    sb.append("    oNTOnlineDuration: ").append(StringUtil.toIndentedString(oNTOnlineDuration)).append("\n");
    sb.append("    oNTLastDownTime: ").append(StringUtil.toIndentedString(oNTLastDownTime)).append("\n");
    sb.append("    diagnosticoGpon: ").append(StringUtil.toIndentedString(diagnosticoGpon)).append("\n");
    sb.append("    facilidades: ").append(StringUtil.toIndentedString(facilidades)).append("\n");
    sb.append("    migradoTripleA: ").append(StringUtil.toIndentedString(migradoTripleA)).append("\n");
    sb.append("    velocidadTripleAUp: ").append(StringUtil.toIndentedString(velocidadTripleAUp)).append("\n");
    sb.append("    velocidadTripleADn: ").append(StringUtil.toIndentedString(velocidadTripleADn)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
