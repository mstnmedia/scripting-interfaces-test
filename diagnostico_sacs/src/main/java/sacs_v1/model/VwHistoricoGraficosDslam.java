package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import java.util.Date;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class VwHistoricoGraficosDslam   {
  
  private Integer id = null;
  private Integer idFacilidades = null;
  private Integer velocidadActualDN = null;
  private Integer velocidadActualUP = null;
  private Integer ruidoDN = null;
  private Integer ruidoUP = null;
  private Integer senalAtenuacionDN = null;
  private Integer senalAtenuacionUP = null;
  private Integer loopAtenuacionDN = null;
  private Integer loopAtenuacionUP = null;
  private Date fecha = null;
  private String telefono = null;
  private String nodeAdress = null;
  private String nodeName = null;
  private Integer maximaVelocidadBajada = null;
  private Integer maximaVelocidadSubida = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Id")
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IdFacilidades")
  public Integer getIdFacilidades() {
    return idFacilidades;
  }
  public void setIdFacilidades(Integer idFacilidades) {
    this.idFacilidades = idFacilidades;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("velocidadActual_DN")
  public Integer getVelocidadActualDN() {
    return velocidadActualDN;
  }
  public void setVelocidadActualDN(Integer velocidadActualDN) {
    this.velocidadActualDN = velocidadActualDN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("velocidadActual_UP")
  public Integer getVelocidadActualUP() {
    return velocidadActualUP;
  }
  public void setVelocidadActualUP(Integer velocidadActualUP) {
    this.velocidadActualUP = velocidadActualUP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ruido_DN")
  public Integer getRuidoDN() {
    return ruidoDN;
  }
  public void setRuidoDN(Integer ruidoDN) {
    this.ruidoDN = ruidoDN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ruido_UP")
  public Integer getRuidoUP() {
    return ruidoUP;
  }
  public void setRuidoUP(Integer ruidoUP) {
    this.ruidoUP = ruidoUP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("senalAtenuacion_DN")
  public Integer getSenalAtenuacionDN() {
    return senalAtenuacionDN;
  }
  public void setSenalAtenuacionDN(Integer senalAtenuacionDN) {
    this.senalAtenuacionDN = senalAtenuacionDN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("senalAtenuacion_UP")
  public Integer getSenalAtenuacionUP() {
    return senalAtenuacionUP;
  }
  public void setSenalAtenuacionUP(Integer senalAtenuacionUP) {
    this.senalAtenuacionUP = senalAtenuacionUP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("loopAtenuacion_DN")
  public Integer getLoopAtenuacionDN() {
    return loopAtenuacionDN;
  }
  public void setLoopAtenuacionDN(Integer loopAtenuacionDN) {
    this.loopAtenuacionDN = loopAtenuacionDN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("loopAtenuacion_UP")
  public Integer getLoopAtenuacionUP() {
    return loopAtenuacionUP;
  }
  public void setLoopAtenuacionUP(Integer loopAtenuacionUP) {
    this.loopAtenuacionUP = loopAtenuacionUP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("fecha")
  public Date getFecha() {
    return fecha;
  }
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Telefono")
  public String getTelefono() {
    return telefono;
  }
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("NodeAdress")
  public String getNodeAdress() {
    return nodeAdress;
  }
  public void setNodeAdress(String nodeAdress) {
    this.nodeAdress = nodeAdress;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("NodeName")
  public String getNodeName() {
    return nodeName;
  }
  public void setNodeName(String nodeName) {
    this.nodeName = nodeName;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Maxima_Velocidad_Bajada")
  public Integer getMaximaVelocidadBajada() {
    return maximaVelocidadBajada;
  }
  public void setMaximaVelocidadBajada(Integer maximaVelocidadBajada) {
    this.maximaVelocidadBajada = maximaVelocidadBajada;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Maxima_Velocidad_Subida")
  public Integer getMaximaVelocidadSubida() {
    return maximaVelocidadSubida;
  }
  public void setMaximaVelocidadSubida(Integer maximaVelocidadSubida) {
    this.maximaVelocidadSubida = maximaVelocidadSubida;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class VwHistoricoGraficosDslam {\n");
    
    sb.append("    id: ").append(StringUtil.toIndentedString(id)).append("\n");
    sb.append("    idFacilidades: ").append(StringUtil.toIndentedString(idFacilidades)).append("\n");
    sb.append("    velocidadActualDN: ").append(StringUtil.toIndentedString(velocidadActualDN)).append("\n");
    sb.append("    velocidadActualUP: ").append(StringUtil.toIndentedString(velocidadActualUP)).append("\n");
    sb.append("    ruidoDN: ").append(StringUtil.toIndentedString(ruidoDN)).append("\n");
    sb.append("    ruidoUP: ").append(StringUtil.toIndentedString(ruidoUP)).append("\n");
    sb.append("    senalAtenuacionDN: ").append(StringUtil.toIndentedString(senalAtenuacionDN)).append("\n");
    sb.append("    senalAtenuacionUP: ").append(StringUtil.toIndentedString(senalAtenuacionUP)).append("\n");
    sb.append("    loopAtenuacionDN: ").append(StringUtil.toIndentedString(loopAtenuacionDN)).append("\n");
    sb.append("    loopAtenuacionUP: ").append(StringUtil.toIndentedString(loopAtenuacionUP)).append("\n");
    sb.append("    fecha: ").append(StringUtil.toIndentedString(fecha)).append("\n");
    sb.append("    telefono: ").append(StringUtil.toIndentedString(telefono)).append("\n");
    sb.append("    nodeAdress: ").append(StringUtil.toIndentedString(nodeAdress)).append("\n");
    sb.append("    nodeName: ").append(StringUtil.toIndentedString(nodeName)).append("\n");
    sb.append("    maximaVelocidadBajada: ").append(StringUtil.toIndentedString(maximaVelocidadBajada)).append("\n");
    sb.append("    maximaVelocidadSubida: ").append(StringUtil.toIndentedString(maximaVelocidadSubida)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
