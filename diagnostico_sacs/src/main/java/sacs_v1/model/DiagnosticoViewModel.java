package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import sacs_v1.model.DIAGNOSTICOGPON;
import sacs_v1.model.DSLAMINFO;
import sacs_v1.model.GPONINFO;
import sacs_v1.model.DIAGNOSTICO;
import sacs_v1.model.Facilidades;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * 
 **/
@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class DiagnosticoViewModel   {
  
  private Integer indexFacilidad = null;
  private DSLAMINFO cobreInfo = null;
  private DIAGNOSTICO diagnosticoCobre = null;
  private GPONINFO gponInfo = null;
  private DIAGNOSTICOGPON diagnosticoGpon = null;
  private Facilidades facilidades = null;
  private Boolean dividir = null;

  
  /**
   * 
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IndexFacilidad")
  public Integer getIndexFacilidad() {
    return indexFacilidad;
  }
  public void setIndexFacilidad(Integer indexFacilidad) {
    this.indexFacilidad = indexFacilidad;
  }

  
  /**
   * 
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("CobreInfo")
  public DSLAMINFO getCobreInfo() {
    return cobreInfo;
  }
  public void setCobreInfo(DSLAMINFO cobreInfo) {
    this.cobreInfo = cobreInfo;
  }

  
  /**
   * 
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("DiagnosticoCobre")
  public DIAGNOSTICO getDiagnosticoCobre() {
    return diagnosticoCobre;
  }
  public void setDiagnosticoCobre(DIAGNOSTICO diagnosticoCobre) {
    this.diagnosticoCobre = diagnosticoCobre;
  }

  
  /**
   * 
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("GponInfo")
  public GPONINFO getGponInfo() {
    return gponInfo;
  }
  public void setGponInfo(GPONINFO gponInfo) {
    this.gponInfo = gponInfo;
  }

  
  /**
   * 
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("DiagnosticoGpon")
  public DIAGNOSTICOGPON getDiagnosticoGpon() {
    return diagnosticoGpon;
  }
  public void setDiagnosticoGpon(DIAGNOSTICOGPON diagnosticoGpon) {
    this.diagnosticoGpon = diagnosticoGpon;
  }

  
  /**
   * 
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("facilidades")
  public Facilidades getFacilidades() {
    return facilidades;
  }
  public void setFacilidades(Facilidades facilidades) {
    this.facilidades = facilidades;
  }

  
  /**
   * Indica si se debe dividir el valor entre 10.
   **/
  @ApiModelProperty(value = "Indica si se debe dividir el valor entre 10.")
  @JsonProperty("dividir")
  public Boolean getDividir() {
    return dividir;
  }
  public void setDividir(Boolean dividir) {
    this.dividir = dividir;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class DiagnosticoViewModel {\n");
    
    sb.append("    indexFacilidad: ").append(StringUtil.toIndentedString(indexFacilidad)).append("\n");
    sb.append("    cobreInfo: ").append(StringUtil.toIndentedString(cobreInfo)).append("\n");
    sb.append("    diagnosticoCobre: ").append(StringUtil.toIndentedString(diagnosticoCobre)).append("\n");
    sb.append("    gponInfo: ").append(StringUtil.toIndentedString(gponInfo)).append("\n");
    sb.append("    diagnosticoGpon: ").append(StringUtil.toIndentedString(diagnosticoGpon)).append("\n");
    sb.append("    facilidades: ").append(StringUtil.toIndentedString(facilidades)).append("\n");
    sb.append("    dividir: ").append(StringUtil.toIndentedString(dividir)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
