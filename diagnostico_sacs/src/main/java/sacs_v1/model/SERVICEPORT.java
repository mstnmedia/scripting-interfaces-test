package sacs_v1.model;

import sacs_v1.invoker.StringUtil;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class SERVICEPORT   {
  
  private Integer ID = null;
  private Integer ID_GPON_INFO = null;
  private String name = null;
  private Integer index = null;
  private Integer vlanId = null;
  private Integer flowPara = null;
  private Integer TX = null;
  private Integer RX = null;
  private String inboundTableName = null;
  private String outboundTableName = null;
  private Boolean adminStatus = null;
  private Boolean state = null;
  private String description = null;
  private String serviceType = null;
  private String connectionType = null;
  private String connectionStatus = null;
  private String iPAccessType = null;
  private String IP = null;
  private String subnetMask = null;
  private String defaultGateway = null;
  private Integer manageVLAN = null;
  private Integer managePriority = null;
  private String VLAN = null;
  private String oNTMACAddress = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ID")
  public Integer getID() {
    return ID;
  }
  public void setID(Integer ID) {
    this.ID = ID;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ID_GPON_INFO")
  public Integer getIDGPONINFO() {
    return ID_GPON_INFO;
  }
  public void setIDGPONINFO(Integer ID_GPON_INFO) {
    this.ID_GPON_INFO = ID_GPON_INFO;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Index")
  public Integer getIndex() {
    return index;
  }
  public void setIndex(Integer index) {
    this.index = index;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Vlan_Id")
  public Integer getVlanId() {
    return vlanId;
  }
  public void setVlanId(Integer vlanId) {
    this.vlanId = vlanId;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Flow_Para")
  public Integer getFlowPara() {
    return flowPara;
  }
  public void setFlowPara(Integer flowPara) {
    this.flowPara = flowPara;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("TX")
  public Integer getTX() {
    return TX;
  }
  public void setTX(Integer TX) {
    this.TX = TX;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("RX")
  public Integer getRX() {
    return RX;
  }
  public void setRX(Integer RX) {
    this.RX = RX;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Inbound_Table_Name")
  public String getInboundTableName() {
    return inboundTableName;
  }
  public void setInboundTableName(String inboundTableName) {
    this.inboundTableName = inboundTableName;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Outbound_Table_Name")
  public String getOutboundTableName() {
    return outboundTableName;
  }
  public void setOutboundTableName(String outboundTableName) {
    this.outboundTableName = outboundTableName;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Admin_Status")
  public Boolean getAdminStatus() {
    return adminStatus;
  }
  public void setAdminStatus(Boolean adminStatus) {
    this.adminStatus = adminStatus;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("State")
  public Boolean getState() {
    return state;
  }
  public void setState(Boolean state) {
    this.state = state;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Description")
  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Service_Type")
  public String getServiceType() {
    return serviceType;
  }
  public void setServiceType(String serviceType) {
    this.serviceType = serviceType;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Connection_Type")
  public String getConnectionType() {
    return connectionType;
  }
  public void setConnectionType(String connectionType) {
    this.connectionType = connectionType;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Connection_Status")
  public String getConnectionStatus() {
    return connectionStatus;
  }
  public void setConnectionStatus(String connectionStatus) {
    this.connectionStatus = connectionStatus;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IP_Access_Type")
  public String getIPAccessType() {
    return iPAccessType;
  }
  public void setIPAccessType(String iPAccessType) {
    this.iPAccessType = iPAccessType;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IP")
  public String getIP() {
    return IP;
  }
  public void setIP(String IP) {
    this.IP = IP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Subnet_Mask")
  public String getSubnetMask() {
    return subnetMask;
  }
  public void setSubnetMask(String subnetMask) {
    this.subnetMask = subnetMask;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Default_Gateway")
  public String getDefaultGateway() {
    return defaultGateway;
  }
  public void setDefaultGateway(String defaultGateway) {
    this.defaultGateway = defaultGateway;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Manage_VLAN")
  public Integer getManageVLAN() {
    return manageVLAN;
  }
  public void setManageVLAN(Integer manageVLAN) {
    this.manageVLAN = manageVLAN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Manage_Priority")
  public Integer getManagePriority() {
    return managePriority;
  }
  public void setManagePriority(Integer managePriority) {
    this.managePriority = managePriority;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VLAN")
  public String getVLAN() {
    return VLAN;
  }
  public void setVLAN(String VLAN) {
    this.VLAN = VLAN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ONTMACAddress")
  public String getONTMACAddress() {
    return oNTMACAddress;
  }
  public void setONTMACAddress(String oNTMACAddress) {
    this.oNTMACAddress = oNTMACAddress;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class SERVICEPORT {\n");
    
    sb.append("    ID: ").append(StringUtil.toIndentedString(ID)).append("\n");
    sb.append("    ID_GPON_INFO: ").append(StringUtil.toIndentedString(ID_GPON_INFO)).append("\n");
    sb.append("    name: ").append(StringUtil.toIndentedString(name)).append("\n");
    sb.append("    index: ").append(StringUtil.toIndentedString(index)).append("\n");
    sb.append("    vlanId: ").append(StringUtil.toIndentedString(vlanId)).append("\n");
    sb.append("    flowPara: ").append(StringUtil.toIndentedString(flowPara)).append("\n");
    sb.append("    TX: ").append(StringUtil.toIndentedString(TX)).append("\n");
    sb.append("    RX: ").append(StringUtil.toIndentedString(RX)).append("\n");
    sb.append("    inboundTableName: ").append(StringUtil.toIndentedString(inboundTableName)).append("\n");
    sb.append("    outboundTableName: ").append(StringUtil.toIndentedString(outboundTableName)).append("\n");
    sb.append("    adminStatus: ").append(StringUtil.toIndentedString(adminStatus)).append("\n");
    sb.append("    state: ").append(StringUtil.toIndentedString(state)).append("\n");
    sb.append("    description: ").append(StringUtil.toIndentedString(description)).append("\n");
    sb.append("    serviceType: ").append(StringUtil.toIndentedString(serviceType)).append("\n");
    sb.append("    connectionType: ").append(StringUtil.toIndentedString(connectionType)).append("\n");
    sb.append("    connectionStatus: ").append(StringUtil.toIndentedString(connectionStatus)).append("\n");
    sb.append("    iPAccessType: ").append(StringUtil.toIndentedString(iPAccessType)).append("\n");
    sb.append("    IP: ").append(StringUtil.toIndentedString(IP)).append("\n");
    sb.append("    subnetMask: ").append(StringUtil.toIndentedString(subnetMask)).append("\n");
    sb.append("    defaultGateway: ").append(StringUtil.toIndentedString(defaultGateway)).append("\n");
    sb.append("    manageVLAN: ").append(StringUtil.toIndentedString(manageVLAN)).append("\n");
    sb.append("    managePriority: ").append(StringUtil.toIndentedString(managePriority)).append("\n");
    sb.append("    VLAN: ").append(StringUtil.toIndentedString(VLAN)).append("\n");
    sb.append("    oNTMACAddress: ").append(StringUtil.toIndentedString(oNTMACAddress)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
