package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import sacs_v1.model.FACILITY;
import sacs_v1.model.DIAGNOSTICO;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class DSLAMINFO   {
  
  private Integer ID = null;
  private Integer ID_FACILIDADES = null;
  private FACILITY facilida = null;
  private String ultimoSincronismo = null;
  private String ultimaBajada = null;
  private Integer distaciaSVDG = null;
  private Boolean condicionOperativa = null;
  private Boolean condicionAdministrativa = null;
  private Integer ruidoBajada = null;
  private Integer ruidoSubida = null;
  private Integer senalAtenuacionBajada = null;
  private Integer senalAtenuacionSubida = null;
  private Integer loopAtenuacionBajada = null;
  private Integer loopAtenuacionSubida = null;
  private String nombreProfile = null;
  private Integer numeroProfile = null;
  private String calidadServicio = null;
  private Integer velocidadADSL = null;
  private Integer maximaVelocidadBajada = null;
  private Integer maximaVelocidadSubida = null;
  private Integer velocidadContratadaDN = null;
  private Integer velocidadContratadaUP = null;
  private Integer velocidadActualDN = null;
  private Integer velocidadActualUP = null;
  private Integer velocidadConfiguradaDN = null;
  private Integer velocidadConfiguradaUP = null;
  private Integer velocidadPuertoDN = null;
  private Integer velocidadPuertoUP = null;
  private Boolean lineaDefectuosa = null;
  private String rinitID = null;
  private String feeder = null;
  private String cabina = null;
  private String vLANId = null;
  private String mACAddress = null;
  private String sessionId = null;
  private String sessionIdIPTV = null;
  private String slot = null;
  private String actualType = null;
  private Boolean enabled = null;
  private String errorStatus = null;
  private String availability = null;
  private String restrtCnt = null;
  private Boolean pVC35 = null;
  private Boolean pVC33 = null;
  private String customerID = null;
  private String user = null;
  private String tipoTarjeta = null;
  private Boolean VLAN_IPTV = null;
  private String serialModem = null;
  private String distancia = null;
  private DIAGNOSTICO diagnosticoCobre = null;
  private Boolean dividirPorMillar = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ID")
  public Integer getID() {
    return ID;
  }
  public void setID(Integer ID) {
    this.ID = ID;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ID_FACILIDADES")
  public Integer getIDFACILIDADES() {
    return ID_FACILIDADES;
  }
  public void setIDFACILIDADES(Integer ID_FACILIDADES) {
    this.ID_FACILIDADES = ID_FACILIDADES;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Facilida")
  public FACILITY getFacilida() {
    return facilida;
  }
  public void setFacilida(FACILITY facilida) {
    this.facilida = facilida;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Ultimo_Sincronismo")
  public String getUltimoSincronismo() {
    return ultimoSincronismo;
  }
  public void setUltimoSincronismo(String ultimoSincronismo) {
    this.ultimoSincronismo = ultimoSincronismo;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Ultima_Bajada")
  public String getUltimaBajada() {
    return ultimaBajada;
  }
  public void setUltimaBajada(String ultimaBajada) {
    this.ultimaBajada = ultimaBajada;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Distacia_SVDG")
  public Integer getDistaciaSVDG() {
    return distaciaSVDG;
  }
  public void setDistaciaSVDG(Integer distaciaSVDG) {
    this.distaciaSVDG = distaciaSVDG;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Condicion_Operativa")
  public Boolean getCondicionOperativa() {
    return condicionOperativa;
  }
  public void setCondicionOperativa(Boolean condicionOperativa) {
    this.condicionOperativa = condicionOperativa;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Condicion_Administrativa")
  public Boolean getCondicionAdministrativa() {
    return condicionAdministrativa;
  }
  public void setCondicionAdministrativa(Boolean condicionAdministrativa) {
    this.condicionAdministrativa = condicionAdministrativa;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Ruido_Bajada")
  public Integer getRuidoBajada() {
    return ruidoBajada;
  }
  public void setRuidoBajada(Integer ruidoBajada) {
    this.ruidoBajada = ruidoBajada;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Ruido_Subida")
  public Integer getRuidoSubida() {
    return ruidoSubida;
  }
  public void setRuidoSubida(Integer ruidoSubida) {
    this.ruidoSubida = ruidoSubida;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Senal_Atenuacion_Bajada")
  public Integer getSenalAtenuacionBajada() {
    return senalAtenuacionBajada;
  }
  public void setSenalAtenuacionBajada(Integer senalAtenuacionBajada) {
    this.senalAtenuacionBajada = senalAtenuacionBajada;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Senal_Atenuacion_Subida")
  public Integer getSenalAtenuacionSubida() {
    return senalAtenuacionSubida;
  }
  public void setSenalAtenuacionSubida(Integer senalAtenuacionSubida) {
    this.senalAtenuacionSubida = senalAtenuacionSubida;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Loop_Atenuacion_Bajada")
  public Integer getLoopAtenuacionBajada() {
    return loopAtenuacionBajada;
  }
  public void setLoopAtenuacionBajada(Integer loopAtenuacionBajada) {
    this.loopAtenuacionBajada = loopAtenuacionBajada;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Loop_Atenuacion_Subida")
  public Integer getLoopAtenuacionSubida() {
    return loopAtenuacionSubida;
  }
  public void setLoopAtenuacionSubida(Integer loopAtenuacionSubida) {
    this.loopAtenuacionSubida = loopAtenuacionSubida;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Nombre_Profile")
  public String getNombreProfile() {
    return nombreProfile;
  }
  public void setNombreProfile(String nombreProfile) {
    this.nombreProfile = nombreProfile;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Numero_Profile")
  public Integer getNumeroProfile() {
    return numeroProfile;
  }
  public void setNumeroProfile(Integer numeroProfile) {
    this.numeroProfile = numeroProfile;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Calidad_Servicio")
  public String getCalidadServicio() {
    return calidadServicio;
  }
  public void setCalidadServicio(String calidadServicio) {
    this.calidadServicio = calidadServicio;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_ADSL")
  public Integer getVelocidadADSL() {
    return velocidadADSL;
  }
  public void setVelocidadADSL(Integer velocidadADSL) {
    this.velocidadADSL = velocidadADSL;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Maxima_Velocidad_Bajada")
  public Integer getMaximaVelocidadBajada() {
    return maximaVelocidadBajada;
  }
  public void setMaximaVelocidadBajada(Integer maximaVelocidadBajada) {
    this.maximaVelocidadBajada = maximaVelocidadBajada;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Maxima_Velocidad_Subida")
  public Integer getMaximaVelocidadSubida() {
    return maximaVelocidadSubida;
  }
  public void setMaximaVelocidadSubida(Integer maximaVelocidadSubida) {
    this.maximaVelocidadSubida = maximaVelocidadSubida;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_Contratada_DN")
  public Integer getVelocidadContratadaDN() {
    return velocidadContratadaDN;
  }
  public void setVelocidadContratadaDN(Integer velocidadContratadaDN) {
    this.velocidadContratadaDN = velocidadContratadaDN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_Contratada_UP")
  public Integer getVelocidadContratadaUP() {
    return velocidadContratadaUP;
  }
  public void setVelocidadContratadaUP(Integer velocidadContratadaUP) {
    this.velocidadContratadaUP = velocidadContratadaUP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_Actual_DN")
  public Integer getVelocidadActualDN() {
    return velocidadActualDN;
  }
  public void setVelocidadActualDN(Integer velocidadActualDN) {
    this.velocidadActualDN = velocidadActualDN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_Actual_UP")
  public Integer getVelocidadActualUP() {
    return velocidadActualUP;
  }
  public void setVelocidadActualUP(Integer velocidadActualUP) {
    this.velocidadActualUP = velocidadActualUP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_Configurada_DN")
  public Integer getVelocidadConfiguradaDN() {
    return velocidadConfiguradaDN;
  }
  public void setVelocidadConfiguradaDN(Integer velocidadConfiguradaDN) {
    this.velocidadConfiguradaDN = velocidadConfiguradaDN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_Configurada_UP")
  public Integer getVelocidadConfiguradaUP() {
    return velocidadConfiguradaUP;
  }
  public void setVelocidadConfiguradaUP(Integer velocidadConfiguradaUP) {
    this.velocidadConfiguradaUP = velocidadConfiguradaUP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_Puerto_DN")
  public Integer getVelocidadPuertoDN() {
    return velocidadPuertoDN;
  }
  public void setVelocidadPuertoDN(Integer velocidadPuertoDN) {
    this.velocidadPuertoDN = velocidadPuertoDN;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Velocidad_Puerto_UP")
  public Integer getVelocidadPuertoUP() {
    return velocidadPuertoUP;
  }
  public void setVelocidadPuertoUP(Integer velocidadPuertoUP) {
    this.velocidadPuertoUP = velocidadPuertoUP;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Linea_Defectuosa")
  public Boolean getLineaDefectuosa() {
    return lineaDefectuosa;
  }
  public void setLineaDefectuosa(Boolean lineaDefectuosa) {
    this.lineaDefectuosa = lineaDefectuosa;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Rinit_ID")
  public String getRinitID() {
    return rinitID;
  }
  public void setRinitID(String rinitID) {
    this.rinitID = rinitID;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Feeder")
  public String getFeeder() {
    return feeder;
  }
  public void setFeeder(String feeder) {
    this.feeder = feeder;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Cabina")
  public String getCabina() {
    return cabina;
  }
  public void setCabina(String cabina) {
    this.cabina = cabina;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VLAN_id")
  public String getVLANId() {
    return vLANId;
  }
  public void setVLANId(String vLANId) {
    this.vLANId = vLANId;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("MAC_Address")
  public String getMACAddress() {
    return mACAddress;
  }
  public void setMACAddress(String mACAddress) {
    this.mACAddress = mACAddress;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Session_Id")
  public String getSessionId() {
    return sessionId;
  }
  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Session_Id_IPTV")
  public String getSessionIdIPTV() {
    return sessionIdIPTV;
  }
  public void setSessionIdIPTV(String sessionIdIPTV) {
    this.sessionIdIPTV = sessionIdIPTV;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Slot")
  public String getSlot() {
    return slot;
  }
  public void setSlot(String slot) {
    this.slot = slot;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Actual_Type")
  public String getActualType() {
    return actualType;
  }
  public void setActualType(String actualType) {
    this.actualType = actualType;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Enabled")
  public Boolean getEnabled() {
    return enabled;
  }
  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Error_Status")
  public String getErrorStatus() {
    return errorStatus;
  }
  public void setErrorStatus(String errorStatus) {
    this.errorStatus = errorStatus;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Availability")
  public String getAvailability() {
    return availability;
  }
  public void setAvailability(String availability) {
    this.availability = availability;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Restrt_Cnt")
  public String getRestrtCnt() {
    return restrtCnt;
  }
  public void setRestrtCnt(String restrtCnt) {
    this.restrtCnt = restrtCnt;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("PVC_35")
  public Boolean getPVC35() {
    return pVC35;
  }
  public void setPVC35(Boolean pVC35) {
    this.pVC35 = pVC35;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("PVC_33")
  public Boolean getPVC33() {
    return pVC33;
  }
  public void setPVC33(Boolean pVC33) {
    this.pVC33 = pVC33;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("CustomerID")
  public String getCustomerID() {
    return customerID;
  }
  public void setCustomerID(String customerID) {
    this.customerID = customerID;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("User")
  public String getUser() {
    return user;
  }
  public void setUser(String user) {
    this.user = user;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Tipo_Tarjeta")
  public String getTipoTarjeta() {
    return tipoTarjeta;
  }
  public void setTipoTarjeta(String tipoTarjeta) {
    this.tipoTarjeta = tipoTarjeta;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VLAN_IPTV")
  public Boolean getVLANIPTV() {
    return VLAN_IPTV;
  }
  public void setVLANIPTV(Boolean VLAN_IPTV) {
    this.VLAN_IPTV = VLAN_IPTV;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("Serial_Modem")
  public String getSerialModem() {
    return serialModem;
  }
  public void setSerialModem(String serialModem) {
    this.serialModem = serialModem;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("distancia")
  public String getDistancia() {
    return distancia;
  }
  public void setDistancia(String distancia) {
    this.distancia = distancia;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("DiagnosticoCobre")
  public DIAGNOSTICO getDiagnosticoCobre() {
    return diagnosticoCobre;
  }
  public void setDiagnosticoCobre(DIAGNOSTICO diagnosticoCobre) {
    this.diagnosticoCobre = diagnosticoCobre;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("dividirPorMillar")
  public Boolean getDividirPorMillar() {
    return dividirPorMillar;
  }
  public void setDividirPorMillar(Boolean dividirPorMillar) {
    this.dividirPorMillar = dividirPorMillar;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class DSLAMINFO {\n");
    
    sb.append("    ID: ").append(StringUtil.toIndentedString(ID)).append("\n");
    sb.append("    ID_FACILIDADES: ").append(StringUtil.toIndentedString(ID_FACILIDADES)).append("\n");
    sb.append("    facilida: ").append(StringUtil.toIndentedString(facilida)).append("\n");
    sb.append("    ultimoSincronismo: ").append(StringUtil.toIndentedString(ultimoSincronismo)).append("\n");
    sb.append("    ultimaBajada: ").append(StringUtil.toIndentedString(ultimaBajada)).append("\n");
    sb.append("    distaciaSVDG: ").append(StringUtil.toIndentedString(distaciaSVDG)).append("\n");
    sb.append("    condicionOperativa: ").append(StringUtil.toIndentedString(condicionOperativa)).append("\n");
    sb.append("    condicionAdministrativa: ").append(StringUtil.toIndentedString(condicionAdministrativa)).append("\n");
    sb.append("    ruidoBajada: ").append(StringUtil.toIndentedString(ruidoBajada)).append("\n");
    sb.append("    ruidoSubida: ").append(StringUtil.toIndentedString(ruidoSubida)).append("\n");
    sb.append("    senalAtenuacionBajada: ").append(StringUtil.toIndentedString(senalAtenuacionBajada)).append("\n");
    sb.append("    senalAtenuacionSubida: ").append(StringUtil.toIndentedString(senalAtenuacionSubida)).append("\n");
    sb.append("    loopAtenuacionBajada: ").append(StringUtil.toIndentedString(loopAtenuacionBajada)).append("\n");
    sb.append("    loopAtenuacionSubida: ").append(StringUtil.toIndentedString(loopAtenuacionSubida)).append("\n");
    sb.append("    nombreProfile: ").append(StringUtil.toIndentedString(nombreProfile)).append("\n");
    sb.append("    numeroProfile: ").append(StringUtil.toIndentedString(numeroProfile)).append("\n");
    sb.append("    calidadServicio: ").append(StringUtil.toIndentedString(calidadServicio)).append("\n");
    sb.append("    velocidadADSL: ").append(StringUtil.toIndentedString(velocidadADSL)).append("\n");
    sb.append("    maximaVelocidadBajada: ").append(StringUtil.toIndentedString(maximaVelocidadBajada)).append("\n");
    sb.append("    maximaVelocidadSubida: ").append(StringUtil.toIndentedString(maximaVelocidadSubida)).append("\n");
    sb.append("    velocidadContratadaDN: ").append(StringUtil.toIndentedString(velocidadContratadaDN)).append("\n");
    sb.append("    velocidadContratadaUP: ").append(StringUtil.toIndentedString(velocidadContratadaUP)).append("\n");
    sb.append("    velocidadActualDN: ").append(StringUtil.toIndentedString(velocidadActualDN)).append("\n");
    sb.append("    velocidadActualUP: ").append(StringUtil.toIndentedString(velocidadActualUP)).append("\n");
    sb.append("    velocidadConfiguradaDN: ").append(StringUtil.toIndentedString(velocidadConfiguradaDN)).append("\n");
    sb.append("    velocidadConfiguradaUP: ").append(StringUtil.toIndentedString(velocidadConfiguradaUP)).append("\n");
    sb.append("    velocidadPuertoDN: ").append(StringUtil.toIndentedString(velocidadPuertoDN)).append("\n");
    sb.append("    velocidadPuertoUP: ").append(StringUtil.toIndentedString(velocidadPuertoUP)).append("\n");
    sb.append("    lineaDefectuosa: ").append(StringUtil.toIndentedString(lineaDefectuosa)).append("\n");
    sb.append("    rinitID: ").append(StringUtil.toIndentedString(rinitID)).append("\n");
    sb.append("    feeder: ").append(StringUtil.toIndentedString(feeder)).append("\n");
    sb.append("    cabina: ").append(StringUtil.toIndentedString(cabina)).append("\n");
    sb.append("    vLANId: ").append(StringUtil.toIndentedString(vLANId)).append("\n");
    sb.append("    mACAddress: ").append(StringUtil.toIndentedString(mACAddress)).append("\n");
    sb.append("    sessionId: ").append(StringUtil.toIndentedString(sessionId)).append("\n");
    sb.append("    sessionIdIPTV: ").append(StringUtil.toIndentedString(sessionIdIPTV)).append("\n");
    sb.append("    slot: ").append(StringUtil.toIndentedString(slot)).append("\n");
    sb.append("    actualType: ").append(StringUtil.toIndentedString(actualType)).append("\n");
    sb.append("    enabled: ").append(StringUtil.toIndentedString(enabled)).append("\n");
    sb.append("    errorStatus: ").append(StringUtil.toIndentedString(errorStatus)).append("\n");
    sb.append("    availability: ").append(StringUtil.toIndentedString(availability)).append("\n");
    sb.append("    restrtCnt: ").append(StringUtil.toIndentedString(restrtCnt)).append("\n");
    sb.append("    pVC35: ").append(StringUtil.toIndentedString(pVC35)).append("\n");
    sb.append("    pVC33: ").append(StringUtil.toIndentedString(pVC33)).append("\n");
    sb.append("    customerID: ").append(StringUtil.toIndentedString(customerID)).append("\n");
    sb.append("    user: ").append(StringUtil.toIndentedString(user)).append("\n");
    sb.append("    tipoTarjeta: ").append(StringUtil.toIndentedString(tipoTarjeta)).append("\n");
    sb.append("    VLAN_IPTV: ").append(StringUtil.toIndentedString(VLAN_IPTV)).append("\n");
    sb.append("    serialModem: ").append(StringUtil.toIndentedString(serialModem)).append("\n");
    sb.append("    distancia: ").append(StringUtil.toIndentedString(distancia)).append("\n");
    sb.append("    diagnosticoCobre: ").append(StringUtil.toIndentedString(diagnosticoCobre)).append("\n");
    sb.append("    dividirPorMillar: ").append(StringUtil.toIndentedString(dividirPorMillar)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
