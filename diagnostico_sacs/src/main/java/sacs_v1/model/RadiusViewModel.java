package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import java.util.Date;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Entidad que representa la información almenacada en RADIUS
 **/
@ApiModel(description = "Entidad que representa la información almenacada en RADIUS")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class RadiusViewModel   {
  
  private Date fechaInicio = null;
  private Date fechaFin = null;
  private String tiempo = null;
  private String motivo = null;
  private String userName = null;
  private String IP = null;
  private String bras = null;
  private String traficoDn = null;
  private String traficoUp = null;

  
  /**
   * Fecha de inicio de conexión.
   **/
  @ApiModelProperty(value = "Fecha de inicio de conexión.")
  @JsonProperty("FechaInicio")
  public Date getFechaInicio() {
    return fechaInicio;
  }
  public void setFechaInicio(Date fechaInicio) {
    this.fechaInicio = fechaInicio;
  }

  
  /**
   * Fecha de desconexión.
   **/
  @ApiModelProperty(value = "Fecha de desconexión.")
  @JsonProperty("FechaFin")
  public Date getFechaFin() {
    return fechaFin;
  }
  public void setFechaFin(Date fechaFin) {
    this.fechaFin = fechaFin;
  }

  
  /**
   * Tiempo duración de la sesión.
   **/
  @ApiModelProperty(value = "Tiempo duración de la sesión.")
  @JsonProperty("Tiempo")
  public String getTiempo() {
    return tiempo;
  }
  public void setTiempo(String tiempo) {
    this.tiempo = tiempo;
  }

  
  /**
   * Motivo de la desconexión.
   **/
  @ApiModelProperty(value = "Motivo de la desconexión.")
  @JsonProperty("Motivo")
  public String getMotivo() {
    return motivo;
  }
  public void setMotivo(String motivo) {
    this.motivo = motivo;
  }

  
  /**
   * Usuario registrado
   **/
  @ApiModelProperty(value = "Usuario registrado")
  @JsonProperty("UserName")
  public String getUserName() {
    return userName;
  }
  public void setUserName(String userName) {
    this.userName = userName;
  }

  
  /**
   * IP del cliente
   **/
  @ApiModelProperty(value = "IP del cliente")
  @JsonProperty("IP")
  public String getIP() {
    return IP;
  }
  public void setIP(String IP) {
    this.IP = IP;
  }

  
  /**
   * BRAS
   **/
  @ApiModelProperty(value = "BRAS")
  @JsonProperty("Bras")
  public String getBras() {
    return bras;
  }
  public void setBras(String bras) {
    this.bras = bras;
  }

  
  /**
   * Tráfico de bajada consumido por el cliente.
   **/
  @ApiModelProperty(value = "Tráfico de bajada consumido por el cliente.")
  @JsonProperty("TraficoDn")
  public String getTraficoDn() {
    return traficoDn;
  }
  public void setTraficoDn(String traficoDn) {
    this.traficoDn = traficoDn;
  }

  
  /**
   * Tráfico de subida consumido por el cliente.
   **/
  @ApiModelProperty(value = "Tráfico de subida consumido por el cliente.")
  @JsonProperty("TraficoUp")
  public String getTraficoUp() {
    return traficoUp;
  }
  public void setTraficoUp(String traficoUp) {
    this.traficoUp = traficoUp;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class RadiusViewModel {\n");
    
    sb.append("    fechaInicio: ").append(StringUtil.toIndentedString(fechaInicio)).append("\n");
    sb.append("    fechaFin: ").append(StringUtil.toIndentedString(fechaFin)).append("\n");
    sb.append("    tiempo: ").append(StringUtil.toIndentedString(tiempo)).append("\n");
    sb.append("    motivo: ").append(StringUtil.toIndentedString(motivo)).append("\n");
    sb.append("    userName: ").append(StringUtil.toIndentedString(userName)).append("\n");
    sb.append("    IP: ").append(StringUtil.toIndentedString(IP)).append("\n");
    sb.append("    bras: ").append(StringUtil.toIndentedString(bras)).append("\n");
    sb.append("    traficoDn: ").append(StringUtil.toIndentedString(traficoDn)).append("\n");
    sb.append("    traficoUp: ").append(StringUtil.toIndentedString(traficoUp)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
