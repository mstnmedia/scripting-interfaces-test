package sacs_v1.model;

import sacs_v1.invoker.StringUtil;
import java.util.Date;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;

@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class VwHistoricoResultOlt {

	private String telefono = null;
	private String nodeAddress = null;
	private String nodeName = null;
	private String usuario = null;
	private String ani = null;
	private String tarjeta = null;
	private String tipo = null;
	private Integer osTicket = null;
	private Integer id = null;
	private Boolean oltadmstate = null;
	private Boolean oltoperstate = null;
	private Boolean ontadmstate = null;
	private Boolean ontoperstate = null;
	private Boolean ontrxpower = null;
	private Boolean onttxpower = null;
	private Boolean ontbiascurrent = null;
	private Boolean onttemp = null;
	private Boolean ontvoltage = null;
	private Boolean oltrxontopticalpower = null;
	private Boolean velconfigurada = null;
	private Boolean password = null;
	private Boolean iptv = null;
	private Boolean adsl = null;
	private Boolean voip = null;
	private Boolean iptvTxRx = null;
	private Boolean iptvServiceportStatus = null;
	private Boolean adslServiceportStatus = null;
	private Boolean voipServiceportStatus = null;
	private Date fecha = null;
	private Integer idDiagnostico = null;
	private Boolean ipInternet = null;
	private Boolean ipIPTV = null;
	private Boolean ipVOIP = null;

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Telefono")
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("NodeAddress")
	public String getNodeAddress() {
		return nodeAddress;
	}

	public void setNodeAddress(String nodeAddress) {
		this.nodeAddress = nodeAddress;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("NodeName")
	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Usuario")
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Ani")
	public String getAni() {
		return ani;
	}

	public void setAni(String ani) {
		this.ani = ani;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Tarjeta")
	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Tipo")
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("OsTicket")
	public Integer getOsTicket() {
		return osTicket;
	}

	public void setOsTicket(Integer osTicket) {
		this.osTicket = osTicket;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Oltadmstate")
	public Boolean getOltadmstate() {
		return oltadmstate;
	}

	public void setOltadmstate(Boolean oltadmstate) {
		this.oltadmstate = oltadmstate;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Oltoperstate")
	public Boolean getOltoperstate() {
		return oltoperstate;
	}

	public void setOltoperstate(Boolean oltoperstate) {
		this.oltoperstate = oltoperstate;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Ontadmstate")
	public Boolean getOntadmstate() {
		return ontadmstate;
	}

	public void setOntadmstate(Boolean ontadmstate) {
		this.ontadmstate = ontadmstate;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Ontoperstate")
	public Boolean getOntoperstate() {
		return ontoperstate;
	}

	public void setOntoperstate(Boolean ontoperstate) {
		this.ontoperstate = ontoperstate;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Ontrxpower")
	public Boolean getOntrxpower() {
		return ontrxpower;
	}

	public void setOntrxpower(Boolean ontrxpower) {
		this.ontrxpower = ontrxpower;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Onttxpower")
	public Boolean getOnttxpower() {
		return onttxpower;
	}

	public void setOnttxpower(Boolean onttxpower) {
		this.onttxpower = onttxpower;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Ontbiascurrent")
	public Boolean getOntbiascurrent() {
		return ontbiascurrent;
	}

	public void setOntbiascurrent(Boolean ontbiascurrent) {
		this.ontbiascurrent = ontbiascurrent;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Onttemp")
	public Boolean getOnttemp() {
		return onttemp;
	}

	public void setOnttemp(Boolean onttemp) {
		this.onttemp = onttemp;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Ontvoltage")
	public Boolean getOntvoltage() {
		return ontvoltage;
	}

	public void setOntvoltage(Boolean ontvoltage) {
		this.ontvoltage = ontvoltage;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Oltrxontopticalpower")
	public Boolean getOltrxontopticalpower() {
		return oltrxontopticalpower;
	}

	public void setOltrxontopticalpower(Boolean oltrxontopticalpower) {
		this.oltrxontopticalpower = oltrxontopticalpower;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Velconfigurada")
	public Boolean getVelconfigurada() {
		return velconfigurada;
	}

	public void setVelconfigurada(Boolean velconfigurada) {
		this.velconfigurada = velconfigurada;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Password")
	public Boolean getPassword() {
		return password;
	}

	public void setPassword(Boolean password) {
		this.password = password;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Iptv")
	public Boolean getIptv() {
		return iptv;
	}

	public void setIptv(Boolean iptv) {
		this.iptv = iptv;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Adsl")
	public Boolean getAdsl() {
		return adsl;
	}

	public void setAdsl(Boolean adsl) {
		this.adsl = adsl;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Voip")
	public Boolean getVoip() {
		return voip;
	}

	public void setVoip(Boolean voip) {
		this.voip = voip;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("IptvTxRx")
	public Boolean getIptvTxRx() {
		return iptvTxRx;
	}

	public void setIptvTxRx(Boolean iptvTxRx) {
		this.iptvTxRx = iptvTxRx;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("IptvServiceportStatus")
	public Boolean getIptvServiceportStatus() {
		return iptvServiceportStatus;
	}

	public void setIptvServiceportStatus(Boolean iptvServiceportStatus) {
		this.iptvServiceportStatus = iptvServiceportStatus;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("AdslServiceportStatus")
	public Boolean getAdslServiceportStatus() {
		return adslServiceportStatus;
	}

	public void setAdslServiceportStatus(Boolean adslServiceportStatus) {
		this.adslServiceportStatus = adslServiceportStatus;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("VoipServiceportStatus")
	public Boolean getVoipServiceportStatus() {
		return voipServiceportStatus;
	}

	public void setVoipServiceportStatus(Boolean voipServiceportStatus) {
		this.voipServiceportStatus = voipServiceportStatus;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("Fecha")
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("IdDiagnostico")
	public Integer getIdDiagnostico() {
		return idDiagnostico;
	}

	public void setIdDiagnostico(Integer idDiagnostico) {
		this.idDiagnostico = idDiagnostico;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("IpInternet")
	public Boolean getIpInternet() {
		return ipInternet;
	}

	public void setIpInternet(Boolean ipInternet) {
		this.ipInternet = ipInternet;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("IpIPTV")
	public Boolean getIpIPTV() {
		return ipIPTV;
	}

	public void setIpIPTV(Boolean ipIPTV) {
		this.ipIPTV = ipIPTV;
	}

	/**
	 *
	 */
	@ApiModelProperty(value = "")
	@JsonProperty("IpVOIP")
	public Boolean getIpVOIP() {
		return ipVOIP;
	}

	public void setIpVOIP(Boolean ipVOIP) {
		this.ipVOIP = ipVOIP;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class VwHistoricoResultOlt {\n");

		sb.append("    telefono: ").append(StringUtil.toIndentedString(telefono)).append("\n");
		sb.append("    nodeAddress: ").append(StringUtil.toIndentedString(nodeAddress)).append("\n");
		sb.append("    nodeName: ").append(StringUtil.toIndentedString(nodeName)).append("\n");
		sb.append("    usuario: ").append(StringUtil.toIndentedString(usuario)).append("\n");
		sb.append("    ani: ").append(StringUtil.toIndentedString(ani)).append("\n");
		sb.append("    tarjeta: ").append(StringUtil.toIndentedString(tarjeta)).append("\n");
		sb.append("    tipo: ").append(StringUtil.toIndentedString(tipo)).append("\n");
		sb.append("    osTicket: ").append(StringUtil.toIndentedString(osTicket)).append("\n");
		sb.append("    id: ").append(StringUtil.toIndentedString(id)).append("\n");
		sb.append("    oltadmstate: ").append(StringUtil.toIndentedString(oltadmstate)).append("\n");
		sb.append("    oltoperstate: ").append(StringUtil.toIndentedString(oltoperstate)).append("\n");
		sb.append("    ontadmstate: ").append(StringUtil.toIndentedString(ontadmstate)).append("\n");
		sb.append("    ontoperstate: ").append(StringUtil.toIndentedString(ontoperstate)).append("\n");
		sb.append("    ontrxpower: ").append(StringUtil.toIndentedString(ontrxpower)).append("\n");
		sb.append("    onttxpower: ").append(StringUtil.toIndentedString(onttxpower)).append("\n");
		sb.append("    ontbiascurrent: ").append(StringUtil.toIndentedString(ontbiascurrent)).append("\n");
		sb.append("    onttemp: ").append(StringUtil.toIndentedString(onttemp)).append("\n");
		sb.append("    ontvoltage: ").append(StringUtil.toIndentedString(ontvoltage)).append("\n");
		sb.append("    oltrxontopticalpower: ").append(StringUtil.toIndentedString(oltrxontopticalpower)).append("\n");
		sb.append("    velconfigurada: ").append(StringUtil.toIndentedString(velconfigurada)).append("\n");
		sb.append("    password: ").append(StringUtil.toIndentedString(password)).append("\n");
		sb.append("    iptv: ").append(StringUtil.toIndentedString(iptv)).append("\n");
		sb.append("    adsl: ").append(StringUtil.toIndentedString(adsl)).append("\n");
		sb.append("    voip: ").append(StringUtil.toIndentedString(voip)).append("\n");
		sb.append("    iptvTxRx: ").append(StringUtil.toIndentedString(iptvTxRx)).append("\n");
		sb.append("    iptvServiceportStatus: ").append(StringUtil.toIndentedString(iptvServiceportStatus)).append("\n");
		sb.append("    adslServiceportStatus: ").append(StringUtil.toIndentedString(adslServiceportStatus)).append("\n");
		sb.append("    voipServiceportStatus: ").append(StringUtil.toIndentedString(voipServiceportStatus)).append("\n");
		sb.append("    fecha: ").append(StringUtil.toIndentedString(fecha)).append("\n");
		sb.append("    idDiagnostico: ").append(StringUtil.toIndentedString(idDiagnostico)).append("\n");
		sb.append("    ipInternet: ").append(StringUtil.toIndentedString(ipInternet)).append("\n");
		sb.append("    ipIPTV: ").append(StringUtil.toIndentedString(ipIPTV)).append("\n");
		sb.append("    ipVOIP: ").append(StringUtil.toIndentedString(ipVOIP)).append("\n");
		sb.append("}");
		return sb.toString();
	}
}
