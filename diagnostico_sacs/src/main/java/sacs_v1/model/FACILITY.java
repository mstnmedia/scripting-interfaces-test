package sacs_v1.model;

import sacs_v1.invoker.StringUtil;



import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class FACILITY   {
  
  private Integer ID = null;
  private String NODE_ADDRESS = null;
  private String IP_ADDRESS = null;
  private String NODE_NAME = null;
  private String EQUIPMENT_NAME = null;
  private Long CIRCUIT_DESIGN_ID = null;
  private String VENDOR_NAME = null;
  private String TYPE = null;
  private String CIRCUIT_ID = null;
  private String CLLI_CODE = null;
  private String STATUS = null;
  private String TELEFONO = null;
  private String TIPO = null;
  private String REFERENCIA = null;
  private Integer FACILITY_COUNT = null;
  private String PUERTO = null;
  private Boolean isCobre = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("ID")
  public Integer getID() {
    return ID;
  }
  public void setID(Integer ID) {
    this.ID = ID;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("NODE_ADDRESS")
  public String getNODEADDRESS() {
    return NODE_ADDRESS;
  }
  public void setNODEADDRESS(String NODE_ADDRESS) {
    this.NODE_ADDRESS = NODE_ADDRESS;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("IP_ADDRESS")
  public String getIPADDRESS() {
    return IP_ADDRESS;
  }
  public void setIPADDRESS(String IP_ADDRESS) {
    this.IP_ADDRESS = IP_ADDRESS;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("NODE_NAME")
  public String getNODENAME() {
    return NODE_NAME;
  }
  public void setNODENAME(String NODE_NAME) {
    this.NODE_NAME = NODE_NAME;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("EQUIPMENT_NAME")
  public String getEQUIPMENTNAME() {
    return EQUIPMENT_NAME;
  }
  public void setEQUIPMENTNAME(String EQUIPMENT_NAME) {
    this.EQUIPMENT_NAME = EQUIPMENT_NAME;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("CIRCUIT_DESIGN_ID")
  public Long getCIRCUITDESIGNID() {
    return CIRCUIT_DESIGN_ID;
  }
  public void setCIRCUITDESIGNID(Long CIRCUIT_DESIGN_ID) {
    this.CIRCUIT_DESIGN_ID = CIRCUIT_DESIGN_ID;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("VENDOR_NAME")
  public String getVENDORNAME() {
    return VENDOR_NAME;
  }
  public void setVENDORNAME(String VENDOR_NAME) {
    this.VENDOR_NAME = VENDOR_NAME;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("TYPE")
  public String getTYPE() {
    return TYPE;
  }
  public void setTYPE(String TYPE) {
    this.TYPE = TYPE;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("CIRCUIT_ID")
  public String getCIRCUITID() {
    return CIRCUIT_ID;
  }
  public void setCIRCUITID(String CIRCUIT_ID) {
    this.CIRCUIT_ID = CIRCUIT_ID;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("CLLI_CODE")
  public String getCLLICODE() {
    return CLLI_CODE;
  }
  public void setCLLICODE(String CLLI_CODE) {
    this.CLLI_CODE = CLLI_CODE;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("STATUS")
  public String getSTATUS() {
    return STATUS;
  }
  public void setSTATUS(String STATUS) {
    this.STATUS = STATUS;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("TELEFONO")
  public String getTELEFONO() {
    return TELEFONO;
  }
  public void setTELEFONO(String TELEFONO) {
    this.TELEFONO = TELEFONO;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("TIPO")
  public String getTIPO() {
    return TIPO;
  }
  public void setTIPO(String TIPO) {
    this.TIPO = TIPO;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("REFERENCIA")
  public String getREFERENCIA() {
    return REFERENCIA;
  }
  public void setREFERENCIA(String REFERENCIA) {
    this.REFERENCIA = REFERENCIA;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("FACILITY_COUNT")
  public Integer getFACILITYCOUNT() {
    return FACILITY_COUNT;
  }
  public void setFACILITYCOUNT(Integer FACILITY_COUNT) {
    this.FACILITY_COUNT = FACILITY_COUNT;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("PUERTO")
  public String getPUERTO() {
    return PUERTO;
  }
  public void setPUERTO(String PUERTO) {
    this.PUERTO = PUERTO;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("isCobre")
  public Boolean getIsCobre() {
    return isCobre;
  }
  public void setIsCobre(Boolean isCobre) {
    this.isCobre = isCobre;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class FACILITY {\n");
    
    sb.append("    ID: ").append(StringUtil.toIndentedString(ID)).append("\n");
    sb.append("    NODE_ADDRESS: ").append(StringUtil.toIndentedString(NODE_ADDRESS)).append("\n");
    sb.append("    IP_ADDRESS: ").append(StringUtil.toIndentedString(IP_ADDRESS)).append("\n");
    sb.append("    NODE_NAME: ").append(StringUtil.toIndentedString(NODE_NAME)).append("\n");
    sb.append("    EQUIPMENT_NAME: ").append(StringUtil.toIndentedString(EQUIPMENT_NAME)).append("\n");
    sb.append("    CIRCUIT_DESIGN_ID: ").append(StringUtil.toIndentedString(CIRCUIT_DESIGN_ID)).append("\n");
    sb.append("    VENDOR_NAME: ").append(StringUtil.toIndentedString(VENDOR_NAME)).append("\n");
    sb.append("    TYPE: ").append(StringUtil.toIndentedString(TYPE)).append("\n");
    sb.append("    CIRCUIT_ID: ").append(StringUtil.toIndentedString(CIRCUIT_ID)).append("\n");
    sb.append("    CLLI_CODE: ").append(StringUtil.toIndentedString(CLLI_CODE)).append("\n");
    sb.append("    STATUS: ").append(StringUtil.toIndentedString(STATUS)).append("\n");
    sb.append("    TELEFONO: ").append(StringUtil.toIndentedString(TELEFONO)).append("\n");
    sb.append("    TIPO: ").append(StringUtil.toIndentedString(TIPO)).append("\n");
    sb.append("    REFERENCIA: ").append(StringUtil.toIndentedString(REFERENCIA)).append("\n");
    sb.append("    FACILITY_COUNT: ").append(StringUtil.toIndentedString(FACILITY_COUNT)).append("\n");
    sb.append("    PUERTO: ").append(StringUtil.toIndentedString(PUERTO)).append("\n");
    sb.append("    isCobre: ").append(StringUtil.toIndentedString(isCobre)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
