package sacs_v1.invoker.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}