package sacs_v1.invoker.auth;

import java.io.UnsupportedEncodingException;
import sacs_v1.invoker.Pair;

import java.util.Map;
import java.util.List;
import javax.xml.bind.DatatypeConverter;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class OAuth implements Authentication {

	String token;

	public OAuth() {
	}

	public OAuth(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public void applyToParams(List<Pair> queryParams, Map<String, String> headerParams) {
		String str = (token == null ? "" : token);
//		try {
			String key = "Authorization";
			String value = "Bearer " + token; //DatatypeConverter.printBase64Binary(str.getBytes("UTF-8"));
			headerParams.put(key, value);
//		} catch (UnsupportedEncodingException e) {
//			throw new RuntimeException(e);
//		}
	}
}
