package sacs_v1.api;

import sacs_v1.invoker.ApiException;
import sacs_v1.invoker.ApiClient;
import sacs_v1.invoker.Configuration;
import sacs_v1.invoker.Pair;
import sacs_v1.invoker.TypeRef;
import sacs_v1.model.ResultString;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class AuthApi {

	private ApiClient apiClient;

	public AuthApi() {
		this(Configuration.getDefaultApiClient());
	}

	public AuthApi(ApiClient apiClient) {
		this.apiClient = apiClient;
	}

	public ApiClient getApiClient() {
		return apiClient;
	}

	public void setApiClient(ApiClient apiClient) {
		this.apiClient = apiClient;
	}

	/**
	 * Genera un token para identificar la aplicación al momento de realizar
	 * algún proceso.
	 *
	 * @param userName Nombre del usuario que está utilizando la aplicación.
	 * @param application Código de aplicación por ejemplo SACS, NetCracker
	 * @param secretCode Código secreto proporcinado para su uso.
	 * @param sessionTimeMinutes Tiempo de duración de la sesión en minutos
	 * @return ResultString
	 */
	public ResultString authGet(String userName, String application, String secretCode, Integer sessionTimeMinutes) throws ApiException {
		Object postBody = null;
		byte[] postBinaryBody = null;

		// verify the required parameter 'userName' is set
		if (userName == null) {
			throw new ApiException(400, "Missing the required parameter 'userName' when calling authGet");
		}

		// verify the required parameter 'application' is set
		if (application == null) {
			throw new ApiException(400, "Missing the required parameter 'application' when calling authGet");
		}

		// verify the required parameter 'secretCode' is set
		if (secretCode == null) {
			throw new ApiException(400, "Missing the required parameter 'secretCode' when calling authGet");
		}

		// verify the required parameter 'sessionTimeMinutes' is set
		if (sessionTimeMinutes == null) {
			throw new ApiException(400, "Missing the required parameter 'sessionTimeMinutes' when calling authGet");
		}

		// create path and map variables
		String path = "/api/Auth".replaceAll("\\{format\\}", "json");

		// query params
		List<Pair> queryParams = new ArrayList<Pair>();
		Map<String, String> headerParams = new HashMap<String, String>();
		Map<String, Object> formParams = new HashMap<String, Object>();

		queryParams.addAll(apiClient.parameterToPairs("", "userName", userName));

		queryParams.addAll(apiClient.parameterToPairs("", "application", application));

		queryParams.addAll(apiClient.parameterToPairs("", "secretCode", secretCode));

		queryParams.addAll(apiClient.parameterToPairs("", "sessionTimeMinutes", sessionTimeMinutes));

		final String[] accepts = {
			"application/json", "text/json", "application/xml", "text/xml"
		};
		final String accept = apiClient.selectHeaderAccept(accepts);

		final String[] contentTypes = {};
		final String contentType = apiClient.selectHeaderContentType(contentTypes);

		String[] authNames = new String[]{};

		TypeRef returnType = new TypeRef<ResultString>() {
		};
		return apiClient.invokeAPI(path, "GET", queryParams, postBody, postBinaryBody, headerParams, formParams, accept, contentType, authNames, returnType);

	}

}
