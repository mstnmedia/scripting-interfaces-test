package sacs_v1.api;

import sacs_v1.invoker.ApiException;
import sacs_v1.invoker.ApiClient;
import sacs_v1.invoker.Configuration;
import sacs_v1.invoker.Pair;
import sacs_v1.invoker.TypeRef;
import sacs_v1.model.ResultRadiusReturnViewModel;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class RadiusApi {

	private ApiClient apiClient;

	public RadiusApi() {
		this(Configuration.getDefaultApiClient());
	}

	public RadiusApi(ApiClient apiClient) {
		this.apiClient = apiClient;
	}

	public ApiClient getApiClient() {
		return apiClient;
	}

	public void setApiClient(ApiClient apiClient) {
		this.apiClient = apiClient;
	}

	/**
	 * Obten el histórico de Radius.
	 *
	 * @param phoneNumber
	 * @return ResultRadiusReturnViewModel
	 */
	public ResultRadiusReturnViewModel radiusGetRadiusHistory(String phoneNumber) throws ApiException {
		Object postBody = null;
		byte[] postBinaryBody = null;

		// verify the required parameter 'phoneNumber' is set
		if (phoneNumber == null) {
			throw new ApiException(400, "Missing the required parameter 'phoneNumber' when calling radiusGetRadiusHistory");
		}

		// create path and map variables
		String path = "/GetRadiusHistory".replaceAll("\\{format\\}", "json");

		// query params
		List<Pair> queryParams = new ArrayList<Pair>();
		Map<String, String> headerParams = new HashMap<String, String>();
		Map<String, Object> formParams = new HashMap<String, Object>();

		queryParams.addAll(apiClient.parameterToPairs("", "phoneNumber", phoneNumber));

		final String[] accepts = {
			"application/json", "text/json", "application/xml", "text/xml"
		};
		final String accept = apiClient.selectHeaderAccept(accepts);

		final String[] contentTypes = {};
		final String contentType = apiClient.selectHeaderContentType(contentTypes);

		String[] authNames = new String[]{};

		TypeRef returnType = new TypeRef<ResultRadiusReturnViewModel>() {
		};
		return apiClient.invokeAPI(path, "GET", queryParams, postBody, postBinaryBody, headerParams, formParams, accept, contentType, authNames, returnType);

	}

}
