package sacs_v1.api;

import sacs_v1.invoker.ApiException;
import sacs_v1.invoker.ApiClient;
import sacs_v1.invoker.Configuration;
import sacs_v1.invoker.Pair;
import sacs_v1.invoker.TypeRef;
import sacs_v1.model.ResultDiagnosticoViewModel;
import sacs_v1.model.ResultDSLAMINFO;
import sacs_v1.model.FacilityViewModel;
import sacs_v1.model.ResultHistoricoViewModel;
import sacs_v1.model.ResultHistoricoGraficoViewModel;
import sacs_v1.model.ResultGPONINFO;
import sacs_v1.model.ResultObject;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class DiagnosticApi {

	private ApiClient apiClient;

	public DiagnosticApi() {
		this(Configuration.getDefaultApiClient());
	}

	public DiagnosticApi(ApiClient apiClient) {
		this.apiClient = apiClient;
	}

	public ApiClient getApiClient() {
		return apiClient;
	}

	public void setApiClient(ApiClient apiClient) {
		this.apiClient = apiClient;
	}

	/**
	 * Obtiene el diagnóstico de la facilidad especificada para un DSLAM U OLT a
	 * través de comandos SNMP.
	 *
	 * @param idFacilidad ID Creado al generar identificador de facilidad
	 * @return ResultDiagnosticoViewModel
	 */
	public ResultDiagnosticoViewModel diagnosticGetDiagnostico(Integer idFacilidad) throws ApiException {
		Object postBody = null;
		byte[] postBinaryBody = null;

		// verify the required parameter 'idFacilidad' is set
		if (idFacilidad == null) {
			throw new ApiException(400, "Missing the required parameter 'idFacilidad' when calling diagnosticGetDiagnostico");
		}

		// create path and map variables
		String path = "/GetDiagnostico".replaceAll("\\{format\\}", "json");

		// query params
		List<Pair> queryParams = new ArrayList<Pair>();
		Map<String, String> headerParams = new HashMap<String, String>();
		Map<String, Object> formParams = new HashMap<String, Object>();

		queryParams.addAll(apiClient.parameterToPairs("", "id_facilidad", idFacilidad));

		final String[] accepts = {
			"application/json", "text/json", "application/xml", "text/xml"
		};
		final String accept = apiClient.selectHeaderAccept(accepts);

		final String[] contentTypes = {};
		final String contentType = apiClient.selectHeaderContentType(contentTypes);

		String[] authNames = new String[]{};

		TypeRef returnType = new TypeRef<ResultDiagnosticoViewModel>() {
		};
		return apiClient.invokeAPI(path, "GET", queryParams, postBody, postBinaryBody, headerParams, formParams, accept, contentType, authNames, returnType);

	}

	/**
	 * Obtiene el diagnóstico de la facilidad especificada para un DSLAM U OLT a
	 * través de comandos SSH.
	 *
	 * @param idFacilidad ID Creado al generar identificador de facilidad
	 * @return ResultDSLAMINFO
	 */
	public ResultDSLAMINFO diagnosticGetDiagnosticoCompletivo(Integer idFacilidad) throws ApiException {
		Object postBody = null;
		byte[] postBinaryBody = null;

		// verify the required parameter 'idFacilidad' is set
		if (idFacilidad == null) {
			throw new ApiException(400, "Missing the required parameter 'idFacilidad' when calling diagnosticGetDiagnosticoCompletivo");
		}

		// create path and map variables
		String path = "/GetDiagnosticoCompletivo".replaceAll("\\{format\\}", "json");

		// query params
		List<Pair> queryParams = new ArrayList<Pair>();
		Map<String, String> headerParams = new HashMap<String, String>();
		Map<String, Object> formParams = new HashMap<String, Object>();

		queryParams.addAll(apiClient.parameterToPairs("", "id_facilidad", idFacilidad));

		final String[] accepts = {
			"application/json", "text/json", "application/xml", "text/xml"
		};
		final String accept = apiClient.selectHeaderAccept(accepts);

		final String[] contentTypes = {};
		final String contentType = apiClient.selectHeaderContentType(contentTypes);

		String[] authNames = new String[]{};

		TypeRef returnType = new TypeRef<ResultDSLAMINFO>() {
		};
		return apiClient.invokeAPI(path, "GET", queryParams, postBody, postBinaryBody, headerParams, formParams, accept, contentType, authNames, returnType);

	}

	/**
	 * Obtiene el histórico de los diagnósticos realizados a una facilidad
	 * específica.
	 *
	 * @param facilidad Facilidad a consultar
	 * @return ResultHistoricoViewModel
	 */
	public ResultHistoricoViewModel diagnosticGetHistoricoDiagnostico(FacilityViewModel facilidad) throws ApiException {
		Object postBody = facilidad;
		byte[] postBinaryBody = null;

		// verify the required parameter 'facilidad' is set
		if (facilidad == null) {
			throw new ApiException(400, "Missing the required parameter 'facilidad' when calling diagnosticGetHistoricoDiagnostico");
		}

		// create path and map variables
		String path = "/GetHistoricoDiagnostico".replaceAll("\\{format\\}", "json");

		// query params
		List<Pair> queryParams = new ArrayList<Pair>();
		Map<String, String> headerParams = new HashMap<String, String>();
		Map<String, Object> formParams = new HashMap<String, Object>();

		final String[] accepts = {
			"application/json", "text/json", "application/xml", "text/xml"
		};
		final String accept = apiClient.selectHeaderAccept(accepts);

		final String[] contentTypes = {
			"application/json", "text/json", "application/xml", "text/xml", "application/x-www-form-urlencoded"
		};
		final String contentType = apiClient.selectHeaderContentType(contentTypes);

		String[] authNames = new String[]{};

		TypeRef returnType = new TypeRef<ResultHistoricoViewModel>() {
		};
		return apiClient.invokeAPI(path, "POST", queryParams, postBody, postBinaryBody, headerParams, formParams, accept, contentType, authNames, returnType);

	}

	/**
	 * Obtiene el histórico en forma numérica para fines de mostrar en forma
	 * gráfica las variaciones en la facilidad del cliente.
	 *
	 * @param facilidad Facilidad a consultar
	 * @return ResultHistoricoGraficoViewModel
	 */
	public ResultHistoricoGraficoViewModel diagnosticGetHistoricoDiagnosticoGrafico(FacilityViewModel facilidad) throws ApiException {
		Object postBody = facilidad;
		byte[] postBinaryBody = null;

		// verify the required parameter 'facilidad' is set
		if (facilidad == null) {
			throw new ApiException(400, "Missing the required parameter 'facilidad' when calling diagnosticGetHistoricoDiagnosticoGrafico");
		}

		// create path and map variables
		String path = "/GetHistoricoDiagnosticoGrafico".replaceAll("\\{format\\}", "json");

		// query params
		List<Pair> queryParams = new ArrayList<Pair>();
		Map<String, String> headerParams = new HashMap<String, String>();
		Map<String, Object> formParams = new HashMap<String, Object>();

		final String[] accepts = {
			"application/json", "text/json", "application/xml", "text/xml"
		};
		final String accept = apiClient.selectHeaderAccept(accepts);

		final String[] contentTypes = {
			"application/json", "text/json", "application/xml", "text/xml", "application/x-www-form-urlencoded"
		};
		final String contentType = apiClient.selectHeaderContentType(contentTypes);

		String[] authNames = new String[]{};

		TypeRef returnType = new TypeRef<ResultHistoricoGraficoViewModel>() {
		};
		return apiClient.invokeAPI(path, "POST", queryParams, postBody, postBinaryBody, headerParams, formParams, accept, contentType, authNames, returnType);

	}

	/**
	 * Obtiene los services profile de una facilidad GPON.
	 *
	 * @param idFacilidad ID creado al generar identificador de facilidad
	 * @return ResultGPONINFO
	 */
	public ResultGPONINFO diagnosticGetServicesProfile(Integer idFacilidad) throws ApiException {
		Object postBody = null;
		byte[] postBinaryBody = null;

		// verify the required parameter 'idFacilidad' is set
		if (idFacilidad == null) {
			throw new ApiException(400, "Missing the required parameter 'idFacilidad' when calling diagnosticGetServicesProfile");
		}

		// create path and map variables
		String path = "/GetServicesProfile".replaceAll("\\{format\\}", "json");

		// query params
		List<Pair> queryParams = new ArrayList<Pair>();
		Map<String, String> headerParams = new HashMap<String, String>();
		Map<String, Object> formParams = new HashMap<String, Object>();

		queryParams.addAll(apiClient.parameterToPairs("", "id_facilidad", idFacilidad));

		final String[] accepts = {
			"application/json", "text/json", "application/xml", "text/xml"
		};
		final String accept = apiClient.selectHeaderAccept(accepts);

		final String[] contentTypes = {};
		final String contentType = apiClient.selectHeaderContentType(contentTypes);

		String[] authNames = new String[]{};

		TypeRef returnType = new TypeRef<ResultGPONINFO>() {
		};
		return apiClient.invokeAPI(path, "GET", queryParams, postBody, postBinaryBody, headerParams, formParams, accept, contentType, authNames, returnType);

	}

	/**
	 * Envia una señal para reiniciar el puerto del cliente.
	 *
	 * @param idFacilidad id de facilidad
	 * @return ResultObject
	 */
	public ResultObject diagnosticPostResetPort(Integer idFacilidad) throws ApiException {
		Object postBody = null;
		byte[] postBinaryBody = null;

		// verify the required parameter 'idFacilidad' is set
		if (idFacilidad == null) {
			throw new ApiException(400, "Missing the required parameter 'idFacilidad' when calling diagnosticPostResetPort");
		}

		// create path and map variables
		String path = "/ResetPort".replaceAll("\\{format\\}", "json");

		// query params
		List<Pair> queryParams = new ArrayList<Pair>();
		Map<String, String> headerParams = new HashMap<String, String>();
		Map<String, Object> formParams = new HashMap<String, Object>();

		queryParams.addAll(apiClient.parameterToPairs("", "id_facilidad", idFacilidad));

		final String[] accepts = {
			"application/json", "text/json", "application/xml", "text/xml"
		};
		final String accept = apiClient.selectHeaderAccept(accepts);

		final String[] contentTypes = {};
		final String contentType = apiClient.selectHeaderContentType(contentTypes);

		String[] authNames = new String[]{};

		TypeRef returnType = new TypeRef<ResultObject>() {
		};
		return apiClient.invokeAPI(path, "GET", queryParams, postBody, postBinaryBody, headerParams, formParams, accept, contentType, authNames, returnType);

	}

}
