package sacs_v1.api;

import sacs_v1.invoker.ApiException;
import sacs_v1.invoker.ApiClient;
import sacs_v1.invoker.Configuration;
import sacs_v1.invoker.Pair;
import sacs_v1.invoker.TypeRef;
import sacs_v1.model.ResultIEnumerableFacilityViewModel;
import sacs_v1.model.ResultObject;
import sacs_v1.model.FacilityViewModel;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-02T14:43:11.969-04:00")
public class FacilityApi {

	private ApiClient apiClient;

	public FacilityApi() {
		this(Configuration.getDefaultApiClient());
	}

	public FacilityApi(ApiClient apiClient) {
		this.apiClient = apiClient;
	}

	public ApiClient getApiClient() {
		return apiClient;
	}

	public void setApiClient(ApiClient apiClient) {
		this.apiClient = apiClient;
	}

	/**
	 * Obtiene las facilidades del cliente, utilizando el número de teléfono.
	 *
	 * @param phoneNumber Número de teléfono
	 * @param token
	 * @return ResultIEnumerableFacilityViewModel
	 */
	public ResultIEnumerableFacilityViewModel facilityGetFacilitiesByPhoneNumber(String phoneNumber) throws ApiException {
		Object postBody = null;
		byte[] postBinaryBody = null;

		// verify the required parameter 'phoneNumber' is set
		if (phoneNumber == null) {
			throw new ApiException(400, "Missing the required parameter 'phoneNumber' when calling facilityGetFacilitiesByPhoneNumber");
		}

		// create path and map variables
		String path = "/GetFacilitiesByPhoneNumber".replaceAll("\\{format\\}", "json");

		// query params
		List<Pair> queryParams = new ArrayList<Pair>();
		Map<String, String> headerParams = new HashMap<String, String>();
		Map<String, Object> formParams = new HashMap<String, Object>();

		queryParams.addAll(apiClient.parameterToPairs("", "phoneNumber", phoneNumber));

		final String[] accepts = {
			"application/json", "text/json", "application/xml", "text/xml"
		};
		final String accept = apiClient.selectHeaderAccept(accepts);

		final String[] contentTypes = {};
		final String contentType = apiClient.selectHeaderContentType(contentTypes);

		String[] authNames = new String[]{};

		TypeRef returnType = new TypeRef<ResultIEnumerableFacilityViewModel>() {
		};
		return apiClient.invokeAPI(path, "GET", queryParams, postBody, postBinaryBody, headerParams, formParams, accept, contentType, authNames, returnType);

	}

	/**
	 * Requerido para realizar la consulta de los demás métodos en el sistema.
	 *
	 * @param facility Datos de la facilidad a consultar
	 * @return ResultObject
	 */
	public ResultObject facilityGetFacilityIdentifier(FacilityViewModel facility) throws ApiException {
		Object postBody = facility;
		byte[] postBinaryBody = null;

		// verify the required parameter 'facility' is set
		if (facility == null) {
			throw new ApiException(400, "Missing the required parameter 'facility' when calling facilityGetFacilityIdentifier");
		}

		// create path and map variables
		String path = "/GetFacilityIdentifier".replaceAll("\\{format\\}", "json");

		// query params
		List<Pair> queryParams = new ArrayList<Pair>();
		Map<String, String> headerParams = new HashMap<String, String>();
		Map<String, Object> formParams = new HashMap<String, Object>();

		final String[] accepts = {
			"application/json", "text/json", "application/xml", "text/xml"
		};
		final String accept = apiClient.selectHeaderAccept(accepts);

		final String[] contentTypes = {
			"application/json", "text/json", "application/xml", "text/xml", "application/x-www-form-urlencoded"
		};
		final String contentType = apiClient.selectHeaderContentType(contentTypes);

		String[] authNames = new String[]{};

		TypeRef returnType = new TypeRef<ResultObject>() {
		};

		return apiClient.invokeAPI(path, "POST", queryParams, postBody, postBinaryBody, headerParams, formParams, accept, contentType, authNames, returnType);
	}

}
