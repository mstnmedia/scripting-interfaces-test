/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.diagnostico.Facility_SACS;
import com.mstn.scripting.interfaces.diagnostico.SACS_RESTApi;
import sacs_v1.api.AuthApi;
import sacs_v1.api.FacilityApi;
import sacs_v1.invoker.ApiClient;
import sacs_v1.invoker.auth.OAuth;
import sacs_v1.model.ResultIEnumerableFacilityViewModel;
import sacs_v1.model.ResultString;

/**
 * Clase ejecutable con métodos de pruebas para probar las diferentes
 * funcionalidades en este proyecto.
 *
 * @author amatos
 */
public class Main {

	static {
		InterfaceConfigs.setConfig();
	}
	static TransactionInterfacesPayload payload = new TransactionInterfacesPayload(
			new V_Transaction(), new Workflow_Step(), "{}"
	);
//	static String phoneNumber = "8095813687";
	static String phoneNumber = "8092618092";
	static String userName = "Abel Matos";
	static String appName = InterfaceConfigs.get("sacsApplication");
	static String secretCode = InterfaceConfigs.get("sacsSecretCode");
	static int sessionTime = Integer.parseInt(InterfaceConfigs.get("sacsSessionTimeMinutes"));

	static ResultString authApi() throws Exception {
		AuthApi api = new AuthApi();
		ResultString result = api.authGet(userName, appName, secretCode, sessionTime);
		return result;
	}

	static Object facility() throws Exception {
		FacilityApi soap = new FacilityApi();
		ApiClient api = soap.getApiClient();
		String token = authApi().getValue();
		api.getAuthentications().put("oauth", new OAuth(token));
		ResultIEnumerableFacilityViewModel result = soap.facilityGetFacilitiesByPhoneNumber(phoneNumber);
		return result;
	}

	static Object testFacility() throws Exception {
		Facility_SACS service = new Facility_SACS();
		Interface inter = service.getInterfaces(true).get(0);
		ObjectNode form = JSON.newObjectNode();
		String[] values = {phoneNumber};
		for (int i = 0; i < inter.getInterface_field().size(); i++) {
			Interface_Field field = inter.getInterface_field().get(i);
			form.put(field.getName(), values[i]);
		}
		payload.setForm(form.toString());
		return service.callMethod(inter, payload, User.SYSTEM);
	}

	public static void main(String[] args) throws Exception {
		Object result = null;
//		Object result = "Hello";
//		Object result = authApi();
//		Object result = facility();
//		Object result = testFacility();
//		SACS_RESTApi.LoginSACS login = new SACS_RESTApi.LoginSACS(InterfaceConfigs.get("sacs_rest_systemname"),InterfaceConfigs.get("sacs_rest_systempassword"));
//		ResponseEntity<String> response = RESTTemplate.post(InterfaceConfigs.get("sacs_rest_api") + "/Authenticate",login, String.class);
//		result = response.getBody();
//		System.out.println(JSON.toString(result));
		SACS_RESTApi sacs = new SACS_RESTApi();
		SACS_RESTApi.SACSRequest params = new SACS_RESTApi.SACSRequest(8097326572L);
//		result = sacs.getServicePorts(params);
//		result = sacs.getGponCondition(params);
//		result = sacs.getPortStatus(params);
//		result = sacs.getPortConfiguration(params);
//		result = sacs.getPortCondition(params);
//		result = sacs.getIPTVInfo(params);
//		result = sacs.getPortConfigurationInfo(params);
		System.out.println(JSON.toString(result));
	}
}
