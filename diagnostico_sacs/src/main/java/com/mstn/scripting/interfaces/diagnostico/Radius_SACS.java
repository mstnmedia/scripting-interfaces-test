/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.diagnostico;

import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import sacs_v1.api.RadiusApi;

/**
 *
 * @author amatos
 */
public class Radius_SACS extends InterfaceBase implements Common.SACS_Base {

	static public final RadiusApi SOAP = new RadiusApi();

	public Radius_SACS() {
		super(0, "sacsr", SOAP);
		IGNORED_METHODS.addAll(Arrays.asList("getApiClient", "setApiClient"));
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("diagnostico_sacs"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		Common.getInterfaceField(field, _class, inter, fieldName);
		return field;
	}

	@Override
	protected Object sendParamsToMethod(Interface inter, Method method, List<Object> params) throws Exception {
		Common.updateToken(this, mapppedResults, SOAP.getApiClient());
		Object value = super.sendParamsToMethod(inter, method, params);
		return value;
	}

	@Override
	public void test() throws Exception {
		Common.test(this);
	}

	@Override
	public void baseTest() throws Exception {
		super.test();
	}
}
