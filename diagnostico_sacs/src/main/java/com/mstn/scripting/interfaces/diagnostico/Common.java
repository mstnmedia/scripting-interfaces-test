/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.diagnostico;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.util.HashMap;
import sacs_v1.api.AuthApi;
import sacs_v1.invoker.ApiClient;
import sacs_v1.invoker.ApiException;
import sacs_v1.model.ResultString;

/**
 * Clase que contiene las funciones comunes para todas la interfaces que se
 * conectan al web service de Diagnóstico SACS: Diagnostic, Facility & Radius.
 *
 * @author amatos
 */
public class Common {

	static public interface SACS_Base {

		public void test() throws Exception;

		public void baseTest() throws Exception;

	}

	static public Interface_Field getInterfaceField(Interface_Field field, Class _class, Interface inter, String fieldName) {
		if (fieldName.endsWith("phoneNumber")) {
			field.setLabel("No. de Teléfono");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_PHONE);
		} else if (fieldName.endsWith("facility")) {
			field.setId_type(InterfaceFieldTypes.TEXTAREA);
			field.setLabel("Facilidad");
		}
		return field;
	}

	static public String updateToken(InterfaceBase base, HashMap<String, Object> results, ApiClient apiClient) throws ApiException {
		String userName = base.getTransactionResultValue(Interfaces.EMPLOYEE_INFO_INTERFACE_NAME + "_name");
		if (Utils.stringIsNullOrEmpty(userName)) {
			base.baseLog("UserName not found in transaction variables: UNDEFINED");
			userName = "UNDEFINED";
		}
		String application = InterfaceConfigs.get("sacsApplication");
		String secretCode = InterfaceConfigs.get("sacsSecretCode");
		int sessionTimeMinutes = Integer.parseInt(
				InterfaceConfigs.get("sacsSessionTimeMinutes", "20")
		);
		AuthApi api = new AuthApi();
		api.getApiClient().addDefaultHeader("Authorization", null);
		ResultString authResponse = api.authGet(userName, application, secretCode, sessionTimeMinutes);
		String token = authResponse.getValue();
		InterfaceConfigs.configs.put("sacsToken", token);
		base.baseLog("New token: " + token);
		apiClient.addDefaultHeader("Authorization", "Bearer " + token);
		return token;
	}

	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	static public void test(SACS_Base base) throws Exception {
		InterfaceConfigs.get("sacsApplication").toString();
		InterfaceConfigs.get("sacsSecretCode").toString();
		Integer.parseInt(InterfaceConfigs.get("sacsSessionTimeMinutes"));
		base.baseTest();
	}
}
