/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.diagnostico;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.RESTTemplate;
import com.mstn.scripting.core.RESTTemplate.Request;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.InterfaceConfigs;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author amatos
 */
public class SACS_RESTApi {

	static String SACS_REST_API_URL = "http://nttappsweb0010/sacsapi/api";
	static String SACS_REST_SYSTEMNAME = "SCRIPTING";
	static String SACS_REST_SYSTEMPASSWORD = "Dy0t8W3TcNekrD+";
	static HashMap<String, Class> GENERIC_CLASS = new HashMap();

	public SACS_RESTApi() {
		SACS_REST_API_URL = InterfaceConfigs.get("sacs_rest_api");
		SACS_REST_SYSTEMNAME = InterfaceConfigs.get("sacs_rest_systemname");
		SACS_REST_SYSTEMPASSWORD = InterfaceConfigs.get("sacs_rest_systempassword");
	}

	static String setToken(Request request) throws Exception {
		try {
			LoginSACS login = new LoginSACS(SACS_REST_SYSTEMNAME, SACS_REST_SYSTEMPASSWORD);
			ResponseEntity<AuthenticateResponse> response = RESTTemplate.post(SACS_REST_API_URL + "/Authenticate", login, AuthenticateResponse.class);
			AuthenticateResponse responseBody = response.getBody();
			if (responseBody.Succeeded) {
				String token = responseBody.Value.Token;
				request.headers.setBearerAuth(token);
				return token;
			}
			String errorMsg = responseBody.Message;
			if (Utils.nonNullOrEmpty(responseBody.Errors)) {
				for (int i = 0; i < responseBody.Errors.size(); i++) {
					GenericResponseErrorSACS error = responseBody.Errors.get(i);
					errorMsg += "\n" + error.Code + " " + error.Message;
				}
			}
			throw new Exception(errorMsg);
		} catch (Exception ex) {
			throw new Exception("La autenticación no fue éxitosa: " + ex.getMessage());
		}
	}

	public List<ServicePortVM> getServicePorts(SACSRequest params) throws Exception {
		String url = SACS_REST_API_URL + "/Scripting/ServicePorts?phoneNumber=" + params.phoneNumber;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(request);
		ResponseEntity<String> response = RESTTemplate.request(request, String.class);
		String jsonResponse = response.getBody();
		List<ServicePortVM> list = JSON.toList(jsonResponse, ServicePortVM.class);
		return list;
	}

	public GponConditionVM getGponCondition(SACSRequest params) throws Exception {
		String url = SACS_REST_API_URL + "/Scripting/GponCondition?phoneNumber=" + params.phoneNumber;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(request);
		ResponseEntity<GponConditionVM> response = RESTTemplate.request(request, GponConditionVM.class);
		GponConditionVM condition = response.getBody();
		return condition;
	}

	public PortStatusVM getPortStatus(SACSRequest params) throws Exception {
		String url = SACS_REST_API_URL + "/Scripting/PortStatus?phoneNumber=" + params.phoneNumber;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(request);
		ResponseEntity<PortStatusVM> response = RESTTemplate.request(request, PortStatusVM.class);
		PortStatusVM condition = response.getBody();
		return condition;
	}

	public PortConfigurationVM getPortConfiguration(SACSRequest params) throws Exception {
		String url = SACS_REST_API_URL + "/Scripting/PortConfiguration?phoneNumber=" + params.phoneNumber;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(request);
		ResponseEntity<PortConfigurationVM> response = RESTTemplate.request(request, PortConfigurationVM.class);
		PortConfigurationVM condition = response.getBody();
		return condition;
	}

	public PortConditionVM getPortCondition(SACSRequest params) throws Exception {
		String url = SACS_REST_API_URL + "/Scripting/PortCondition?phoneNumber=" + params.phoneNumber;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(request);
		ResponseEntity<PortConditionVM> response = RESTTemplate.request(request, PortConditionVM.class);
		PortConditionVM condition = response.getBody();
		return condition;
	}

	public IPTVInfoVM getIPTVInfo(SACSRequest params) throws Exception {
		String url = SACS_REST_API_URL + "/Scripting/IPTVInfo?phoneNumber=" + params.phoneNumber;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(request);
		ResponseEntity<IPTVInfoVM> response = RESTTemplate.request(request, IPTVInfoVM.class);
		IPTVInfoVM condition = response.getBody();
		return condition;
	}

	public PortConfigurationInfoVM getPortConfigurationInfo(SACSRequest params) throws Exception {
		String url = SACS_REST_API_URL + "/Scripting/PortConfigurationInfo?phoneNumber=" + params.phoneNumber;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(request);
		ResponseEntity<PortConfigurationInfoVM> response = RESTTemplate.request(request, PortConfigurationInfoVM.class);
		PortConfigurationInfoVM condition = response.getBody();
		return condition;
	}

	static public class LoginSACS {

		public String Username;
		public String Password;

		public LoginSACS() {
		}

		public LoginSACS(String Username, String Password) {
			this.Username = Username;
			this.Password = Password;
		}
	}

	static public class GenericResponseErrorSACS {

		public int Code;
		public String Message;

		public GenericResponseErrorSACS() {
		}
		
	}

	static public class AuthenticateResponse {

		public boolean Succeeded;
		public String Message;
		public AuthenticateValueResponse Value;
		public List<GenericResponseErrorSACS> Errors;

		public AuthenticateResponse() {
		}
		
	}

	static public class AuthenticateValueResponse {

		public String Token;
		public String RefreshToken;
		public Double ExpireTime;

		public AuthenticateValueResponse() {
		}
		
	}

	static public class SACSRequest {

		public long phoneNumber;

		public SACSRequest() {
		}

		public SACSRequest(long phoneNumber) {
			this.phoneNumber = phoneNumber;
		}

	}

	static public class CommandResponse {

		public boolean Succeeded;
		public String Message;
		public List<GenericResponseErrorSACS> Errors;

		public CommandResponse() {
		}
		
	}

	static public class RefreshValueResponse {

		public String Token;
		public String RefreshToken;

		public RefreshValueResponse() {
		}
		
	}

	static public class ServicePortVM {

		public int ID;
		public int ID_GPON_INFO;
		public String Name; // ADSL, IPTV, VOIP
		public int Index;
		public int Vlan_Id;
		public int Flow_Para;
		public int TX;
		public int RX;
		public String Inbound_Table_Name;
		public String Outbound_Table_Name;
		public boolean Admin_Status;
		public boolean State;
		public String Description;
		public String Service_Type;
		public String Connection_Type;
		public String Connection_Status;
		public String IP_Access_Type;
		public String IP;
		public String Subnet_Mask;
		public String Default_Gateway;
		public int Manage_VLAN;
		public int Manage_Priority;
		public String VLAN;
		public String ONTMACAddress;

		public ServicePortVM() {
		}

	}

	static public class GponConditionVM {

		public boolean ONTAdmState;
		public boolean ONTOperState;
		public String ONTSerialNumber;
		public double Velocidad_Configurada_UP;
		public double Velocidad_Configurada_DN;
		public double ONTTxPower;
		public double ONTRxPower;
		public double OLTRxONTOpticalPower;
		public int ONTBiasCurrent;
		public int ONTTemp;
		public double ONTVoltage;
		public double OLTPortTxPower;
		public int ONTDistancia;
		public String ONTLastDownCause;
		public String ONTLastDownTime;
		public String ONTOnlineDuration;
		public String OLTPassword;
		public String ONTUpTime;
		public String ONTDescription;
		public String ONTLastDown;
		public String ONTMemUsage;
		public String ONTLineProfile;
		public String ONTVendor;
		public String ONTVersion;
		public String OntProductDescription;
		public String ONTSoftwareVersion;
		public String ONTBatteryStatus;
		public String ONTPassword;

		public GponConditionVM() {
		}

	}

	static public class PortStatusVM {

		public boolean Condicion_Administrativa;
		public boolean Condicion_Operativa;
		public String Rinit_ID;
		public boolean Rinit_ID_Indicator;

		public PortStatusVM() {
		}

	}

	static public class PortConfigurationVM {

		public String calidadServicio;
		public boolean pvc35;
		public boolean pvc33;
		public String vlanId;
		public String velocidadContratadaDN;
		public String velocidadContratadaUP;

		public PortConfigurationVM() {
		}

	}

	static public class PortConditionVM {

		public String telefono;
		public String Serial_Modem;
		public String Distancia_SVDG;
		public int Velocidad_Actual_DN;
		public int Velocidad_Actual_UP;
		public int Maxima_Velocidad_Bajada;
		public int Maxima_Velocidad_Subida;
		public boolean Ruido;
		public boolean Ruido_Bajada;
		public int Ruido_DN;
		public boolean Ruido_Subida;
		public int Ruido_UP;
		public boolean Atenuacion_Bajada;
		public int Atenuacion_DN;
		public boolean Atenuacion_Subida;
		public int Atenuacion_UP;
		public boolean Atenuacion_Bajada_Loop;
		public int Atenuacion_Loop_DN;
		public boolean Atenuacion_Subida_Loop;
		public int Atenuacion_Loop_UP;
		public boolean Desbalance;
		public boolean MargenEstabilidad;
		public String Nombre_Profile;
		public int Numero_Profile;
		public String Ultimo_Sincronismo;
		public String Ultima_Bajada;

		public PortConditionVM() {
		}

	}

	static public class RadiusVM {

		public Date FechaInicio;
		public Date FechaFin;
		public String Tiempo;
		public String Motivo;
		public String UserName;
		public String IP;
		public String Bras;
		public String TraficoDn;
		public String TraficoUp;

		public RadiusVM() {
		}

	}

	static public class IPTVInfoVM {

		public String Session;

		public IPTVInfoVM() {
		}

	}

	static public class PortConfigurationInfoVM {

		public String Calidad_Servicio;
		public boolean PVC_35;
		public boolean PVC_33;
		public String VLAN_id;
		public int Velocidad_Contratada_DN;
		public int Velocidad_Contratada_UP;
		public int Velocidad_Configurada_DN;
		public int Velocidad_Configurada_UP;
		public boolean Enabled;
		public String Availability;
		public String Tipo_Tarjeta;

		public PortConfigurationInfoVM() {
		}

	}

	static public class FacilityVM {

		public int ID;
		public String NODE_ADDRESS;
		public String IP_ADDRESS;
		public String NODE_NAME;
		public String EQUIPMENT_NAME;
		public long CIRCUIT_DESIGN_ID;
		public String VENDOR_NAME;
		public String TYPE;
		public String CIRCUIT_ID;
		public String CLLI_CODE;
		public String STATUS;
		public String TELEFONO;
		public String TIPO;
		public String REFERENCIA;
		public int FACILITY_COUNT;
		public String PUERTO;
		public boolean isCobre;

		public FacilityVM() {
		}

	}

	static public class DslamInfoVM {

		public int ID;
		public int ID_FACILIDADES;
		public FacilityVM Facilida;
		public String Ultimo_Sincronismo;
		public String Ultima_Bajada;
		public int Distacia_SVDG;
		public boolean Condicion_Operativa;
		public boolean Condicion_Administrativa;
		public double Ruido_Bajada;
		public double Ruido_Subida;
		public double Senal_Atenuacion_Bajada;
		public double Senal_Atenuacion_Subida;
		public double Loop_Atenuacion_Bajada;
		public double Loop_Atenuacion_Subida;
		public String Nombre_Profile;
		public int Numero_Profile;
		public String Calidad_Servicio;
		public int Velocidad_ADSL;
		public int Maxima_Velocidad_Bajada;
		public int Maxima_Velocidad_Subida;
		public int Velocidad_Contratada_DN;
		public int Velocidad_Contratada_UP;
		public int Velocidad_Actual_DN;
		public int Velocidad_Actual_UP;
		public int Velocidad_Configurada_DN;
		public int Velocidad_Configurada_UP;
		public int Velocidad_Puerto_DN;
		public int Velocidad_Puerto_UP;
		public boolean Linea_Defectuosa;
		public String Rinit_ID;
		public String Feeder;
		public String Cabina;
		public String VLAN_id;
		public String MAC_Address;
		public String Session_Id;
		public String Session_Id_IPTV;
		public String Slot;
		public String Actual_Type;
		public boolean Enabled;
		public String Error_Status;
		public String Availability;
		public String Restrt_Cnt;
		public boolean PVC_35;
		public boolean PVC_33;
		public String CustomerID;
		public String User;
		public String Tipo_Tarjeta;
		public boolean VLAN_IPTV;
		public String Serial_Modem;
		public String distancia;
		public Object DiagnosticoCobre;
		public boolean dividirPorMillar;
		public int cantidadAlarmas;
		public String Tipo_Rack;
		public String Fibra_T1;
		public String Ip;

		public DslamInfoVM() {
		}

	}

	static public class GponInfoVM {

		public int ID;
		public int ID_FACILIDADES;
		public FacilityVM Facilida;
		public boolean OLTAdmState;
		public boolean OLTOperState;
		public boolean ONTAdmState;
		public boolean ONTOperState;
		public double OLTPortTxPower;
		public double OLTPortRxPower;
		public String OLTUptime;
		public String OLTFirmware;
		public double ONTRxPower;
		public double ONTTxPower;
		public int ONTBiasCurrent;
		public int ONTTemp;
		public double ONTVoltage;
		public int ONTDistancia;
		public String ONTMemUsage;
		public String ONTCPUUsage;
		public int DataUsageDS;
		public int DataUsageUS;
		public String ONTLastDown;
		public String ONTLastDownCause;
		public String ONTUpTime;
		public String CurrentThroughput;
		public String ONTDescription;
		public String ONTSerialNumber;
		public String ONTLineProfile;
		public String ONTVendor;
		public String ONTVersion;
		public String ONTSoftwareVersion;
		public String OntProductDescription;
		public String ONTBatteryStatus;
		public int Velocidad_Contratada_DN;
		public int Velocidad_Contratada_UP;
		public double Velocidad_Configurada_DN;
		public double Velocidad_Configurada_UP;
		public String OLTPassword;
		public List<ServicePortVM> Service_Port;
		public double OLTRxONTOpticalPower;
		public String ONTOnlineDuration;
		public String ONTLastDownTime;
		public DiagnosticoGponVM DiagnosticoGpon;
		public FacilityVM facilidades;

		public GponInfoVM() {
		}

	}

	static public class DiagnosticoGponVM {

		public int ID;
		public int ID_FACILIDADES;
		public boolean OLTAdmState;
		public boolean OLTOperState;
		public boolean ONTAdmState;
		public boolean ONTOperState;
		public boolean ONTRxPower;
		public boolean ONTTxPower;
		public boolean ONTBiasCurrent;
		public boolean ONTTemp;
		public boolean ONTVoltage;
		public boolean OLTRxONTOpticalPower;
		public boolean VelConfigurada;
		public boolean VelConfiguradaUP;
		public boolean VelConfiguradaDN;
		public boolean Password;
		public boolean IPTV;
		public boolean ADSL;
		public boolean VOIP;
		public boolean IPTV_Tx_Rx;
		public boolean IPTV_ServicePort_Status;
		public boolean ADSL_ServicePort_Status;
		public boolean VOIP_ServicePort_Status;
		public boolean IP_IPTV;
		public boolean IP_ADSL;
		public boolean IP_VOIP;
		public boolean TestResult;

		public DiagnosticoGponVM() {
		}

	}

	public class DiagnosticoVM {

		public int ID;
		public int ID_FACILIDADES;
		public boolean CondAdministrativa;
		public boolean CondOperativa;
		public boolean Atenuacion;
		public boolean Atenuacion_Subida;
		public boolean Atenuacion_Bajada;
		public boolean Atenuacion_Subida_Loop;
		public boolean Atenuacion_Bajada_Loop;
		public boolean Desbalance;
		public boolean Velocidad;
		public boolean Service_Profile;
		public boolean Configuracion;
		public boolean Ruido;
		public boolean Ruido_Bajada;
		public boolean Ruido_Subida;
		public boolean PVC35;
		public boolean PVC33;
		public boolean AutenticadorIPTV;
		public boolean IP;
		public boolean IP_VIDEO;
		public boolean VLAN;
		public boolean VLAN_IPTV;
		public boolean TestResult;
		public boolean MargenEstabilidad;

		public DiagnosticoVM() {
		}

	}

	static public class DiagnosticoResultVM {

		public int codigoPrueba;
		public String messege;
		public String testResult;
		public FacilityVM facilidades;
		public DslamInfoVM dslamInfo;
		public DiagnosticoVM diagnostico;
		public GponInfoVM gponInfo;
		public DiagnosticoGponVM diagnosticoGpon;

		public DiagnosticoResultVM() {
		}

	}
}
