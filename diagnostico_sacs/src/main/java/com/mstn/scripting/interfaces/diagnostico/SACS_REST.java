/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.diagnostico;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;

/**
 * Clase que implementa métodos del web service de Diagnóstico IDW para ser
 * usados en la interfaz estática {@link DW_DiagnosticResult}.
 *
 * @author amatos
 */
public class SACS_REST extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = SACS_REST.class;
		try {
			SOAP = new SACS_RESTApi();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public SACS_REST() {
		super(0, "sacsrest", SOAP);
		IGNORED_METHODS.add("setToken");
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("sacs_rest_api"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("_phoneNumber")) {
			field.setLabel("Número de Suscripción");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_PHONE);
		}
		return field;
	}

//	@Override
//	protected Object getParamValueFromForm(Class paramType, String paramName) throws Exception {
//		if (paramName.endsWith("_user") || paramName.endsWith("_arg0")) {
//			return user.getId();
//		}
//		return super.getParamValueFromForm(paramType, paramName);
//	}
//	@Override
//	protected List<Interface_Field> getStepFields() throws Exception {
//		List<Interface_Field> stepFields = super.getStepFields().stream()
//				.filter(i -> i.getName().endsWith("_user") || i.getName().endsWith("_arg0"))
//				.collect(Collectors.toList());
//		return stepFields;
//	}
	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("dth_rest_api").toString();
		InterfaceConfigs.get("dth_rest_systemname").toString();
		InterfaceConfigs.get("dth_rest_systempassword").toString();
		super.test();
	}

}
