/**
 * Este proyecto contiene la interfaz que conecta con Diagnostico SACS.
 * Esta interfaz agrega a Scripting la posibilidad de que los usuarios puedan 
 * obtener información de diagnóstico dentro de una transacción en curso.
 */
