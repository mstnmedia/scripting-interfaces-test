/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.omsorderdetails.OMSOrderDetails;
import java.util.List;

/**
 * Clase ejecutable con métodos de pruebas para probar las diferentes
 * funcionalidades en este proyecto.
 *
 * @author amatos
 */
public class Main {

	static TransactionInterfacesPayload payload = new TransactionInterfacesPayload(
			new V_Transaction(), new Workflow_Step(), "{}"
	);
	static String numeroOS = "113540466";

	static Object testOMSOrderDetails() throws Exception {
		OMSOrderDetails service = new OMSOrderDetails();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces.stream().filter(i
				-> i.getName().endsWith("omsorderdetails_getDetailsByTypeServiceAndServiceId")
		//		-> i.getName().endsWith("omsorderdetails_getDetailsOrderId")
		).findFirst().get();
		String form = JSON.newObjectNode()
				.put("omsorderdetails_getDetailsByTypeServiceAndServiceId_arg0", "8092211534")
				.put("omsorderdetails_getDetailsByTypeServiceAndServiceId_arg1", "IPTV")
				.put("test", "")
				.toString();
		payload.setForm(form);
		return service.callMethod(inter, payload, User.SYSTEM);
	}

	/**
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		InterfaceConfigs.setConfig();
		Object result = null;
		result = testOMSOrderDetails();
		System.out.println(JSON.toString(result));
	}

}
