
package com.claro.omsorderdetails;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import net.java.dev.jaxb.array.StringArray;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.3-b02-
 * Generated source version: 2.1
 * 
 */
@WebService(name = "OmsOrderDetails", targetNamespace = "http://omsorderdetails.claro.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@XmlSeeAlso({
    net.java.dev.jaxb.array.ObjectFactory.class,
    com.claro.omsorderdetails.ObjectFactory.class
})
public interface OmsOrderDetails {


    /**
     * 
     * @param orderId
     * @return
     *     returns com.claro.omsorderdetails.ListOfOrderActionsDetailsModel
     */
    @WebMethod
    @WebResult(name = "orderActions", partName = "orderActions")
    public ListOfOrderActionsDetailsModel getDetailsOrderId(
        @WebParam(name = "OrderId", partName = "OrderId")
        String orderId);

    /**
     * 
     * @param customerId
     * @return
     *     returns com.claro.omsorderdetails.CountListModelV1
     */
    @WebMethod
    @WebResult(name = "OrdenesPendientes", partName = "OrdenesPendientes")
    public CountListModelV1 getOrdersByCustomerId(
        @WebParam(name = "CustomerId", partName = "CustomerId")
        String customerId);

    /**
     * 
     * @param serviceType
     * @param customerId
     * @param serviceId
     * @param status
     * @return
     *     returns com.claro.omsorderdetails.CountListModel
     */
    @WebMethod
    @WebResult(partName = "return")
    public CountListModel getDetailsByCustomerId(
        @WebParam(name = "CustomerId", partName = "CustomerId")
        String customerId,
        @WebParam(name = "ServiceId", partName = "ServiceId")
        StringArray serviceId,
        @WebParam(name = "ServiceType", partName = "ServiceType")
        StringArray serviceType,
        @WebParam(name = "Status", partName = "Status")
        StringArray status);

    /**
     * 
     * @param serviceType
     * @param serviceId
     * @return
     *     returns com.claro.omsorderdetails.ListOfDynamicProductDetailsModel
     */
    @WebMethod
    @WebResult(name = "AssignedProduct", partName = "AssignedProduct")
    public ListOfDynamicProductDetailsModel getDetailsByTypeServiceAndServiceId(
        @WebParam(name = "ServiceId", partName = "ServiceId")
        String serviceId,
        @WebParam(name = "ServiceType", partName = "ServiceType")
        String serviceType);

}
