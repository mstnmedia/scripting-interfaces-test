
package com.claro.omsorderdetails;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.claro.omsorderdetails package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.claro.omsorderdetails
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListOfDynamicProductDetailsModel.Product }
     * 
     */
    public ListOfDynamicProductDetailsModel.Product createListOfDynamicProductDetailsModelProduct() {
        return new ListOfDynamicProductDetailsModel.Product();
    }

    /**
     * Create an instance of {@link ComponentModel }
     * 
     */
    public ComponentModel createComponentModel() {
        return new ComponentModel();
    }

    /**
     * Create an instance of {@link ListOfOrderActionsDetailsModel }
     * 
     */
    public ListOfOrderActionsDetailsModel createListOfOrderActionsDetailsModel() {
        return new ListOfOrderActionsDetailsModel();
    }

    /**
     * Create an instance of {@link CustomerIdModel }
     * 
     */
    public CustomerIdModel createCustomerIdModel() {
        return new CustomerIdModel();
    }

    /**
     * Create an instance of {@link CountListModel }
     * 
     */
    public CountListModel createCountListModel() {
        return new CountListModel();
    }

    /**
     * Create an instance of {@link AttributesModel }
     * 
     */
    public AttributesModel createAttributesModel() {
        return new AttributesModel();
    }

    /**
     * Create an instance of {@link OrderSonModel }
     * 
     */
    public OrderSonModel createOrderSonModel() {
        return new OrderSonModel();
    }

    /**
     * Create an instance of {@link CountListModelV1 }
     * 
     */
    public CountListModelV1 createCountListModelV1() {
        return new CountListModelV1();
    }

    /**
     * Create an instance of {@link OrderActionsDetailsModel }
     * 
     */
    public OrderActionsDetailsModel createOrderActionsDetailsModel() {
        return new OrderActionsDetailsModel();
    }

    /**
     * Create an instance of {@link DynamicProductDetailsModel }
     * 
     */
    public DynamicProductDetailsModel createDynamicProductDetailsModel() {
        return new DynamicProductDetailsModel();
    }

    /**
     * Create an instance of {@link ListOfDynamicProductDetailsModel }
     * 
     */
    public ListOfDynamicProductDetailsModel createListOfDynamicProductDetailsModel() {
        return new ListOfDynamicProductDetailsModel();
    }

    /**
     * Create an instance of {@link ListOfObject }
     * 
     */
    public ListOfObject createListOfObject() {
        return new ListOfObject();
    }

    /**
     * Create an instance of {@link ListOfOrderActionsDetailsModel.OrderAction }
     * 
     */
    public ListOfOrderActionsDetailsModel.OrderAction createListOfOrderActionsDetailsModelOrderAction() {
        return new ListOfOrderActionsDetailsModel.OrderAction();
    }

    /**
     * Create an instance of {@link OrderMotherDetailsModel }
     * 
     */
    public OrderMotherDetailsModel createOrderMotherDetailsModel() {
        return new OrderMotherDetailsModel();
    }

}
