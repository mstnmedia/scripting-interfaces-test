/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.omsorderdetails;

import com.claro.omsorderdetails.CountListModel;
import com.claro.omsorderdetails.CountListModelV1;
import com.claro.omsorderdetails.ListOfDynamicProductDetailsModel;
import com.claro.omsorderdetails.ListOfOrderActionsDetailsModel;
import com.claro.omsorderdetails.OmsOrderDetails;
import com.claro.omsorderdetails.OrderDetailsOmsService;
import com.mstn.scripting.core.Utils;
import net.java.dev.jaxb.array.StringArray;

/**
 *
 * @author amatos
 */
public class OmsOrderDetailsSoap implements OmsOrderDetails {

	private final OmsOrderDetails base;

	public OmsOrderDetailsSoap() {
		try {
			this.base = OrderDetailsOmsService.getInstance();
		} catch (Exception ex) {
			Utils.logException(this.getClass(), "Error instanciando endpoint DTH_SAT", ex);
			throw ex;
		}
	}

	@Override
	public ListOfOrderActionsDetailsModel getDetailsOrderId(String orderId) {
		return base.getDetailsOrderId(orderId);
	}

	@Override
	public CountListModelV1 getOrdersByCustomerId(String customerId) {
		return base.getOrdersByCustomerId(customerId);
	}

	@Override
	public CountListModel getDetailsByCustomerId(String customerId, StringArray serviceId, StringArray serviceType, StringArray status) {
		return base.getDetailsByCustomerId(customerId, serviceId, serviceType, status);
	}

	@Override
	public ListOfDynamicProductDetailsModel getDetailsByTypeServiceAndServiceId(String serviceId, String serviceType) {
		return base.getDetailsByTypeServiceAndServiceId(serviceId, serviceType);
	}

}
