/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.omsorderdetails;

import com.claro.omsorderdetails.OmsOrderDetails;
import com.claro.omsorderdetails.OrderDetailsOmsService;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;

/**
 * Interfaz dinámica que obtiene el detalle de una orden.
 *
 * @author amatos
 */
public class OMSOrderDetails extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = OMSOrderDetails.class;
		try {
			SOAP = OrderDetailsOmsService.getInstance();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public OMSOrderDetails() {
		super(0, "omsorderdetails", SOAP);
		showBaseLogs = false;
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("oms_orderdetails"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if ("omsorderdetails_getDetailsByTypeServiceAndServiceId".equals(inter.getName())) {
			if (fieldName.endsWith("_arg0")) {
				field.setLabel("ID de Servicio");
				field.setId_type(InterfaceFieldTypes.NUMBER);
				field.setOnvalidate(InterfaceFieldTypes.VALIDATE_PHONE);
			} else if (fieldName.endsWith("_arg1")) {
				field.setLabel("Tipo de Servicio");
			}
		} else if ("omsorderdetails_getDetailsOrderId".equals(inter.getName())) {
			if (fieldName.endsWith("_arg0")) {
				field.setLabel("ID de la Orden");
			}
		} else if ("omsorderdetails_getDetailsByCustomerId".equals(inter.getName())) {
			if (fieldName.endsWith("_arg0")) {
				field.setLabel("CCU");
				field.setId_type(InterfaceFieldTypes.NUMBER);
			} else if (fieldName.endsWith("_arg1_item")) {
				field.setLabel("ID de Servicio");
				field.setId_type(InterfaceFieldTypes.NUMBER);
				field.setOnvalidate(InterfaceFieldTypes.VALIDATE_PHONE);
				field.setRequired(false);
			} else if (fieldName.endsWith("item") && parentName.endsWith("_arg1_item")) {
				field.setLabel("Valores");
			} else if (fieldName.endsWith("_arg2_item")) {
				field.setLabel("Tipo de Servicio");
				field.setRequired(false);
			} else if (fieldName.endsWith("item") && parentName.endsWith("_arg2_item")) {
				field.setLabel("Valores");
			} else if (fieldName.endsWith("_arg3_item")) {
				field.setLabel("Estado");
				field.setRequired(false);
			} else if (fieldName.endsWith("item") && parentName.endsWith("_arg3_item")) {
				field.setLabel("Valores");
			}
		} else if ("omsorderdetails_getOrdersByCustomerId".equals(inter.getName())) {
			if (fieldName.endsWith("_arg0")) {
				field.setLabel("CCU");
				field.setId_type(InterfaceFieldTypes.NUMBER);
			}
		}
		return field;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("oms_orderdetails").toString();
		super.test();
	}

}
