
package com.claro.api.sad.service.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for perpendicular complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="perpendicular">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="calleReferencia" type="{http://core.service.sad.api.claro.com/}calle" minOccurs="0"/>
 *         &lt;element name="idt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="perpendicular1" type="{http://core.service.sad.api.claro.com/}calle" minOccurs="0"/>
 *         &lt;element name="perpendicular2" type="{http://core.service.sad.api.claro.com/}calle" minOccurs="0"/>
 *         &lt;element name="rangoFin" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="rangoIni" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "perpendicular", propOrder = {
    "calleReferencia",
    "idt",
    "perpendicular1",
    "perpendicular2",
    "rangoFin",
    "rangoIni"
})
public class Perpendicular {

    protected Calle calleReferencia;
    @XmlElement(namespace = "")
    protected Long idt;
    protected Calle perpendicular1;
    protected Calle perpendicular2;
    @XmlElement(namespace = "")
    protected int rangoFin;
    @XmlElement(namespace = "")
    protected int rangoIni;

    /**
     * Gets the value of the calleReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Calle }
     *     
     */
    public Calle getCalleReferencia() {
        return calleReferencia;
    }

    /**
     * Sets the value of the calleReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Calle }
     *     
     */
    public void setCalleReferencia(Calle value) {
        this.calleReferencia = value;
    }

    /**
     * Gets the value of the idt property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdt() {
        return idt;
    }

    /**
     * Sets the value of the idt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdt(Long value) {
        this.idt = value;
    }

    /**
     * Gets the value of the perpendicular1 property.
     * 
     * @return
     *     possible object is
     *     {@link Calle }
     *     
     */
    public Calle getPerpendicular1() {
        return perpendicular1;
    }

    /**
     * Sets the value of the perpendicular1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Calle }
     *     
     */
    public void setPerpendicular1(Calle value) {
        this.perpendicular1 = value;
    }

    /**
     * Gets the value of the perpendicular2 property.
     * 
     * @return
     *     possible object is
     *     {@link Calle }
     *     
     */
    public Calle getPerpendicular2() {
        return perpendicular2;
    }

    /**
     * Sets the value of the perpendicular2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Calle }
     *     
     */
    public void setPerpendicular2(Calle value) {
        this.perpendicular2 = value;
    }

    /**
     * Gets the value of the rangoFin property.
     * 
     */
    public int getRangoFin() {
        return rangoFin;
    }

    /**
     * Sets the value of the rangoFin property.
     * 
     */
    public void setRangoFin(int value) {
        this.rangoFin = value;
    }

    /**
     * Gets the value of the rangoIni property.
     * 
     */
    public int getRangoIni() {
        return rangoIni;
    }

    /**
     * Sets the value of the rangoIni property.
     * 
     */
    public void setRangoIni(int value) {
        this.rangoIni = value;
    }

}
