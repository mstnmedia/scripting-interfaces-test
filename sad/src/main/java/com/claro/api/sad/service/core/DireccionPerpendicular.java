
package com.claro.api.sad.service.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for direccionPerpendicular complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="direccionPerpendicular">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="perpendicular" type="{http://core.service.sad.api.claro.com/}perpendicular" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "direccionPerpendicular", propOrder = {
    "idt",
    "perpendicular"
})
public class DireccionPerpendicular {

    @XmlElement(namespace = "")
    protected Long idt;
    protected Perpendicular perpendicular;

    /**
     * Gets the value of the idt property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdt() {
        return idt;
    }

    /**
     * Sets the value of the idt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdt(Long value) {
        this.idt = value;
    }

    /**
     * Gets the value of the perpendicular property.
     * 
     * @return
     *     possible object is
     *     {@link Perpendicular }
     *     
     */
    public Perpendicular getPerpendicular() {
        return perpendicular;
    }

    /**
     * Sets the value of the perpendicular property.
     * 
     * @param value
     *     allowed object is
     *     {@link Perpendicular }
     *     
     */
    public void setPerpendicular(Perpendicular value) {
        this.perpendicular = value;
    }

}
