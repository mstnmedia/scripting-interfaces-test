
package com.claro.api.sad.service.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for distrito complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="distrito">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ciudad" type="{http://core.service.sad.api.claro.com/}ciudad" minOccurs="0"/>
 *         &lt;element name="codigoDistrito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idtCiudad" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idtSector" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="sector" type="{http://core.service.sad.api.claro.com/}sector" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "distrito", propOrder = {
    "ciudad",
    "codigoDistrito",
    "idtCiudad",
    "idtSector",
    "sector"
})
public class Distrito {

    protected Ciudad ciudad;
    @XmlElement(namespace = "")
    protected String codigoDistrito;
    @XmlElement(namespace = "")
    protected Long idtCiudad;
    @XmlElement(namespace = "")
    protected Long idtSector;
    protected Sector sector;

    /**
     * Gets the value of the ciudad property.
     * 
     * @return
     *     possible object is
     *     {@link Ciudad }
     *     
     */
    public Ciudad getCiudad() {
        return ciudad;
    }

    /**
     * Sets the value of the ciudad property.
     * 
     * @param value
     *     allowed object is
     *     {@link Ciudad }
     *     
     */
    public void setCiudad(Ciudad value) {
        this.ciudad = value;
    }

    /**
     * Gets the value of the codigoDistrito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDistrito() {
        return codigoDistrito;
    }

    /**
     * Sets the value of the codigoDistrito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDistrito(String value) {
        this.codigoDistrito = value;
    }

    /**
     * Gets the value of the idtCiudad property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdtCiudad() {
        return idtCiudad;
    }

    /**
     * Sets the value of the idtCiudad property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdtCiudad(Long value) {
        this.idtCiudad = value;
    }

    /**
     * Gets the value of the idtSector property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdtSector() {
        return idtSector;
    }

    /**
     * Sets the value of the idtSector property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdtSector(Long value) {
        this.idtSector = value;
    }

    /**
     * Gets the value of the sector property.
     * 
     * @return
     *     possible object is
     *     {@link Sector }
     *     
     */
    public Sector getSector() {
        return sector;
    }

    /**
     * Sets the value of the sector property.
     * 
     * @param value
     *     allowed object is
     *     {@link Sector }
     *     
     */
    public void setSector(Sector value) {
        this.sector = value;
    }

}
