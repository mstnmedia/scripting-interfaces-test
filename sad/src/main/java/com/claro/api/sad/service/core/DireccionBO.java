
package com.claro.api.sad.service.core;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for direccionBO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="direccionBO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="prefijo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="posfijo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="actualizado" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="calle" type="{http://core.service.sad.api.claro.com/}calle" minOccurs="0"/>
 *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="perpendicular" type="{http://core.service.sad.api.claro.com/}direccionPerpendicular" minOccurs="0"/>
 *         &lt;element name="callePerpendicular1" type="{http://core.service.sad.api.claro.com/}calle" minOccurs="0"/>
 *         &lt;element name="callePerpendicular2" type="{http://core.service.sad.api.claro.com/}calle" minOccurs="0"/>
 *         &lt;element name="latitud" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="longitud" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="coordenadasValidas" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="nivelList" type="{http://core.service.sad.api.claro.com/}nivel" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "direccionBO", propOrder = {
    "idt",
    "prefijo",
    "numero",
    "posfijo",
    "actualizado",
    "calle",
    "referencia",
    "perpendicular",
    "callePerpendicular1",
    "callePerpendicular2",
    "latitud",
    "longitud",
    "coordenadasValidas",
    "nivelList"
})
public class DireccionBO {

    @XmlElement(namespace = "")
    protected Long idt;
    @XmlElement(namespace = "")
    protected String prefijo;
    @XmlElement(namespace = "")
    protected BigInteger numero;
    @XmlElement(namespace = "")
    protected String posfijo;
    @XmlElement(namespace = "")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar actualizado;
    @XmlElement(namespace = "")
    protected Calle calle;
    @XmlElement(namespace = "")
    protected String referencia;
    @XmlElement(namespace = "")
    protected DireccionPerpendicular perpendicular;
    @XmlElement(namespace = "")
    protected Calle callePerpendicular1;
    @XmlElement(namespace = "")
    protected Calle callePerpendicular2;
    @XmlElement(namespace = "")
    protected BigDecimal latitud;
    @XmlElement(namespace = "")
    protected BigDecimal longitud;
    @XmlElement(namespace = "")
    protected Boolean coordenadasValidas;
    @XmlElement(namespace = "")
    protected List<Nivel> nivelList;

    /**
     * Gets the value of the idt property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdt() {
        return idt;
    }

    /**
     * Sets the value of the idt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdt(Long value) {
        this.idt = value;
    }

    /**
     * Gets the value of the prefijo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrefijo() {
        return prefijo;
    }

    /**
     * Sets the value of the prefijo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrefijo(String value) {
        this.prefijo = value;
    }

    /**
     * Gets the value of the numero property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumero() {
        return numero;
    }

    /**
     * Sets the value of the numero property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumero(BigInteger value) {
        this.numero = value;
    }

    /**
     * Gets the value of the posfijo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosfijo() {
        return posfijo;
    }

    /**
     * Sets the value of the posfijo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosfijo(String value) {
        this.posfijo = value;
    }

    /**
     * Gets the value of the actualizado property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActualizado() {
        return actualizado;
    }

    /**
     * Sets the value of the actualizado property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActualizado(XMLGregorianCalendar value) {
        this.actualizado = value;
    }

    /**
     * Gets the value of the calle property.
     * 
     * @return
     *     possible object is
     *     {@link Calle }
     *     
     */
    public Calle getCalle() {
        return calle;
    }

    /**
     * Sets the value of the calle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Calle }
     *     
     */
    public void setCalle(Calle value) {
        this.calle = value;
    }

    /**
     * Gets the value of the referencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * Sets the value of the referencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencia(String value) {
        this.referencia = value;
    }

    /**
     * Gets the value of the perpendicular property.
     * 
     * @return
     *     possible object is
     *     {@link DireccionPerpendicular }
     *     
     */
    public DireccionPerpendicular getPerpendicular() {
        return perpendicular;
    }

    /**
     * Sets the value of the perpendicular property.
     * 
     * @param value
     *     allowed object is
     *     {@link DireccionPerpendicular }
     *     
     */
    public void setPerpendicular(DireccionPerpendicular value) {
        this.perpendicular = value;
    }

    /**
     * Gets the value of the callePerpendicular1 property.
     * 
     * @return
     *     possible object is
     *     {@link Calle }
     *     
     */
    public Calle getCallePerpendicular1() {
        return callePerpendicular1;
    }

    /**
     * Sets the value of the callePerpendicular1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Calle }
     *     
     */
    public void setCallePerpendicular1(Calle value) {
        this.callePerpendicular1 = value;
    }

    /**
     * Gets the value of the callePerpendicular2 property.
     * 
     * @return
     *     possible object is
     *     {@link Calle }
     *     
     */
    public Calle getCallePerpendicular2() {
        return callePerpendicular2;
    }

    /**
     * Sets the value of the callePerpendicular2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Calle }
     *     
     */
    public void setCallePerpendicular2(Calle value) {
        this.callePerpendicular2 = value;
    }

    /**
     * Gets the value of the latitud property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLatitud() {
        return latitud;
    }

    /**
     * Sets the value of the latitud property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLatitud(BigDecimal value) {
        this.latitud = value;
    }

    /**
     * Gets the value of the longitud property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLongitud() {
        return longitud;
    }

    /**
     * Sets the value of the longitud property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLongitud(BigDecimal value) {
        this.longitud = value;
    }

    /**
     * Gets the value of the coordenadasValidas property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCoordenadasValidas() {
        return coordenadasValidas;
    }

    /**
     * Sets the value of the coordenadasValidas property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCoordenadasValidas(Boolean value) {
        this.coordenadasValidas = value;
    }

    /**
     * Gets the value of the nivelList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nivelList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNivelList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Nivel }
     * 
     * 
     */
    public List<Nivel> getNivelList() {
        if (nivelList == null) {
            nivelList = new ArrayList<Nivel>();
        }
        return this.nivelList;
    }

}
