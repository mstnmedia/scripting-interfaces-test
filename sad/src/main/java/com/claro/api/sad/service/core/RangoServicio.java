
package com.claro.api.sad.service.core;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for rangoServicio complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rangoServicio">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actualizado" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="calle" type="{http://core.service.sad.api.claro.com/}calle" minOccurs="0"/>
 *         &lt;element name="idt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idtRangoSistExt" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="modo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nivelcad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nivelesServicio" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nivelServicio" type="{http://core.service.sad.api.claro.com/}nivelServicio" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="posfijo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prefijo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rangoFin" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="rangoIni" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rangoServicio", propOrder = {
    "actualizado",
    "calle",
    "idt",
    "idtRangoSistExt",
    "modo",
    "nivelcad",
    "nivelesServicio",
    "posfijo",
    "prefijo",
    "rangoFin",
    "rangoIni"
})
public class RangoServicio {

    @XmlElement(namespace = "")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar actualizado;
    protected Calle calle;
    @XmlElement(namespace = "")
    protected Long idt;
    @XmlElement(namespace = "")
    protected int idtRangoSistExt;
    @XmlElement(namespace = "")
    protected int modo;
    @XmlElement(namespace = "")
    protected String nivelcad;
    @XmlElement(namespace = "")
    protected RangoServicio.NivelesServicio nivelesServicio;
    @XmlElement(namespace = "")
    protected String posfijo;
    @XmlElement(namespace = "")
    protected String prefijo;
    @XmlElement(namespace = "")
    protected int rangoFin;
    @XmlElement(namespace = "")
    protected int rangoIni;

    /**
     * Gets the value of the actualizado property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActualizado() {
        return actualizado;
    }

    /**
     * Sets the value of the actualizado property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActualizado(XMLGregorianCalendar value) {
        this.actualizado = value;
    }

    /**
     * Gets the value of the calle property.
     * 
     * @return
     *     possible object is
     *     {@link Calle }
     *     
     */
    public Calle getCalle() {
        return calle;
    }

    /**
     * Sets the value of the calle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Calle }
     *     
     */
    public void setCalle(Calle value) {
        this.calle = value;
    }

    /**
     * Gets the value of the idt property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdt() {
        return idt;
    }

    /**
     * Sets the value of the idt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdt(Long value) {
        this.idt = value;
    }

    /**
     * Gets the value of the idtRangoSistExt property.
     * 
     */
    public int getIdtRangoSistExt() {
        return idtRangoSistExt;
    }

    /**
     * Sets the value of the idtRangoSistExt property.
     * 
     */
    public void setIdtRangoSistExt(int value) {
        this.idtRangoSistExt = value;
    }

    /**
     * Gets the value of the modo property.
     * 
     */
    public int getModo() {
        return modo;
    }

    /**
     * Sets the value of the modo property.
     * 
     */
    public void setModo(int value) {
        this.modo = value;
    }

    /**
     * Gets the value of the nivelcad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNivelcad() {
        return nivelcad;
    }

    /**
     * Sets the value of the nivelcad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNivelcad(String value) {
        this.nivelcad = value;
    }

    /**
     * Gets the value of the nivelesServicio property.
     * 
     * @return
     *     possible object is
     *     {@link RangoServicio.NivelesServicio }
     *     
     */
    public RangoServicio.NivelesServicio getNivelesServicio() {
        return nivelesServicio;
    }

    /**
     * Sets the value of the nivelesServicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link RangoServicio.NivelesServicio }
     *     
     */
    public void setNivelesServicio(RangoServicio.NivelesServicio value) {
        this.nivelesServicio = value;
    }

    /**
     * Gets the value of the posfijo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosfijo() {
        return posfijo;
    }

    /**
     * Sets the value of the posfijo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosfijo(String value) {
        this.posfijo = value;
    }

    /**
     * Gets the value of the prefijo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrefijo() {
        return prefijo;
    }

    /**
     * Sets the value of the prefijo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrefijo(String value) {
        this.prefijo = value;
    }

    /**
     * Gets the value of the rangoFin property.
     * 
     */
    public int getRangoFin() {
        return rangoFin;
    }

    /**
     * Sets the value of the rangoFin property.
     * 
     */
    public void setRangoFin(int value) {
        this.rangoFin = value;
    }

    /**
     * Gets the value of the rangoIni property.
     * 
     */
    public int getRangoIni() {
        return rangoIni;
    }

    /**
     * Sets the value of the rangoIni property.
     * 
     */
    public void setRangoIni(int value) {
        this.rangoIni = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nivelServicio" type="{http://core.service.sad.api.claro.com/}nivelServicio" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nivelServicio"
    })
    public static class NivelesServicio {

        protected List<NivelServicio> nivelServicio;

        /**
         * Gets the value of the nivelServicio property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the nivelServicio property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getNivelServicio().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link NivelServicio }
         * 
         * 
         */
        public List<NivelServicio> getNivelServicio() {
            if (nivelServicio == null) {
                nivelServicio = new ArrayList<NivelServicio>();
            }
            return this.nivelServicio;
        }

    }

}
