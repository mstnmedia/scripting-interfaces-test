/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.sad;

import com.claro.api.sad.service.core.DireccionService;
import com.claro.api.sad.service.core.GetDireccionPorIDDResponse;
import com.claro.api.sad.service.core.GetDireccionPorIDTResponse;
import com.claro.api.sad.service.core.GetDireccionPorTNResponse;
//import com.claro.api.sad.service.core.GetDireccionesPorDetalleResponse;
import com.claro.api.sad.service.core.GetDireccionesPorIDTBarrioResponse;
import com.claro.api.sad.service.core.GetDireccionesPorIDTCalleResponse;
import com.claro.api.sad.service.core.GetDireccionesPorIDTCiudadResponse;
import com.claro.api.sad.service.core.GetDireccionesPorIDTSectorResponse;
import com.claro.api.sad.service.core.GetDireccionesPorIDTsResponse;
//import com.claro.api.sad.service.core.ParValue;
import com.claro.api.sad.service.core.V2_002fDireccionService;
import com.mstn.scripting.core.Utils;

/**
 * Clase que envuelve a una instancia del servicio de direcciones de SAD y hace
 * públicos métodos específicos, los mismos que los usuarios tendrán
 * disponibles.
 *
 * @author amatos
 */
public class DireccionServiceImpl {

	private final DireccionService base;

	public DireccionServiceImpl() {
		try {
			this.base = V2_002fDireccionService.getInstance();
		} catch (Exception ex) {
			Utils.logException(this.getClass(), "Error invocando endpoint de SAD", ex);
			throw ex;
		}
	}

	public GetDireccionPorIDTResponse.SADResponse getDireccionPorIDT(String idt) {
		return base.getDireccionPorIDT(idt);
	}

	public GetDireccionesPorIDTCalleResponse.SADResponse getDireccionesPorIDTCalle(String idtCalle) {
		return base.getDireccionesPorIDTCalle(idtCalle);
	}

	public GetDireccionesPorIDTSectorResponse.SADResponse getDireccionesPorIDTSector(String idtSector) {
		return base.getDireccionesPorIDTSector(idtSector);
	}

	public GetDireccionesPorIDTBarrioResponse.SADResponse getDireccionesPorIDTBarrio(String idtBarrio) {
		return base.getDireccionesPorIDTBarrio(idtBarrio);
	}

	public GetDireccionesPorIDTCiudadResponse.SADResponse getDireccionesPorIDTCiudad(String idtCiudad) {
		return base.getDireccionesPorIDTCiudad(idtCiudad);
	}

	public GetDireccionPorIDDResponse.SADResponse getDireccionPorIDD(String idd) {
		return base.getDireccionPorIDD(idd);
	}

	public GetDireccionPorTNResponse.SADResponse getDireccionPorTN(String tn) {
		GetDireccionPorTNResponse.SADResponse result = base.getDireccionPorTN(tn);
		return result;
	}

	public GetDireccionesPorIDTsResponse.SADResponse getDireccionesPorIDTs(String idTs) {
		return base.getDireccionesPorIDTs(idTs);
	}

//	public UpdateDireccionResponse.SADResponse updateDireccion(DireccionBO direccionBO, String idt) {
//		return base.updateDireccion(direccionBO, idt);
//	}
//
//	public GetDireccionesPorDetalleResponse.SADResponse getDireccionesPorDetalle(Long idtCalle, Long numeroInmueble, String posfijo, String prefijo, ListParValue niveles) {
//		return base.getDireccionesPorDetalle(idtCalle, numeroInmueble, posfijo, prefijo, niveles.parValues);
//	}
//	static public class ListParValue {
//
//		public List<ParValue> parValues;
//	}
}
