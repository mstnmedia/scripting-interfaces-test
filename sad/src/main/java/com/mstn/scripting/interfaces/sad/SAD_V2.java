package com.mstn.scripting.interfaces.sad;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import java.lang.reflect.Method;

/**
 * Interfaz que agrega a Scripting la conexión a SAD desde una transacción en
 * curso.
 *
 * @author amatos
 */
public class SAD_V2 extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = SAD_V2.class;
		try {
			SOAP = new DireccionServiceImpl();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public SAD_V2() {
		super(0, "sad", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("sad_v2"));
		return inter;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("sad_v2").toString();
		super.test();
	}
}
