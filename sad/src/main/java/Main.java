/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.claro.api.sad.service.core.GetDireccionPorTNResponse;
import com.claro.api.sad.service.core.GetDireccionesPorIDTCalleResponse;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.sad.DireccionServiceImpl;
import com.mstn.scripting.interfaces.sad.SAD_V2;
import java.util.List;

/**
 * Clase ejecutable con métodos de pruebas para probar las diferentes
 * funcionalidades en este proyecto.
 *
 * @author amatos
 */
public class Main {

	static TransactionInterfacesPayload payload = new TransactionInterfacesPayload(
			new V_Transaction(), new Workflow_Step(), "{}"
	);
	static String tn = "8095302179";

	static Object getDireccionPorTN() throws Exception {
		DireccionServiceImpl soap = new DireccionServiceImpl();
		GetDireccionPorTNResponse.SADResponse result = soap.getDireccionPorTN(tn);
		return result;
	}

	static Object getDireccionesPorIDTCalle() throws Exception {
		DireccionServiceImpl soap = new DireccionServiceImpl();
		GetDireccionesPorIDTCalleResponse.SADResponse result = soap.getDireccionesPorIDTCalle("7707");
		return result;
	}

	static Object testSAD() throws Exception {
		SAD_V2 service = new SAD_V2();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces
				.stream()
				.filter(item -> item.getName().endsWith("consultarClientePorCuenta"))
				.findFirst().get();
		ObjectNode form = JSON.newObjectNode();
		String[] value = new String[]{tn, "6789", "SCRIPTING"};
		for (int i = 0; i < inter.getInterface_field().size(); i++) {
			Interface_Field field = inter.getInterface_field().get(i);
			form.put(field.getName(), value[i]);
		}
		payload.setForm(form.toString());
		return service.callMethod(inter, payload, null);
	}

	public static void main(String[] args) throws Exception {
		InterfaceConfigs.setConfig();
		Object result = null;
//		result = testSAD();
		result = getDireccionPorTN();

		System.out.println(JSON.toString(result));
	}

}
