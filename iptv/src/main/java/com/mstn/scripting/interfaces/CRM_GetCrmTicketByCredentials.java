/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces;

import _do.com.claro.soa.services.customer.getcrmticketbycredentials.GetCrmTicketByCredentials;
import _do.com.claro.soa.services.customer.getcrmticketbycredentials.GetCrmTicketByCredentials_Service;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import java.lang.reflect.Method;
import java.util.List;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

/**
 *
 * @author amatos
 */
public class CRM_GetCrmTicketByCredentials extends InterfaceBase {

	public CRM_GetCrmTicketByCredentials() {
		super(0, "crmticketbycredentials");
	}

	@Override
	protected Object processINSTANCE(Object INSTANCE) {
		try {
			GetCrmTicketByCredentials_Service SERVICE = new GetCrmTicketByCredentials_Service();
			GetCrmTicketByCredentials SOAP = SERVICE.getGetCrmTicketByCredentialsSOAP();
			return SOAP;
		} catch (Exception ex) {
			Utils.logException(this.getClass(), "Error instanciando endpoint GetCrmTicketByCredentials", ex);
			throw ex;
		}
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("crm_ticketbycredentials"));
		return inter;
	}

	@Override
	protected Object sendParamsToMethod(Interface inter, Method method, List<Object> params) throws Exception {
		BindingProvider bp = (BindingProvider) this.INSTANCE;
		String username = "CT319078";
		String password = "";
		List<Handler> handlerChain = bp.getBinding().getHandlerChain();
		handlerChain.add(new SOAPHeaderHandler(username, password));
		bp.getBinding().setHandlerChain(handlerChain);

		return super.sendParamsToMethod(inter, method, params);
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("crm_ticketbycredentials").toString();
		super.test();
	}

}
