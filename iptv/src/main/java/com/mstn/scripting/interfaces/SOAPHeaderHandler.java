/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces;

import java.util.Set;
import java.util.TreeSet;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 *
 * @author amatos
 */
//https://stackoverflow.com/questions/5976940/how-to-add-soap-header-in-java
public class SOAPHeaderHandler implements SOAPHandler<SOAPMessageContext> {

	private final String username;
	private final String password;

	public SOAPHeaderHandler(String username, String password) {
		this.username = username;
		this.password = password;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if (outboundProperty) {
			try {
				SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();
				SOAPFactory factory = SOAPFactory.newInstance();
				String prefix = "wsse";
				String uri = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
				SOAPElement securityElem = factory.createElement("Security", prefix, uri);
				SOAPElement usernameTokenElem = factory.createElement("UsernameToken", prefix, uri);
				SOAPElement usernameElem = factory.createElement("Username", prefix, uri);
				SOAPElement passwordElem = factory.createElement("PasswordText", prefix, uri);
				usernameElem.addTextNode(username);
				passwordElem.addTextNode(password);
				usernameTokenElem.addChildElement(usernameElem);
				usernameTokenElem.addChildElement(passwordElem);
				securityElem.addChildElement(usernameTokenElem);

				SOAPHeader header = envelope.getHeader();
				if (header == null) {
					header = envelope.addHeader();
				} else {
					header.removeContents();
				}
				header.setPrefix("soapenv");
				header.addChildElement(securityElem);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			// inbound
		}
		return true;
	}

	@Override
	public Set<QName> getHeaders() {
		return new TreeSet();
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		return false;
	}

	@Override
	public void close(MessageContext context) {
		//
	}
}
