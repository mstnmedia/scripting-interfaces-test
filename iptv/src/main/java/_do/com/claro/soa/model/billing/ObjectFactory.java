
package _do.com.claro.soa.model.billing;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import _do.com.claro.soa.model.generic.TelephoneNumber;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro.soa.model.billing package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BillingAccountNumber_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "billingAccountNumber");
    private final static QName _FinancingPaymentsDetails_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "financingPaymentsDetails");
    private final static QName _RecurringCharge_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "recurringCharge");
    private final static QName _SubscriptionRefillTransactions_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "subscriptionRefillTransactions");
    private final static QName _Installment_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "installment");
    private final static QName _SubscriptionRefill_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "subscriptionRefill");
    private final static QName _Subscriptions_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "subscriptions");
    private final static QName _Arrangement_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "arrangement");
    private final static QName _Discounts_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "discounts");
    private final static QName _BillingAccountType_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "billingAccountType");
    private final static QName _HistoryInformation_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "historyInformation");
    private final static QName _PendingChargesAndCredits_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "pendingChargesAndCredits");
    private final static QName _FutureBillingPlan_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "futureBillingPlan");
    private final static QName _BillFormat_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "billFormat");
    private final static QName _SubscriberNo_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "subscriberNo");
    private final static QName _AdditionalFeatures_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "additionalFeatures");
    private final static QName _Bills_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "bills");
    private final static QName _PayOnBillAvailability_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "payOnBillAvailability");
    private final static QName _Bill_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "bill");
    private final static QName _FinacialSummary_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "finacialSummary");
    private final static QName _BillingPlan_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "billingPlan");
    private final static QName _Memo_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "memo");
    private final static QName _Discount_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "discount");
    private final static QName _BillingAccounts_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "billingAccounts");
    private final static QName _Guarantor_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "guarantor");
    private final static QName _BillingAccount_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "billingAccount");
    private final static QName _AdditionalFeature_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "additionalFeature");
    private final static QName _FinancingAvailability_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "financingAvailability");
    private final static QName _PlanFeature_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "planFeature");
    private final static QName _Payment_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "payment");
    private final static QName _AutomaticTreatment_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "automaticTreatment");
    private final static QName _Arrangements_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "arrangements");
    private final static QName _BillingAccountCollectionInfo_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "billingAccountCollectionInfo");
    private final static QName _Installments_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "installments");
    private final static QName _BillingAccountNumbers_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "billingAccountNumbers");
    private final static QName _FinancedDevices_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "financedDevices");
    private final static QName _Charge_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "charge");
    private final static QName _PlanFeatures_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "planFeatures");
    private final static QName _FinancedDevice_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "financedDevice");
    private final static QName _Subscription_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "subscription");
    private final static QName _Payments_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "payments");
    private final static QName _Financing_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "financing");
    private final static QName _GuarantorOf_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "guarantorOf");
    private final static QName _PendingChargeAndCredit_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "pendingChargeAndCredit");
    private final static QName _Memos_QNAME = new QName("http://www.claro.com.do/soa/model/billing", "memos");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro.soa.model.billing
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FinancialSummary }
     * 
     */
    public FinancialSummary createFinancialSummary() {
        return new FinancialSummary();
    }

    /**
     * Create an instance of {@link SubscriptionRefill }
     * 
     */
    public SubscriptionRefill createSubscriptionRefill() {
        return new SubscriptionRefill();
    }

    /**
     * Create an instance of {@link Guarantor }
     * 
     */
    public Guarantor createGuarantor() {
        return new Guarantor();
    }

    /**
     * Create an instance of {@link FinancingAvailability }
     * 
     */
    public FinancingAvailability createFinancingAvailability() {
        return new FinancingAvailability();
    }

    /**
     * Create an instance of {@link BillingAccountType }
     * 
     */
    public BillingAccountType createBillingAccountType() {
        return new BillingAccountType();
    }

    /**
     * Create an instance of {@link BillingPlan }
     * 
     */
    public BillingPlan createBillingPlan() {
        return new BillingPlan();
    }

    /**
     * Create an instance of {@link FinancingPaymentDetails }
     * 
     */
    public FinancingPaymentDetails createFinancingPaymentDetails() {
        return new FinancingPaymentDetails();
    }

    /**
     * Create an instance of {@link Bills }
     * 
     */
    public Bills createBills() {
        return new Bills();
    }

    /**
     * Create an instance of {@link Installments }
     * 
     */
    public Installments createInstallments() {
        return new Installments();
    }

    /**
     * Create an instance of {@link BillingAccountCollectionInfo }
     * 
     */
    public BillingAccountCollectionInfo createBillingAccountCollectionInfo() {
        return new BillingAccountCollectionInfo();
    }

    /**
     * Create an instance of {@link Financing }
     * 
     */
    public Financing createFinancing() {
        return new Financing();
    }

    /**
     * Create an instance of {@link PendingChargesAndCredits }
     * 
     */
    public PendingChargesAndCredits createPendingChargesAndCredits() {
        return new PendingChargesAndCredits();
    }

    /**
     * Create an instance of {@link Payment }
     * 
     */
    public Payment createPayment() {
        return new Payment();
    }

    /**
     * Create an instance of {@link Memos }
     * 
     */
    public Memos createMemos() {
        return new Memos();
    }

    /**
     * Create an instance of {@link MonthData }
     * 
     */
    public MonthData createMonthData() {
        return new MonthData();
    }

    /**
     * Create an instance of {@link Discount }
     * 
     */
    public Discount createDiscount() {
        return new Discount();
    }

    /**
     * Create an instance of {@link Payments }
     * 
     */
    public Payments createPayments() {
        return new Payments();
    }

    /**
     * Create an instance of {@link BillingAccount }
     * 
     */
    public BillingAccount createBillingAccount() {
        return new BillingAccount();
    }

    /**
     * Create an instance of {@link Installment }
     * 
     */
    public Installment createInstallment() {
        return new Installment();
    }

    /**
     * Create an instance of {@link GuarantorOf }
     * 
     */
    public GuarantorOf createGuarantorOf() {
        return new GuarantorOf();
    }

    /**
     * Create an instance of {@link HistoryInformation }
     * 
     */
    public HistoryInformation createHistoryInformation() {
        return new HistoryInformation();
    }

    /**
     * Create an instance of {@link FinancedDevices }
     * 
     */
    public FinancedDevices createFinancedDevices() {
        return new FinancedDevices();
    }

    /**
     * Create an instance of {@link PlanFeature }
     * 
     */
    public PlanFeature createPlanFeature() {
        return new PlanFeature();
    }

    /**
     * Create an instance of {@link Discounts }
     * 
     */
    public Discounts createDiscounts() {
        return new Discounts();
    }

    /**
     * Create an instance of {@link AdditionalFeature }
     * 
     */
    public AdditionalFeature createAdditionalFeature() {
        return new AdditionalFeature();
    }

    /**
     * Create an instance of {@link BillingAccountNumbers }
     * 
     */
    public BillingAccountNumbers createBillingAccountNumbers() {
        return new BillingAccountNumbers();
    }

    /**
     * Create an instance of {@link PayOnBillAvailability }
     * 
     */
    public PayOnBillAvailability createPayOnBillAvailability() {
        return new PayOnBillAvailability();
    }

    /**
     * Create an instance of {@link AdditionalFeatures }
     * 
     */
    public AdditionalFeatures createAdditionalFeatures() {
        return new AdditionalFeatures();
    }

    /**
     * Create an instance of {@link Bill }
     * 
     */
    public Bill createBill() {
        return new Bill();
    }

    /**
     * Create an instance of {@link Subscriptions }
     * 
     */
    public Subscriptions createSubscriptions() {
        return new Subscriptions();
    }

    /**
     * Create an instance of {@link Subscription }
     * 
     */
    public Subscription createSubscription() {
        return new Subscription();
    }

    /**
     * Create an instance of {@link Arrangements }
     * 
     */
    public Arrangements createArrangements() {
        return new Arrangements();
    }

    /**
     * Create an instance of {@link FinancedDevice }
     * 
     */
    public FinancedDevice createFinancedDevice() {
        return new FinancedDevice();
    }

    /**
     * Create an instance of {@link Arrangement }
     * 
     */
    public Arrangement createArrangement() {
        return new Arrangement();
    }

    /**
     * Create an instance of {@link AutomaticTreatmentStep }
     * 
     */
    public AutomaticTreatmentStep createAutomaticTreatmentStep() {
        return new AutomaticTreatmentStep();
    }

    /**
     * Create an instance of {@link Charge }
     * 
     */
    public Charge createCharge() {
        return new Charge();
    }

    /**
     * Create an instance of {@link AutomaticTreatment }
     * 
     */
    public AutomaticTreatment createAutomaticTreatment() {
        return new AutomaticTreatment();
    }

    /**
     * Create an instance of {@link BillFormat }
     * 
     */
    public BillFormat createBillFormat() {
        return new BillFormat();
    }

    /**
     * Create an instance of {@link RecurringCharge }
     * 
     */
    public RecurringCharge createRecurringCharge() {
        return new RecurringCharge();
    }

    /**
     * Create an instance of {@link BillingAccounts }
     * 
     */
    public BillingAccounts createBillingAccounts() {
        return new BillingAccounts();
    }

    /**
     * Create an instance of {@link SubscriptionRefillTransactions }
     * 
     */
    public SubscriptionRefillTransactions createSubscriptionRefillTransactions() {
        return new SubscriptionRefillTransactions();
    }

    /**
     * Create an instance of {@link FutureBillingPlan }
     * 
     */
    public FutureBillingPlan createFutureBillingPlan() {
        return new FutureBillingPlan();
    }

    /**
     * Create an instance of {@link Charge.Override }
     * 
     */
    public Charge.Override createChargeOverride() {
        return new Charge.Override();
    }

    /**
     * Create an instance of {@link PendingChargeAndCredit }
     * 
     */
    public PendingChargeAndCredit createPendingChargeAndCredit() {
        return new PendingChargeAndCredit();
    }

    /**
     * Create an instance of {@link PlanFeatures }
     * 
     */
    public PlanFeatures createPlanFeatures() {
        return new PlanFeatures();
    }

    /**
     * Create an instance of {@link Memo }
     * 
     */
    public Memo createMemo() {
        return new Memo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "billingAccountNumber")
    public JAXBElement<String> createBillingAccountNumber(String value) {
        return new JAXBElement<String>(_BillingAccountNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinancingPaymentDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "financingPaymentsDetails")
    public JAXBElement<FinancingPaymentDetails> createFinancingPaymentsDetails(FinancingPaymentDetails value) {
        return new JAXBElement<FinancingPaymentDetails>(_FinancingPaymentsDetails_QNAME, FinancingPaymentDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecurringCharge }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "recurringCharge")
    public JAXBElement<RecurringCharge> createRecurringCharge(RecurringCharge value) {
        return new JAXBElement<RecurringCharge>(_RecurringCharge_QNAME, RecurringCharge.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubscriptionRefillTransactions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "subscriptionRefillTransactions")
    public JAXBElement<SubscriptionRefillTransactions> createSubscriptionRefillTransactions(SubscriptionRefillTransactions value) {
        return new JAXBElement<SubscriptionRefillTransactions>(_SubscriptionRefillTransactions_QNAME, SubscriptionRefillTransactions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Installment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "installment")
    public JAXBElement<Installment> createInstallment(Installment value) {
        return new JAXBElement<Installment>(_Installment_QNAME, Installment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubscriptionRefill }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "subscriptionRefill")
    public JAXBElement<SubscriptionRefill> createSubscriptionRefill(SubscriptionRefill value) {
        return new JAXBElement<SubscriptionRefill>(_SubscriptionRefill_QNAME, SubscriptionRefill.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Subscriptions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "subscriptions")
    public JAXBElement<Subscriptions> createSubscriptions(Subscriptions value) {
        return new JAXBElement<Subscriptions>(_Subscriptions_QNAME, Subscriptions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Arrangement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "arrangement")
    public JAXBElement<Arrangement> createArrangement(Arrangement value) {
        return new JAXBElement<Arrangement>(_Arrangement_QNAME, Arrangement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Discounts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "discounts")
    public JAXBElement<Discounts> createDiscounts(Discounts value) {
        return new JAXBElement<Discounts>(_Discounts_QNAME, Discounts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillingAccountType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "billingAccountType")
    public JAXBElement<BillingAccountType> createBillingAccountType(BillingAccountType value) {
        return new JAXBElement<BillingAccountType>(_BillingAccountType_QNAME, BillingAccountType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HistoryInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "historyInformation")
    public JAXBElement<HistoryInformation> createHistoryInformation(HistoryInformation value) {
        return new JAXBElement<HistoryInformation>(_HistoryInformation_QNAME, HistoryInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PendingChargesAndCredits }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "pendingChargesAndCredits")
    public JAXBElement<PendingChargesAndCredits> createPendingChargesAndCredits(PendingChargesAndCredits value) {
        return new JAXBElement<PendingChargesAndCredits>(_PendingChargesAndCredits_QNAME, PendingChargesAndCredits.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FutureBillingPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "futureBillingPlan")
    public JAXBElement<FutureBillingPlan> createFutureBillingPlan(FutureBillingPlan value) {
        return new JAXBElement<FutureBillingPlan>(_FutureBillingPlan_QNAME, FutureBillingPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillFormat }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "billFormat")
    public JAXBElement<BillFormat> createBillFormat(BillFormat value) {
        return new JAXBElement<BillFormat>(_BillFormat_QNAME, BillFormat.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TelephoneNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "subscriberNo")
    public JAXBElement<TelephoneNumber> createSubscriberNo(TelephoneNumber value) {
        return new JAXBElement<TelephoneNumber>(_SubscriberNo_QNAME, TelephoneNumber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdditionalFeatures }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "additionalFeatures")
    public JAXBElement<AdditionalFeatures> createAdditionalFeatures(AdditionalFeatures value) {
        return new JAXBElement<AdditionalFeatures>(_AdditionalFeatures_QNAME, AdditionalFeatures.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Bills }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "bills")
    public JAXBElement<Bills> createBills(Bills value) {
        return new JAXBElement<Bills>(_Bills_QNAME, Bills.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayOnBillAvailability }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "payOnBillAvailability")
    public JAXBElement<PayOnBillAvailability> createPayOnBillAvailability(PayOnBillAvailability value) {
        return new JAXBElement<PayOnBillAvailability>(_PayOnBillAvailability_QNAME, PayOnBillAvailability.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Bill }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "bill")
    public JAXBElement<Bill> createBill(Bill value) {
        return new JAXBElement<Bill>(_Bill_QNAME, Bill.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinancialSummary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "finacialSummary")
    public JAXBElement<FinancialSummary> createFinacialSummary(FinancialSummary value) {
        return new JAXBElement<FinancialSummary>(_FinacialSummary_QNAME, FinancialSummary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillingPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "billingPlan")
    public JAXBElement<BillingPlan> createBillingPlan(BillingPlan value) {
        return new JAXBElement<BillingPlan>(_BillingPlan_QNAME, BillingPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Memo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "memo")
    public JAXBElement<Memo> createMemo(Memo value) {
        return new JAXBElement<Memo>(_Memo_QNAME, Memo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Discount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "discount")
    public JAXBElement<Discount> createDiscount(Discount value) {
        return new JAXBElement<Discount>(_Discount_QNAME, Discount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillingAccounts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "billingAccounts")
    public JAXBElement<BillingAccounts> createBillingAccounts(BillingAccounts value) {
        return new JAXBElement<BillingAccounts>(_BillingAccounts_QNAME, BillingAccounts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Guarantor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "guarantor")
    public JAXBElement<Guarantor> createGuarantor(Guarantor value) {
        return new JAXBElement<Guarantor>(_Guarantor_QNAME, Guarantor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillingAccount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "billingAccount")
    public JAXBElement<BillingAccount> createBillingAccount(BillingAccount value) {
        return new JAXBElement<BillingAccount>(_BillingAccount_QNAME, BillingAccount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdditionalFeature }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "additionalFeature")
    public JAXBElement<AdditionalFeature> createAdditionalFeature(AdditionalFeature value) {
        return new JAXBElement<AdditionalFeature>(_AdditionalFeature_QNAME, AdditionalFeature.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinancingAvailability }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "financingAvailability")
    public JAXBElement<FinancingAvailability> createFinancingAvailability(FinancingAvailability value) {
        return new JAXBElement<FinancingAvailability>(_FinancingAvailability_QNAME, FinancingAvailability.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlanFeature }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "planFeature")
    public JAXBElement<PlanFeature> createPlanFeature(PlanFeature value) {
        return new JAXBElement<PlanFeature>(_PlanFeature_QNAME, PlanFeature.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Payment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "payment")
    public JAXBElement<Payment> createPayment(Payment value) {
        return new JAXBElement<Payment>(_Payment_QNAME, Payment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AutomaticTreatment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "automaticTreatment")
    public JAXBElement<AutomaticTreatment> createAutomaticTreatment(AutomaticTreatment value) {
        return new JAXBElement<AutomaticTreatment>(_AutomaticTreatment_QNAME, AutomaticTreatment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Arrangements }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "arrangements")
    public JAXBElement<Arrangements> createArrangements(Arrangements value) {
        return new JAXBElement<Arrangements>(_Arrangements_QNAME, Arrangements.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillingAccountCollectionInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "billingAccountCollectionInfo")
    public JAXBElement<BillingAccountCollectionInfo> createBillingAccountCollectionInfo(BillingAccountCollectionInfo value) {
        return new JAXBElement<BillingAccountCollectionInfo>(_BillingAccountCollectionInfo_QNAME, BillingAccountCollectionInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Installments }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "installments")
    public JAXBElement<Installments> createInstallments(Installments value) {
        return new JAXBElement<Installments>(_Installments_QNAME, Installments.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillingAccountNumbers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "billingAccountNumbers")
    public JAXBElement<BillingAccountNumbers> createBillingAccountNumbers(BillingAccountNumbers value) {
        return new JAXBElement<BillingAccountNumbers>(_BillingAccountNumbers_QNAME, BillingAccountNumbers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinancedDevices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "financedDevices")
    public JAXBElement<FinancedDevices> createFinancedDevices(FinancedDevices value) {
        return new JAXBElement<FinancedDevices>(_FinancedDevices_QNAME, FinancedDevices.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Charge }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "charge")
    public JAXBElement<Charge> createCharge(Charge value) {
        return new JAXBElement<Charge>(_Charge_QNAME, Charge.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlanFeatures }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "planFeatures")
    public JAXBElement<PlanFeatures> createPlanFeatures(PlanFeatures value) {
        return new JAXBElement<PlanFeatures>(_PlanFeatures_QNAME, PlanFeatures.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinancedDevice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "financedDevice")
    public JAXBElement<FinancedDevice> createFinancedDevice(FinancedDevice value) {
        return new JAXBElement<FinancedDevice>(_FinancedDevice_QNAME, FinancedDevice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Subscription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "subscription")
    public JAXBElement<Subscription> createSubscription(Subscription value) {
        return new JAXBElement<Subscription>(_Subscription_QNAME, Subscription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Payments }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "payments")
    public JAXBElement<Payments> createPayments(Payments value) {
        return new JAXBElement<Payments>(_Payments_QNAME, Payments.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Financing }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "financing")
    public JAXBElement<Financing> createFinancing(Financing value) {
        return new JAXBElement<Financing>(_Financing_QNAME, Financing.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GuarantorOf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "guarantorOf")
    public JAXBElement<GuarantorOf> createGuarantorOf(GuarantorOf value) {
        return new JAXBElement<GuarantorOf>(_GuarantorOf_QNAME, GuarantorOf.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PendingChargeAndCredit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "pendingChargeAndCredit")
    public JAXBElement<PendingChargeAndCredit> createPendingChargeAndCredit(PendingChargeAndCredit value) {
        return new JAXBElement<PendingChargeAndCredit>(_PendingChargeAndCredit_QNAME, PendingChargeAndCredit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Memos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/billing", name = "memos")
    public JAXBElement<Memos> createMemos(Memos value) {
        return new JAXBElement<Memos>(_Memos_QNAME, Memos.class, null, value);
    }

}
