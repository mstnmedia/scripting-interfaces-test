
package _do.com.claro.soa.model.generic;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Sign.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Sign">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NEGATIVE"/>
 *     &lt;enumeration value="POSITIVE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Sign")
@XmlEnum
public enum Sign {

    NEGATIVE,
    POSITIVE;

    public String value() {
        return name();
    }

    public static Sign fromValue(String v) {
        return valueOf(v);
    }

}
