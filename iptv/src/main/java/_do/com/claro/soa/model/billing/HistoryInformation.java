
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         @Created SOA-235
 *         Represents the lists of activities of the account such as 
 *         suspensions, payments, cancelations, etc.
 * 
 *         The HistoryInformation includes the following fields:
 *         -Active: Indicates the suspensions that an account has had during the year.
 *         -DCK: Indicates how many times the customer have had credit card restrictions or returned checks.
 *       
 * 
 * <p>Java class for HistoryInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HistoryInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="active" type="{http://www.claro.com.do/soa/model/billing}MonthData"/>
 *         &lt;element name="DCK" type="{http://www.claro.com.do/soa/model/billing}MonthData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HistoryInformation", propOrder = {
    "active",
    "dck"
})
public class HistoryInformation {

    @XmlElement(required = true)
    protected MonthData active;
    @XmlElement(name = "DCK", required = true)
    protected MonthData dck;

    /**
     * Gets the value of the active property.
     * 
     * @return
     *     possible object is
     *     {@link MonthData }
     *     
     */
    public MonthData getActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     * @param value
     *     allowed object is
     *     {@link MonthData }
     *     
     */
    public void setActive(MonthData value) {
        this.active = value;
    }

    /**
     * Gets the value of the dck property.
     * 
     * @return
     *     possible object is
     *     {@link MonthData }
     *     
     */
    public MonthData getDCK() {
        return dck;
    }

    /**
     * Sets the value of the dck property.
     * 
     * @param value
     *     allowed object is
     *     {@link MonthData }
     *     
     */
    public void setDCK(MonthData value) {
        this.dck = value;
    }

}
