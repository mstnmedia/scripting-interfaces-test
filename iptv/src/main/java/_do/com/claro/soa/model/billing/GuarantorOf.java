
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         @Created SOA-257
 *         Represents a list of the BANs a BAN is guarantor of.
 *       
 * 
 * <p>Java class for GuarantorOf complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GuarantorOf">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}billingAccountNumbers"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GuarantorOf", propOrder = {
    "billingAccountNumbers"
})
public class GuarantorOf {

    @XmlElement(required = true, nillable = true)
    protected BillingAccountNumbers billingAccountNumbers;

    /**
     * Gets the value of the billingAccountNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link BillingAccountNumbers }
     *     
     */
    public BillingAccountNumbers getBillingAccountNumbers() {
        return billingAccountNumbers;
    }

    /**
     * Sets the value of the billingAccountNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingAccountNumbers }
     *     
     */
    public void setBillingAccountNumbers(BillingAccountNumbers value) {
        this.billingAccountNumbers = value;
    }

}
