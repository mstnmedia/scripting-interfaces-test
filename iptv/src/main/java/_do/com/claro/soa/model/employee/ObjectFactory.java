
package _do.com.claro.soa.model.employee;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro.soa.model.employee package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EmployeeId_QNAME = new QName("http://www.claro.com.do/soa/model/employee", "employeeId");
    private final static QName _Employee_QNAME = new QName("http://www.claro.com.do/soa/model/employee", "employee");
    private final static QName _Employees_QNAME = new QName("http://www.claro.com.do/soa/model/employee", "employees");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro.soa.model.employee
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Employees }
     * 
     */
    public Employees createEmployees() {
        return new Employees();
    }

    /**
     * Create an instance of {@link Employee }
     * 
     */
    public Employee createEmployee() {
        return new Employee();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/employee", name = "employeeId")
    public JAXBElement<String> createEmployeeId(String value) {
        return new JAXBElement<String>(_EmployeeId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Employee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/employee", name = "employee")
    public JAXBElement<Employee> createEmployee(Employee value) {
        return new JAXBElement<Employee>(_Employee_QNAME, Employee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Employees }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/employee", name = "employees")
    public JAXBElement<Employees> createEmployees(Employees value) {
        return new JAXBElement<Employees>(_Employees_QNAME, Employees.class, null, value);
    }

}
