
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.generic.Frequency;


/**
 * 
 *           @Created: SOA-140
 *           Similar to 'charge' but recurring.
 *         
 * 
 * <p>Java class for RecurringCharge complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RecurringCharge">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.claro.com.do/soa/model/billing}Charge">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/generic}frequency"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecurringCharge", propOrder = {
    "frequency"
})
public class RecurringCharge
    extends Charge
{

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/generic", required = true)
    protected Frequency frequency;

    /**
     * Gets the value of the frequency property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getFrequency() {
        return frequency;
    }

    /**
     * Sets the value of the frequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setFrequency(Frequency value) {
        this.frequency = value;
    }

}
