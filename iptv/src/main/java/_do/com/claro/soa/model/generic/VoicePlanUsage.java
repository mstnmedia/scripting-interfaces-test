
package _do.com.claro.soa.model.generic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         @Created: SOA-258 Information about a fixed or mobile
 *         postpaid voice plan usage
 *         @Modified: SOA-293 to include freeInOperatorMinutes element.
 *       
 * 
 * <p>Java class for VoicePlanUsage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VoicePlanUsage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hasUnlimitedMinutes" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="totalMinutesInPlan" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="rolledMinutes" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="bonusMinutes" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="usedMinutes" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="freeInOperatorMinutes" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="minutesUsedOutsidePlan" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="availableMinutes" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="planCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoicePlanUsage", propOrder = {
    "hasUnlimitedMinutes",
    "totalMinutesInPlan",
    "rolledMinutes",
    "bonusMinutes",
    "usedMinutes",
    "freeInOperatorMinutes",
    "minutesUsedOutsidePlan",
    "availableMinutes",
    "planCode"
})
public class VoicePlanUsage {

    protected boolean hasUnlimitedMinutes;
    protected int totalMinutesInPlan;
    protected float rolledMinutes;
    protected float bonusMinutes;
    protected float usedMinutes;
    protected float freeInOperatorMinutes;
    protected float minutesUsedOutsidePlan;
    protected float availableMinutes;
    @XmlElement(required = true)
    protected String planCode;

    /**
     * Gets the value of the hasUnlimitedMinutes property.
     * 
     */
    public boolean isHasUnlimitedMinutes() {
        return hasUnlimitedMinutes;
    }

    /**
     * Sets the value of the hasUnlimitedMinutes property.
     * 
     */
    public void setHasUnlimitedMinutes(boolean value) {
        this.hasUnlimitedMinutes = value;
    }

    /**
     * Gets the value of the totalMinutesInPlan property.
     * 
     */
    public int getTotalMinutesInPlan() {
        return totalMinutesInPlan;
    }

    /**
     * Sets the value of the totalMinutesInPlan property.
     * 
     */
    public void setTotalMinutesInPlan(int value) {
        this.totalMinutesInPlan = value;
    }

    /**
     * Gets the value of the rolledMinutes property.
     * 
     */
    public float getRolledMinutes() {
        return rolledMinutes;
    }

    /**
     * Sets the value of the rolledMinutes property.
     * 
     */
    public void setRolledMinutes(float value) {
        this.rolledMinutes = value;
    }

    /**
     * Gets the value of the bonusMinutes property.
     * 
     */
    public float getBonusMinutes() {
        return bonusMinutes;
    }

    /**
     * Sets the value of the bonusMinutes property.
     * 
     */
    public void setBonusMinutes(float value) {
        this.bonusMinutes = value;
    }

    /**
     * Gets the value of the usedMinutes property.
     * 
     */
    public float getUsedMinutes() {
        return usedMinutes;
    }

    /**
     * Sets the value of the usedMinutes property.
     * 
     */
    public void setUsedMinutes(float value) {
        this.usedMinutes = value;
    }

    /**
     * Gets the value of the freeInOperatorMinutes property.
     * 
     */
    public float getFreeInOperatorMinutes() {
        return freeInOperatorMinutes;
    }

    /**
     * Sets the value of the freeInOperatorMinutes property.
     * 
     */
    public void setFreeInOperatorMinutes(float value) {
        this.freeInOperatorMinutes = value;
    }

    /**
     * Gets the value of the minutesUsedOutsidePlan property.
     * 
     */
    public float getMinutesUsedOutsidePlan() {
        return minutesUsedOutsidePlan;
    }

    /**
     * Sets the value of the minutesUsedOutsidePlan property.
     * 
     */
    public void setMinutesUsedOutsidePlan(float value) {
        this.minutesUsedOutsidePlan = value;
    }

    /**
     * Gets the value of the availableMinutes property.
     * 
     */
    public float getAvailableMinutes() {
        return availableMinutes;
    }

    /**
     * Sets the value of the availableMinutes property.
     * 
     */
    public void setAvailableMinutes(float value) {
        this.availableMinutes = value;
    }

    /**
     * Gets the value of the planCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanCode() {
        return planCode;
    }

    /**
     * Sets the value of the planCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanCode(String value) {
        this.planCode = value;
    }

}
