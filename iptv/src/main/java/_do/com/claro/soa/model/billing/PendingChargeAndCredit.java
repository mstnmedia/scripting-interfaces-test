
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import _do.com.claro.soa.model.generic.Balance;
import _do.com.claro.soa.model.generic.TelephoneNumber;


/**
 * 
 *         @Created: SOA-236
 *         Type used to represent the pending charges and credits of any Ensemble Billing Account (BAN).
 * 
 *         -code: The code of the charge or credit
 *         -description: A brief description of the charge or credit
 *         -productType: The product type the charge or credit is being applied to (mobile, fixed, etc)
 *         -subscriberNo: This is the subscriber number (associated with the ban) the charge or credit is being applied to
 *         -fromDate: The date since when the credit or charge will be applied
 *         -toDate: The date until when the charge or credit will be applied
 *         -amount: The amount of the charge 
 *         -tax: The tax applied to the charge or credit
 *         -total: This is the amount plus the tax (amount + tax = total)
 *         -balanceImpact: Indicates the procedence of the charge that will impact the balance (B = Billing, I = Inmediate Charge)
 *         -billDate: The date on bill when the charge or credit will be applied
 *         -creationDate: The date when the charge or credit was created
 *         -type: The type of the charge or credit (inmediate, on time charge, recurrent charge, advanced recurrent charge, etc)
 *         -appliedCredit: The amount of the credit applied
 *         -displayOnBill: A boolean value that determines whether the charge or credit is display on bill
 *         -adjustmentReversal: A reversal to the charge or credit
 *         -soc: Represents the code of the service.
 *         -numInstallments: The current number of installments made
 *         -totalInstallments: The total number of installments to be made
 *         -invoice: The number of the invoice
 *       
 * 
 * <p>Java class for PendingChargeAndCredit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PendingChargeAndCredit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}subscriberNo"/>
 *         &lt;element name="fromDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="toDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="amount" type="{http://www.claro.com.do/soa/model/generic}Balance"/>
 *         &lt;element name="tax" type="{http://www.claro.com.do/soa/model/generic}Balance"/>
 *         &lt;element name="total" type="{http://www.claro.com.do/soa/model/generic}Balance"/>
 *         &lt;element name="balanceImpact" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="billDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="creationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="type" type="{http://www.claro.com.do/soa/model/billing}ChargeAndCreditType"/>
 *         &lt;element name="appliedCredit" type="{http://www.claro.com.do/soa/model/generic}Balance"/>
 *         &lt;element name="displayOnBill" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="adjustmentReversal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="soc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numInstallments" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="totalInstallments" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="invoice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PendingChargeAndCredit", propOrder = {
    "code",
    "description",
    "productType",
    "subscriberNo",
    "fromDate",
    "toDate",
    "amount",
    "tax",
    "total",
    "balanceImpact",
    "billDate",
    "creationDate",
    "type",
    "appliedCredit",
    "displayOnBill",
    "adjustmentReversal",
    "soc",
    "numInstallments",
    "totalInstallments",
    "invoice"
})
public class PendingChargeAndCredit {

    @XmlElement(required = true)
    protected String code;
    @XmlElement(required = true)
    protected String description;
    @XmlElement(required = true)
    protected String productType;
    @XmlElement(required = true, nillable = true)
    protected TelephoneNumber subscriberNo;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fromDate;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar toDate;
    @XmlElement(required = true, nillable = true)
    protected Balance amount;
    @XmlElement(required = true, nillable = true)
    protected Balance tax;
    @XmlElement(required = true, nillable = true)
    protected Balance total;
    protected boolean balanceImpact;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar billDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationDate;
    @XmlElement(required = true, nillable = true)
    protected ChargeAndCreditType type;
    @XmlElement(required = true, nillable = true)
    protected Balance appliedCredit;
    protected boolean displayOnBill;
    @XmlElement(required = true, nillable = true)
    protected String adjustmentReversal;
    @XmlElement(required = true, nillable = true)
    protected String soc;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer numInstallments;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer totalInstallments;
    @XmlElement(required = true, nillable = true)
    protected String invoice;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the subscriberNo property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneNumber }
     *     
     */
    public TelephoneNumber getSubscriberNo() {
        return subscriberNo;
    }

    /**
     * Sets the value of the subscriberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneNumber }
     *     
     */
    public void setSubscriberNo(TelephoneNumber value) {
        this.subscriberNo = value;
    }

    /**
     * Gets the value of the fromDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFromDate() {
        return fromDate;
    }

    /**
     * Sets the value of the fromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFromDate(XMLGregorianCalendar value) {
        this.fromDate = value;
    }

    /**
     * Gets the value of the toDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getToDate() {
        return toDate;
    }

    /**
     * Sets the value of the toDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setToDate(XMLGregorianCalendar value) {
        this.toDate = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link Balance }
     *     
     */
    public Balance getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Balance }
     *     
     */
    public void setAmount(Balance value) {
        this.amount = value;
    }

    /**
     * Gets the value of the tax property.
     * 
     * @return
     *     possible object is
     *     {@link Balance }
     *     
     */
    public Balance getTax() {
        return tax;
    }

    /**
     * Sets the value of the tax property.
     * 
     * @param value
     *     allowed object is
     *     {@link Balance }
     *     
     */
    public void setTax(Balance value) {
        this.tax = value;
    }

    /**
     * Gets the value of the total property.
     * 
     * @return
     *     possible object is
     *     {@link Balance }
     *     
     */
    public Balance getTotal() {
        return total;
    }

    /**
     * Sets the value of the total property.
     * 
     * @param value
     *     allowed object is
     *     {@link Balance }
     *     
     */
    public void setTotal(Balance value) {
        this.total = value;
    }

    /**
     * Gets the value of the balanceImpact property.
     * 
     */
    public boolean isBalanceImpact() {
        return balanceImpact;
    }

    /**
     * Sets the value of the balanceImpact property.
     * 
     */
    public void setBalanceImpact(boolean value) {
        this.balanceImpact = value;
    }

    /**
     * Gets the value of the billDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBillDate() {
        return billDate;
    }

    /**
     * Sets the value of the billDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBillDate(XMLGregorianCalendar value) {
        this.billDate = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDate(XMLGregorianCalendar value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link ChargeAndCreditType }
     *     
     */
    public ChargeAndCreditType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargeAndCreditType }
     *     
     */
    public void setType(ChargeAndCreditType value) {
        this.type = value;
    }

    /**
     * Gets the value of the appliedCredit property.
     * 
     * @return
     *     possible object is
     *     {@link Balance }
     *     
     */
    public Balance getAppliedCredit() {
        return appliedCredit;
    }

    /**
     * Sets the value of the appliedCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Balance }
     *     
     */
    public void setAppliedCredit(Balance value) {
        this.appliedCredit = value;
    }

    /**
     * Gets the value of the displayOnBill property.
     * 
     */
    public boolean isDisplayOnBill() {
        return displayOnBill;
    }

    /**
     * Sets the value of the displayOnBill property.
     * 
     */
    public void setDisplayOnBill(boolean value) {
        this.displayOnBill = value;
    }

    /**
     * Gets the value of the adjustmentReversal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdjustmentReversal() {
        return adjustmentReversal;
    }

    /**
     * Sets the value of the adjustmentReversal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdjustmentReversal(String value) {
        this.adjustmentReversal = value;
    }

    /**
     * Gets the value of the soc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoc() {
        return soc;
    }

    /**
     * Sets the value of the soc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoc(String value) {
        this.soc = value;
    }

    /**
     * Gets the value of the numInstallments property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumInstallments() {
        return numInstallments;
    }

    /**
     * Sets the value of the numInstallments property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumInstallments(Integer value) {
        this.numInstallments = value;
    }

    /**
     * Gets the value of the totalInstallments property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalInstallments() {
        return totalInstallments;
    }

    /**
     * Sets the value of the totalInstallments property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalInstallments(Integer value) {
        this.totalInstallments = value;
    }

    /**
     * Gets the value of the invoice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoice() {
        return invoice;
    }

    /**
     * Sets the value of the invoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoice(String value) {
        this.invoice = value;
    }

}
