
package _do.com.claro.soa.model.customer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import _do.com.claro.soa.model.generic.TelephoneNumber;
import _do.com.claro.soa.model.product.Product;


/**
 * <p>Java class for Case complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Case">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}subscriberNo"/>
 *         &lt;element name="creationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}topic"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}creator"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}channel"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}closer"/>
 *         &lt;element name="closeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="closeReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contactMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contactInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}caseContact"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}notes"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}flexibleAttributes" minOccurs="0"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}diagnoses" minOccurs="0"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}attachments" minOccurs="0"/>
 *         &lt;element name="source" type="{http://www.claro.com.do/soa/model/customer}Source"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}customerID"/>
 *         &lt;element name="includesMaintenanceInContract" type="{http://www.claro.com.do/soa/model/customer}MaintenanceInContractInd"/>
 *         &lt;element name="isLetterPrintable" type="{http://www.claro.com.do/soa/model/customer}LetterPrintableInd"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/product}product"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Case", propOrder = {
    "id",
    "subscriberNo",
    "creationDate",
    "dueDate",
    "topic",
    "status",
    "creator",
    "channel",
    "closer",
    "closeDate",
    "closeReason",
    "contactMode",
    "contactInfo",
    "caseContact",
    "notes",
    "flexibleAttributes",
    "diagnoses",
    "attachments",
    "source",
    "customerID",
    "includesMaintenanceInContract",
    "isLetterPrintable",
    "product"
})
public class Case {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/billing", required = true, nillable = true)
    protected TelephoneNumber subscriberNo;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dueDate;
    @XmlElement(required = true)
    protected Topic topic;
    @XmlElement(required = true)
    protected String status;
    @XmlElement(required = true)
    protected Creator creator;
    @XmlElement(required = true, nillable = true)
    protected Channel channel;
    @XmlElement(required = true, nillable = true)
    protected Closer closer;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar closeDate;
    @XmlElement(required = true, nillable = true)
    protected String closeReason;
    @XmlElement(required = true, nillable = true)
    protected String contactMode;
    @XmlElement(required = true, nillable = true)
    protected String contactInfo;
    @XmlElement(required = true, nillable = true)
    protected CaseContact caseContact;
    @XmlElement(required = true, nillable = true)
    protected Notes notes;
    @XmlElement(nillable = true)
    protected FlexibleAttributes flexibleAttributes;
    protected Diagnoses diagnoses;
    protected Attachments attachments;
    @XmlElement(required = true)
    protected Source source;
    @XmlElement(required = true)
    protected CustomerID customerID;
    @XmlElement(required = true)
    protected MaintenanceInContractInd includesMaintenanceInContract;
    @XmlElement(required = true)
    protected LetterPrintableInd isLetterPrintable;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/product", required = true, nillable = true)
    protected Product product;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the subscriberNo property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneNumber }
     *     
     */
    public TelephoneNumber getSubscriberNo() {
        return subscriberNo;
    }

    /**
     * Sets the value of the subscriberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneNumber }
     *     
     */
    public void setSubscriberNo(TelephoneNumber value) {
        this.subscriberNo = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDate(XMLGregorianCalendar value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDueDate(XMLGregorianCalendar value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the topic property.
     * 
     * @return
     *     possible object is
     *     {@link Topic }
     *     
     */
    public Topic getTopic() {
        return topic;
    }

    /**
     * Sets the value of the topic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Topic }
     *     
     */
    public void setTopic(Topic value) {
        this.topic = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the creator property.
     * 
     * @return
     *     possible object is
     *     {@link Creator }
     *     
     */
    public Creator getCreator() {
        return creator;
    }

    /**
     * Sets the value of the creator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Creator }
     *     
     */
    public void setCreator(Creator value) {
        this.creator = value;
    }

    /**
     * Gets the value of the channel property.
     * 
     * @return
     *     possible object is
     *     {@link Channel }
     *     
     */
    public Channel getChannel() {
        return channel;
    }

    /**
     * Sets the value of the channel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Channel }
     *     
     */
    public void setChannel(Channel value) {
        this.channel = value;
    }

    /**
     * Gets the value of the closer property.
     * 
     * @return
     *     possible object is
     *     {@link Closer }
     *     
     */
    public Closer getCloser() {
        return closer;
    }

    /**
     * Sets the value of the closer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Closer }
     *     
     */
    public void setCloser(Closer value) {
        this.closer = value;
    }

    /**
     * Gets the value of the closeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCloseDate() {
        return closeDate;
    }

    /**
     * Sets the value of the closeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCloseDate(XMLGregorianCalendar value) {
        this.closeDate = value;
    }

    /**
     * Gets the value of the closeReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCloseReason() {
        return closeReason;
    }

    /**
     * Sets the value of the closeReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseReason(String value) {
        this.closeReason = value;
    }

    /**
     * Gets the value of the contactMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactMode() {
        return contactMode;
    }

    /**
     * Sets the value of the contactMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactMode(String value) {
        this.contactMode = value;
    }

    /**
     * Gets the value of the contactInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactInfo() {
        return contactInfo;
    }

    /**
     * Sets the value of the contactInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactInfo(String value) {
        this.contactInfo = value;
    }

    /**
     * Gets the value of the caseContact property.
     * 
     * @return
     *     possible object is
     *     {@link CaseContact }
     *     
     */
    public CaseContact getCaseContact() {
        return caseContact;
    }

    /**
     * Sets the value of the caseContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseContact }
     *     
     */
    public void setCaseContact(CaseContact value) {
        this.caseContact = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link Notes }
     *     
     */
    public Notes getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Notes }
     *     
     */
    public void setNotes(Notes value) {
        this.notes = value;
    }

    /**
     * Gets the value of the flexibleAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link FlexibleAttributes }
     *     
     */
    public FlexibleAttributes getFlexibleAttributes() {
        return flexibleAttributes;
    }

    /**
     * Sets the value of the flexibleAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexibleAttributes }
     *     
     */
    public void setFlexibleAttributes(FlexibleAttributes value) {
        this.flexibleAttributes = value;
    }

    /**
     * Gets the value of the diagnoses property.
     * 
     * @return
     *     possible object is
     *     {@link Diagnoses }
     *     
     */
    public Diagnoses getDiagnoses() {
        return diagnoses;
    }

    /**
     * Sets the value of the diagnoses property.
     * 
     * @param value
     *     allowed object is
     *     {@link Diagnoses }
     *     
     */
    public void setDiagnoses(Diagnoses value) {
        this.diagnoses = value;
    }

    /**
     * Gets the value of the attachments property.
     * 
     * @return
     *     possible object is
     *     {@link Attachments }
     *     
     */
    public Attachments getAttachments() {
        return attachments;
    }

    /**
     * Sets the value of the attachments property.
     * 
     * @param value
     *     allowed object is
     *     {@link Attachments }
     *     
     */
    public void setAttachments(Attachments value) {
        this.attachments = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link Source }
     *     
     */
    public Source getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link Source }
     *     
     */
    public void setSource(Source value) {
        this.source = value;
    }

    /**
     * Gets the value of the customerID property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerID }
     *     
     */
    public CustomerID getCustomerID() {
        return customerID;
    }

    /**
     * Sets the value of the customerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerID }
     *     
     */
    public void setCustomerID(CustomerID value) {
        this.customerID = value;
    }

    /**
     * Gets the value of the includesMaintenanceInContract property.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceInContractInd }
     *     
     */
    public MaintenanceInContractInd getIncludesMaintenanceInContract() {
        return includesMaintenanceInContract;
    }

    /**
     * Sets the value of the includesMaintenanceInContract property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceInContractInd }
     *     
     */
    public void setIncludesMaintenanceInContract(MaintenanceInContractInd value) {
        this.includesMaintenanceInContract = value;
    }

    /**
     * Gets the value of the isLetterPrintable property.
     * 
     * @return
     *     possible object is
     *     {@link LetterPrintableInd }
     *     
     */
    public LetterPrintableInd getIsLetterPrintable() {
        return isLetterPrintable;
    }

    /**
     * Sets the value of the isLetterPrintable property.
     * 
     * @param value
     *     allowed object is
     *     {@link LetterPrintableInd }
     *     
     */
    public void setIsLetterPrintable(LetterPrintableInd value) {
        this.isLetterPrintable = value;
    }

    /**
     * 
     *             @Created: ISFSOA-385
     *             This element indicates the product used to create the case.
     *           
     * 
     * @return
     *     possible object is
     *     {@link Product }
     *     
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link Product }
     *     
     */
    public void setProduct(Product value) {
        this.product = value;
    }

}
