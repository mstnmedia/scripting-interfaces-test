/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.CRM_GetCrmTicketByCredentials;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author amatos
 */
public class Main {

	static TransactionInterfacesPayload payload = new TransactionInterfacesPayload(
			new V_Transaction(), new Workflow_Step(), "{}"
	);

	static public Object testToken() throws Exception {
		CRM_GetCrmTicketByCredentials base = new CRM_GetCrmTicketByCredentials();
		List<Interface> interfaces = base.getInterfaces(true);
		Interface inter = interfaces.stream()
				.filter(i -> i.getName().endsWith("get"))
				.findFirst()
				.get();
		ObjectNode form = JSON.newObjectNode()
				.put("crmticketbycredentials_get_arg0_in", "319343")
				.put("test", "es");
		payload.setForm(form.toString());
		TransactionInterfacesResponse response = base.callMethod(inter, payload, User.SYSTEM);
		return response;
	}

	public static void main(String[] args) throws Exception {
		InterfaceConfigs.setConfig();
		Object result = null;
		result = testToken();
		System.out.println(JSON.toString(result));
	}

}
