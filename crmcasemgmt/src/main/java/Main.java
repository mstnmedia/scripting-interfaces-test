/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.casemgmt.CaseMGMT;
import com.tcs.iesb.crm.casemgmt.CaseManagementService;
import com.tcs.iesb.crm.casemgmt.CaseManagementService_Service;
import com.tcs.iesb.crm.casemgmt.types.CloseCaseRequest;
import com.tcs.iesb.crm.casemgmt.types.FailureReason;
import com.tcs.iesb.crm.casemgmt.types.GetCaseDetailsByIdRequest;
import com.tcs.iesb.crm.casemgmt.types.GetCaseDetailsByIdResponse;
import com.tcs.iesb.crm.casemgmt.types.XCloseCaseReq;
import java.math.BigDecimal;
//import com.mstn.scripting.interfaces.casemgmt.DireccionServiceImpl;
//import com.mstn.scripting.interfaces.casemgmt.SAD_V2;
import java.util.List;

/**
 * Clase ejecutable con métodos de pruebas para probar las diferentes
 * funcionalidades en este proyecto.
 *
 * @author amatos
 */
public class Main {

	static TransactionInterfacesPayload payload = new TransactionInterfacesPayload(
			new V_Transaction(), new Workflow_Step(), "{}"
	);
//	static String caseId = "12281843";
//	static String caseId = "34881581";
//	static String caseId = "34881581";
//	static String caseId = "10002942";
	static String caseId = "10002942";

	static Object getCaseDetailsById() throws Exception {
		CaseManagementService soap = CaseManagementService_Service.getInstance();
		GetCaseDetailsByIdRequest parameters = new GetCaseDetailsByIdRequest();
		parameters.setCaseId(caseId);
//		parameters.setTicket("TksmauJTNecNEY8Sb8axaTJVKxMS0FZQw6k3PY0");
		GetCaseDetailsByIdResponse result = soap.getCaseDetailsById(parameters);
		return result;
	}

	static Object closeCase() throws Exception {
		CaseManagementService soap = CaseManagementService_Service.getInstance();
		CloseCaseRequest parameters = new CloseCaseRequest();
		parameters.setTicket("EXT<TksmauiRt5UY2hUowWejbVaUgtXs{kUQoTv3AI0;appId=OMS;>");
		XCloseCaseReq value = new XCloseCaseReq();
		value.setCaseId("10002950");
		parameters.setCaseDetails(value);
//		parameters.setTicket("TksmauJTNecNEY8Sb8axaTJVKxMS0FZQw6k3PY0");
		FailureReason result = soap.closeCase(parameters);
		return result;
	}

	static Object testCaseMGMT() throws Exception {
		CaseMGMT service = new CaseMGMT();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces
				.stream()
				.filter(item -> item.getName().endsWith("getCaseDetailsById"))
				.findFirst().get();
		ObjectNode form = JSON.newObjectNode();
		String[] value = new String[]{"", caseId, "6789", "SCRIPTING"};
		for (int i = 0; i < inter.getInterface_field().size(); i++) {
			Interface_Field field = inter.getInterface_field().get(i);
			form.put(field.getName(), value[i]);
		}
		payload.setForm(form.toString());
		TransactionInterfacesResponse response = service.callMethod(inter, payload, User.SYSTEM);
		return response;
	}

	static Object testCreateCase() throws Exception {
		CaseMGMT service = new CaseMGMT();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces
				.stream()
				.filter(item -> item.getName().endsWith("createCase"))
				.findFirst().get();
		ObjectNode form = JSON.newObjectNode();
//		String[] value = new String[]{"", caseId, "6789", "SCRIPTING"};
		for (int i = 0; i < inter.getInterface_field().size(); i++) {
			Interface_Field field = inter.getInterface_field().get(i);
			if (field.getName().endsWith("ticket")) {
				form.put(field.getName(), "EXT<TksmauiRt5UY2hUowWejbVaUgtXs{kUQoTv3AI0;appId=OMS;>");
			} else if (field.getName().endsWith("phoneStation")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("caseTypeLevel1")) {
				form.put(field.getName(), (String) "Averias");
			} else if (field.getName().endsWith("caseTypeLevel2")) {
				form.put(field.getName(), (String) "IPTV");
			} else if (field.getName().endsWith("caseTypeLevel3")) {
				form.put(field.getName(), (String) "No senal-Con IP de video");
			} else if (field.getName().endsWith("severity")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("queueName")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("contractId")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("parentCaseId")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("productType")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("autoDispatch")) {
				form.put(field.getName(), (String) "false");
			} else if (field.getName().endsWith("visitPeriod")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("contactMode")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("caseTitle")) {
				form.put(field.getName(), (String) "A8095825386");
			} else if (field.getName().endsWith("priority")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("parentChild")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("contactPhone")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("notes")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("contactLastName")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("technicianID")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("accountId")) {
				form.put(field.getName(), (String) "7366391");
			} else if (field.getName().endsWith("subscriberNumber")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("visitDate")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("contactInfo")) {
				form.put(field.getName(), (String) null);
			} else if (field.getName().endsWith("contactFirstName")) {
				form.put(field.getName(), (String) null);
			}
		}
		payload.setForm(form.toString());
		TransactionInterfacesResponse response = service.callMethod(inter, payload, User.SYSTEM);
		return response;
	}

	public static void main(String[] args) throws Exception {
		InterfaceConfigs.setConfig();
		Object result = null;
//		result = getCaseDetailsById();
//		result = testCaseMGMT();
		result = testCreateCase();
//		result = closeCase();

		System.out.println(JSON.toString(result));
	}

}
