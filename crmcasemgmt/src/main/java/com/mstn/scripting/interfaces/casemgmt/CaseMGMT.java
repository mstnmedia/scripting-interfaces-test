package com.mstn.scripting.interfaces.casemgmt;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Log;
import com.tcs.iesb.crm.casemgmt.CaseManagementService_Service;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

/**
 * Interfaz que agrega a Scripting la conexión al web service para
 * administración de casos de CRM desde una transacción en curso.
 *
 * @author amatos
 */
public class CaseMGMT extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = CaseMGMT.class;
		try {
			if ("true".equals(InterfaceConfigs.get("ignore_casemgmt"))) {
				Utils.logWarn(_class, "Ignorando interfaces en clase " + _class.getCanonicalName());
				SOAP = new Object();
			} else {
				SOAP = CaseManagementService_Service.getInstance();
			}
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint CaseManagementService", ex);
			if ("true".equals(InterfaceConfigs.get("ignoreExceptions_casemgmt"))) {
				Utils.logWarn(_class, "Error ignorado por configuración. Continuará el proceso.");
				SOAP = new Object();
			} else {
				throw ex;
			}
		}
	}

	public CaseMGMT() {
		super(0, "casemgmt", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("crm_casemgmt"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("ticket")) {
			field.setLabel("Token");
		} else if (fieldName.endsWith("accountId")) {
			field.setLabel("CCU");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if ("casemgmt_addCaseNotes".equals(inter.getName())) {
			if (fieldName.endsWith("caseId")) {
				field.setLabel("No. de Caso");
				field.setId_type(InterfaceFieldTypes.NUMBER);
			} else if (fieldName.endsWith("notes")) {
				field.setLabel("Notas");
				field.setId_type(InterfaceFieldTypes.TEXTAREA);
			} else if (fieldName.endsWith("actionType")) {
				field.setLabel("Tipo de Acción");
				field.setRequired(false);
			}
		} else if ("casemgmt_closeCase".equals(inter.getName())) {
			if (fieldName.endsWith("rootCause")) {
				field.setLabel("Código de Causa");
				field.setRequired(false);
			} else if (fieldName.endsWith("rootSubCause")) {
				field.setLabel("Código de Sub-Causa");
				field.setRequired(false);
			} else if (fieldName.endsWith("caseId")) {
				field.setLabel("No. de Caso");
				field.setId_type(InterfaceFieldTypes.NUMBER);
			} else if (fieldName.endsWith("adjustmentAmount")) {
				field.setLabel("Monto de Ajuste");
				field.setRequired(false);
			} else if (fieldName.endsWith("technicianId")) {
				field.setLabel("Tarjeta");
				field.setRequired(false);
			} else if (fieldName.endsWith("notes")) {
				field.setLabel("Notas");
				field.setId_type(InterfaceFieldTypes.TEXTAREA);
				field.setRequired(false);
			} else if (fieldName.endsWith("maintenanceContract")) {
				field.setLabel("Contrato de Mantenimiento");
				field.setId_type(InterfaceFieldTypes.BOOLEAN);
				field.setRequired(false);
			} else if (fieldName.endsWith("adjustmentType")) {
				field.setLabel("Tipo de Ajuste");
				field.setRequired(false);
			} else if (fieldName.endsWith("status")) {
				field.setLabel("Estatus");
				field.setRequired(false);
			} else if (fieldName.endsWith("resolution")) {
				field.setLabel("Resolución");
				field.setRequired(false);
			} else if (fieldName.endsWith("dispositionCode")) {
				field.setLabel("Código de Disposición");
				field.setRequired(false);
			} else if (fieldName.endsWith("dispositionSubCode")) {
				field.setLabel("Sub-Código de Disposición");
				field.setRequired(false);
			}
		} else if ("casemgmt_createCase".equals(inter.getName())) {
			if (fieldName.endsWith("phoneStation")) {
				field.setLabel("Estación");
				field.setId_type(InterfaceFieldTypes.TEXT);
				field.setOnvalidate(InterfaceFieldTypes.VALIDATE_NUMBER);
				field.setRequired(false);
			} else if (fieldName.endsWith("caseTypeLevel1")) {
				field.setLabel("Tipo 1");
			} else if (fieldName.endsWith("severity")) {
				field.setLabel("Severidad");
				field.setRequired(false);
			} else if (fieldName.endsWith("queueName")) {
				field.setLabel("Nombre de la Cola");
				field.setRequired(false);
			} else if (fieldName.endsWith("contractId")) {
				field.setLabel("ID de Contrato");
				field.setRequired(false);
			} else if (fieldName.endsWith("parentCaseId")) {
				field.setLabel("ID Padre-Hijo");
				field.setRequired(false);
			} else if (fieldName.endsWith("productType")) {
				field.setLabel("Tipo de Servicio");
				field.setRequired(false);
			} else if (fieldName.endsWith("Autodispatch")) {
				field.setLabel("Autodispatch");
				field.setId_type(InterfaceFieldTypes.BOOLEAN);
				field.setDefault_value("true");
				field.setRequired(false);
			} else if (fieldName.endsWith("visitPeriod")) {
				field.setLabel("Tanda de Visita");
				field.setRequired(false);
			} else if (fieldName.endsWith("contactMode")) {
				field.setLabel("Contactar Vía");
				field.setRequired(false);
			} else if (fieldName.endsWith("caseTitle")) {
				field.setLabel("Título del Caso");
			} else if (fieldName.endsWith("priority")) {
				field.setLabel("Prioridad");
				field.setRequired(false);
			} else if (fieldName.endsWith("caseTypeLevel3")) {
				field.setLabel("Tipo 3");
			} else if (fieldName.endsWith("parentChild")) {
				field.setLabel("Padre-Hijo");
				field.setRequired(false);
			} else if (fieldName.endsWith("contactPhone")) {
				field.setLabel("Teléfono");
				field.setRequired(false);
			} else if (fieldName.endsWith("notes")) {
				field.setLabel("Notas");
				field.setId_type(InterfaceFieldTypes.TEXTAREA);
				field.setRequired(false);
			} else if (fieldName.endsWith("contactLastName")) {
				field.setLabel("Apellido");
				field.setRequired(false);
			} else if (fieldName.endsWith("technicianID")) {
				field.setLabel("Tarjeta");
				field.setRequired(false);
			} else if (fieldName.endsWith("subscriberNumber")) {
				field.setLabel("Numero de Suscriptor");
				field.setRequired(false);
			} else if (fieldName.endsWith("visitDate")) {
				field.setLabel("Fecha de Visita");
				field.setId_type(InterfaceFieldTypes.DATE);
				field.setRequired(false);
			} else if (fieldName.endsWith("contactInfo")) {
				field.setLabel("Info de Contacto");
				field.setRequired(false);
			} else if (fieldName.endsWith("contactFirstName")) {
				field.setLabel("Nombre");
				field.setRequired(false);
			} else if (fieldName.endsWith("caseTypeLevel2")) {
				field.setLabel("Tipo 2");
			}
		} else if ("casemgmt_dispatchCase".equals(inter.getName())) {
			if (fieldName.endsWith("caseId")) {
				field.setLabel("No. de Casos");
			} else if (fieldName.endsWith(LIST_TEMPLATE) && parentName.endsWith("cases")) {
				field.setLabel("Caso");
				field.setId_type(InterfaceFieldTypes.NUMBER);
			} else if (fieldName.endsWith("queue")) {
				field.setLabel("Cola");
			}
		} else if ("casemgmt_getCaseDetailsById".equals(inter.getName())) {
			if (fieldName.endsWith("caseId")) {
				field.setLabel("No. de Casos");
				field.setId_type(InterfaceFieldTypes.NUMBER);
			}
		} else if ("casemgmt_searchCase".equals(inter.getName())) {
			if (fieldName.endsWith("user")) {
				field.setLabel("Usuario");
				field.setRequired(false);
			} else if (fieldName.endsWith("originator")) {
				field.setLabel("Tarjeta del Creador");
				field.setRequired(false);
			} else if (fieldName.endsWith("subscriber")) {
				field.setLabel("Suscriptor");
				field.setRequired(false);
			} else if (fieldName.endsWith("contactLastName")) {
				field.setLabel("Apellido");
				field.setRequired(false);
			} else if (fieldName.endsWith("status")) {
				field.setLabel("Estatus");
				field.setRequired(false);
			} else if (fieldName.endsWith("contactFirstName")) {
				field.setLabel("Nombre");
				field.setRequired(false);
			} else if (fieldName.endsWith("condition")) {
				field.setLabel("Condición");
				field.setRequired(false);
			}
		} else if ("casemgmt_updateCase".equals(inter.getName())) {
			if (fieldName.endsWith("visitPeriod")) {
				field.setLabel("Tanda Visita");
				field.setRequired(false);
			} else if (fieldName.endsWith("visitDate")) {
				field.setLabel("Fecha de Visita");
				field.setId_type(InterfaceFieldTypes.DATE);
				field.setRequired(false);
			} else if (fieldName.endsWith("phoneStation")) {
				field.setLabel("Estación");
				field.setId_type(InterfaceFieldTypes.TEXT);
				field.setOnvalidate(InterfaceFieldTypes.VALIDATE_NUMBER);
				field.setRequired(false);
			} else if (fieldName.endsWith("caseId")) {
				field.setLabel("No. de Casos");
				field.setId_type(InterfaceFieldTypes.NUMBER);
			} else if (fieldName.endsWith("technicianId")) {
				field.setLabel("Tarjeta");
				field.setRequired(false);
			} else if (fieldName.endsWith("status")) {
				field.setLabel("Estatus");
				field.setRequired(false);
			} else if (fieldName.endsWith("flexAttributes")) {
				field.setLabel("Atributos Flexibles");
				field.setRequired(false);
			} else if (fieldName.endsWith("value")) {
				field.setLabel("Valor");
			} else if (fieldName.endsWith("name")) {
				field.setLabel("Nombre");
			}
		} else if ("casemgmt_updateParentChildCase".equals(inter.getName())) {
			if (fieldName.endsWith("parentCaseId")) {
				field.setLabel("Caso Padre");
			} else if (fieldName.endsWith("childCaseId")) {
				field.setLabel("Caso Hijo");
			}
		}
		return field;
	}

	@Override
	protected Object sendParamsToMethod(Interface inter, Method method, List<Object> params) throws Exception {
		Object responseObj = super.sendParamsToMethod(inter, method, params);
		if ("casemgmt_createCase".equals(inter.getName())) {
			response.getLogs().add(new Log(
					0, user.getId(), "CRM Casos", transaction.getId(),
					"Crear caso", new Date(),
					JSON.toString(params), JSON.toString(responseObj), user.getIp()));
		}
		return responseObj;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("crm_casemgmt").toString();
		super.test();
	}
}
