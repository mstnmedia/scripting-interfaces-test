
package com.tcs.iesb.crm.casemgmt.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchCaseResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchCaseResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FailureReason" type="{http://iesb.tcs.com/crm/casemgmt/types/}FailureReason"/>
 *         &lt;element name="CaseDetails" type="{http://iesb.tcs.com/crm/casemgmt/types/}XSearchCaseResp" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCaseResponse", propOrder = {
    "failureReason",
    "caseDetails"
})
public class SearchCaseResponse {

    @XmlElement(name = "FailureReason", required = true, nillable = true)
    protected FailureReason failureReason;
    @XmlElement(name = "CaseDetails", nillable = true)
    protected List<XSearchCaseResp> caseDetails;

    /**
     * Gets the value of the failureReason property.
     * 
     * @return
     *     possible object is
     *     {@link FailureReason }
     *     
     */
    public FailureReason getFailureReason() {
        return failureReason;
    }

    /**
     * Sets the value of the failureReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailureReason }
     *     
     */
    public void setFailureReason(FailureReason value) {
        this.failureReason = value;
    }

    /**
     * Gets the value of the caseDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the caseDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCaseDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XSearchCaseResp }
     * 
     * 
     */
    public List<XSearchCaseResp> getCaseDetails() {
        if (caseDetails == null) {
            caseDetails = new ArrayList<XSearchCaseResp>();
        }
        return this.caseDetails;
    }

}
