
package com.tcs.iesb.crm.casemgmt.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XParentChildCaseReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XParentChildCaseReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="parentCaseId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="childCaseId" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XParentChildCaseReq", propOrder = {
    "parentCaseId",
    "childCaseId"
})
public class XParentChildCaseReq {

    @XmlElement(required = true, nillable = true)
    protected String parentCaseId;
    @XmlElement(nillable = true)
    protected List<String> childCaseId;

    /**
     * Gets the value of the parentCaseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentCaseId() {
        return parentCaseId;
    }

    /**
     * Sets the value of the parentCaseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentCaseId(String value) {
        this.parentCaseId = value;
    }

    /**
     * Gets the value of the childCaseId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the childCaseId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChildCaseId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getChildCaseId() {
        if (childCaseId == null) {
            childCaseId = new ArrayList<String>();
        }
        return this.childCaseId;
    }

}
