
package com.tcs.iesb.crm.casemgmt.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XParentChildCaseResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XParentChildCaseResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="childCaseErrMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="childCaseStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="childCaseId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XParentChildCaseResp", propOrder = {
    "childCaseErrMsg",
    "childCaseStatus",
    "childCaseId"
})
public class XParentChildCaseResp {

    @XmlElement(required = true, nillable = true)
    protected String childCaseErrMsg;
    @XmlElement(required = true, nillable = true)
    protected String childCaseStatus;
    @XmlElement(required = true, nillable = true)
    protected String childCaseId;

    /**
     * Gets the value of the childCaseErrMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChildCaseErrMsg() {
        return childCaseErrMsg;
    }

    /**
     * Sets the value of the childCaseErrMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChildCaseErrMsg(String value) {
        this.childCaseErrMsg = value;
    }

    /**
     * Gets the value of the childCaseStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChildCaseStatus() {
        return childCaseStatus;
    }

    /**
     * Sets the value of the childCaseStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChildCaseStatus(String value) {
        this.childCaseStatus = value;
    }

    /**
     * Gets the value of the childCaseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChildCaseId() {
        return childCaseId;
    }

    /**
     * Sets the value of the childCaseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChildCaseId(String value) {
        this.childCaseId = value;
    }

}
