
package com.tcs.iesb.crm.casemgmt.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddCaseNotesRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddCaseNotesRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ticket" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CaseNotes" type="{http://iesb.tcs.com/crm/casemgmt/types/}XAddCaseNotesReq"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddCaseNotesRequest", propOrder = {
    "ticket",
    "caseNotes"
})
public class AddCaseNotesRequest {

    @XmlElement(required = true, nillable = true)
    protected String ticket;
    @XmlElement(name = "CaseNotes", required = true, nillable = true)
    protected XAddCaseNotesReq caseNotes;

    /**
     * Gets the value of the ticket property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicket() {
        return ticket;
    }

    /**
     * Sets the value of the ticket property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicket(String value) {
        this.ticket = value;
    }

    /**
     * Gets the value of the caseNotes property.
     * 
     * @return
     *     possible object is
     *     {@link XAddCaseNotesReq }
     *     
     */
    public XAddCaseNotesReq getCaseNotes() {
        return caseNotes;
    }

    /**
     * Sets the value of the caseNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link XAddCaseNotesReq }
     *     
     */
    public void setCaseNotes(XAddCaseNotesReq value) {
        this.caseNotes = value;
    }

}
