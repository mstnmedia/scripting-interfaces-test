
package com.tcs.iesb.crm.casemgmt.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchCaseRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchCaseRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ticket" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CaseDetails" type="{http://iesb.tcs.com/crm/casemgmt/types/}XSearchCaseReq"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCaseRequest", propOrder = {
    "ticket",
    "caseDetails"
})
public class SearchCaseRequest {

    @XmlElement(required = true, nillable = true)
    protected String ticket;
    @XmlElement(name = "CaseDetails", required = true, nillable = true)
    protected XSearchCaseReq caseDetails;

    /**
     * Gets the value of the ticket property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicket() {
        return ticket;
    }

    /**
     * Sets the value of the ticket property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicket(String value) {
        this.ticket = value;
    }

    /**
     * Gets the value of the caseDetails property.
     * 
     * @return
     *     possible object is
     *     {@link XSearchCaseReq }
     *     
     */
    public XSearchCaseReq getCaseDetails() {
        return caseDetails;
    }

    /**
     * Sets the value of the caseDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link XSearchCaseReq }
     *     
     */
    public void setCaseDetails(XSearchCaseReq value) {
        this.caseDetails = value;
    }

}
