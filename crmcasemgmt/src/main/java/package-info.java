/**
 * Este proyecto contiene la interfaz que conecta con SAD.
 * Esta interfaz agrega a Scripting la posibilidad de que los usuarios puedan 
 * obtener información de las direcciones, calles, barrios, sectores y provincias
 * del país dentro de una transacción en curso.
 */
